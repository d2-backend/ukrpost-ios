//
//  DetailTransferRegionView.swift
//  UkrPoshta
//
//  Created by Zhekon on 08.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetailTransferRegionView: UIView {

    let infoLabel = UILabel()
    var infoImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        infoLabel.numberOfLines = 0
        infoLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        infoLabel.textColor = UPColor.darkGray
        infoLabel.text = NSLocalizedString("DetailTransferV.infoLabelTwo.text", comment: "")
        infoImage.contentMode = .scaleAspectFill
        infoImage.image = UIImage(named:"cover_international_transfers")
        addSubview(infoLabel)
        addSubview(infoImage)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        infoImage.pin.left(0).right(0).height(250).top(0)
        infoLabel.pin.below(of: infoImage).marginTop(10).left(25).right(15).height(115)
    }
}
