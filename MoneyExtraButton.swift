//
//  MoneyExtraButton.swift
//  UkrPoshta
//
//  Created by Zhekon on 10.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class MoneyExtraButton: UIButton {
    
    let extraLabel = UILabel()
    var extraImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.addSubview(extraLabel)
        self.addSubview(extraImage)
        self.backgroundColor = .white
        self.contentHorizontalAlignment = .left
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0)
        self.setTitleColor(UPColor.lightGray, for: .normal)
        self.setTitleColor(UPColor.darkGray, for: .selected)
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        extraLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        extraLabel.textColor = UPColor.lightGray
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        extraImage.pin.left(13).vCenter().height(20).width(20)
        extraLabel.pin.after(of: extraImage).marginLeft(10).vCenter(to: extraImage.edge.vCenter).height(20).right(20)
    }
}







