//
//  MoneyTransferViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 07.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class MoneyTransferViewController: UIViewController {

     let mainView = MoneyTransferView()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.title =  NSLocalizedString("MoneyTransferVC.title", comment: "")
        self.mainView.fromCardHomeButton.addTarget(self, action: #selector(fromCardHomeButtonPressed), for: .touchUpInside)
        self.mainView.fromCardDepartmentButton.addTarget(self, action: #selector(fromCardDepartmentButtonPressed), for: .touchUpInside)
        self.mainView.fromCardToCardButton.addTarget(self, action: #selector(fromCardToCardButtonPressed), for: .touchUpInside)
        self.mainView.extraButtonPostTransfer.addTarget(self, action: #selector(postalOrderButtonPressed), for: .touchUpInside)
        self.mainView.extraButtonInternatioanlTransfer.addTarget(self, action: #selector(internationalOrderButtonPressed), for: .touchUpInside)
    }
    
    @objc func fromCardHomeButtonPressed() {
        if self.mainView.fromCardHomeButton.isTouchInside == true {
            let URL = "https://www.ukrposhta.ua/perekaz-z-kartki-dodomu/mobile_address/"
            let urlToBrowser = NSURL(string: URL)!
            UIApplication.shared.openURL(urlToBrowser as URL)
        }
    }
    
    @objc func fromCardDepartmentButtonPressed() {
        if self.mainView.fromCardDepartmentButton.isTouchInside == true {
              let URL = "https://www.ukrposhta.ua/perekaz-z-kartki-dodomu/mobile_urgent/"
            let urlToBrowser = NSURL(string: URL)!; UIApplication.shared.openURL(urlToBrowser as URL);
        }
    }
    
    @objc func fromCardToCardButtonPressed() {
        if self.mainView.fromCardToCardButton.isTouchInside == true {
             let URL = "https://www.portmone.com.ua/r3/uk/ukrposhta/services/card-to-card/"
            let urlToBrowser = NSURL(string: URL)!
            UIApplication.shared.openURL(urlToBrowser as URL)
        }
    }
    
    @objc func postalOrderButtonPressed() {
        let vc = DetailPostTransferViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func internationalOrderButtonPressed() {
        let vc = DetailRegionTransferViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
