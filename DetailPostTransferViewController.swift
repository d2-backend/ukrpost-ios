//
//  DetailTransferViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 08.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import WebKit

class DetailPostTransferViewController: UIViewController {
    let mainView = DetailTransferPostView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
    }
}
