//
//  MapTransferView.swift
//  UkrPoshta
//
//  Created by Zhekon on 07.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class MapTransferView: UIView {
    
    let contentView = UIView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        self.contentView.backgroundColor = .white
        self.addSubview(contentView)
    }
}
