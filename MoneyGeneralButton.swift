//
//  MoneyGeneralButton.swift
//  UkrPoshta
//
//  Created by Zhekon on 07.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class MoneyGeneralButton: UIButton {
    
    let fromCardHomeLabelFirst = UILabel()
    let fromCardHomeLabelSecond = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white
        self.contentHorizontalAlignment = .left
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0)
        self.setTitleColor(UPColor.lightGray, for: .normal)
        self.setTitleColor(UPColor.darkGray, for: .selected)
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        self.addSubview(fromCardHomeLabelFirst)
        self.addSubview(fromCardHomeLabelSecond)
        
        fromCardHomeLabelFirst.numberOfLines = 2
        fromCardHomeLabelFirst.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        fromCardHomeLabelFirst.textColor = UPColor.darkGray
        
        fromCardHomeLabelSecond.numberOfLines = 2
        fromCardHomeLabelSecond.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        fromCardHomeLabelSecond.textColor = UPColor.lightGray
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        fromCardHomeLabelFirst.pin.top(10).left(13).right(20).height(60)
        fromCardHomeLabelSecond.pin.below(of: fromCardHomeLabelFirst).marginTop(0).left(13).right(20).height(40)
    }
}


