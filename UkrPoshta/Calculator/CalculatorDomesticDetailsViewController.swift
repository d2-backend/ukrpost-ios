//
//  CalculatorDomesticDetailsViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/8/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import TGPControls

let possibleWeights = ["0", "0.25","0.5","1","2","3","4","5","6","7","8","9","10","15","20", "30"]
let possibleSizes = ["0","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80", "85","90","95","100","125","150","175","200"]
let possibleLetterSizes = ["0","250","500","1000","2000"]

class CalculatorDomesticDetailsViewController: UIViewController {

    let mainView = CalculatorDomesticDetailsView()
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var isLetterImage = false
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Розрахунок вартостi"
      
      let clearButton = UIBarButtonItem(title: "Очистити", style: UIBarButtonItemStyle.plain, target:  self, action: #selector(clearPressed))
       clearButton.setTitleTextAttributes( [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 11.0),NSAttributedStringKey.foregroundColor : UPColor.lightGray], for: .normal)
      self.navigationItem.rightBarButtonItem = clearButton
      
      
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        self.mainView.submitSizesButton.isEnabled = false
      
        self.mainView.header.text = "\(addressFrom?.cityName ?? "none") - \(addressTo?.cityName ?? "none")"
        
        self.mainView.parcelButton.addTarget(self, action: #selector(parcelButtonPressed), for: .touchUpInside)
        self.mainView.letterButton.addTarget(self, action: #selector(letterButtonPressed), for: .touchUpInside)
       self.mainView.weightSlider.addTarget(self,action: #selector(valueChanged(_:event:)), for: .valueChanged)
        self.mainView.weightLabel.text = "0"
        
        self.mainView.sizeSlider.addTarget(self,action: #selector(valueChanged(_:event:)), for: .valueChanged)
        self.mainView.sizeLabel.text = "0"
        
        self.mainView.letterWeightSlider.addTarget(self,action: #selector(valueChanged(_:event:)), for: .valueChanged)
        self.mainView.letterWeightLabel.text = "0"
        
        self.mainView.submitSizesButton.addTarget(self, action: #selector(submitButtonPressed), for: .touchUpInside)
        self.mainView.submitSizesButton.isEnabled = false
        self.mainView.submitSizesButton.setTitle(NSLocalizedString("calculator.nextButtonCalculate.title", comment: ""), for: .normal)    }

      @objc func clearPressed() {
            mainView.weightSlider.value = 0
            mainView.sizeSlider.value = 0
            mainView.letterWeightSlider.value = 0
            mainView.weightLabel.text = "0"
          mainView.sizeLabel.text = "0"
            mainView.letterWeightLabel.text = "0"
            self.mainView.submitSizesButton.isEnabled = false
      }
      
    @objc func parcelButtonPressed() {
        if !self.mainView.parcelButton.isSelected {
            
            UIView.animate(withDuration: 0.3, animations: {
                  self.mainView.letterWeightHeader.alpha = 0.0
                  self.mainView.letterWeightLabel.alpha = 0.0
                  self.mainView.letterWeightSlider.alpha = 0.0
                  self.mainView.minWeight.alpha = 0.0
                  self.mainView.maxWeight.alpha = 0.0
                  self.mainView.minSize.isHidden = false
                  self.mainView.maxSize.isHidden = false
                  self.mainView.maxWeight.text = "30"
            }) { (bool) in
                  UIView.animate(withDuration: 0.3, animations: {
                        self.mainView.weightHeader.alpha = 1.0
                        self.mainView.weightLabel.alpha = 1.0
                        self.mainView.weightSlider.alpha = 1.0
                        self.mainView.sizeHeader.alpha = 1.0
                        self.mainView.sizeLabel.alpha = 1.0
                        self.mainView.sizeSlider.alpha = 1.0
                        self.mainView.minSize.alpha = 1.0
                        self.mainView.maxSize.alpha = 1.0
                        self.mainView.minWeight.alpha = 1.0
                        self.mainView.maxWeight.alpha = 1.0
                    self.redrawSubmitButton()
                })
            }
            self.mainView.parcelButton.isSelected = true
            self.mainView.letterButton.isSelected = false
        }
    }
      
      @objc func valueChanged(_ sender: TGPDiscreteSlider, event:UIEvent) {
            let value = Int(sender.value)
            if sender == mainView.weightSlider {
                  self.mainView.weightLabel.text = possibleWeights[value]
            }
            if sender == mainView.sizeSlider {
                  self.mainView.sizeLabel.text = possibleSizes[value]
            }
            if sender == mainView.letterWeightSlider {
                  self.mainView.letterWeightLabel.text = possibleLetterSizes[value]
            }
            redrawSubmitButton()
      }
      
      
    @objc func letterButtonPressed() {
        if !self.mainView.letterButton.isSelected {

            UIView.animate(withDuration: 0.3, animations: {
                  self.mainView.weightHeader.alpha = 0.0
                  self.mainView.weightLabel.alpha = 0.0
                  self.mainView.weightSlider.alpha = 0.0
                  self.mainView.sizeHeader.alpha = 0.0
                  self.mainView.sizeLabel.alpha = 0.0
                  self.mainView.sizeSlider.alpha = 0.0
                  self.mainView.minSize.alpha = 0.0
                  self.mainView.maxSize.alpha = 0.0
                  self.mainView.minWeight.alpha = 0.0
                  self.mainView.maxWeight.alpha = 0.0
                  self.mainView.minSize.isHidden = true
                  self.mainView.maxSize.isHidden = true
                  self.mainView.maxWeight.text = "2000"
            }) { (bool) in
                  UIView.animate(withDuration: 0.3, animations: {
                        self.mainView.letterWeightHeader.alpha = 1.0
                        self.mainView.letterWeightLabel.alpha = 1.0
                        self.mainView.letterWeightSlider.alpha = 1.0
                        self.mainView.minWeight.alpha = 1.0
                        self.mainView.maxWeight.alpha = 1.0
                        self.redrawSubmitButton()
                  })
            }
            isLetterImage = true
            self.mainView.parcelButton.isSelected = false
            self.mainView.letterButton.isSelected = true
            
        }
    }
    
    func redrawSubmitButton() {
        //self.mainView.submitSizesButton.isEnabled = true
        self.mainView.submitSizesButton.isEnabled = false
        if self.mainView.parcelButton.isSelected {
            if self.mainView.weightLabel.text != "0" && self.mainView.sizeLabel.text != "0" {
                self.mainView.submitSizesButton.isEnabled = true
            }
        } else if self.mainView.letterButton.isSelected {
            if self.mainView.letterWeightLabel.text != "0" {
                self.mainView.submitSizesButton.isEnabled = true
            }
        }
        
    }
    
    @objc func submitButtonPressed() {
//            let vc = CalculatorDomesticResultViewController()
//            vc.currentPrice = nil
//            self.navigationController?.pushViewController(vc, animated: true)
        var currentLength: String?
        var currentWeight: String?
        self.mainView.submitSizesButton.isEnabled = false
        if self.mainView.parcelButton.isSelected {
            currentLength = self.mainView.sizeLabel.text
            currentWeight = createWeightString(from: self.mainView.weightLabel.text!)
        } else {
            currentLength = "35"
            currentWeight = self.mainView.letterWeightLabel.text
        }

        UPAPIManager.getDeliveryPrice(postcodeFrom: self.addressFrom!.postcode, postcodeTo: self.addressTo!.postcode, weight: currentWeight, length: currentLength!, type: SendType.standart, deliveryType: DeliveryType.warehouseToWarehouse, declaredValue: nil, withSMS: false, isRecomended: nil) { (result) in
            switch result {
            case .success(let data):
                print(data)
                let vc = CalculatorDomesticResultViewController()
                vc.currentPrice = data
                vc.addressFrom = self.addressFrom
                vc.addressTo = self.addressTo
                vc.weight = currentWeight
                vc.length = currentLength
                vc.isLetter = self.mainView.letterButton.isSelected
                vc.isLetterImage = self.isLetterImage
                self.navigationController?.pushViewController(vc, animated: true)

            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }

    }
    
    func createWeightString(from string: String) -> (String) {
        
        let weightFloat = Float(string)
        let weightFloatInGramms = weightFloat! * 1000
        let weightInt = Int(weightFloatInGramms)
        let weightStringInGramms = String(weightInt)
        return weightStringInGramms
    }
}
