//
//  CountryViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/18/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

protocol CountrySearchDelegate {
    func setCountry(country: Country)
}

struct Country {
    let name: String
    let code: String
}

class CountryViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    private let mainView = IndexSearchView()
    var delegate: CountrySearchDelegate?
    private var countriesToDisplay = [Country]()
    private var countriesRawList = [Country]()

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Розрахунок вартостi"
        hideBackNavigationButtonText()
        self.fillCountriesData()
        mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.searchField.placeholder = NSLocalizedString("calculator.country.placeholder", comment: "")
        self.mainView.searchField.delegate = self
        self.mainView.tableView.register(SuggestionCell.self, forCellReuseIdentifier: "SuggestionCell")
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        
        self.mainView.searchField.becomeFirstResponder()        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector:#selector(keybordWasShown) , name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keybordWasShown(notification: NSNotification) {
        
        let keyboardRect: CGRect = notification.userInfo!["UIKeyboardBoundsUserInfoKey"] as! CGRect
        let keyboardHeight = keyboardRect.size.height
        
        let frame = self.view.frame
        let newHeight = frame.size.height - keyboardHeight + 64
        self.mainView.frame = CGRect(x: 0, y: 0, width: frame.width, height: newHeight)
        self.mainView.layoutSubviews()
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionCell") as! SuggestionCell
        let aCountry = countriesToDisplay[indexPath.row]
        cell.mainLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        cell.mainLabel.text = aCountry.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCountry = self.countriesToDisplay[indexPath.row]
        delegate?.setCountry(country: selectedCountry)
        self.navigationController?.popViewController(animated: true)
    }
    
    func fillCountriesData() {
        var result = [Country]()
        for dic in kCountriesCodes {
            let country = Country(name: dic.value, code: dic.key)
            result.append(country)
        }
        
        self.countriesRawList = result.sorted(by: sortByUkrainianName)
        self.countriesToDisplay = countriesRawList
        self.mainView.tableView.reloadData()
    }
    func sortByUkrainianName(first:Country, second:Country) -> Bool {
        let defaultCompareString = " аАбБвВгГґҐдДеЕєЄёЁжЖзЗиИіІїЇйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩьЬъЪыЫэЭюЮяЯaAbBcCdDeEfFgGhHiIkKlLmMnNoOpPqQrRsStTvVxXyYzZ"
        for (index, char) in first.name.enumerated() {
            
            var indexOfLeftWord: Int = 9999
            if let indexL = defaultCompareString.index(of: char) {
                let distanceL = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexL)
                indexOfLeftWord = distanceL
            }
            
            let indexOfRight = second.name.index (second.name.startIndex, offsetBy: index)
            let rightChar = second.name[indexOfRight]
            var indexOfRightWord: Int = 9999
            
            if let indexR = defaultCompareString.index(of: rightChar) {
                let distanceR = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexR)
                indexOfRightWord = distanceR
            }
            if indexOfLeftWord > indexOfRightWord || indexOfLeftWord < indexOfRightWord {
                return indexOfLeftWord < indexOfRightWord
            } else {
                return false
            }
        }
        return false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.mainView.searchField.textColor = UPColor.darkGray
        self.countriesToDisplay = []
        self.mainView.tableView.reloadData()
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }
        
        if newString != "" {
            for country in self.countriesRawList {
                if country.name.lowercased().range(of: newString.lowercased()) != nil {
                    countriesToDisplay.append(country)
                }
            }
        } else {
            self.countriesToDisplay = self.countriesRawList
        }
        
        self.mainView.tableView.reloadData()

        return true
    }

}
