//
//  CalculatorDomesticResultViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/10/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CalculatorDomesticResultViewController: UIViewController, UITextFieldDelegate {
      
      private let mainView = CalculatorDomesticResultView()
      var currentPrice: DeliveryPrice?
      var addressFrom: CityDetails?
      var addressTo: CityDetails?
      var isLetter = false
      var isLetterImage = false
      var weight: String?
      var length: String?
      
      func hideBackNavigationButtonText() {
            let imgBack = UIImage(named: "ic_back")
            navigationController?.navigationBar.backIndicatorImage = imgBack
            navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
            navigationItem.leftItemsSupplementBackButton = true
            navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
      }
      
      override func viewDidLoad() {
            super.viewDidLoad()
            self.title = "Розрахунок вартостi"
            hideBackNavigationButtonText()
            self.mainView.frame = self.view.frame
            self.view.addSubview(self.mainView)
            self.mainView.predefinedValueField.delegate = self
            
            var displayString = "\(addressFrom?.cityName ?? "none") - \(addressTo?.cityName ?? "none")"
            
            drawDeliveryTime()
            fetchData()
            
            if isLetter {
                  displayString += ", \(NSLocalizedString("calculator.type.letter", comment: ""))"
                  self.mainView.expressButton.isSelected = true
                  self.mainView.standartButton.normalImage = UIImage(named: "letterIcon")!
                  self.mainView.standartButton.selectedImage = UIImage(named: "letterIconSelected")!
                  self.mainView.expressButton.normalImage = UIImage(named: "letterIcon")!
                  self.mainView.expressButton.selectedImage = UIImage(named: "letterIconSelected")!
                  self.mainView.isShort = true
                  self.mainView.isLetterImage = true
                  self.mainView.smartBoxButton.isHidden = true
                  self.mainView.layoutSubviews()
            } else {
                  displayString += ", \(NSLocalizedString("calculator.type.parcel", comment: ""))"
                  displayString += ", \(length ?? "none") см"
                  let lengthInt = Int(length!)
                  if lengthInt! >= 50 {
                        self.mainView.isShort = true
                        self.mainView.smartBoxButton.isHidden = true
                        self.mainView.layoutSubviews()
                  }
            }
            
            displayString += ", \(weight ?? "none") г"
            
            self.mainView.header.text = displayString
            self.mainView.expressButton.addTarget(self, action: #selector(typeButtonPressed(sender:)), for: .touchUpInside)
            self.mainView.smartBoxButton.addTarget(self, action: #selector(typeButtonPressed(sender:)), for: .touchUpInside)
            self.mainView.standartButton.addTarget(self, action: #selector(typeButtonPressed(sender:)), for: .touchUpInside)
            self.mainView.predefinedValueButton.addTarget(self, action: #selector(switchPressed), for: .touchUpInside)
            self.mainView.courierTakeButton.addTarget(self, action: #selector(switchPressed), for: .touchUpInside)
            self.mainView.courierDeliverButton.addTarget(self, action: #selector(switchPressed), for: .touchUpInside)
            self.mainView.smsButton.addTarget(self, action: #selector(switchPressed), for: .touchUpInside)
            self.mainView.packagingButton.addTarget(self, action: #selector(switchPressed), for: .touchUpInside)
            self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
            self.mainView.submitButton.setTitle(NSLocalizedString("error.default.ok", comment: ""), for: .normal)
            redrawSubmitButton()
      }
      
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            self.mainView.predefinedValueField.maxLength = 6
            if self.mainView.predefinedValueField.isEditing  {
                  let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
                  let compSepByCharInSet = string.components(separatedBy: aSet)
                  let numberFiltered = compSepByCharInSet.joined(separator: "")
                  return string == numberFiltered
            }
            return true
      }
      
      func textFieldDidEndEditing(_ textField: UITextField) {
            fetchData()
      }
      
      override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
      }
      
      @objc func submitPressed() {
            if self.mainView.standartButton.isSelected{
                  showMapViewController()
            } else {
                  showCheckoutViewController()
            }
      }
      
      func showCheckoutViewController(){
            let vc = CheckoutViewController()
            var weight = Int()
            weight = Int(self.weight!)!
            vc.weight = weight
            vc.length = self.length
            if self.mainView.expressButton.isSelected {
                  vc.sendType = .express
            } else {
                  vc.sendType = .smartBox
            }
            self.navigationController?.pushViewController(vc, animated: true)
      }
      
      func showMapViewController(){
            let storyboard = UIStoryboard(name: "Map", bundle:nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MapVC") as! UINavigationController
            let firstVC = controller.viewControllers[0] as! MapViewController
            self.navigationController?.pushViewController(firstVC, animated: true)
      }
      
      @objc func switchPressed(myButton: UIButton) {
            myButton.isSelected = !myButton.isSelected
            fetchData()
      }
      
      @objc func typeButtonPressed(sender: UIButton) {
            
            if !sender.isSelected {
                  self.mainView.expressButton.isSelected = false
                  self.mainView.smartBoxButton.isSelected = false
                  self.mainView.standartButton.isSelected = false
                  sender.isSelected = true
            }
            redrawSubmitButton()
      }
      func redrawSubmitButton() {
            if self.mainView.expressButton.isSelected || self.mainView.smartBoxButton.isSelected {
                  self.mainView.submitButton.setTitle(NSLocalizedString("main.parcelRegistrationButton.title", comment: ""), for: .normal)
            } else {
                  self.mainView.submitButton.setTitle(NSLocalizedString("calculator.submit.map", comment: ""), for: .normal)
            }
      }
      func fetchData() {
            getStandartPrice()
            getExpressPrice()
            getSmartBoxPrice()
      }
      
      func getDeliveryType() -> DeliveryType {
            var deliveryType: DeliveryType
            if self.mainView.courierTakeButton.isSelected {
                  if self.mainView.courierDeliverButton.isSelected {
                        deliveryType = .doorToDoor
                  } else {
                        deliveryType = .doorToWarehouse
                  }
            } else {
                  if self.mainView.courierDeliverButton.isSelected {
                        deliveryType = .warehouseToDoor
                  } else {
                        deliveryType = .warehouseToWarehouse
                  }
            }
            return deliveryType
      }
      
      func getStandartPrice() {
            self.mainView.standartButton.priceLabel.text = ""
            
            let predefinedValueFloat = selectedPredefinedPrice()
            let deliveryType = getDeliveryType()
            UPAPIManager.getDeliveryPrice(postcodeFrom: self.addressFrom!.postcode, postcodeTo: self.addressTo!.postcode, weight: self.weight, length: self.length!, type: SendType.standart, deliveryType: deliveryType, declaredValue: predefinedValueFloat, withSMS: self.mainView.smsButton.isSelected, isRecomended: nil) { (result) in
                  switch result {
                  case .success(let data):
                        var resultPrice = data.deliveryPrice ?? 0.0
                        if self.mainView.packagingButton.isSelected {
                              resultPrice += 7.2
                        }
                        self.mainView.standartButton.priceLabel.text = self.priceStringFrom(float: resultPrice)
                        
                  case .partialSuccess(let string):
                        print(string)
                  case .failure(let error):
                        print(error)
                  }
                  self.mainView.activityIndicator.stopAnimating()
            }
      }
      
      func getExpressPrice() {
            self.mainView.expressButton.priceLabel.text = ""
            
            let predefinedValueFloat = selectedPredefinedPrice()
            let deliveryType = getDeliveryType()
            UPAPIManager.getDeliveryPrice(postcodeFrom: self.addressFrom!.postcode, postcodeTo: self.addressTo!.postcode, weight: self.weight, length: self.length!, type: SendType.express, deliveryType: deliveryType, declaredValue: predefinedValueFloat, withSMS: self.mainView.smsButton.isSelected, isRecomended: nil) { (result) in
                  switch result {
                  case .success(let data):
                        var resultPrice = data.deliveryPrice ?? 0.0
                        if self.mainView.packagingButton.isSelected {
                              resultPrice += 7.2
                        }
                        self.mainView.expressButton.priceLabel.text = self.priceStringFrom(float: resultPrice)
                        
                  case .partialSuccess(let string):
                        print(string)
                  case .failure(let error):
                        print(error)
                  }
                  self.mainView.activityIndicator.stopAnimating()
            }
      }
      
      func priceStringFrom(float: Float) -> String {
        var neededString = String()
            let string = String(format: "%.2f", float)
        let array = string.components(separatedBy: ".")
        if array.count > 1 {
            var aStr = array[1]
            if aStr.last == "0" {
                aStr.removeLast()
                if aStr.last == "0" {
                    neededString = array[0]
                } else {
                    neededString = array[0] + "." + aStr
                }
            } else {
                neededString = array[0] + "." + aStr
            }
            
        } else {
            neededString = string
        }
        return neededString + " грн"
        
      }
      
      func getSmartBoxPrice() {
            let length = Int(self.length!)
            if length! <= 14 {
                  self.mainView.smartBoxButton.priceLabel.text = "30 грн"
            } else if length! > 14 && length! <= 22 {
                  self.mainView.smartBoxButton.priceLabel.text = "33 грн"
            } else if length! > 22 && length! <= 30 {
                  self.mainView.smartBoxButton.priceLabel.text = "39 грн"
            } else if length! > 30 && length! <= 49 {
                  self.mainView.smartBoxButton.priceLabel.text = "45 грн"
            }
            
      }
      
      func selectedPredefinedPrice() -> Float? {
            if self.mainView.predefinedValueButton.isSelected && self.mainView.predefinedValueField.text != "" {
                  return Float(self.mainView.predefinedValueField.text!)
            } else {
                  return nil
            }
      }
      
      func drawDeliveryTime() {
            if self.addressFrom?.regionID != self.addressTo?.regionID {
                  self.mainView.standartButton.timeLabel.text = "до 6 днів"
                  self.mainView.smartBoxButton.timeLabel.text = "2-4 дні"
                  self.mainView.expressButton.timeLabel.text = "2-4 дні"
            } else {
                  self.mainView.standartButton.timeLabel.text = "до 4 днів"
                  self.mainView.smartBoxButton.timeLabel.text = "1-2 дні"
                  self.mainView.expressButton.timeLabel.text = "1-2 дні"
            }
      }
}

