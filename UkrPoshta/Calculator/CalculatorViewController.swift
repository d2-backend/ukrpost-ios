//
//  CalculatorViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/5/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

enum ButtonForEditType {
    case from
    case to
}

enum PackageType {
    case goods
    case docs
}

enum DeliveryMethod {
    case avia
    case ground
}

class CalculatorViewController: UPViewController, IndexMainDelegate, UITextFieldDelegate, CountrySearchDelegate{
    
    let mainView = CalculatorView()
    var buttonForEdit: ButtonForEditType?
    private var addressFrom:CityDetails?
    private var addressTo:CityDetails?
    private var countryTo: Country?
    
    
     
      
      @objc func backButtonPressed() {
            let _ = self.navigationController?.popViewController(animated: true)
      }
      
      
    override func viewDidLoad() {
        super.viewDidLoad()
            hideTextBackNavigationButton()
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17.0)]
      
    
      
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
      
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))
      
        self.mainView.domesticButton.addTarget(self, action: #selector(domesticPressed), for: .touchUpInside)
        self.mainView.internationalButton.addTarget(self, action: #selector(internationalPressed), for: .touchUpInside)
        self.mainView.internationalButton.isSelected = false
        
        self.mainView.calculatorInputView.fromButton.addTarget(self, action: #selector(addressButtonPressed(sender:)), for: .touchUpInside)
        self.mainView.calculatorInputView.toButton.addTarget(self, action: #selector(addressButtonPressed(sender:)), for: .touchUpInside)

        self.mainView.calculatorInputView.swapButton.addTarget(self, action: #selector(swapPressed), for: .touchUpInside)
        
        self.mainView.calculatorInputView.domesticSubmitButton.addTarget(self, action: #selector(domesticSubmitPressed), for: .touchUpInside)
        self.mainView.calculatorInputView.domesticSubmitButton.isEnabled = false
        self.mainView.calculatorInputView.domesticSubmitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        
        self.mainView.calculatorInputView.docsButton.addTarget(self, action: #selector(docsButtonPressed), for: .touchUpInside)
        self.mainView.calculatorInputView.docsButton.isSelected = true
        self.mainView.calculatorInputView.goodsButton.addTarget(self, action: #selector(goodsButtonPressed), for: .touchUpInside)

        self.mainView.calculatorInputView.foreignSubmitButton.addTarget(self, action: #selector(foreignSubmitPressed), for: .touchUpInside)
        self.mainView.calculatorInputView.foreignSubmitButton.isEnabled = false

        self.mainView.calculatorInputView.countryButton.addTarget(self, action:  #selector(countryPressed), for: .touchUpInside)
//        UPAPIManager.getTarifs { (result) in
//            switch result {
//            case .success(let tariffs):
//                TariffsStored.setup(list: tariffs)
//            case .partialSuccess(let string):
//                self.showAlertWith(text: string)
//            case .failure(let error):
//                self.showAlertWith(text: error.localizedDescription)
//            }
//        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mainView.frame = self.view.bounds
    }
    
    @objc func foreignSubmitPressed() {
        let vc = ForeignDetailsViewController()
        vc.country = self.countryTo
        vc.type = self.mainView.calculatorInputView.docsButton.isSelected ? .docs : .goods
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func countryPressed() {
        let vc = CountryViewController()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc func docsButtonPressed() {
        if !self.mainView.calculatorInputView.docsButton.isSelected {
            self.mainView.calculatorInputView.docsButton.isSelected = true
            self.mainView.calculatorInputView.goodsButton.isSelected = false
        }
    }
    
    @objc func goodsButtonPressed() {
        if !self.mainView.calculatorInputView.goodsButton.isSelected {
            self.mainView.calculatorInputView.goodsButton.isSelected = true
            self.mainView.calculatorInputView.docsButton.isSelected = false
            
        }
    }
    
    @objc func domesticPressed() {
        self.mainView.internationalButton.isSelected = false
        self.mainView.domesticButton.isSelected = true
        self.mainView.calculatorInputView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)

    }
    
    @objc func internationalPressed() {
        let width = UIScreen.main.bounds.size.width
        self.mainView.internationalButton.isSelected = true
        self.mainView.domesticButton.isSelected = false
        self.mainView.calculatorInputView.setContentOffset(CGPoint(x: width, y: 0), animated: true)
        
    }
    
    @objc func addressButtonPressed(sender: UIButton) {
        if sender == self.mainView.calculatorInputView.fromButton {
            self.buttonForEdit = .from
        } else {
            self.buttonForEdit = .to
        }
        
        let vc = MixedSearchViewControllerProvider().instantiate(with: self)
        navigationController?.pushViewController(vc, animated: true)
    }
      @objc func closeTapped(){
            let vc = MainViewController()
            vc.isAnimationNeeded = false
            let nc = UINavigationController(rootViewController: vc)
            nc.navigationBar.shadowImage = UIImage()
            nc.navigationBar.isTranslucent = false
            nc.navigationBar.tintColor = UPColor.darkGray
            self.present(nc, animated: true, completion: nil)
      }
    
    @objc func swapPressed(sender: UIButton) {
        let newFromAddress = self.addressTo
        self.addressTo = self.addressFrom
        self.addressFrom = newFromAddress
        drawInfoOnButtons()
    }
    
    @objc func domesticSubmitPressed() {
        let vc = CalculatorDomesticDetailsViewController()
        vc.addressFrom = self.addressFrom
        vc.addressTo = self.addressTo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func drawInfoOnButtons() {
        if let addressFrom = self.addressFrom {
            self.mainView.calculatorInputView.fromButton.isSelected = true
            let cityType = addressFrom.cityTypeName
            let cityName = addressFrom.cityName
       //     let postcode = addressFrom.postcode
       //     let regionName = addressFrom.regionName
            self.mainView.calculatorInputView.fromButton.upperLabel.text = "\(cityType ?? ""). \(cityName ?? "")"
                   //   self.mainView.calculatorInputView.fromButton.bottomLabel.text = "\(postcode ?? "") \(regionName ?? "")"
        } else {
            self.mainView.calculatorInputView.fromButton.isSelected = false
            self.mainView.calculatorInputView.fromButton.upperLabel.text = ""
            self.mainView.calculatorInputView.fromButton.bottomLabel.text = ""
        }
        
        if let addressTo = self.addressTo {
            self.mainView.calculatorInputView.toButton.isSelected = true
            let cityType = addressTo.cityTypeName
            let cityName = addressTo.cityName
      //      let postcode = addressTo.postcode
       //     let regionName = addressTo.regionName
            self.mainView.calculatorInputView.toButton.upperLabel.text = "\(cityType ?? ""). \(cityName ?? "")"
            //      self.mainView.calculatorInputView.toButton.bottomLabel.text = "\(postcode ?? "") \(regionName ?? "")"
        } else {
            self.mainView.calculatorInputView.toButton.isSelected = false
            self.mainView.calculatorInputView.toButton.upperLabel.text = ""
            self.mainView.calculatorInputView.toButton.bottomLabel.text = ""
        }
        
        if self.addressTo != nil && self.addressFrom != nil {
            self.mainView.calculatorInputView.domesticSubmitButton.isEnabled = true
        } else {
            self.mainView.calculatorInputView.domesticSubmitButton.isEnabled = false
        }
    }
    
    func setCityDetails(cityDetails: CityDetails) {
        var button: AddressSubmitButton?
        
        if self.buttonForEdit == .from {
            button = self.mainView.calculatorInputView.fromButton
            self.addressFrom = cityDetails
        } else {
            button = self.mainView.calculatorInputView.toButton
            self.addressTo = cityDetails
        }
        if self.addressFrom != nil && self.addressTo != nil {
            self.mainView.calculatorInputView.domesticSubmitButton.isEnabled = true

        }
        drawInfoOnButtons()
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func setCountry(country: Country) {
        self.countryTo = country
        self.mainView.calculatorInputView.countryButton.setTitle(country.name, for: .normal)
        self.mainView.calculatorInputView.countryButton.isSelected = true
        self.mainView.calculatorInputView.foreignSubmitButton.isEnabled = true

    }
    
}

extension CalculatorViewController: MixedSearchControllerDelegate {
    func recievedResult(cityDetails: CityDetails) {
        setCityDetails(cityDetails: cityDetails)
    }
}
