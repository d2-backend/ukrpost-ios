//
//  CalculatorDomesticDetailsView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/8/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import TGPControls

class CalculatorDomesticDetailsView: UIView {
      
      let header = UILabel()
      private let separator = UIView()
      private let typeHeader = UPHeader()
      let parcelButton = UPHorizontalButton(normalImageName: "parcelIcon", selectedImageName: "parcelIconSelected")
      let letterButton = UPHorizontalButton(normalImageName: "letterIcon", selectedImageName: "letterIconSelected")
      let weightHeader = UPHeader()
      let weightLabel = UILabel()
      let weightSlider = TGPDiscreteSlider()
      
      let sizeHeader = UPHeader()
      let sizeLabel = UILabel()
      let sizeSlider = TGPDiscreteSlider()
      
      let letterWeightHeader = UPHeader()
      let letterWeightLabel = UILabel()
      let letterWeightSlider = TGPDiscreteSlider()
      let submitSizesButton = UPSubmitButton()
      
      let minWeight = UILabel()
      var maxWeight = UILabel()
      let minSize = UILabel()
      let maxSize = UILabel()
      
      override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
      }
      
      required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
      }
      
      private func commonInit() {
            self.backgroundColor = UPColor.whiteSmoke
            
            self.addSubview(header)
            header.textColor = UPColor.darkGray
            header.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
            
            self.addSubview(separator)
            separator.backgroundColor = UPColor.separatorGray
            
            self.addSubview(typeHeader)
            typeHeader.text = NSLocalizedString("calculator.type.header", comment: "")
            
            self.addSubview(parcelButton)
            parcelButton.label.text = NSLocalizedString("calculator.type.parcel", comment: "")
            parcelButton.isSelected = true
            
            self.addSubview(letterButton)
            letterButton.label.text = NSLocalizedString("calculator.type.letter", comment: "")
            letterButton.isSelected = false
            
            self.addSubview(weightHeader)
            weightHeader.text = NSLocalizedString("calculator.weight.header", comment: "")
            
            self.addSubview(weightLabel)
            let font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title3)
            let boldFont = UIFont(descriptor: font.fontDescriptor.withSymbolicTraits(.traitBold)!, size: font.pointSize)
            weightLabel.font = boldFont
            weightLabel.textColor = UPColor.darkGray
            weightLabel.textAlignment = .center
            
            self.addSubview(weightSlider)
             weightSlider.minimumValue = 0
            weightSlider.trackStyle = 2
            weightSlider.minimumTrackTintColor = UPColor.headerDarkYellow
            weightSlider.maximumTrackTintColor = UPColor.lightGray
            weightSlider.trackThickness = 5
            weightSlider.tickCount = 16
            weightSlider.tickStyle = 2
            weightSlider.tickSize = CGSize(width: 3, height: 3)
            weightSlider.maximumTickTintColor = UPColor.whiteSmoke
            weightSlider.minimumTickTintColor = UPColor.whiteSmoke
            weightSlider.thumbStyle = 0
            weightSlider.backgroundColor = .clear
            weightSlider.thumbTintColor = .white
            self.addSubview(sizeHeader)
            sizeHeader.text = NSLocalizedString("calculator.size.header", comment: "")
            
            self.addSubview(sizeLabel)
            sizeLabel.font = boldFont
            sizeLabel.textColor = UPColor.darkGray
            sizeLabel.textAlignment = .center
            
            self.addSubview(sizeSlider)
            sizeSlider.minimumValue = 0
            sizeSlider.trackStyle = 2
            sizeSlider.minimumTrackTintColor = UPColor.headerDarkYellow
            sizeSlider.maximumTrackTintColor = UPColor.lightGray
            sizeSlider.trackThickness = 5
            sizeSlider.tickCount = 24
            sizeSlider.tickStyle = 2
            sizeSlider.tickSize = CGSize(width: 3, height: 3)
            sizeSlider.maximumTickTintColor = UPColor.whiteSmoke
            sizeSlider.minimumTickTintColor = UPColor.whiteSmoke
            sizeSlider.thumbStyle = 0
            sizeSlider.backgroundColor = .clear
            sizeSlider.thumbTintColor = .white
            
            self.addSubview(letterWeightHeader)
            letterWeightHeader.textColor = UPColor.lightGray
            letterWeightHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
            letterWeightHeader.text = NSLocalizedString("calculator.letterWeight.header", comment: "")
            letterWeightHeader.alpha = 0.0
            self.addSubview(letterWeightLabel)
            letterWeightLabel.font = boldFont
            letterWeightLabel.textColor = UPColor.darkGray
            letterWeightLabel.textAlignment = .center
            letterWeightLabel.alpha = 0.0
            self.addSubview(letterWeightSlider)
            letterWeightSlider.trackStyle = 2
            letterWeightSlider.minimumTrackTintColor = UPColor.headerDarkYellow
            letterWeightSlider.maximumTrackTintColor = UPColor.lightGray
            letterWeightSlider.trackThickness = 5
            letterWeightSlider.tickCount = 5
            letterWeightSlider.tickStyle = 2
            letterWeightSlider.tickSize = CGSize(width: 3, height: 3)
            letterWeightSlider.maximumTickTintColor = UPColor.whiteSmoke
            letterWeightSlider.minimumTickTintColor = UPColor.whiteSmoke
            letterWeightSlider.thumbStyle = 0
            letterWeightSlider.backgroundColor = .clear
            letterWeightSlider.thumbTintColor = .white
            letterWeightSlider.minimumValue = 0
            letterWeightSlider.alpha = 0.0
            self.addSubview(submitSizesButton)
            
            self.addSubview(minWeight)
            minWeight.text = "0"
            self.addSubview(maxWeight)
            maxWeight.text = "30"
            self.addSubview(minSize)
            minSize.text = "0"
            self.addSubview(maxSize)
            maxSize.text = "200"
      }
      
      override func layoutSubviews() {
            super.layoutSubviews()
            let currentWidth = UIScreen.main.bounds.size.width
            
            header.pin.top(15).left(20).right(20).height(10)
            separator.pin.below(of: header).marginTop(15).left(20).right(20).height(1)
            typeHeader.pin.below(of: separator).marginTop(15).left(20).right(20).height(10)
            
            parcelButton.pin.below(of: typeHeader).marginTop(10).left(20).height(50).width(currentWidth/2 - 30)
            letterButton.pin.below(of: typeHeader).marginTop(10).after(of: parcelButton).marginLeft(20).right(20).height(50)
            
            weightHeader.pin.below(of: parcelButton).marginTop(20).left(20).right(20).height(10)
            
            weightLabel.pin.top(to: weightHeader.edge.bottom).marginTop(5).hCenter().height(25).width(50)
            
            weightSlider.pin.below(of: weightLabel).marginTop(10).left(20).right(20).height(35)
            minWeight.pin.above(of: weightSlider).marginTop(0).left(30).width(20).height(15)
            maxWeight.pin.above(of: weightSlider).marginTop(0).right(10).width(50).height(15)
            
            sizeHeader.pin.below(of: weightSlider).marginTop(30).left(20).right(20).height(10)
            sizeLabel.pin.top(to: sizeHeader.edge.bottom).marginTop(10).hCenter().height(30).width(50)
            
            sizeSlider.pin.below(of: sizeLabel).marginTop(20).left(20).right(20).height(35)
            minSize.pin.above(of: sizeSlider).marginTop(0).left(30).width(20).height(15)
            maxSize.pin.above(of: sizeSlider).marginTop(0).right(20).width(40).height(15)
            
            letterWeightHeader.pin.below(of: parcelButton).marginTop(20).left(20).right(20).height(10)
            letterWeightLabel.pin.top(to: weightHeader.edge.bottom).marginTop(10).hCenter().height(30).width(60)
            letterWeightSlider.pin.below(of: weightLabel).marginTop(10).left(20).right(10).height(50)
            
            submitSizesButton.pin.left().bottom(64).right().height(64)
      }
}
