//
//  ForeignResultViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/13/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignResultViewController: UIViewController {

    private var mainView: UIView?
    var countryTo: Country? = nil
    var packageType: PackageType? = nil
    var deliveryMethod: DeliveryMethod? = nil
    var length: Int?
    var height: Int?
    var width: Int?
    var weight: Int?
    
    private var currency: Currency? = nil

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchData()
        self.title = "Розрахунок вартостi"
        hideKeyboard()
        hideBackNavigationButtonText()
        if packageType == .docs {
            let aView = ForeignResultDocsView()
            aView.header.text = createHeader()
            
            if weight! <= 2000 {
                
                aView.upperFirstItem.typeLabel.text = NSLocalizedString("calculator.type.letter", comment: "")
                aView.upperFirstItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
                aView.isRecomendedFirst.addTarget(self, action: #selector(docsFirstButtonPressed), for: .touchUpInside)
                aView.isRecomendedFirst.isSelected = true
                aView.isSimpleFirst.addTarget(self, action: #selector(docsFirstButtonPressed), for: .touchUpInside)
                
                aView.upperSecondItem.typeLabel.text = NSLocalizedString("calculator.type.packed", comment: "")
                aView.upperSecondItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
                aView.upperSecondItem.icon.image = UIImage(named: "bagIconSelected")
            } else {
                aView.upperFirstItem.isHidden = true
                aView.upperSecondItem.isHidden = true
            }
            

            

            aView.upperThirdItem.typeLabel.text = NSLocalizedString("calculator.type.banderole", comment: "")
            aView.upperThirdItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
            aView.upperThirdItem.icon.image = UIImage(named: "smartBoxSelected")
            aView.isRecomendedThird.addTarget(self, action: #selector(docsThirdButtonPressed), for: .touchUpInside)
            aView.isRecomendedThird.isSelected = true
            aView.isSimpleThird.addTarget(self, action: #selector(docsThirdButtonPressed), for: .touchUpInside)
            
            aView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
            aView.submitButton.setTitle(NSLocalizedString("calculator.submit.map", comment: ""), for: .normal)
            
            self.mainView = aView

        } else {
            let aView = ForeignResultGoodsView()
            aView.header.text = createHeader()

            
            if weight! <= 2000 {
                
                aView.upperFirstItem.typeLabel.text = NSLocalizedString("calculator.type.packed", comment: "")
                aView.upperFirstItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
                aView.isRecomendedFirst.isSelected = true
                aView.isRecomendedFirst.isEnabled = false
                
                aView.upperSecondItem.typeLabel.text = NSLocalizedString("calculator.type.banderole", comment: "")
                aView.upperSecondItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
                aView.isRecomendedSecond.addTarget(self, action: #selector(goodsSecondButtonPressed), for: .touchUpInside)
                aView.isRecomendedSecond.isSelected = true
                aView.isSimpleSecond.addTarget(self, action: #selector(goodsSecondButtonPressed), for: .touchUpInside)
                
            } else if weight! <= 5000 {
                aView.upperFirstItem.isHidden = true
                
                aView.upperSecondItem.typeLabel.text = NSLocalizedString("calculator.type.banderole", comment: "")
                aView.upperSecondItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
                aView.isRecomendedSecond.addTarget(self, action: #selector(goodsSecondButtonPressed), for: .touchUpInside)
                aView.isRecomendedSecond.isSelected = true
                aView.isSimpleSecond.addTarget(self, action: #selector(goodsSecondButtonPressed), for: .touchUpInside)
                

            } else {
                aView.upperFirstItem.isHidden = true
                aView.upperSecondItem.isHidden = true
            }

            aView.upperThirdItem.typeLabel.text = NSLocalizedString("calculator.type.parcel", comment: "")
            aView.upperThirdItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
            aView.isRecomendedThird.isSelected = true
            aView.isRecomendedThird.addTarget(self, action: #selector(goodsThirdButtonPressed), for: .touchUpInside)
            aView.isSimpleThird.addTarget(self, action: #selector(goodsThirdButtonPressed), for: .touchUpInside)
            aView.predefinedValueButton.addTarget(self, action: #selector(predefinedValuePressed), for: .touchUpInside)
            aView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
            aView.submitButton.setTitle(NSLocalizedString("calculator.submit.map", comment: ""), for: .normal)

            self.mainView = aView
        }
        self.mainView!.frame = self.view.frame
        self.view.addSubview(self.mainView!)
        
        
    }
    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard) )
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc func submitPressed() {
        let storyboard = UIStoryboard(name: "Map", bundle:nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MapVC") as! UINavigationController
        let firstVC = controller.viewControllers[0] as! MapViewController
        self.navigationController?.pushViewController(firstVC, animated: true)
    }
    
    @objc func docsFirstButtonPressed() {
        (self.mainView as! ForeignResultDocsView).isRecomendedFirst.isSelected = !(self.mainView as! ForeignResultDocsView).isRecomendedFirst.isSelected
        (self.mainView as! ForeignResultDocsView).isSimpleFirst.isSelected = !(self.mainView as! ForeignResultDocsView).isSimpleFirst.isSelected
        if (self.mainView as! ForeignResultDocsView).isRecomendedFirst.isSelected {
            (self.mainView as! ForeignResultDocsView).upperFirstItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
        } else {
            (self.mainView as! ForeignResultDocsView).upperFirstItem.infoLabel.text = NSLocalizedString("calculator.result.withoutTracking", comment: "")
        }
        fetchDocsLetterPrice()
    }
    
    @objc func docsThirdButtonPressed() {
        (self.mainView as! ForeignResultDocsView).isRecomendedThird.isSelected = !(self.mainView as! ForeignResultDocsView).isRecomendedThird.isSelected
        (self.mainView as! ForeignResultDocsView).isSimpleThird.isSelected = !(self.mainView as! ForeignResultDocsView).isSimpleThird.isSelected
        if (self.mainView as! ForeignResultDocsView).isRecomendedThird.isSelected {
            (self.mainView as! ForeignResultDocsView).upperThirdItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
        } else {
            (self.mainView as! ForeignResultDocsView).upperThirdItem.infoLabel.text = NSLocalizedString("calculator.result.withoutTracking", comment: "")
        }
        fetchDocsBanderolePrice()
    }
    
    @objc func goodsSecondButtonPressed() {
        (self.mainView as! ForeignResultGoodsView).isRecomendedSecond.isSelected = !(self.mainView as! ForeignResultGoodsView).isRecomendedSecond.isSelected
        (self.mainView as! ForeignResultGoodsView).isSimpleSecond.isSelected = !(self.mainView as! ForeignResultGoodsView).isSimpleSecond.isSelected
        if (self.mainView as! ForeignResultGoodsView).isRecomendedSecond.isSelected {
            (self.mainView as! ForeignResultGoodsView).upperSecondItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
        } else {
            (self.mainView as! ForeignResultGoodsView).upperSecondItem.infoLabel.text = NSLocalizedString("calculator.result.withoutTracking", comment: "")
        }
        fetchGoodsBanderolePrice()
    }
    
    @objc func goodsThirdButtonPressed() {
        (self.mainView as! ForeignResultGoodsView).isRecomendedThird.isSelected = !(self.mainView as! ForeignResultGoodsView).isRecomendedThird.isSelected
        (self.mainView as! ForeignResultGoodsView).isSimpleThird.isSelected = !(self.mainView as! ForeignResultGoodsView).isSimpleThird.isSelected
        if (self.mainView as! ForeignResultGoodsView).isRecomendedThird.isSelected {
            (self.mainView as! ForeignResultGoodsView).upperThirdItem.infoLabel.text = NSLocalizedString("calculator.result.withTracking", comment: "")
        } else {
            (self.mainView as! ForeignResultGoodsView).upperThirdItem.infoLabel.text = NSLocalizedString("calculator.result.withoutTracking", comment: "")
        }
        fetchGoodsParcelPrice()

    }
    
    @objc func predefinedValuePressed() {
        (self.mainView as! ForeignResultGoodsView).predefinedValueButton.isSelected = !(self.mainView as! ForeignResultGoodsView).predefinedValueButton.isSelected

    }
    
    func createHeader() -> (String) {
        let countryFrom = NSLocalizedString("calculator.country.ukraine", comment: "")
        let countryTo = self.countryTo?.name ?? "none"
        
        let package = packageType == .docs ? NSLocalizedString("calculator.docsButton.label", comment: "") : NSLocalizedString("calculator.goodsButton.label", comment: "")
        
        let method = deliveryMethod == .avia ? NSLocalizedString("calculator.type.avia", comment: ""):NSLocalizedString("calculator.type.ground", comment: "")
      let weight = "\(Float(self.weight!) / 1000 )"
        let size = "\(length ?? 0)*\(height ?? 0)*\(width ?? 0)"
        
        let result = "\(countryFrom) - \(countryTo),\n\(package), \(method), \(weight) кг, \(size) см"
        return result
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func fetchData() {
        UPAPIManager.getCurrency { (result) in
            switch result {
            case .success(let currency):
                self.currency = currency
                if self.packageType == .docs {
                    if self.weight! <= 2000 {
                        self.fetchDocsLetterPrice()
                        self.fetchDocsBagPrice()
                        self.fetchDocsBanderolePrice()
                    } else {
                        self.fetchDocsBanderolePrice()
                    }
                } else {
                    if self.weight! <= 2000 {
                        self.fetchGoodsBagPrice()
                        self.fetchGoodsBanderolePrice()
                        self.fetchGoodsParcelPrice()
                    } else if self.weight! <= 5000 {
                        self.fetchGoodsBanderolePrice()
                        self.fetchGoodsParcelPrice()
                    } else {
                        self.fetchGoodsParcelPrice()
                    }
                }
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func fetchDocsLetterPrice() {
        let isRecomended = (self.mainView as! ForeignResultDocsView).isRecomendedFirst.isSelected
        (self.mainView as! ForeignResultDocsView).upperFirstItem.priceLabel.text = "..."
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: Int(self.weight ?? 0),
                                              length: maxSide(),
                                              calculationDescription: nil,
                                              packageType: APIPackageType.letter.rawValue,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate ,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: countryTo?.code,
                                              declaredPrice: nil,
                                              deliveryPriceCurr: nil,
                                              recommended: isRecomended,
                                              bulky: false,
                                              fragile: false,
                                              transportType: self.deliveryMethod == .avia ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: false,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                (self.mainView as! ForeignResultDocsView).upperFirstItem.priceLabel.text = "\(price.deliveryPrice ?? 0)" + " " + NSLocalizedString("curencyShort", comment: "")
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func fetchDocsBagPrice() {

        (self.mainView as! ForeignResultDocsView).upperSecondItem.priceLabel.text = "..."
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: Int(self.weight ?? 0),
                                              length: maxSide(),
                                              calculationDescription: nil,
                                              packageType: APIPackageType.smallBag.rawValue,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: countryTo?.code,
                                              declaredPrice: nil,
                                              deliveryPriceCurr: nil,
                                              recommended: true,
                                              bulky: false,
                                              fragile: false,
                                              transportType: self.deliveryMethod == .avia ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: nil,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                (self.mainView as! ForeignResultDocsView).upperSecondItem.priceLabel.text = "\(price.deliveryPrice ?? 0)" + " " + NSLocalizedString("curencyShort", comment: "")
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func fetchDocsBanderolePrice() {
        let isRecomended = (self.mainView as! ForeignResultDocsView).isRecomendedThird.isSelected

        (self.mainView as! ForeignResultDocsView).upperThirdItem.priceLabel.text = "..."
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: Int(self.weight ?? 0),
                                              length: maxSide(),
                                              calculationDescription: nil,
                                              packageType: APIPackageType.banderole.rawValue,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: countryTo?.code,
                                              declaredPrice: nil,
                                              deliveryPriceCurr: nil,
                                              recommended: isRecomended,
                                              bulky: false,
                                              fragile: false,
                                              transportType: self.deliveryMethod == .avia ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: nil,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                (self.mainView as! ForeignResultDocsView).upperThirdItem.priceLabel.text = "\(price.deliveryPrice ?? 0)" + " " + NSLocalizedString("curencyShort", comment: "")
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    ///// goods
    
    func fetchGoodsBagPrice() {
        
        (self.mainView as! ForeignResultGoodsView).upperFirstItem.priceLabel.text = "..."
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: Int(self.weight ?? 0),
                                              length: maxSide(),
                                              calculationDescription: nil,
                                              packageType: APIPackageType.smallBag.rawValue,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: countryTo?.code,
                                              declaredPrice: nil,
                                              deliveryPriceCurr: nil,
                                              recommended: true,
                                              bulky: false,
                                              fragile: false,
                                              transportType: self.deliveryMethod == .avia ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: nil,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                (self.mainView as! ForeignResultGoodsView).upperFirstItem.priceLabel.text = "\(price.deliveryPrice ?? 0)" + " " + NSLocalizedString("curencyShort", comment: "")
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func fetchGoodsBanderolePrice() {
        let isRecomended = (self.mainView as! ForeignResultGoodsView).isRecomendedSecond.isSelected
        
        (self.mainView as! ForeignResultGoodsView).upperSecondItem.priceLabel.text = "..."
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: Int(self.weight ?? 0),
                                              length: maxSide(),
                                              calculationDescription: nil,
                                              packageType: APIPackageType.banderole.rawValue,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: countryTo?.code,
                                              declaredPrice: nil,
                                              deliveryPriceCurr: nil,
                                              recommended: isRecomended,
                                              bulky: false,
                                              fragile: false,
                                              transportType: self.deliveryMethod == .avia ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: nil,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                (self.mainView as! ForeignResultGoodsView).upperSecondItem.priceLabel.text = "\(price.deliveryPrice ?? 0)" + " " + NSLocalizedString("curencyShort", comment: "")
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func fetchGoodsParcelPrice() {
        let isRecomended = (self.mainView as! ForeignResultGoodsView).isRecomendedThird.isSelected
        
        (self.mainView as! ForeignResultGoodsView).upperThirdItem.priceLabel.text = "..."
        let declaredPrice = (self.mainView as! ForeignResultGoodsView).predefinedValueField.text
        let numberFormatter = NumberFormatter()
        let number = numberFormatter.number(from: declaredPrice ?? "0.0")
        let declaredFloat = number?.floatValue
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: Int(self.weight ?? 0),
                                              length: maxSide(),
                                              calculationDescription: nil,
                                              packageType: APIPackageType.parcel.rawValue,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: countryTo?.code,
                                              declaredPrice: declaredFloat,
                                              deliveryPriceCurr: nil,
                                              recommended: isRecomended,
                                              bulky: false,
                                              fragile: false,
                                              transportType: self.deliveryMethod == .avia ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: nil,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                (self.mainView as! ForeignResultGoodsView).upperThirdItem.priceLabel.text = "\(price.deliveryPrice ?? 0)" + " " + NSLocalizedString("curencyShort", comment: "")
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func maxSide() -> Float {
        return Float(max(max(self.length ?? 0, self.height ?? 0), self.width ?? 0))
    }
    

}
