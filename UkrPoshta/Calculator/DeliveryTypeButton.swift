//
//  DeliveryTypeButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/10/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DeliveryTypeButton: UIButton {

    private let icon = UIImageView()
    var normalImage = UIImage()
    var selectedImage = UIImage()
    
    let label = UILabel()
    let priceLabel = UILabel()
    let timeLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white
        self.addSubview(icon)
        icon.image = normalImage
        self.icon.isUserInteractionEnabled = false
        
        self.addSubview(label)
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        self.label.isUserInteractionEnabled = false
        
        self.addSubview(priceLabel)
        priceLabel.textAlignment = .center
        priceLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        priceLabel.isUserInteractionEnabled = false
        
        self.addSubview(timeLabel)
        timeLabel.textAlignment = .center
        timeLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        timeLabel.textColor = UPColor.lightGray
        timeLabel.isUserInteractionEnabled = false


    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        label.pin.left().right().top(6).height(12)
        icon.pin.below(of: label).marginTop(6).hCenter().width(20).height(20)
        priceLabel.pin.below(of: icon).marginTop(6).height(20).left().right()
        timeLabel.pin.below(of: priceLabel).marginTop(6).height(20).left().right()
    }
    
    override var isSelected: Bool {
        
        didSet {
            if isSelected {
                icon.image = selectedImage
                label.textColor = UPColor.darkGray
                priceLabel.textColor = UPColor.darkGray
            } else {
                icon.image = normalImage
                label.textColor = UPColor.lightGray
                priceLabel.textColor = UPColor.lightGray

            }
        }
    }
}
