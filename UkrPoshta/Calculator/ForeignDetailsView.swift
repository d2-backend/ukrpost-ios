//
//  ForeignDetailsView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/12/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit



class ForeignDetailsView: UIView {

    let header = UILabel()
    private let separator = UIView()
    
    private let typeHeader = UPHeader()
    let aviaButton = UPHorizontalButton(normalImageName: "avia", selectedImageName: "aviaSelected")
    let groundButton = UPHorizontalButton(normalImageName: "car", selectedImageName: "carSelected")
    
    private let sizeHeader = UPHeader()
    private let sizesBackground = UIView()
    private let firstSeparator = UIView()
    private let secondSeparator = UIView()
    
    let lengthField = UPTextField()
    let heightField = UPTextField()
    let widthField = UPTextField()
    
    private let weightLabel = UILabel()
    let weightField = UPTextField()
    
    let submitButton = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke

        self.addSubview(header)
        header.textColor = UPColor.darkGray
        header.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        
        separator.backgroundColor = UPColor.separatorGray
        self.addSubview(separator)
        
        typeHeader.text = NSLocalizedString("calculator.typeForeign.header", comment: "")
        self.addSubview(typeHeader)
        
        aviaButton.label.text = NSLocalizedString("calculator.type.avia", comment: "")
        aviaButton.isSelected = true
        self.addSubview(aviaButton)
        
        groundButton.label.text = NSLocalizedString("calculator.type.ground", comment: "")
        self.addSubview(groundButton)
        
        sizeHeader.text = NSLocalizedString("calculator.allSizes.header", comment: "")
        self.addSubview(sizeHeader)
        
        
        
        self.addSubview(sizesBackground)
        sizesBackground.backgroundColor = .white
        sizesBackground.layer.masksToBounds = false
        sizesBackground.layer.shadowColor = UPColor.darkGray.cgColor
        sizesBackground.layer.shadowOpacity = 0.1
        sizesBackground.layer.shadowOffset = .zero
        sizesBackground.layer.shadowRadius = 2
        sizesBackground.layer.cornerRadius = 3.0
        
        sizesBackground.addSubview(lengthField)
        lengthField.backgroundColor = .clear
        lengthField.textAlignment = .center
        lengthField.keyboardType = .numbersAndPunctuation

        sizesBackground.addSubview(heightField)
        heightField.backgroundColor = .clear
        heightField.textAlignment = .center
        heightField.keyboardType = .numbersAndPunctuation

        sizesBackground.addSubview(widthField)
        widthField.backgroundColor = .clear
        widthField.textAlignment = .center
        widthField.keyboardType = .numbersAndPunctuation

        sizesBackground.addSubview(firstSeparator)
        firstSeparator.backgroundColor = UPColor.separatorGray
        sizesBackground.addSubview(secondSeparator)
        secondSeparator.backgroundColor = UPColor.separatorGray
        
        self.addSubview(weightLabel)
        weightLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        weightLabel.textColor = UPColor.darkGray
        weightLabel.text = NSLocalizedString("calculator.details.weight.label", comment: "")

        self.addSubview(weightField)
        weightField.textAlignment = .center
        weightField.keyboardType = .numberPad
        self.addSubview(submitButton)
        submitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width

        header.pin.top(15).left(20).right(20).height(10)
        separator.pin.below(of: header).marginTop(15).left(20).right(20).height(1)
        
        typeHeader.pin.below(of: separator).marginTop(15).left(20).right(20).height(10)
        aviaButton.pin.below(of: typeHeader).marginTop(10).left(20).height(50).width(currentWidth/2 - 30)
        groundButton.pin.below(of: typeHeader).marginTop(10).after(of: aviaButton).marginLeft(20).right(20).height(50)
        
        sizeHeader.pin.top(to: aviaButton.edge.bottom).marginTop(15).left(20).right(20).height(10)
        sizesBackground.pin.below(of: sizeHeader).marginTop(10).left(20).height(50).right(20)
        let tripleWidth = (currentWidth - 40) / 3
        
        lengthField.pin.top().left().bottom().width(tripleWidth)
        heightField.pin.left(to: lengthField.edge.right).top().bottom().width(tripleWidth)
        widthField.pin.top().bottom().left(to: heightField.edge.right).width(tripleWidth)
        firstSeparator.pin.top(5).bottom(5).left(to: heightField.edge.left).width(1)
        secondSeparator.pin.top(5).bottom(5).right(to: heightField.edge.right).width(1)

        weightField.pin.below(of: sizesBackground).marginTop(20).right(20).height(50).width(tripleWidth)
        weightLabel.pin.height(20).left(20).centerRight(to: weightField.anchor.centerLeft)
        
        submitButton.pin.left().bottom(64).right().height(64)
    }
}

