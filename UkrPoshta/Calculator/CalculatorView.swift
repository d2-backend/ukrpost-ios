//
//  CalculatorView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/5/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CalculatorView: UIView {

    let internationalButton = IndexTabButton()
    let domesticButton = IndexTabButton()
    let contentView = UIView()
    let calculatorInputView = CalculatorInputView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(internationalButton)
        self.internationalButton.setTitle(NSLocalizedString("calculator.international.tabButton", comment: ""), for: .normal)
        
        self.addSubview(domesticButton)
        self.domesticButton.setTitle(NSLocalizedString("calculator.domestic.tabButton", comment: ""), for: .normal)
        
        
        self.addSubview(contentView)
        contentView.backgroundColor = .green
        
        contentView.addSubview(calculatorInputView)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width

        self.domesticButton.pin.top().left().height(40).width(currentWidth / 2)
        
        self.internationalButton.pin.after(of: domesticButton).top().right().height(40).width(currentWidth / 2)
        
        self.contentView.pin.below(of: domesticButton).marginTop(10).left().right().bottom()
        calculatorInputView.pin.top().left().bottom().right()

        
    }

}
