//
//  MixedSearchController.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/2/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import UIKit

protocol MixedSearchControllerDelegate: class {
    func recievedResult(cityDetails: CityDetails)
}

protocol MixedSearchControllerProtocol: NSObjectProtocol {
    var delegate: MixedSearchControllerDelegate? {get set}
}

final class MixedSearchController: NSObject, MixedSearchControllerProtocol {
    weak var delegate: MixedSearchControllerDelegate?
    private weak var view: MixedSearchViewProtocol?
    
    private var resultToDisplay: [Any] = []
    private var rawResult: [Any] = []
    
    init(with view: MixedSearchViewProtocol?) {
        self.view = view
    }
    
    private func fetchData(byString string: String) {
        view?.stopSpinner()
        view?.startSpinner()

        UPAPIManager.getCityList(string: string, regionID: nil, districtID: nil) { [weak self] result in
            switch result {
            case .success(let data):
                self?.resultToDisplay = data
                self?.rawResult = data
                self?.view?.hideErrorMessage()
                self?.view?.reloadData()
            case .partialSuccess(let text):
                self?.view?.partialSuccess(text: text)
            case .failure(let error):
                self?.view?.partialSuccess(text: error.localizedDescription)
            }
            self?.view?.stopSpinner()
        }
    }
    
    private func fetchData(byIndex index: String) {
        UPAPIManager.getCityDetailsBy(postcode: index) { [weak self] result in
            switch result {
            case .success(let city):
                self?.resultToDisplay = [city]
                self?.rawResult = [city]
                self?.view?.hideErrorMessage()
                self?.view?.reloadData()
            case .partialSuccess(let string):
                self?.view?.partialSuccess(text: string)
            case .failure(let error):
                self?.view?.partialSuccess(text: error.localizedDescription)
            }
            self?.view?.stopSpinner()
        }
    }

    private func fetchCityDetails(for city: City) {
        UPAPIManager
            .getCityDetailsFromPostOfficeBy(cityID: city.cityID,
                                            regionID: city.regionID,
                                            districtID: city.districtID) { [weak self] result in
                switch result {
                case .success(var cityDetails):
                    cityDetails.update(with: city)
                    self?.delegate?.recievedResult(cityDetails: cityDetails)
                    self?.view?.popViewController()
                case .partialSuccess(let text):
                    self?.view?.showAlertWith(text: text)
                case .failure(let error):
                    self?.view?.showAlertWith(text: error.localizedDescription)
                }
        }
    }
    
}


extension MixedSearchController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            return false
        }
        view?.hideErrorMessage()
        view?.defaultTextFieldStatus()
        self.resultToDisplay = []
        view?.reloadData()
        
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }

        if newString.isNumber && newString.count == 5 {
            fetchData(byIndex: newString)
        } else if !newString.isNumber && newString.count > 2 {
            fetchData(byString: newString)
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        resultToDisplay = []
        view?.hideErrorMessage()
        view?.reloadData()
        
        return true
    }
}


extension MixedSearchController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionExtendedCell") as! SuggestionExtendedCell
        if let city = resultToDisplay[indexPath.row] as? City {
            config(cell: cell, with: city)
        }
        if let cityDetails = resultToDisplay[indexPath.row] as? CityDetails {
            config(cell: cell, with: cityDetails)
        }
        
        return cell
    }
    
    private func config(cell: SuggestionExtendedCell, with city: City) {
        cell.mainLabel.text = city.name
        cell.descriptionLabel.text = city.descriptionName
    }
    
    private func config(cell: SuggestionExtendedCell, with city: CityDetails) {
        cell.mainLabel.text = city.name
        cell.descriptionLabel.text = city.descriptionName
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = resultToDisplay[indexPath.row]
        if let item = item as? CityDetails {
            delegate?.recievedResult(cityDetails: item)
            view?.popViewController()
        } else if let city = item as? City {
            fetchCityDetails(for: city)
        }
    }
    
}
