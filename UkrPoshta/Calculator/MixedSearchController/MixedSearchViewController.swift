//
//  IndexSearchViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import PinLayout

protocol MixedSearchViewProtocol: class {
    func startSpinner()
    func stopSpinner()
    func reloadData()
    func popViewController()
    func partialSuccess(text: String?)
    func defaultTextFieldStatus()
    func hideErrorMessage()
    func showAlertWith(text: String)
}

class MixedSearchViewController: UIViewController {
    let mainView = IndexSearchView()
    var controller: MixedSearchControllerProtocol!
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
    }

    private func configUI() {
        view.backgroundColor = UPColor.whiteSmoke
        title = "Місто або індекс"
        hideTextBackNavigationButton()
        configMainViewUI()
    }

    private func configMainViewUI() {
        view.addSubview(mainView)

        mainView.pin.all(.zero)
        mainView.searchField.keyboardType = .namePhonePad
        mainView.tableView.rowHeight = 60
        mainView.searchField.delegate = controller as? UITextFieldDelegate
        mainView.tableView.delegate = controller as? UITableViewDelegate
        mainView.tableView.dataSource = controller as? UITableViewDataSource
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        mainView.searchField.becomeFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter
            .default
            .addObserver(self, selector:#selector(keybordWasShown) , name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keybordWasShown(notification: NSNotification) {
        let mainFrame = self.view.frame
        let keyboardRect: CGRect = notification.userInfo!["UIKeyboardBoundsUserInfoKey"] as! CGRect
        let keyboardHeight = keyboardRect.size.height
        
        let newHeight = mainFrame.size.height - keyboardHeight + 64
        mainView.frame = CGRect(x: 0, y: 0, width: mainFrame.width, height: newHeight)
        mainView.layoutSubviews()
    }
    

}

extension MixedSearchViewController: MixedSearchViewProtocol {
    func startSpinner() {
        mainView.activityIndicator.startAnimating()
    }

    func stopSpinner() {
        mainView.activityIndicator.stopAnimating()
    }

    func reloadData() {
        mainView.tableView.reloadData()
    }

    func popViewController() {
        navigationController?.popViewController(animated: true)
    }

    func hideErrorMessage() {
        mainView.errorMessageLabel.isHidden = true
    }

    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))

        present(alert, animated: true, completion: nil)
    }

    func partialSuccess(text: String?) {
        mainView.searchField.textColor = .red
        if let text = text {
            mainView.errorMessageLabel.isHidden = false
            mainView.errorMessageLabel.text = text
            mainView.layoutSubviews()
        }
    }

    func defaultTextFieldStatus() {
        mainView.searchField.textColor = UPColor.darkGray
    }

}
