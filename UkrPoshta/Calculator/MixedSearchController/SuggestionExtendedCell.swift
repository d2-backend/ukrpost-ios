//
//  SuggestionExtendedCell.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/2/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

import UIKit

class SuggestionExtendedCell: UITableViewCell {

    let mainLabel = UILabel()
    let descriptionLabel = UILabel()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    private func commonInit() {
        addSubview(mainLabel)
        addSubview(descriptionLabel)
        mainLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        mainLabel.textColor = UPColor.darkGray
        descriptionLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
        descriptionLabel.textColor = UPColor.lightGray
        backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        mainLabel.pin.left(20).right(20).top(10).height(20)
        descriptionLabel.pin.left(20).right(20).height(20).bottom(8)
    }

}
