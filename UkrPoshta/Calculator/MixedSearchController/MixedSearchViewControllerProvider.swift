//
//  MixedSearchViewControllerProvider.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/2/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

final class MixedSearchViewControllerProvider {
    func instantiate(with delegate: MixedSearchControllerDelegate) -> MixedSearchViewController {
        let viewController = MixedSearchViewController()
        let controller = MixedSearchController(with: viewController)
        controller.delegate = delegate
        viewController.controller = controller

        return viewController
    }
}
