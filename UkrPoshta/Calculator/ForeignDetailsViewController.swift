//
//  ForeignDetailsViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/12/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignDetailsViewController: UPViewController, UITextFieldDelegate {
      
      private let mainView = ForeignDetailsView()
      var country: Country? = nil
      var type: PackageType? = nil
      
      func hideBackNavigationButtonText() {
            let imgBack = UIImage(named: "ic_back")
            navigationController?.navigationBar.backIndicatorImage = imgBack
            navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
            navigationItem.leftItemsSupplementBackButton = true
            navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
      }
      
      override func viewDidLoad() {
            super.viewDidLoad()
            self.title = "Розрахунок вартостi"
            hideBackNavigationButtonText()
            self.mainView.frame = self.view.bounds
            self.view.addSubview(self.mainView)
            self.mainView.heightField.keyboardType = .numbersAndPunctuation
            self.mainView.widthField.keyboardType = .numbersAndPunctuation
            self.mainView.lengthField.keyboardType = .numbersAndPunctuation
            self.mainView.weightField.keyboardType = .numbersAndPunctuation
            let countryFrom = NSLocalizedString("calculator.country.ukraine", comment: "")
            
            self.mainView.header.text = "\(countryFrom) - \(country?.name ?? "none")"
            
            self.mainView.aviaButton.addTarget(self, action: #selector(methodPressed), for: .touchUpInside)
            self.mainView.groundButton.addTarget(self, action: #selector(methodPressed), for: .touchUpInside)
            
            self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
            self.mainView.submitButton.isEnabled = false
            
            self.mainView.lengthField.delegate = self
            self.mainView.heightField.delegate = self
            self.mainView.widthField.delegate = self
            self.mainView.weightField.delegate = self
      }
      
      @objc func methodPressed() {
            self.mainView.aviaButton.isSelected = !self.mainView.aviaButton.isSelected
            self.mainView.groundButton.isSelected = !self.mainView.groundButton.isSelected
      }
      
      @objc func submitPressed() {
            let vc = ForeignResultViewController()
            vc.countryTo = self.country
            vc.packageType = self.type
            vc.deliveryMethod = self.mainView.aviaButton.isSelected ? .avia : .ground
            vc.height = Int(self.mainView.heightField.text!)
            vc.length = Int(self.mainView.lengthField.text!)
            vc.width = Int(self.mainView.widthField.text!)
            vc.weight = Int(Float(self.mainView.weightField.text!)! * 1000)
            self.navigationController?.pushViewController(vc, animated: true)
      }
      
      func textFieldDidEndEditing(_ textField: UITextField) {
            let sizeLimit: Int!
            if self.type == .docs  {
                  sizeLimit = 90
            } else {
                  sizeLimit = 200
            }
            
            if self.mainView.lengthField.text != "" &&  self.mainView.heightField.text != "" && self.mainView.widthField.text != "" {
                  guard let heightStr = self.mainView.heightField.text else {return }
                  let heightInt = Int(heightStr) ?? 0
                  guard let widthStr = self.mainView.widthField.text else {return }
                  let widthInt = Int(widthStr) ?? 0
                  guard let lengthStr = self.mainView.lengthField.text else {return }
                  let lengthInt = Int(lengthStr) ?? 0
                  if heightInt + widthInt + lengthInt > sizeLimit {
                        self.mainView.lengthField.text = ""
                        self.mainView.widthField.text = ""
                        self.mainView.heightField.text = ""
                        let alert = UIAlertController(title: "Помилка", message: "Сума сторiн не має перевищувати \(sizeLimit!) см.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "ок", style: .default, handler: nil))
                        self.present(alert, animated: true)
                  }
            }
            if self.mainView.lengthField.text != "" &&  self.mainView.heightField.text != "" && self.mainView.widthField.text != "" && self.mainView.weightField.text != ""{
                  self.mainView.submitButton.isEnabled = true
            } else {
                  self.mainView.submitButton.isEnabled = false
            }
      }
      
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            var oldString = textField.text
            var newString: String!
            switch string {
            case "":
                  oldString!.removeLast()
                  newString = oldString
            case "0","1","2","3","4","5","6","7","8","9" :
                  newString = oldString! + string
            case ".",",":
                  if textField == mainView.weightField {
                        if oldString?.range(of: ".") != nil {
                              newString = oldString
                        } else {
                              newString = oldString! + "."
                        }
                        if newString == "." {
                              newString = "0."
                        }
                  } else {
                        return false
                  }
            default :
                  return false
            }
            if newString == "" {
                  textField.text = newString
                  return false
            }
            let sizeLimit: Int
            let weightLimit: Int
            if type == .docs {
                  sizeLimit = 60
                  weightLimit = 2000
            } else {
                  sizeLimit = 105
                  weightLimit = 30000
            }
            if textField == mainView.weightField {
                  let arr = newString.components(separatedBy: ".")
                  if Int(Float(newString)! * 1000 ) > weightLimit {
                        textField.text = "\(Float(weightLimit) / 1000)"
                  } else if arr.count > 1 && arr[1].count > 3 {
                        return false
                  } else {
                        textField.text = newString
                  }
            } else if textField == mainView.lengthField || textField == mainView.heightField || textField == mainView.widthField {
                  if Int(newString)! > sizeLimit {
                        textField.text = "\(sizeLimit)"
                  } else {
                        textField.text = newString
                  }
            }
            return false
      }
}


