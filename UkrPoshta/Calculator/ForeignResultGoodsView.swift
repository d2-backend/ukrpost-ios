//
//  ForeignResultGoodsView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/21/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignResultGoodsView: UIView, UIScrollViewDelegate{

    let header = UILabel()
    let upperInvisibleScroll = UIScrollView()
    let upperScroll = UIScrollView()
    let upperFirstItem = ResultCostView()
    let upperSecondItem = ResultCostView()
    let upperThirdItem = ResultCostView()
    
    let bottomScroll = UIScrollView()
    let bottomFirstItem = UIView()
    let bottomSecondItem = UIView()
    let bottomThirdItem = UIView()
    
    let pageControll = UIPageControl()
    
    let isRecomendedFirst = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let isRecomendedSecond = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let isSimpleSecond = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")

    let isRecomendedThird = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let isSimpleThird = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    private let thirdSeparator = UIView()
    let predefinedValueButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let predefinedValueField = UPTextField()
    
    let submitButton = UPSubmitButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(header)
        header.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        header.textColor = UPColor.darkGray
        header.numberOfLines = 0
        header.lineBreakMode = .byWordWrapping
        
        
        self.addSubview(upperScroll)
        upperScroll.backgroundColor = .clear
        upperScroll.isUserInteractionEnabled = false
        upperScroll.layer.masksToBounds = false
        
        upperScroll.addSubview(upperFirstItem)
        upperFirstItem.icon.image = UIImage(named: "bagIconSelected")
        
        upperScroll.addSubview(upperSecondItem)
        upperSecondItem.icon.image = UIImage(named: "smartBoxSelected")
        
        upperScroll.addSubview(upperThirdItem)
        upperThirdItem.icon.image = UIImage(named: "parcelIconSelected")
        
        self.addSubview(upperInvisibleScroll)
        upperInvisibleScroll.isPagingEnabled = true
        upperInvisibleScroll.delegate = self
        upperInvisibleScroll.isExclusiveTouch = false
        upperInvisibleScroll.canCancelContentTouches = false
        upperInvisibleScroll.isUserInteractionEnabled = true
        self.addSubview(bottomScroll)
        bottomScroll.isUserInteractionEnabled = false
        bottomScroll.addSubview(bottomFirstItem)
        
        upperInvisibleScroll.addSubview(isRecomendedFirst)
        isRecomendedFirst.label.text = NSLocalizedString("checkout.price.recomended", comment: "")
        isRecomendedFirst.backgroundColor = .clear
        isRecomendedFirst.label.textAlignment = .left
        isRecomendedFirst.layer.masksToBounds = true
        
        self.addSubview(pageControll)
        pageControll.currentPage = 0
        pageControll.pageIndicatorTintColor = UPColor.lightGray
        pageControll.currentPageIndicatorTintColor = UPColor.headerDarkYellow
        pageControll.isUserInteractionEnabled = false
        
        bottomScroll.addSubview(bottomSecondItem)
        upperInvisibleScroll.addSubview(isRecomendedSecond)
        isRecomendedSecond.label.text = NSLocalizedString("checkout.price.recomended", comment: "")
        isRecomendedSecond.backgroundColor = .clear
        isRecomendedSecond.label.textAlignment = .left
        isRecomendedSecond.layer.masksToBounds = true
        isRecomendedSecond.isSelected = true
        
        upperInvisibleScroll.addSubview(isSimpleSecond)
        isSimpleSecond.label.text = NSLocalizedString("calculator.delivery.simple", comment: "")
        isSimpleSecond.backgroundColor = .clear
        isSimpleSecond.label.textAlignment = .left
        isSimpleSecond.layer.masksToBounds = true
        
        bottomScroll.addSubview(bottomThirdItem)
        
        upperInvisibleScroll.addSubview(isRecomendedThird)
        isRecomendedThird.label.text = NSLocalizedString("checkout.price.recomended", comment: "")
        isRecomendedThird.backgroundColor = .clear
        isRecomendedThird.label.textAlignment = .left
        isRecomendedThird.layer.masksToBounds = true
        
        upperInvisibleScroll.addSubview(isSimpleThird)
        isSimpleThird.label.text = NSLocalizedString("calculator.delivery.simple", comment: "")
        isSimpleThird.backgroundColor = .clear
        isSimpleThird.label.textAlignment = .left
        isSimpleThird.layer.masksToBounds = true
        
        upperInvisibleScroll.addSubview(thirdSeparator)
        thirdSeparator.backgroundColor = UPColor.separatorGray
        
        upperInvisibleScroll.addSubview(predefinedValueButton)
        predefinedValueButton.label.text = NSLocalizedString("calculator.predefinedValue.label", comment: "")
        predefinedValueButton.backgroundColor = .clear
        predefinedValueButton.label.textAlignment = .left
        predefinedValueButton.layer.masksToBounds = true
        
        upperInvisibleScroll.addSubview(predefinedValueField)
        predefinedValueField.textAlignment = .center
        predefinedValueField.keyboardType = .numbersAndPunctuation
 
        self.addSubview(submitButton)
        submitButton.setTitle(NSLocalizedString("error.default.ok", comment: ""), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width
        
        header.pin.top(20).left(20).right(20).height(40)
        
        let upperItemWidth: CGFloat = currentWidth - 40
        let upperWidth: CGFloat!
        let upperInvisibleWidth: CGFloat!

        if upperFirstItem.isHidden == false && upperSecondItem.isHidden == false && upperThirdItem.isHidden == false {
            upperWidth = 40 + (upperItemWidth * 3) + 20
            upperInvisibleWidth = currentWidth * 3
            pageControll.numberOfPages = 3
        } else if upperFirstItem.isHidden == true && upperSecondItem.isHidden == false && upperThirdItem.isHidden == false {
            upperWidth = 40 + (upperItemWidth * 2) + 20
            upperInvisibleWidth = currentWidth * 2
            pageControll.numberOfPages = 2
        } else {
            upperWidth = currentWidth
            upperInvisibleWidth = currentWidth
            pageControll.isHidden = true
        }
        
        upperScroll.pin.below(of: header).marginTop(10).left().height(100).right()
        upperScroll.contentSize = CGSize(width: upperWidth, height: 100)
        
        upperInvisibleScroll.pin.below(of: header).marginTop(10).left().height(310).right()
        bottomScroll.pin.below(of: upperScroll).marginTop(10).left().height(200).right()
        
        upperInvisibleScroll.contentSize = CGSize(width: upperInvisibleWidth, height: 260)
        bottomScroll.contentSize = CGSize(width: upperInvisibleWidth, height: 200)

        if upperFirstItem.isHidden == false && upperSecondItem.isHidden == false && upperThirdItem.isHidden == false {
            upperFirstItem.pin.top().left(20).bottom().width(upperItemWidth)
            upperSecondItem.pin.top().bottom().left(to: upperFirstItem.edge.right).width(upperItemWidth).marginLeft(10)
            upperThirdItem.pin.top().bottom().left(to: upperSecondItem.edge.right).width(upperItemWidth).marginLeft(10)
            bottomFirstItem.pin.top().left().bottom().width(currentWidth)
            bottomSecondItem.pin.top().bottom().left(to: bottomFirstItem.edge.right).width(currentWidth)
            bottomThirdItem.pin.top().bottom().left(to: bottomSecondItem.edge.right).width(currentWidth)
        } else if upperFirstItem.isHidden == true && upperSecondItem.isHidden == false && upperThirdItem.isHidden == false {
            upperSecondItem.pin.top().left(20).bottom().width(upperItemWidth)
            upperThirdItem.pin.top().bottom().left(to: upperSecondItem.edge.right).width(upperItemWidth).marginLeft(10)
            bottomSecondItem.pin.top().left().bottom().width(currentWidth)
            bottomThirdItem.pin.top().bottom().left(to: bottomSecondItem.edge.right).width(currentWidth)
            
        } else {
            upperThirdItem.pin.top().left(20).bottom().width(upperItemWidth)
            bottomThirdItem.pin.top().left().bottom().width(currentWidth)
        }
        
        isRecomendedFirst.pin.topLeft(to: bottomFirstItem.anchor.topLeft).topRight(to: bottomFirstItem.anchor.topRight).marginTop(30).height(30).marginHorizontal(20)
        pageControll.pin.below(of: upperScroll).left().right().height(10).marginTop(10)
        
        isRecomendedSecond.pin.topLeft(to: bottomSecondItem.anchor.topLeft).topRight(to: bottomSecondItem.anchor.topRight).marginTop(30).height(30).marginHorizontal(20)
        isSimpleSecond.pin.topLeft(to: isRecomendedSecond.anchor.bottomLeft).topRight(to: isRecomendedSecond.anchor.bottomRight).marginTop(10).height(30)
        
        isRecomendedThird.pin.topLeft(to: bottomThirdItem.anchor.topLeft).topRight(to: bottomThirdItem.anchor.topRight).marginTop(30).height(30).marginHorizontal(20)
        isSimpleThird.pin.topLeft(to: isRecomendedThird.anchor.bottomLeft).topRight(to: isRecomendedThird.anchor.bottomRight).marginTop(10).height(30)
        thirdSeparator.pin.topRight(to: isSimpleThird.anchor.bottomRight).topLeft(to: isSimpleThird.anchor.bottomLeft).marginTop(10).height(1)
        predefinedValueField.pin.topRight(to: isSimpleThird.anchor.bottomRight).height(50).width(100).marginTop(20)
        predefinedValueButton.pin.left(to: isSimpleThird.edge.left).height(20).vCenter(to: predefinedValueField.edge.vCenter).right(to: predefinedValueField.edge.left)
        
        submitButton.pin.left().bottom(64).right().height(64)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentWidth = UIScreen.main.bounds.size.width
        
        let point = scrollView.contentOffset
        
        let upperItemWidth = currentWidth - 40
        let coef = (upperItemWidth + 10) / currentWidth
        upperScroll.setContentOffset(CGPoint(x: point.x * coef, y: point.y), animated: false)
        bottomScroll.setContentOffset(CGPoint(x: point.x, y: point.y), animated: false)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.x
        
        self.pageControll.currentPage = Int(position / UIScreen.main.bounds.size.width)
        print("\(position)")
    }
}
