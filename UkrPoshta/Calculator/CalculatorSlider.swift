//
//  CalculatorSlider.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/8/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

enum FilterMode: Float {
    case a = 0.0
    case b = 5.0
    case c = 15.0
    case d = 30.0
}

class CalculatorSlider: UISlider {

    private var thumbTouchSize = CGSize(width: 40, height: 40)

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.minimumTrackTintColor = UPColor.headerDarkYellow
        self.maximumTrackTintColor = UPColor.lightGray
        
    }

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let increasedBounds = bounds.insetBy(dx: -thumbTouchSize.width, dy: -thumbTouchSize.height)
        let containsPoint = increasedBounds.contains(point)
        return containsPoint
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let percentage = CGFloat((value - minimumValue) / (maximumValue - minimumValue))
        let thumbSizeHeight = thumbRect(forBounds: bounds, trackRect:trackRect(forBounds: bounds), value:0).size.height
        let thumbPosition = thumbSizeHeight + (percentage * (bounds.size.width - (2 * thumbSizeHeight)))
        let touchLocation = touch.location(in: self)
        return touchLocation.x <= (thumbPosition + thumbTouchSize.width) && touchLocation.x >= (thumbPosition - thumbTouchSize.width)
    }

}
