//
//  CalculatorDomesticResultView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/10/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CalculatorDomesticResultView: UIView {

    let header = UILabel()
    
    let expressButton = DeliveryTypeButton()
    let smartBoxButton = DeliveryTypeButton()
    let standartButton = DeliveryTypeButton()

    let predefinedValueButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let predefinedValueField = UPTextField()
    
    let courierTakeButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    
    let courierDeliverButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    
    let smsButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let packagingButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    
    let submitButton = UPSubmitButton()

    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)

    var isShort = false
    var isLetterImage = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(header)
        header.textColor = UPColor.darkGray
        header.numberOfLines = 0
        header.lineBreakMode = .byWordWrapping
        header.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        
        expressButton.normalImage = UIImage(named: "parcelIcon")!
        expressButton.selectedImage =  UIImage(named: "parcelIconSelected")!
        expressButton.label.text = NSLocalizedString("calculator.type.express", comment: "")
        expressButton.isSelected = true
        self.addSubview(expressButton)
        
        if !isShort {
            smartBoxButton.normalImage = UIImage(named: "smartBox")!
            smartBoxButton.selectedImage =  UIImage(named: "smartBoxSelected")!
            smartBoxButton.label.text = NSLocalizedString("calculator.type.smartBox", comment: "")
            smartBoxButton.isSelected = false
            self.addSubview(smartBoxButton)
        }
        

        standartButton.normalImage = UIImage(named: "parcelIcon")!
        standartButton.selectedImage =  UIImage(named: "parcelIconSelected")!
        standartButton.label.text = NSLocalizedString("calculator.type.standart", comment: "")
        standartButton.isSelected = false
        self.addSubview(standartButton)
        
        self.addSubview(predefinedValueButton)
        predefinedValueButton.backgroundColor = .clear
        predefinedValueButton.label.text = NSLocalizedString("calculator.predefinedValue.label", comment: "")
        predefinedValueButton.isSelected = false
        predefinedValueButton.label.textAlignment = .left
        
        
        self.addSubview(predefinedValueField)
        predefinedValueField.backgroundColor = .white
        predefinedValueField.textAlignment = .right
        predefinedValueField.textColor = UPColor.darkGray
        predefinedValueField.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        predefinedValueField.keyboardType = .numbersAndPunctuation
        predefinedValueField.autocorrectionType = .no
        
//        self.addSubview(courierTakeButton)
//        courierTakeButton.backgroundColor = .clear
//        courierTakeButton.label.text = NSLocalizedString("calculator.courierTake.label", comment: "")
//        courierTakeButton.isSelected = false
//        courierTakeButton.label.textAlignment = .left
//
//        self.addSubview(courierDeliverButton)
//        courierDeliverButton.backgroundColor = .clear
//        courierDeliverButton.label.text = NSLocalizedString("calculator.courierDeliver.label", comment: "")
//        courierDeliverButton.isSelected = false
//        courierDeliverButton.label.textAlignment = .left
        
        self.addSubview(smsButton)
        smsButton.backgroundColor = .clear
        smsButton.label.text = NSLocalizedString("calculator.sms.label", comment: "")
        smsButton.isSelected = false
        smsButton.label.textAlignment = .left
        
        self.addSubview(packagingButton)
        packagingButton.backgroundColor = .clear
        packagingButton.label.text = NSLocalizedString("calculator.packaging.label", comment: "")
        packagingButton.isSelected = false
        packagingButton.label.textAlignment = .left
        
        self.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        
        self.addSubview(submitButton)        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width

        header.pin.top(10).left(20).right(20).height(50)
        
        if isShort {
            let buttonWidth = (currentWidth - 50)/2
            expressButton.pin.below(of: header).marginTop(10).left(20).height(100).width(buttonWidth)
            standartButton.pin.vCenter(to: expressButton.edge.vCenter).right(20).height(100).width(buttonWidth)
        } else {
            let buttonWidth = (currentWidth - 60)/3
            expressButton.pin.below(of: header).marginTop(10).left(20).height(100).width(buttonWidth)
            smartBoxButton.pin.vCenter(to: expressButton.edge.vCenter).after(of: expressButton).before(of: standartButton).marginHorizontal(10).height(100).width(buttonWidth)
            standartButton.pin.vCenter(to: expressButton.edge.vCenter).right(20).height(100).width(buttonWidth)
        }
        
        predefinedValueField.pin.below(of: expressButton).marginTop(20).right(20).width(100).height(50)
        predefinedValueButton.pin.vCenter(to: predefinedValueField.edge.vCenter).left(10).height(30).right(to: predefinedValueField.edge.left)

     //   courierTakeButton.pin.below(of: predefinedValueField).marginTop(10).left(10).right(20).height(30)
        
//        courierDeliverButton.pin.below(of: courierTakeButton).marginTop(10).left(10).height(30).right(20)
        
        smsButton.pin.below(of: predefinedValueButton).marginTop(10).left(10).height(30).right(20)
        
        packagingButton.pin.below(of: smsButton).marginTop(10).left(10).height(30).right(20)

        activityIndicator.pin.center()
        
        submitButton.pin.left().bottom(64).right(0).height(64)

    }

}
