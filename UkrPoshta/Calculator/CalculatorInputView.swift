//
//  CalculatorInputView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/7/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CalculatorInputView: UIScrollView {

    let viewA = UIView()
    let viewB = UIView()
    
    let fromButtonHeader = UPHeader()
    let fromButton = AddressSubmitButton()
    let swapButton = UIButton()
    let toButtonHeader = UPHeader()
    let toButton = AddressSubmitButton()
    let domesticSubmitButton = UPSubmitButton()
    
    let countryHeader = UPHeader()
    let countryButton = UPTextButton()
    
    let typeHeader = UPHeader()
    
    let docsButton = UPHorizontalButton(normalImageName: "letterIcon", selectedImageName: "letterIconSelected")
    let goodsButton = UPHorizontalButton(normalImageName: "parcelIcon", selectedImageName: "parcelIconSelected")
    
    let foreignSubmitButton = UPSubmitButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.isPagingEnabled = true
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(viewA)
        viewA.backgroundColor = .clear
        
        viewA.addSubview(fromButtonHeader)
        fromButtonHeader.text = NSLocalizedString("calculator.domestic.from.header", comment: "")
        
        viewA.addSubview(fromButton)
        fromButton.backgroundColor = .white
        fromButton.setTitle(NSLocalizedString("calculator.domestic.button.title", comment: "Мiсто або индекс"), for: .normal)
        fromButton.setTitle("", for: .selected)
        fromButton.setTitleColor(UPColor.lightGray, for: .normal)
        fromButton.layer.borderColor = UIColor.clear.cgColor
        fromButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 150)
        fromButton.titleLabel!.font =  UIFont(name: "", size: 12)

        viewA.addSubview(swapButton)

        swapButton.setImage(UIImage(named: "swapIcon"), for: .normal)
        
        viewA.addSubview(toButton)
        toButton.backgroundColor = .white
        toButton.setTitle(NSLocalizedString("calculator.domestic.button.title", comment: ""), for: .normal)
        toButton.setTitle("", for: .selected)
        toButton.setTitleColor(UPColor.lightGray, for: .normal)
        toButton.layer.borderColor = UIColor.clear.cgColor
        toButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 150)
        toButton.titleLabel!.font =  UIFont(name: "", size: 12)

        viewA.addSubview(toButtonHeader)
        toButtonHeader.text = NSLocalizedString("calculator.domestic.to.header", comment: "")
        toButtonHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        toButtonHeader.textColor = UPColor.lightGray

        viewA.addSubview(domesticSubmitButton)
        
        self.addSubview(viewB)
        viewB.backgroundColor = .clear
        
        viewB.addSubview(countryHeader)
        countryHeader.text = NSLocalizedString("calculator.country.header", comment: "")
        
        viewB.addSubview(countryButton)
        countryButton.setTitle(NSLocalizedString("calculator.country.placeholder", comment: ""), for: .normal)
        countryButton.contentHorizontalAlignment = .left
        countryButton.titleLabel!.font =  UIFont(name: "", size: 12)

        
        viewB.addSubview(typeHeader)
        typeHeader.text = NSLocalizedString("calculator.type.header", comment: "")
        
        viewB.addSubview(docsButton)
        docsButton.label.text = NSLocalizedString("calculator.docsButton.label", comment: "")
        
        viewB.addSubview(goodsButton)
        goodsButton.label.text = NSLocalizedString("calculator.goodsButton.label", comment: "")
        
        viewB.addSubview(foreignSubmitButton)
        foreignSubmitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let currentWidth = UIScreen.main.bounds.size.width
        self.contentSize = CGSize(width: currentWidth * 2, height: self.frame.size.height)
        
        viewA.pin.left().top().bottom().width(currentWidth)
        fromButtonHeader.pin.top(20).left(20).right(20).height(15)
        fromButton.pin.below(of: fromButtonHeader).marginTop(10).left(20).right(20).height(60)
        
        swapButton.pin.below(of: fromButton).marginTop(10).height(25).width(25).right(20)
        
        toButtonHeader.pin.below(of: fromButton).marginTop(20).left(20).right(20).height(15)
        toButton.pin.below(of: toButtonHeader).marginTop(10).left(20).right(20).height(60)

        domesticSubmitButton.pin.left(0).bottom().right(0).height(64)
        viewB.pin.left(to: viewA.edge.right).top().bottom().right().width(currentWidth)
        countryHeader.pin.top(20).left(20).right(20).height(10)
        countryButton.pin.below(of: countryHeader).marginTop(10).left(20).right(20).height(60)
        typeHeader.pin.below(of: countryButton).marginTop(10).left(20).right(20).height(40)
        
        let viewBWidth = viewB.frame.size.width
        docsButton.pin.below(of: typeHeader).marginTop(10).left(20).height(50).width(viewBWidth/2 - 30)
        goodsButton.pin.below(of: typeHeader).marginTop(10).after(of: docsButton).marginLeft(20).right(20).height(50)
        
        foreignSubmitButton.pin.left().right().bottom().height(64)
    }

}
