//
//  ResultCostView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/13/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ResultCostView: UIView {

    var icon = UIImageView()
    let typeLabel = UILabel()
    let priceLabel = UILabel()
    let infoLabel = UILabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white        
        self.addSubview(icon)
        icon.contentMode = .scaleAspectFit
        
        self.addSubview(typeLabel)
        typeLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        typeLabel.textColor = UPColor.darkGray
        typeLabel.text = "shoto"
        
        self.addSubview(priceLabel)
        priceLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        priceLabel.textColor = UPColor.darkGray
        priceLabel.textAlignment = .right
        priceLabel.text = "123 uah"
        
        self.addSubview(infoLabel)
        infoLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        infoLabel.textColor = UPColor.lightGray
        infoLabel.text = "adsfasdf sdaf sad fdsg sdfgdsfg sdfgsdfg sdf dsf "
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        self.layer.cornerRadius = 3.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        icon.pin.centerLeft(to: self.anchor.centerLeft).marginLeft(20).height(25).width(25)
        typeLabel.pin.after(of: icon).marginLeft(10).top(20).height(25).width(100)
        priceLabel.pin.after(of: typeLabel).marginLeft(10).vCenter(to: typeLabel.edge.vCenter).height(25).right(20)
        infoLabel.pin.topLeft(to: typeLabel.anchor.bottomLeft).marginTop(10).height(20).right(20)
    }
    
}
