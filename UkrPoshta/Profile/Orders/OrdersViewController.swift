//
//  OrdersViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 6/1/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    private let mainView = OrdersView()
    private var orders = [OrderItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        orders = User.currentOrders()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        self.mainView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell")
        let currentOrder = orders[indexPath.row]
        cell?.textLabel?.text = currentOrder.code
        cell?.backgroundColor = .clear
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = orders[indexPath.row]
        self.getSticker(code: item.code, uuid: item.uuid)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
     func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        let copy = UITableViewRowAction(style: .normal, title: "Скопiювати") { action, index in
            UIPasteboard.general.string = "\(self.orders[editActionsForRowAt.row].code)"
        }
        copy.backgroundColor = .gray
        
        let delete = UITableViewRowAction(style: .normal, title: "Видалити") { action, index in
            print("delete button tapped")
            User.removeOrder(order: self.orders[editActionsForRowAt.row])
            self.orders.remove(at: editActionsForRowAt.row)
            tableView.reloadData()
        }
        delete.backgroundColor = .red

        return [delete, copy]
    }
    
    
    func fetchStatusesWith(barcode: String) {
        UPAPIManager.getTrackingStatuses(barcode: barcode) { (result) in
            switch result {
            case .success(let statuses):
                print(statuses)
                if statuses.count > 0 {
                    User.saveTrackingHistory(barcode: barcode)
                    self.navigateToTrackingList(list: statuses)
                } else {
                    self.showAlertWith(text: NSLocalizedString("error.tracking.noData", comment: ""))
                }
                
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
                
            }
        }
    }
    
    func navigateToTrackingList(list: [TrackingStatus]) {
        let vc = TrackingViewController()
        vc.statuses = list
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func getSticker(code: String, uuid: String) {
        let vc = StickerViewController()
        vc.orderCode = code
        vc.uuid = uuid
        self.navigationController?.pushViewController(vc, animated: true)

    }
}
