//
//  OrdersView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 6/1/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class OrdersView: UIView {
    
    let tableView = UITableView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        self.addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView(frame: .zero)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tableView.pin.top(20).bottom(74).left(20).right(20)
        
    }


}
