//
//  EditProfileViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/23/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class EditProfileViewController: UPViewController, UITextFieldDelegate {
    
    let mainView = EditProfileView()
    let user: User = User.currentUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("profile.edit.save", comment: ""), style: .plain, target: self, action: #selector(saveTapped))
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.nameTextField.delegate = self
        self.mainView.nameTextField.text = user.firstName
        
        self.mainView.lastNameTextField.delegate = self
        self.mainView.lastNameTextField.text = user.lastName
        
        self.mainView.middleNameTextField.delegate = self
        self.mainView.middleNameTextField.text = user.middleName
        
        self.mainView.phoneTextField.delegate = self
        self.mainView.phoneTextField.text = UPPhoneHelper.displayStringFrom(rawString: user.phoneNumber)
        
        self.mainView.emailTextField.delegate = self
        self.mainView.emailTextField.text = user.email
        
        
        
        self.mainView.isIndividualButton.addTarget(self, action: #selector(radioButtonPressed), for: .touchUpInside)
        self.mainView.isLegalButton.addTarget(self, action: #selector(radioButtonPressed), for: .touchUpInside)
        
        if user.individual! {
            self.mainView.isIndividualButton.isSelected = true
            self.mainView.isLegalButton.isSelected = false
        } else {
            self.mainView.isIndividualButton.isSelected = false
            self.mainView.isLegalButton.isSelected = true
        }

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        super.viewWillDisappear(animated)
    }
    
    
    
    
    @objc func radioButtonPressed() {
        self.mainView.isLegalButton.isSelected = !self.mainView.isLegalButton.isSelected
        self.mainView.isIndividualButton.isSelected = !self.mainView.isIndividualButton.isSelected
    }
    

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if user.uuid != "" {
            switch textField {
            case self.mainView.emailTextField :
                if !isValidEmail(testStr: self.mainView.emailTextField.text!)  {
                    self.mainView.emailTextField.textColor = .red
                }
                if self.mainView.emailTextField.textColor == .red  {
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                } else {
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                }
            default:
                break
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case self.mainView.emailTextField : self.mainView.emailTextField.textColor = UPColor.darkGray
        case self.mainView.phoneTextField:
            let oldString = textField.text
            let newString = oldString! + string
            if (newString.count <= 6 || newString.count > 19) {
                return false
            } else {
                var oldRawString: String = UPPhoneHelper.rawStringFrom(displayString: oldString!)!
                switch string {
                case "":
                    oldRawString.removeLast()
                case "0","1","2","3","4","5","6","7","8","9" :
                    oldRawString = oldRawString + string
                default :
                    return false
                }
                textField.text = UPPhoneHelper.displayStringFrom(rawString: oldRawString)
            }
            
            return false
        default:
            return true
        }
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    @objc func saveTapped() {
        
        self.mainView.activityIndicator.startAnimating()
        if user.uuid != "" {
            UPAPIManager.saveUser(user: user.uuid!, isIndividual: self.mainView.isIndividualButton.isSelected, firstName: self.mainView.nameTextField.text, lastName: self.mainView.lastNameTextField.text, middleName: self.mainView.middleNameTextField.text, phone: UPPhoneHelper.rawStringFrom(displayString: self.mainView.phoneTextField.text), email: self.mainView.emailTextField.text) { (result) in
                switch result {
                case .success(let user):
                    User.save(user: user)
                    self.navigationController?.popViewController(animated: true)
                case .partialSuccess(let string):
                    print(string)
                    self.mainView.activityIndicator.stopAnimating()
                case .failure(let error):
                    print(error.localizedDescription)
                    self.mainView.activityIndicator.stopAnimating()
                }
                self.mainView.activityIndicator.stopAnimating()
            }
        } else {
            var newUser = User()
            if mainView.lastNameTextField.text != "" {
                newUser.lastName = mainView.lastNameTextField.text
                newUser.name = newUser.lastName
            }
            if mainView.nameTextField.text != "" {
                newUser.firstName = mainView.nameTextField.text
                if newUser.name != "" {
                    newUser.name = newUser.name! + " " + newUser.firstName!
                } else {
                    newUser.name = newUser.firstName
                }
            }
            if mainView.middleNameTextField.text != "" {
                newUser.middleName = mainView.middleNameTextField.text
                if newUser.name != "" {
                    newUser.name = newUser.name! + " " + newUser.middleName!
                } else {
                    newUser.name = newUser.middleName
                }
            }
            if UPPhoneHelper.rawStringFrom(displayString: self.mainView.phoneTextField.text)?.count == 12 {
                newUser.phoneNumber = UPPhoneHelper.rawStringFrom(displayString: self.mainView.phoneTextField.text)
            }
            if isValidEmail(testStr: self.mainView.emailTextField.text!) {
                newUser.email = self.mainView.emailTextField.text
            }
            newUser.individual = self.mainView.isIndividualButton.isSelected
            newUser.type = self.mainView.isIndividualButton.isSelected ? APIUserType.individual.rawValue : APIUserType.privateEnterpreneur.rawValue
            User.save(user: newUser)
            self.navigationController?.popViewController(animated: true)
            self.mainView.activityIndicator.stopAnimating()

        }

    }
    
}

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}

