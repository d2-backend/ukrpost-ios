//
//  EditProfileView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/23/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class EditProfileView: UIView {
    
    let isIndividualButton = UPHorizontalButton(normalImageName:"radioButton" , selectedImageName: "radioButtonSelected")
    let isLegalButton = UPHorizontalButton(normalImageName:"radioButton" , selectedImageName: "radioButtonSelected")

    let separatorA = UIView()

    let nameHeader = UILabel()
    let nameTextField = UPTextField()
    
    let lastNameHeader = UILabel()
    let lastNameTextField = UPTextField()
    
    let middleNameHeader = UILabel()
    let middleNameTextField = UPTextField()
    
    let phoneHeader = UILabel()
    let phoneTextField = UPTextField()
    
    let emailHeader = UILabel()
    let emailTextField = UPTextField()
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(isIndividualButton)
        isIndividualButton.label.text = NSLocalizedString("profile.edit.individualLabel", comment: "")
        isIndividualButton.backgroundColor = .clear
        isIndividualButton.label.textAlignment = .left
        
        self.addSubview(isLegalButton)
        isLegalButton.label.text = NSLocalizedString("profile.edit.legalLabel", comment: "")
        isLegalButton.backgroundColor = .clear
        isLegalButton.label.textAlignment = .left

        self.addSubview(separatorA)
        separatorA.backgroundColor = UPColor.separatorGray
        
        self.addSubview(lastNameHeader)
        self.lastNameHeader.text = NSLocalizedString("profile.edit.lastNameHeader", comment: "")
        self.lastNameHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        self.lastNameHeader.textColor = UPColor.lightGray
        
        self.addSubview(lastNameTextField)
        
        self.addSubview(nameHeader)
        nameHeader.text = NSLocalizedString("profile.edit.nameHeader", comment: "")
        self.nameHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        self.nameHeader.textColor = UPColor.lightGray
        
        self.addSubview(nameTextField)
        
        self.addSubview(middleNameHeader)
        self.middleNameHeader.text = NSLocalizedString("profile.edit.middleNameHeader", comment: "")
        self.middleNameHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        self.middleNameHeader.textColor = UPColor.lightGray

        self.addSubview(middleNameTextField)

        self.addSubview(phoneHeader)
        self.phoneHeader.text = NSLocalizedString("profile.phoneCell.header", comment: "")
        self.phoneHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        self.phoneHeader.textColor = UPColor.lightGray
        
        self.addSubview(phoneTextField)
        
        self.addSubview(emailHeader)
        self.emailHeader.text = NSLocalizedString("profile.emailCell.header", comment: "")
        self.emailHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        self.emailHeader.textColor = UPColor.lightGray
        
        self.addSubview(emailTextField)

        self.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        isIndividualButton.pin.top(15).left(20).right(20).height(30)
        isLegalButton.pin.top(to: isIndividualButton.edge.bottom).marginTop(10).left(20).right(20).height(30)

        self.separatorA.pin.below(of: isLegalButton).marginTop(5).left(20).right(0).height(1)
        
        self.lastNameHeader.pin.below(of: separatorA).marginTop(10).left(20).right(0).height(20)
        self.lastNameTextField.pin.below(of: lastNameHeader).marginTop(5).left(30).right(30).height(50)

        self.nameHeader.pin.below(of: lastNameTextField).marginTop(7).left(20).right(0).height(20)
        self.nameTextField.pin.below(of: nameHeader).marginTop(5).left(30).right(30).height(50)
        
        self.middleNameHeader.pin.below(of: nameTextField).marginTop(7).left(20).right(0).height(20)
        self.middleNameTextField.pin.below(of: middleNameHeader).marginTop(5).left(30).right(30).height(50)
        
        self.phoneHeader.pin.below(of: middleNameTextField).marginTop(7).left(20).right(0).height(20)
        self.phoneTextField.pin.below(of: phoneHeader).marginTop(5).left(30).right(30).height(50)
        
        self.emailHeader.pin.below(of: phoneTextField).marginTop(7).left(20).right(0).height(20)
        self.emailTextField.pin.below(of: emailHeader).marginTop(5).left(30).right(30).height(50)
        
        self.activityIndicator.pin.center(to: self.anchor.center)

        
    }
    
}
