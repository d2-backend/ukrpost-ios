//
//  ProfileHeaderView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/17/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ProfileHeaderView: UIView {

    @IBOutlet weak var bigView: UIView!
    @IBOutlet weak var bigViewTitle: UILabel!
    @IBOutlet weak var bigViewMessage: UILabel!
    @IBOutlet weak var bigViewSeparator: UIView!
    @IBOutlet weak var bigViewTextField: UITextField!
    @IBOutlet weak var smallView: UIView!
    @IBOutlet weak var smallViewTitle: UILabel!
    @IBOutlet weak var smallViewMessage: UILabel!
    @IBOutlet weak var smallViewIcon: UIImageView!
    @IBOutlet var mainView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ProfileHeaderView", owner: self, options: nil)
        addSubview(mainView)
        mainView.frame = self.bounds
        mainView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        bigViewTitle.text = NSLocalizedString("profile.bigView.title", comment: "")
        bigViewTitle.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        bigViewTitle.textColor = UPColor.darkGray
        
        bigViewMessage.text = NSLocalizedString("profile.bigView.message", comment: "")
        bigViewMessage.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        bigViewMessage.lineBreakMode = .byWordWrapping
        bigViewMessage.numberOfLines = 0
        
        bigViewSeparator.backgroundColor = UPColor.whiteSmoke
        
        bigViewTextField.placeholder = NSLocalizedString("profile.bigView.textField.placeholder", comment: "")

        smallView.isHidden = true
        
        smallViewTitle.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        smallViewTitle.textColor = UPColor.darkGray
        smallViewTitle.text = NSLocalizedString("profile.bigView.title", comment: "")
        
        smallViewMessage.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        smallViewMessage.textColor = UPColor.darkGray
        
        smallViewIcon.image = UIImage(named: "UUIDIcon")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setHeaderBigGradient()
        setHeaderSmallGradient()
    }
    
    func setHeaderBigGradient(){
        let gradient = UPColor.yellowGradient(frame: CGRect(x: 0, y: 0, width: Int(self.frame.size.width), height: Int(headerBigHeightConstant)))
            self.bigView.layer.addSublayer(gradient)
    }
    func setHeaderSmallGradient(){
        let gradient = UPColor.yellowGradient(frame: CGRect(x: 0, y: 0, width: Int(self.frame.size.width), height: Int(headerSmallHeightConstant)))
            self.smallView.layer.addSublayer(gradient)
    }

}
