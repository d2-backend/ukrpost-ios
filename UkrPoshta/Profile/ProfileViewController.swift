//
//  ProfileViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/12/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

let headerBigHeightConstant: CGFloat = 190.0
let headerSmallHeightConstant: CGFloat = 85.0


class ProfileViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var headerView: ProfileHeaderView!
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
   
    
    var detailedAddresses:Array<Address> = [Address]()
    var currentUser: User?
    
    fileprivate var request: AnyObject?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UPColor.whiteSmoke
        self.title = "Профiль"

        headerView.bigViewTextField.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        activityIndicator.hidesWhenStopped = true
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.translatesAutoresizingMaskIntoConstraints = false

        let nibProfileCell = UINib.init(nibName: "ProfileCell", bundle: nil)
        self.tableView.register(nibProfileCell, forCellReuseIdentifier: "ProfileCell")
        
        let nibProfileAddressHeaderCell = UINib.init(nibName: "ProfileAddressHeaderCell", bundle: nil)
        self.tableView.register(nibProfileAddressHeaderCell, forCellReuseIdentifier: "ProfileAddressHeaderCell")
        
        let nibProfileAddressCell = UINib.init(nibName: "ProfileAddressCell", bundle: nil)
        self.tableView.register(nibProfileAddressCell, forCellReuseIdentifier: "ProfileAddressCell")
        
        self.tableView.register(ProfileSettingsCell.self, forCellReuseIdentifier: "ProfileSettingsCell")
     
        let rightBarButton = UIBarButtonItem(title: NSLocalizedString("profile.barButton.exit", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ProfileViewController.rightBarButtonItemTapped(_:)))
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc func rightBarButtonItemTapped(_ sender:UIBarButtonItem!)
    {
            User.removeUser()
        self.detailedAddresses.removeAll()
        self.currentUser = User.currentUser()
        self.redrawHeader()
        tableView.reloadData()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let user: User = User.currentUser()
        if user.uuid != "" {
            self.fetchToken(bearerUUID: user.uuid!)
        } else {
            self.currentUser = user
        }
        
        showHeader()
        self.tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
 
    func checkForAddresses() {
        self.detailedAddresses.removeAll()
        for addressCode in (self.currentUser?.addresses)! {
            
            UPAPIManager.getAddress(id: addressCode.addressId!) { (result) in
                switch result {
                case .success(let address):
                    self.detailedAddresses.append(address)
                    if self.currentUser?.addresses.count == self.detailedAddresses.count {
                        self.tableView.reloadSections(IndexSet(integer: 3), with: .automatic)
                        self.tableView.reloadData()
                    }
                case .partialSuccess(let string) :
                    print(string)
                    self.showAlertWith(text: string)
                case .failure(let error):
                    self.showAlertWith(text: error.localizedDescription)
                }
            }
        }
    }
    
    func showHeader() {
        if self.currentUser?.uuid != "" {
            self.headerView.bigView.isHidden = true
            self.headerView.smallView.isHidden = false
            self.headerHeight.constant = headerSmallHeightConstant
            
            self.headerView.smallViewMessage.text = self.currentUser?.uuid
        } else {
            self.headerView.bigView.isHidden = false
            self.headerView.smallView.isHidden = true
            self.headerHeight.constant = headerBigHeightConstant
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.fetchToken(bearerUUID: textField.text!)
        textField.resignFirstResponder()
        return true
    }
    
    
    
    func fetchToken(bearerUUID: String) {
        self.activityIndicator.startAnimating()

        UPAPIManager.getToken(bearerUUID: bearerUUID) { (result) in
            switch result {
            case .success(let user):
                User.save(user: user)
                self.currentUser = user
                self.redrawHeader()
//             self.tableView.reloadData()
                self.checkForAddresses()
                self.headerView.smallViewMessage.text = self.currentUser?.uuid
            case .partialSuccess(let string):
                self.showAlertWith(text: string)

            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
            self.activityIndicator.stopAnimating()
        }
    }
    

    @objc func redrawHeader() {
        if self.headerView.bigView.isHidden && self.currentUser?.uuid == "" {
            UIView.transition(from: headerView.smallView, to: headerView.bigView, duration: 0.5, options: [.transitionCrossDissolve, .showHideTransitionViews], completion: nil)
            UIView.animate(withDuration: 0.5) {
                self.headerHeight.constant = headerBigHeightConstant
                self.view.layoutIfNeeded()
            }
        } else if self.headerView.smallView.isHidden && self.currentUser?.uuid != "" {
            UIView.transition(from: headerView.bigView, to: headerView.smallView, duration: 0.5, options: [.transitionCrossDissolve, .showHideTransitionViews], completion: nil)

            UIView.animate(withDuration: 0.5) {
                self.headerHeight.constant = headerSmallHeightConstant
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            // name
            return 1
        case 1:
            // phone
            return 1
        case 2:
            // emails
            return 1
        case 3:
            // address
            return self.detailedAddresses.count + 1
        case 4:
            // other
            return 3
        default:
            return 0
        }
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 4 {
            return 60
        } else {
            return UITableViewAutomaticDimension
        }
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if self.currentUser?.name != "" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.headerLabel.text = NSLocalizedString("profile.nameCell.header", comment: "")
                cell.mainLabel.text = self.currentUser?.name
                cell.editLabel.text = NSLocalizedString("profile.nameCell.haveName.edit", comment: "")
                cell.backgroundColor = .clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.backgroundColor = UPColor.separatorGray
                cell.headerLabel.text = NSLocalizedString("profile.nameCell.header", comment: "")
                cell.mainLabel.text = NSLocalizedString("profile.nameCell.NoName.message", comment: "")

                return cell
            }
        } else if indexPath.section == 1 {
            if self.currentUser?.phoneNumber != "" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.headerLabel.text = NSLocalizedString("profile.phoneCell.header", comment: "")
                cell.mainLabel.text = UPPhoneHelper.displayStringFrom(rawString: self.currentUser?.phoneNumber) 
                cell.editLabel.text = NSLocalizedString("profile.nameCell.haveName.edit", comment: "")
                cell.backgroundColor = .clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.backgroundColor = UPColor.separatorGray
                cell.headerLabel.text = NSLocalizedString("profile.phoneCell.header", comment: "")
                cell.mainLabel.text = NSLocalizedString("profile.nameCell.NoName.message", comment: "")
                return cell
            }
        } else if indexPath.section == 2 {
            if self.currentUser?.email != "" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.headerLabel.text = NSLocalizedString("profile.emailCell.header", comment: "")
                cell.mainLabel.text = self.currentUser?.email
                cell.editLabel.text = NSLocalizedString("profile.nameCell.haveName.edit", comment: "")
                cell.backgroundColor = .clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.backgroundColor = UPColor.separatorGray
                cell.headerLabel.text = NSLocalizedString("profile.emailCell.header", comment: "")
                cell.mainLabel.text = NSLocalizedString("profile.nameCell.NoName.message", comment: "")
                
                return cell
            }
        } else if indexPath.section == 3 {

            if self.currentUser?.addresses != nil {
                if indexPath.row == 0 {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAddressHeaderCell") as! ProfileAddressHeaderCell
                    return cell
                } else {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileAddressCell") as! ProfileAddressCell

                    let currentAddressCode = self.currentUser?.addresses[indexPath.row - 1]
                    
                    for someAddress in self.detailedAddresses {
                        if someAddress.id == currentAddressCode?.addressId {
                            cell.mainLabel.text = someAddress.detailedInfo
                            break
                        }
                    }
                
                    if (currentAddressCode?.main!)! {
                        cell.statusIcon.isHidden = false
                    } else {
                        cell.statusIcon.isHidden = true
                    }
                    
                    return cell
                }
                
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
                cell.headerLabel.text = NSLocalizedString("profile.addressCell.header", comment: "")
                cell.mainLabel.text = NSLocalizedString("profile.nameCell.NoName.message", comment: "")
                
                return cell
            }
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSettingsCell") as! ProfileSettingsCell

            if indexPath.row == 0 {
                cell.iconView.image = UIImage(named: "favoritesIcon")
                cell.iconView.image = cell.iconView.image?.withRenderingMode(.alwaysTemplate)
                cell.iconView.tintColor = UPColor.darkGray
                cell.mainLabel.text = NSLocalizedString("profile.favorites.title", comment: "")
                
            } else if indexPath.row == 1{
                cell.iconView.image = UIImage(named: "folderIcon")
                cell.mainLabel.text = NSLocalizedString("profile.orders.title", comment: "")
            } else {
                cell.iconView.image = UIImage(named: "settingsIcon")
                cell.mainLabel.text = NSLocalizedString("profile.settings.title", comment: "")
            }
            //cell.accessoryType = .disclosureIndicator
            return cell
            
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 4 {
            if indexPath.row == 0 {
                let vc = FavoritesViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else if indexPath.row == 1{
                let vc = OrdersViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = PreferencesViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            }

        }
        
        if self.currentUser?.uuid != "" && indexPath.section == 3 && indexPath.row == 0 {
            let vc = EditAddressViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        if self.currentUser?.uuid != "" && indexPath.section == 3 && indexPath.row != 0 {
            
            let currentAddressCodes = self.currentUser?.addresses[indexPath.row - 1]
            
            if !(currentAddressCodes?.main)! {
                // запрещаем редактировать ГЛАВНЫЙ адрес
                let vc = EditAddressViewController()
                
                for someAddress in self.detailedAddresses {
                    if someAddress.id == currentAddressCodes?.addressId {
                        vc.currentAddress = someAddress
                    }
                }
                vc.currentAddressCode = currentAddressCodes
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        if (indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2) {
            let vc = EditProfileViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let currentAddressShort = self.currentUser?.addresses[indexPath.row - 1]
            if !(currentAddressShort?.main)! {
                UPAPIManager.deleteAddress(addressUUID: currentAddressShort?.uuid) { (result) in
                    switch result {
                    case .success(_):
                        self.fetchToken(bearerUUID: (self.currentUser?.uuid)!)
                    case .partialSuccess(let string):
                        self.showAlertWith(text: string)
                    case .failure(let error):
                        self.showAlertWith(text: error.localizedDescription)
                    }
                    
                }
            }

            self.tableView.reloadData()
        }
    }

    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.currentUser?.uuid != "" && indexPath.section == 3 && indexPath.row != 0 {
            let currentAddressShort = self.currentUser?.addresses[indexPath.row - 1]
            if !(currentAddressShort?.main)! {
                return true
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return NSLocalizedString("profile.edit.delete", comment: "")
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}






