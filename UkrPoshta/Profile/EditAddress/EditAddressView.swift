//
//  EditAddressView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/4/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class EditAddressView: UIView {

    let indexField = UPTextField()
//    let switchLabel = UILabel()
//    let isMainSwitch = UISwitch()
    let isMainButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected", imgAlignment: .right)
    let regionField = UPTextField()
    let districtField = UPTextField()
    let cityField = UPTextField()
    let streetField = UPTextField()
    let houseNumberField = UPTextField()
    let apartmentField = UPTextField()
    let saveButton = UPSubmitButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        let anAttributes = [NSAttributedStringKey.foregroundColor: UPColor.lightGray, NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)]

        self.addSubview(indexField)
        indexField.placeholder = NSLocalizedString("indexToAddressHeader.title", comment: "")
        
        self.addSubview(isMainButton)
        isMainButton.label.text = NSLocalizedString("profile.address.isMain", comment: "")
        isMainButton.backgroundColor = .clear
        isMainButton.layer.masksToBounds = true
        
        self.addSubview(regionField)
        regionField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("addressToIndex.region.title", comment: ""), attributes: anAttributes)
        
        self.addSubview(districtField)
        districtField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("addressToIndex.district.title", comment: ""), attributes: anAttributes)
        
        self.addSubview(cityField)
        cityField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("addressToIndex.city.title", comment: ""), attributes: anAttributes)
        
        self.addSubview(streetField)
        streetField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("addressToIndex.street.title", comment: ""), attributes: anAttributes)
        
        self.addSubview(houseNumberField)
        houseNumberField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("addressToIndex.building.title", comment: ""), attributes: anAttributes)

        self.addSubview(apartmentField)
        apartmentField.placeholder = NSLocalizedString("addressToIndex.apartment.title", comment: "")
        
        self.addSubview(saveButton)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = UIScreen.main.bounds.width
        
        indexField.pin.left(20).top(20).height(50).width(100)
        isMainButton.pin.left(to: indexField.edge.right).marginLeft(50).top(20).right(20).height(50)
        
        regionField.pin.left(20).below(of: indexField).marginTop(10).right(20).height(50)
        districtField.pin.left(20).below(of: regionField).marginTop(10).right(20).height(50)
        cityField.pin.left(20).below(of: districtField).marginTop(10).right(20).height(50)
        streetField.pin.left(20).below(of: cityField).marginTop(10).right(20).height(50)
        houseNumberField.pin.left(20).below(of: streetField).marginTop(10).height(50).width((width/2) - 30)
        apartmentField.pin.right(20).height(50).width((width/2) - 30).vCenter(to: houseNumberField.edge.vCenter)

        saveButton.pin.left().bottom(64).right().height(64)
        
    }

}
