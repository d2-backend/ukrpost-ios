//
//  EditAddressViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/4/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class EditAddressViewController: UIViewController, UITextFieldDelegate {

    var currentAddress: Address?
    var currentAddressCode: AddressCode?
   
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    private let mainView = EditAddressView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Налаштування"
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.saveButton.setTitle(NSLocalizedString("profile.edit.save", comment: ""), for: .normal)
        self.mainView.saveButton.addTarget(self, action: #selector(saveButtonPressed), for: .touchUpInside)
        self.mainView.isMainButton.addTarget(self, action:#selector(isMainPressed), for: .touchUpInside)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.mainView.indexField.text = currentAddress?.postcode
        self.mainView.indexField.delegate = self
        self.mainView.regionField.text = currentAddress?.region
        self.mainView.regionField.delegate = self
        self.mainView.districtField.text = currentAddress?.district
        self.mainView.districtField.delegate = self
        self.mainView.cityField.text = currentAddress?.city
        self.mainView.cityField.delegate = self
        self.mainView.streetField.text = currentAddress?.street
        self.mainView.streetField.delegate = self
        self.mainView.houseNumberField.text = currentAddress?.houseNumber
        self.mainView.houseNumberField.delegate = self
        self.mainView.apartmentField.text = currentAddress?.apartmentNumber
        self.mainView.apartmentField.delegate = self


        if currentAddressCode != nil {
            if currentAddressCode!.main! {
                self.mainView.isMainButton.isSelected = true
                self.mainView.isMainButton.isEnabled = false
            } else {
                self.mainView.isMainButton.isSelected = false
            }
            
        }
    }

    
    @objc func isMainPressed() {
        self.mainView.isMainButton.isSelected = !self.mainView.isMainButton.isSelected
    }
    
    @objc func saveButtonPressed() {
        if self.currentAddressCode != nil {
            self.deleteAddress(addressUUID: (self.currentAddressCode?.uuid)!)
        } else {
            self.createAddress()
        }
    }

    func deleteAddress(addressUUID: String) {
        UPAPIManager.deleteAddress(addressUUID: addressUUID) { (result) in
            switch result {
            case .success(_):
                self.createAddress()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
            
        }
    }
    
    func createAddress() {
        UPAPIManager.createAddress(postCode: self.mainView.indexField.text, region: self.mainView.regionField.text, district: self.mainView.districtField.text, city:  self.mainView.cityField.text, street: self.mainView.streetField.text, houseNumber: self.mainView.houseNumberField.text, apartmentNumber: self.mainView.apartmentField.text) { (result) in
            switch result {
            case .success(let data):
                let newAddress = data
                let user: User = User.currentUser()
                UPAPIManager.addAddress(addressID: String(newAddress.id!), isMain: self.mainView.isMainButton.isSelected, UUID: user.uuid!, completion: { (result) in
                    switch result {
                    case .success(_):

                        self.navigationController?.popViewController(animated: true)
                    case .partialSuccess(let string):
                        self.showAlertWith(text: string)
                    case .failure(let error):
                        self.showAlertWith(text: error.localizedDescription)
                    }
                })
                
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }

    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if self.view.frame.origin.y != 64 {
            UIView.animate(withDuration: 0.25) {
                self.view.frame = CGRect(x:self.view.frame.origin.x , y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}
