//
//  FAQTableViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 10.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private let faqView = FAQView()
    
        private struct cellData {
        var opened = Bool()
        var title = String()
        var sectionData = [String]()
    }
    private var tableViewData = [cellData]()
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        self.title = "FAQ"
        hideBackNavigationButtonText()
        super.viewDidLoad()
        self.faqView.frame = self.view.frame
        self.view.addSubview(self.faqView)
        self.faqView.FAQTableView.delegate = self
        self.faqView.FAQTableView.dataSource = self
tableViewData = [
cellData(opened: false, title: NSLocalizedString("preferences.Cell1Title", comment: ""), sectionData: [NSLocalizedString("preferences.Cell1SubTitle", comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell2Title", comment: ""), sectionData: [NSLocalizedString("preferences.Cell2SubTitle", comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell3Title", comment: ""), sectionData: [NSLocalizedString("preferences.Cell3SubTitle", comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell4Title", comment: ""), sectionData: [NSLocalizedString("preferences.Cell4SubTitle", comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell5Title", comment: ""), sectionData: [NSLocalizedString("preferences.Cell5SubTitle", comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell6Title", comment: ""), sectionData: [NSLocalizedString("preferences.Cell6SubTitle" , comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell7Title" , comment: ""), sectionData: [NSLocalizedString("preferences.Cell7SubTitle" , comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell8Title" , comment: ""), sectionData: [NSLocalizedString("preferences.Cell8SubTitle" , comment: "")]),
cellData(opened: false, title: NSLocalizedString("preferences.Cell9Title" , comment: ""), sectionData: [NSLocalizedString("preferences.Cell9SubTitle" , comment: "")]),
cellData(opened: false, title: "\n", sectionData: [""])]}

    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true {
            return tableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell") else {return UITableViewCell()}
            cell.textLabel?.text = tableViewData[indexPath.section].title
            cell.textLabel?.textColor = UPColor.darkGray
            cell.textLabel?.numberOfLines = 0
            var imageView : UIImageView
            imageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = UIImage(named:"chevron_down")
            cell.accessoryView = imageView
            if indexPath.section == 9 {
                imageView.image = UIImage(named:"")
                cell.accessoryView = imageView
            }
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FAQCell") else {return UITableViewCell()}
            cell.textLabel?.text = tableViewData[indexPath.section].sectionData[indexPath.row - 1]
             cell.textLabel?.textColor = UPColor.lightGray
             cell.textLabel?.numberOfLines = 0
            var imageView : UIImageView
            imageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = UIImage(named:"")
            cell.accessoryView = imageView
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableViewData[indexPath.section].opened == true {
            tableViewData[indexPath.section].opened = false
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .automatic)
        } else {
            tableViewData[indexPath.section].opened = true
            let section = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(section, with: .automatic)
        }
    }
}
