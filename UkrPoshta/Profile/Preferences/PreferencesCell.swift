//
//  PreferencesCell.swift
//  UkrPoshta
//
//  Created by Zhekon on 10.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class PreferencesCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
