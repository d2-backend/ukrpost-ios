//
//  PreferencesViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 10.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import MessageUI

class PreferencesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    private let preferencesView = PreferencesView()
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Налаштування"
        preferencesView.versionLabel.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        hideBackNavigationButtonText()
        self.view.addSubview(self.preferencesView)
        self.preferencesView.preferencesTableView.delegate = self
        self.preferencesView.preferencesTableView.dataSource = self
        self.preferencesView.preferencesTableView.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.preferencesView.frame = self.view.bounds
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PreferencesCell") as! PreferencesCell
        var imageView : UIImageView
        imageView  = UIImageView(frame:CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.image = UIImage(named:"chevron_right_Copy@4x")
        if indexPath.row == 0 {cell.textLabel?.text = "FAQ";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator}
        if indexPath.row == 1 {cell.textLabel?.text = "Зателефонувати";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator }
        if indexPath.row == 2 {cell.textLabel?.text = "Написати";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator }
        if indexPath.row == 3 {cell.textLabel?.text = "Політика конфіденційності";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator; }
       // if indexPath.row == 4 {cell.textLabel?.text = "Угода користувача";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator }
        if indexPath.row == 4 {cell.textLabel?.text = "Сайт Укрпошта"; cell.accessoryView = imageView}
        if indexPath.row == 5 {cell.textLabel?.text = "Facebook";cell.accessoryView = imageView}
        if indexPath.row == 6 {cell.textLabel?.text = "Instagram";cell.accessoryView = imageView }
        if indexPath.row == 7 {cell.textLabel?.text = "Про додаток";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator }
        if indexPath.row == 8 {cell.textLabel?.text = "Про нас";cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator }
      //   if indexPath.row == 10 {cell.textLabel?.text = "Вихiд"; }
        return cell
    }
    
    @available(iOS 10.0, *)
    func makeAPhoneCall()  {
        let url: NSURL = URL(string: "TEL://0800300545")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 { let vc = FAQViewController(); self.navigationController?.pushViewController(vc, animated: true)}
        
        if indexPath.row == 1 {if #available(iOS 10.0, *) {
            makeAPhoneCall()
        } else {
            print("version of IOS is not 10")
            }}
        
        if indexPath.row == 2 {let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients(["ukrposhta@ukrposhta.ua"])
            self.present(composeVC, animated: true, completion: nil)}
        
        if indexPath.row == 3 {let vc = DetailInfoViewController(); vc.titleVC = "Полiтика конфiденцiйностi"; self.navigationController?.pushViewController(vc, animated: true); vc.mainView.infoLabel.text = NSLocalizedString("preferences.politicalConfidantial", comment: "")  }
        
     //   if indexPath.row == 4 {let vc = DetailInfoViewController(); self.navigationController?.pushViewController(vc, animated: true); vc.mainView.infoLabel.text = NSLocalizedString("preferences.aboutCompany", comment: "") }
        
        if indexPath.row == 4 {let URL = "http://ukrposhta.ua/?utm_source=up_app&utm_medium=profile_button";let urlToBrowser = NSURL(string: URL)!; UIApplication.shared.openURL(urlToBrowser as URL);  }
        
        if indexPath.row == 5 { let URL = "https://www.facebook.com/ukrposhta"; let urlToBrowser = NSURL(string: URL)!; UIApplication.shared.openURL(urlToBrowser as URL);  }
        
        if indexPath.row == 6 {let URL = "https://www.instagram.com/ukrposhta/" ; let urlToBrowser = NSURL(string: URL)!; UIApplication.shared.openURL(urlToBrowser as URL); }
        
        if indexPath.row == 7 {let vc = DetailInfoViewController(); vc.titleVC = "Про додаток"; self.navigationController?.pushViewController(vc, animated: true); vc.mainView.infoLabel.text = NSLocalizedString("preferences.aboutApplication", comment: "") }
        
        if indexPath.row == 8 {let vc = DetailInfoViewController();vc.titleVC = "Про нас"; self.navigationController?.pushViewController(vc, animated: true); vc.mainView.infoLabel.text = NSLocalizedString("preferences.aboutUs", comment: "")  }
        if indexPath.row == 9 {
//          User.removeUser()
//          let vc = MainViewController()
//          self.navigationController?.pushViewController(vc, animated: true)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - функция возвращения на таблицу с экрана отправки имейла, если пользователь решил отменить отправку письма
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
