//
//  DetailInfoViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 11.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetailInfoViewController: UIViewController {

    let mainView = DetailInfoView()    
    var titleVC = ""
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        self.title = titleVC
        hideBackNavigationButtonText()
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
    }
}
