//
//  FAQView.swift
//  UkrPoshta
//
//  Created by Zhekon on 10.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class FAQView: UIView {

    
    let FAQTableView = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
     
        self.addSubview(FAQTableView)
        self.FAQTableView.register(FAQCell.self, forCellReuseIdentifier: "FAQCell")
        self.FAQTableView.backgroundColor = .clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        FAQTableView.pin.all()
    }
}
