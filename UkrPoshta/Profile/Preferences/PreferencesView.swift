//
//  PreferencesView.swift
//  UkrPoshta
//
//  Created by Zhekon on 10.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class PreferencesView: UIView {

    let preferencesTableView = UITableView()
    let versionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        self.addSubview(preferencesTableView)
        self.preferencesTableView.register(PreferencesCell.self, forCellReuseIdentifier: "PreferencesCell")
        self.preferencesTableView.backgroundColor = .clear
        
        versionLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        versionLabel.textColor = UPColor.lightGray
        self.addSubview(versionLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        preferencesTableView.pin.all()
        versionLabel.pin.left(10).bottom(10).height(10).width(60)
    }
    
    
}
