//
//  DetailInfoView.swift
//  UkrPoshta
//
//  Created by Zhekon on 11.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetailInfoView: UIView {

    let infoLabel = UILabel()
    let infoScrollView = UIScrollView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        infoLabel.numberOfLines = 0
        infoLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        infoLabel.textColor = UPColor.darkGray
        infoLabel.sizeToFit()
        addSubview(infoScrollView)
        infoScrollView.addSubview(infoLabel)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        let marginLeft:CGFloat = 20.0
        let marginRight: CGFloat = 10.0
        let marginTop: CGFloat = 20
        let navBarHeight: CGFloat = 64
        let width = UIScreen.main.bounds.width - marginRight - marginLeft
        let height = infoLabel.text?.height(constraintedWidth: CGFloat(width), font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote))
        infoScrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: height! + marginTop + marginTop + navBarHeight)
        infoScrollView.pin.all()
        infoLabel.pin.top(20).right(10).left(20).bottom(0).height(height!)
    }
}

extension String {
    func height(constraintedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.text = self
        label.font = font
        label.sizeToFit()
        return label.frame.height
    }
}
