//
//  ProfileSettingsCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/23/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ProfileSettingsCell: UITableViewCell {

    let mainLabel = UILabel()
    let separatorBottom = UIView()
    let iconView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(iconView)
        
        self.contentView.addSubview(mainLabel)
        self.mainLabel.textAlignment = .left
        
        self.contentView.addSubview(separatorBottom)
        self.separatorBottom.backgroundColor = UPColor.separatorGray
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.pin.height(60)
        self.iconView.pin.top(20).left(23).bottom(20).width(20).height(20)
        
        self.mainLabel.pin.height(20).vCenter(to: self.iconView.edge.vCenter).left(to: iconView.edge.right).margin(20).right(40)
        self.separatorBottom.pin.bottom(0).left(23).right(0).height(1)

        self.updateConstraints()
    }
}
