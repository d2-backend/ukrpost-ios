//
//  ProfileAddressCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/20/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ProfileAddressCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var statusIcon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.separatorView.backgroundColor = UPColor.separatorGray
        self.mainLabel.numberOfLines = 0
        self.mainLabel.lineBreakMode = .byWordWrapping
        self.mainLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
        self.mainLabel.textColor = UPColor.lightGray
        self.statusIcon.image = UIImage(named: "index")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
