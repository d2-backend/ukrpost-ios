//
//  ProfileAddressHeaderCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/20/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ProfileAddressHeaderCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        mainLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        mainLabel.textColor = UPColor.lightGray
        mainLabel.text = NSLocalizedString("profile.addressCell.header", comment: "")
        
        editLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        editLabel.textColor = UPColor.sunglow
        editLabel.text = NSLocalizedString("profile.nameCell.NoName.edit", comment: "")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
