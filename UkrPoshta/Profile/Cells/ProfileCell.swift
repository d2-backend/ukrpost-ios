//
//  ProfileCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/19/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .clear
        self.separatorView.backgroundColor = UPColor.separatorGray
        
        headerLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        headerLabel.textColor = UPColor.lightGray
        
        mainLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
        mainLabel.textColor = UPColor.darkGray

        editLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        editLabel.textColor = UPColor.sunglow
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
