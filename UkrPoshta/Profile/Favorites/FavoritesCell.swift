//
//  FavoritesCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/26/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class FavoritesCell: UITableViewCell {

    let barcodeLabel = UILabel()
    let statusLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        
        self.addSubview(barcodeLabel)

        barcodeLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        barcodeLabel.textColor = UPColor.darkGray
        
        self.addSubview(statusLabel)
        statusLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
        statusLabel.textColor = UPColor.lightGray
        statusLabel.textAlignment = .right
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        barcodeLabel.pin.left(20).top(10).height(20).right(20)
        statusLabel.pin.below(of: barcodeLabel).marginTop(10).right(20).height(10).left(20)
        
    }
}
