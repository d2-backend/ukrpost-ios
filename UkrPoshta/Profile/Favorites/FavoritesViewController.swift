//
//  FavoritesViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/26/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private let mainView = FavoritesView()
    private var favoritesList:[FavoriteItem] = []
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Обране"
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoritesList = User.currentFavorites()
        self.mainView.tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesCell") as! FavoritesCell
        if favoritesList[indexPath.row].name != nil && favoritesList[indexPath.row].name != "" {
            cell.barcodeLabel.text = favoritesList[indexPath.row].name
        } else {
            cell.barcodeLabel.text = favoritesList[indexPath.row].barcode
        }
        cell.statusLabel.text = favoritesList[indexPath.row].type
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = favoritesList[indexPath.row]
        self.fetchStatusesWith(barcode: item.barcode)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func fetchStatusesWith(barcode: String) {
        self.mainView.activityIndicator.startAnimating()
        UPAPIManager.getTrackingStatuses(barcode: barcode) { (result) in
            switch result {
            case .success(let statuses):
                print(statuses)
                if statuses.count > 0 {
                    User.saveTrackingHistory(barcode: barcode)
                    self.navigateToTrackingList(list: statuses)
                } else {
                    self.showAlertWith(text: NSLocalizedString("error.tracking.noData", comment: ""))
                }
                
                self.mainView.activityIndicator.stopAnimating()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
                self.mainView.activityIndicator.stopAnimating()
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
                self.mainView.activityIndicator.stopAnimating()
                
            }
        }
    }
    
    func navigateToTrackingList(list: [TrackingStatus]) {
        let vc = TrackingViewController()
        vc.statuses = list
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
