//
//  FavoritesView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/26/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class FavoritesView: UIView {

    let tableView = UITableView()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(tableView)
        self.tableView.register(FavoritesCell.self, forCellReuseIdentifier: "FavoritesCell")
        self.tableView.rowHeight = 60
        self.tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView(frame: .zero)

        self.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        tableView.pin.all()
        self.activityIndicator.pin.center(to: self.anchor.center)

    }

}
