//
//  MainViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 15.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import BarcodeScanner
class MainViewController: UIViewController, BarcodeScannerCodeDelegate {

    var isAnimationNeeded = true
    let mainView = MainView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isAnimationNeeded {
            showStartAnimation()
        }
        
        setImageInNavBar()
        let rightBarButton = UIBarButtonItem(image: UIImage(named: "profile"), style: .plain, target: self, action: #selector(navigateToProfile))
        self.navigationItem.rightBarButtonItem = rightBarButton
        self.navigationItem.title = ""
        
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        
        self.mainView.trackingContainer.trackingButton.addTarget(self, action: #selector(trackingButtonPressed), for: .touchUpInside)
        self.mainView.trackingContainer.cameraButton.addTarget(self, action: #selector(qrButtonPressed), for: .touchUpInside)

        self.mainView.stampButton.addTarget(self, action: #selector(stampPressed), for: .touchUpInside)
        self.mainView.calculatorButton.addTarget(self, action: #selector(calculatorPressed), for: .touchUpInside)
        self.mainView.checkoutButton.addTarget(self, action: #selector(parcelRegistrationPressed), for: .touchUpInside)
        self.mainView.moneyButton.addTarget(self, action: #selector(moneyTransferPressed), for: .touchUpInside)
        self.mainView.indexButton.addTarget(self, action: #selector(indexesCellPressed), for: .touchUpInside)
        self.mainView.mapButton.addTarget(self, action: #selector(departmentPressed), for: .touchUpInside)
}
      
      override  func viewWillAppear(_ animated: Bool) {
            //self.title = ""
            super.viewWillAppear(animated)
      }
      
    func setImageInNavBar(){

        let firsImageView = UIImageView(image: UIImage(named: "logo_with_text"))
        firsImageView.contentMode = .scaleAspectFit
        
        let resultView = UIView(frame: CGRect(x: 0, y: 0, width: 112, height: 30))
        firsImageView.frame = resultView.bounds
        resultView.addSubview(firsImageView)
        
        let resultItem = UIBarButtonItem(customView: resultView)
        navigationItem.leftBarButtonItem = resultItem

    }
    
    @objc func navigateToProfile() {
        let vc = ProfileViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func trackingButtonPressed() {
        let vc = TrackingRequestViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @objc func stampPressed() {
        let vc = StampViewController()
        self.navigationController?.pushViewController(vc, animated: true)
//        let vc = ForeignCalculatorViewController()
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func calculatorPressed() {
        let vc = CalculatorViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func parcelRegistrationPressed() {
        let vc = CheckoutViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func moneyTransferPressed() {
        let vc = MoneyTransferViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func indexesCellPressed() {
        let vc = IndexesViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func constructScannerController() -> BarcodeScannerViewController {
        let controller = BarcodeScannerViewController()
        controller.title = "Пошук по штрих коду"
        controller.codeDelegate = self
        controller.headerViewController.titleLabel.text = NSLocalizedString("MainViewController.ButtonQr", comment: "")
        //controller.headerViewController.view.isHidden = true
        controller.headerViewController.closeButton.setImage(UIImage(named: "backIcon"), for: .normal)
        controller.headerViewController.closeButton.imageView?.contentMode = .scaleAspectFit
        controller.headerViewController.closeButton.setTitle("", for: .normal)
        controller.headerViewController.closeButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        //controller.headerViewController.closeButton.
        controller.headerViewController.closeButton.backgroundColor = UPColor.green
        controller.messageViewController.messages.scanningText = NSLocalizedString("MainViewController.label", comment: "")
        controller.headerViewController.closeButton.tintColor = UPColor.darkGray
        controller.cameraViewController.barCodeFocusViewType = .twoDimensions
        return controller
        
    }
    
    @objc func qrButtonPressed() {
        let controller = self.constructScannerController()
        //present(controller, animated: true, completion: nil)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        navigationController?.popViewController(animated: true)
        self.fetchStatusesWith(barcode: code)
    }
    
    func fetchStatusesWith(barcode: String) {
        mainView.trackingContainer.activityIndicator.startAnimating()
        mainView.trackingContainer.cameraButton.isHidden = true

        // зачем то в штрихкоде закодировано 13значное число с добавлением 0 в конце. Вот этот 0 я надеюсь и убирается.
        var newString = barcode
        if newString.count == 14 && newString.last == "0" {
            newString.removeLast()
        }
        UPAPIManager.getTrackingStatuses(barcode: newString) { (result) in
            self.mainView.trackingContainer.activityIndicator.stopAnimating()
            self.mainView.trackingContainer.cameraButton.isHidden = false
            switch result {
            case .success(let statuses):
                print(statuses)
                if statuses.count > 0 {
                    User.saveTrackingHistory(barcode: newString)
                    self.navigateToTrackingList(list: statuses)
                } else {
                    self.showAlertWith(text: NSLocalizedString("error.tracking.noData", comment: ""))
                }
                
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
                
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
                
            }
        }
    }
    
    func navigateToTrackingList(list: [TrackingStatus]) {
        let vc = TrackingViewController()
        vc.statuses = list
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showStartAnimation()  {
        let startIcon = UIImageView()
        let startView = UIView()
        
        UIApplication.shared.keyWindow?.addSubview(startView)
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        startView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        startView.backgroundColor = UPColor.headerDarkYellow
        startIcon.frame.size = CGSize(width: 120, height: 160)
        startIcon.center = CGPoint(x: width/2, y: height/2)
        startIcon.image = UIImage(named: "logo_light_gray")
        startIcon.contentMode = .scaleAspectFit
        startView.addSubview(startIcon)
        
        UIView.animate(withDuration: 1.5, delay: 0, options: [ .preferredFramesPerSecond60], animations: {
            startIcon.transform = CGAffineTransform(scaleX: 0.35, y: 0.35)
            startIcon.layer.speed = 1.75
        }) { (bool) in
            UIView.animate(withDuration: 1.75, delay: 0, options: [ .preferredFramesPerSecond60], animations: {
                startIcon.transform = CGAffineTransform(scaleX: 10, y: 10)
                startIcon.layer.opacity = 0.0
                startView.backgroundColor = .clear
            }) { (bool) in
                startView.removeFromSuperview()
            }
        }
    }
    
    @objc func departmentPressed() {
        let storyboard = UIStoryboard(name: "Map", bundle:nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MapVC") as! UINavigationController
        let firstVC = controller.viewControllers[0] as! MapViewController
        self.navigationController?.pushViewController(firstVC, animated: true)
    }
}






