//
//  ParallaxPromo.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/10/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ParallaxPromo: UIView, UIScrollViewDelegate{
    
    var firstLevel: UIScrollView?
    var secondLevel: UIScrollView?
    let secondLevelCoeficient: CGFloat = 1.5
    var thirdLevel: UIScrollView?
    let thirdLevelCoeficient: CGFloat = 1.0
    var fourthLevel: UIScrollView?
    let fourthLevelCoeficient: CGFloat = 2.0
    var fifthLevel: UIScrollView?
    let fifthLevelCoeficient: CGFloat = 2.5

    var pageControll: UIPageControl?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.firstLevel = UIScrollView(frame: self.frame)
        self.secondLevel = UIScrollView(frame: self.frame)
        self.thirdLevel = UIScrollView(frame: self.frame)
        self.fourthLevel = UIScrollView(frame: self.frame)
        self.fifthLevel = UIScrollView(frame: self.frame)
        
        self.firstLevel?.delegate = self
        self.firstLevel?.showsVerticalScrollIndicator = false
        self.firstLevel?.showsHorizontalScrollIndicator = false
        
        let viewWidth = UIScreen.main.bounds.width
        let viewHeight = self.bounds.size.height
        
        let parallaxFirst = UIImageView(image: UIImage(named: "parallax_1_1"))
        parallaxFirst.frame = CGRect(x: 0.0, y: 0.0, width: viewWidth, height: viewHeight)
        firstLevel?.addSubview(parallaxFirst)
        
        let parallaxSecond = UIImageView(image: UIImage(named: "parallax_2_1"))
        parallaxSecond.frame = CGRect(x: viewWidth, y: 0.0, width: viewWidth, height: viewHeight)
        firstLevel?.addSubview(parallaxSecond)
        
        let parallaxThird = UIImageView(image: UIImage(named: "parallax_3_1"))
        parallaxThird.frame = CGRect(x: viewWidth * 2, y: 0, width: viewWidth, height: viewHeight)
        
        firstLevel?.addSubview(parallaxThird)
        
        let parallaxFourth = UIImageView(image: UIImage(named: "parallax_4_1"))
        parallaxFourth.frame = CGRect(x: viewWidth * 3, y: 0, width: viewWidth, height: viewHeight)
        firstLevel?.addSubview(parallaxFourth)
        
        firstLevel?.contentSize = CGSize(width:viewWidth * 4, height: viewHeight)
        firstLevel?.isPagingEnabled = true
        firstLevel?.bounces = false
        self.addSubview(firstLevel!)
        
        /// second layer items
        let secondLayerFirstImage = UIImageView(image: UIImage(named: "parallax_1_2"))
        secondLayerFirstImage.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        secondLevel?.addSubview(secondLayerFirstImage)
        
        let secondLayerSecondImage = UIImageView(image: UIImage(named: "parallax_2_2"))
        secondLayerSecondImage.frame = CGRect(x: viewWidth * secondLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        secondLevel?.addSubview(secondLayerSecondImage)
        
        let secondLayerThirdImage = UIImageView(image: UIImage(named: "parallax_3_2"))
        secondLayerThirdImage.frame = CGRect(x: viewWidth * 2 * secondLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        secondLevel?.addSubview(secondLayerThirdImage)
        
        let secondLayerFourthImage = UIImageView(image: UIImage(named: "parallax_4_2"))
        secondLayerFourthImage.frame = CGRect(x: viewWidth * 3 * secondLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        secondLevel?.addSubview(secondLayerFourthImage)
        
        secondLevel?.contentSize = CGSize(width: viewWidth * 4 * secondLevelCoeficient, height: viewHeight)
        secondLevel?.isUserInteractionEnabled = false
        self.addSubview(secondLevel!)
        
        /// third layer items
        
        let thirdLayerFirstImage = UIImageView(image: UIImage(named: "parallax_1_3"))
        thirdLayerFirstImage.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        thirdLevel?.addSubview(thirdLayerFirstImage)
        
        let thirdLayerSecondImage = UIImageView(image: UIImage(named: "parallax_2_3"))
        thirdLayerSecondImage.frame = CGRect(x: viewWidth * thirdLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        thirdLevel?.addSubview(thirdLayerSecondImage)
        
        let thirdLayerThirdImage = UIImageView(image: UIImage(named: "parallax_3_3"))
        thirdLayerThirdImage.frame = CGRect(x: viewWidth * 2 * thirdLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        thirdLevel?.addSubview(thirdLayerThirdImage)
        
        let thirdLayerFourthImage = UIImageView(image: UIImage(named: "parallax_4_3"))
        thirdLayerFourthImage.frame = CGRect(x: viewWidth * 3 * thirdLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        thirdLevel?.addSubview(thirdLayerFourthImage)
        
        thirdLevel?.contentSize = CGSize(width: viewWidth * 4 * thirdLevelCoeficient, height: viewHeight)
        thirdLevel?.isUserInteractionEnabled = false
        self.addSubview(thirdLevel!)
        
        /// fourth layer items
        
        let fourthLayerFirstImage = UIImageView(image: UIImage(named: "parallax_1_4"))
        fourthLayerFirstImage.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        fourthLevel?.addSubview(fourthLayerFirstImage)
        
        let fourthLayerSecondImage = UIImageView(image: UIImage(named: "parallax_2_4a"))
        fourthLayerSecondImage.frame = CGRect(x: viewWidth * fourthLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        fourthLevel?.addSubview(fourthLayerSecondImage)
        
        let fourthLayerThirdImage = UIImageView(image: UIImage(named: "parallax_3_4"))
        fourthLayerThirdImage.frame = CGRect(x: viewWidth * 2 * fourthLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        fourthLevel?.addSubview(fourthLayerThirdImage)
        
        let fourthLayerFourthImage = UIImageView(image: UIImage(named: "parallax_4_4"))
        fourthLayerFourthImage.frame = CGRect(x: viewWidth * 3 * fourthLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        fourthLevel?.addSubview(fourthLayerFourthImage)
        
        fourthLevel?.contentSize = CGSize(width: viewWidth * 4 * fourthLevelCoeficient, height: viewHeight)
        fourthLevel?.isUserInteractionEnabled = false
        self.addSubview(fourthLevel!)
        
        /// fifth layer items
        
        let fifthLayerFirstImage = UIImageView(image: UIImage(named: "parallax_1_5"))
        fifthLayerFirstImage.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        fifthLevel?.addSubview(fifthLayerFirstImage)
        
        let fifthLayerSecondImage = UIImageView(image: UIImage(named: "parallax_2_8"))
        fifthLayerSecondImage.frame = CGRect(x: viewWidth * fifthLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        fifthLevel?.addSubview(fifthLayerSecondImage)
        
        let fifthLayerThirdImage = UIImageView(image: UIImage(named: "parallax_3_5"))
        fifthLayerThirdImage.frame = CGRect(x: viewWidth * 2 * fifthLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        fifthLevel?.addSubview(fifthLayerThirdImage)
        
        let fifthLayerFourthImage = UIImageView(image: UIImage(named: "parallax_4_5"))
        fifthLayerFourthImage.frame = CGRect(x: viewWidth * 3 * fifthLevelCoeficient, y: 0, width: viewWidth, height: viewHeight)
        fifthLevel?.addSubview(fifthLayerFourthImage)
        
        fifthLevel?.contentSize = CGSize(width: viewWidth * 4 * fifthLevelCoeficient, height: viewHeight)
        fifthLevel?.isUserInteractionEnabled = false
        self.addSubview(fifthLevel!)
        
        
        pageControll = UIPageControl(frame: CGRect(x: 20, y: viewHeight - 65, width: 100, height: 20))
        pageControll?.numberOfPages = 4
        pageControll?.currentPage = 0
        pageControll?.pageIndicatorTintColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
        
        self.addSubview(pageControll!)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //let point = scrollView.contentOffset
        secondLevel?.setContentOffset(CGPoint(x: scrollView.contentOffset.x * secondLevelCoeficient, y: scrollView.contentOffset.y), animated: false)
        thirdLevel?.setContentOffset(CGPoint(x: scrollView.contentOffset.x * thirdLevelCoeficient, y: scrollView.contentOffset.y), animated: false)
        fourthLevel?.setContentOffset(CGPoint(x: scrollView.contentOffset.x * fourthLevelCoeficient, y: scrollView.contentOffset.y), animated: false)
        fifthLevel?.setContentOffset(CGPoint(x: scrollView.contentOffset.x * fifthLevelCoeficient, y: scrollView.contentOffset.y), animated: false)
        
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let point = scrollView.contentOffset.x
        pageControll?.currentPage = Int(point / UIScreen.main.bounds.width)
    }

}
