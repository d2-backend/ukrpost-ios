//
//  MainView.swift
//  UkrPoshta
//
//  Created by Zhekon on 15.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class MainView: UIView {
    
    let calculatorButton  = ItemButton()
    let checkoutButton  = ItemButton()
    let mapButton  = ItemButton()
    let moneyButton = ItemButton()
    let indexButton  = ItemButton()
    let stampButton  = ItemButton()
    
    let parallaxPromo = ParallaxPromo()
    let trackingContainer = TrackingContainer()
    
    private var firstButton: ItemButton?
    private var secondButton: ItemButton?
    private var thirdButton: ItemButton?
    private var fourthButton: ItemButton?
    private var fifthButton: ItemButton?
    private var sixthButton: ItemButton?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        self.calculatorButton.generalLabel.text = NSLocalizedString("generalLabelOne", comment: "")
        self.calculatorButton.generalImage.image = UIImage(named: "collectionCalculator")
        self.checkoutButton.generalLabel.text = NSLocalizedString("generalLabelTwo", comment: "")
        self.checkoutButton.generalImage.image = UIImage(named: "parcelRegistration")
        self.mapButton.generalLabel.text = NSLocalizedString("generalLabelThree", comment: "")
        self.mapButton.generalImage.image = UIImage(named: "department")
        self.moneyButton.generalLabel.text = NSLocalizedString("generalLabelFour", comment: "")
        self.moneyButton.generalImage.image = UIImage(named: "moneyTransfer")
        self.indexButton.generalLabel.text = NSLocalizedString("generalLabelFive", comment: "")
        self.indexButton.generalImage.image = UIImage(named: "index")
        self.stampButton.generalLabel.text = NSLocalizedString("generalLabelSix", comment: "")
        self.stampButton.generalImage.image = UIImage(named: "stamp")
        
        firstButton = calculatorButton
        secondButton = checkoutButton
        thirdButton = mapButton
        fourthButton = moneyButton
        fifthButton = indexButton
        sixthButton = stampButton
        
        self.addSubview(parallaxPromo)
        self.addSubview(trackingContainer)
        self.addSubview(firstButton!)
        self.addSubview(secondButton!)
        self.addSubview(thirdButton!)
        self.addSubview(fourthButton!)
        self.addSubview(fifthButton!)
        self.addSubview(sixthButton!)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let size = UIScreen.main.bounds.size
        let itemWidth = (size.width - 50) / 3
        let parHeight = (size.width / 36) * 25
        let itemHeight = (size.height - parHeight - 30 - 35 - 64) / 2

        parallaxPromo.pin.top().left().right().height(parHeight)
        trackingContainer.pin.top(to: parallaxPromo.edge.bottom).left(20).right(20).height(60).marginTop(-30)
        
        firstButton!.pin.left(20).top(to: trackingContainer.edge.bottom).marginTop(15).width(itemWidth).height(itemHeight)
        secondButton!.pin.left(to: firstButton!.edge.right).top(to: trackingContainer.edge.bottom).marginTop(15).width(itemWidth).height(itemHeight).marginLeft(5)
        thirdButton!.pin.left(to: secondButton!.edge.right).top(to: trackingContainer.edge.bottom).marginTop(15).width(itemWidth).height(itemHeight).marginLeft(5)
        
        fourthButton!.pin.left(20).top(to: firstButton!.edge.bottom).marginTop(5).width(itemWidth).height(itemHeight)
        fifthButton!.pin.left(to: fourthButton!.edge.right).top(to: secondButton!.edge.bottom).marginTop(5).width(itemWidth).height(itemHeight).marginLeft(5)
        sixthButton!.pin.left(to: fifthButton!.edge.right).top(to: thirdButton!.edge.bottom).marginTop(5).width(itemWidth).height(itemHeight).marginLeft(5)
    }
}

















