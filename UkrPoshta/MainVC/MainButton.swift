//
//  GeneralViewButton.swift
//  UkrPoshta
//
//  Created by Zhekon on 15.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ItemButton: UIButton {

      let generalLabel = UILabel()
      let invisibleView = UIView()
      var generalImage = UIImageView()

      override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
      }

      required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
      }

      private func commonInit() {
            super.layoutSubviews()
            self.backgroundColor = .white
            generalLabel.numberOfLines = 0
            generalLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
            generalLabel.textColor = UPColor.darkGray
            generalLabel.textAlignment = .center
            generalImage.contentMode = .scaleAspectFit

            self.layer.masksToBounds = false
            self.layer.shadowColor = UPColor.darkGray.cgColor
            self.layer.shadowOpacity = 0.2
            self.layer.shadowOffset = .zero
            self.layer.shadowRadius = 2
            self.layer.cornerRadius = 3
            self.addSubview(generalLabel)
            self.addSubview(invisibleView)
            invisibleView.addSubview(generalImage)
            invisibleView.isUserInteractionEnabled = false
            generalImage.isUserInteractionEnabled = false
      }

      override func layoutSubviews() {
            generalLabel.pin.left(3).right(3).bottom(3).height(self.frame.height * 0.6).minHeight(60)
            invisibleView.pin.left(3).right(2).top(3).bottom(to: generalLabel.edge.top)
            let margin = invisibleView.frame.height * 0.5
            generalImage.pin.top(to: invisibleView.edge.top).left().right().maxHeight(30).marginTop(margin).bottom()
      }
}








