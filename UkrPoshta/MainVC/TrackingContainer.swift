//
//  TrackingButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/24/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingContainer: UIView {

    let cameraButton = UIButton()
    let trackingButton = UIButton()
    private let titleLabel = UILabel()
    private let iconImage = UIImageView()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {

        self.backgroundColor = .white
        self.addSubview(trackingButton)
        
        trackingButton.addSubview(titleLabel)
        titleLabel.text = NSLocalizedString("main.trackingButton.placeholder", comment: "")
        titleLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        titleLabel.textColor = UPColor.lightGray
        
        trackingButton.addSubview(iconImage)
        iconImage.image = UIImage.init(named: "searchIcon")

        addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        
        self.addSubview(cameraButton)
        cameraButton.setImage(UIImage.init(named: "qrIcon"), for: .normal)
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        
        self.layer.cornerRadius = 3.0

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.cameraButton.pin.right().vCenter().height(40).width(40)
        self.trackingButton.pin.left().top().bottom().before(of: self.cameraButton)
        iconImage.pin.left(10).vCenter().height(20).width(20)
        titleLabel.pin.after(of: iconImage).vCenter().marginLeft(5).right().height(20)
        activityIndicator.pin.right().vCenter().height(40).width(40)
    }

}
