//
//  TrackingCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/25/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingCell: UITableViewCell {
    
    let statusLabel = UILabel()
    let addressUpperLabel = UILabel()
    let addressBottomLabel = UILabel()
    let dateLabel = UILabel()
    
    let upperLine = UIView()
    let statusDot = UIImageView()
    let bottomLine = UIView()
    let fakeBottomLine = UIView()
    let bottomDots = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = .clear
        
        self.addSubview(statusLabel)
        statusLabel.numberOfLines = 0
        statusLabel.lineBreakMode = .byWordWrapping
        statusLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        statusLabel.textColor = UPColor.darkGray
        
        self.addSubview(addressUpperLabel)
        addressUpperLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
        addressUpperLabel.textColor = UPColor.lightGray
        self.addSubview(addressBottomLabel)
        addressBottomLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
        addressBottomLabel.textColor = UPColor.lightGray
        
        self.addSubview(dateLabel)
        dateLabel.font = UIFont.preferredFont(forTextStyle: .caption2)
        dateLabel.textColor = UPColor.lightGray
        
        upperLine.backgroundColor = UPColor.headerDarkYellow
        self.addSubview(upperLine)
        self.addSubview(statusDot)
        bottomLine.backgroundColor = UPColor.headerDarkYellow
        self.addSubview(bottomLine)
        fakeBottomLine.backgroundColor = UPColor.headerDarkYellow
        self.addSubview(fakeBottomLine)
        bottomDots.image = UIImage(named: "TrackDots")
        bottomDots.contentMode = .scaleAspectFit
        self.addSubview(bottomDots)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.height
        statusDot.pin.top(10).left().width(14).height(14)
        upperLine.pin.top().bottom(to: statusDot.edge.top).hCenter(to: statusDot.edge.hCenter).width(2)
        bottomLine.pin.top(to: statusDot.edge.bottom).bottom().hCenter(to: statusDot.edge.hCenter).width(2)
        fakeBottomLine.pin.top(to: statusDot.edge.bottom).hCenter(to: statusDot.edge.hCenter).width(2).height(height * 0.2)
        bottomDots.pin.top(to: fakeBottomLine.edge.bottom).hCenter(to: fakeBottomLine.edge.hCenter).width(2).bottom()
        
        statusLabel.pin.top(10).left(25).right(10).marginBottom(10).sizeToFit(.width)
        addressUpperLabel.pin.below(of: statusLabel).marginTop(10).left(25).height(15).right(110)
        
        dateLabel.pin.vCenter(to: addressUpperLabel.edge.vCenter).maxWidth(100).height(15).right(10).sizeToFit(.width)
        addressBottomLabel.pin.below(of: addressUpperLabel).marginTop(10).right(10).left(25).height(15)        
    }
}
