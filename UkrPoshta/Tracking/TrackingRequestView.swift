//
//  TrackingRequestView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/25/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingRequestView: UIView {

    let textField = UITextField()
    let tableView = UITableView()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    let submitButton = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(textField)
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        imageView.image = UIImage.init(named: "searchIcon")
        imageView.contentMode = .center
        textField.leftViewMode = .always
        textField.leftView = imageView
        textField.clearButtonMode = .always
        textField.backgroundColor = .white
        textField.keyboardType = .namePhonePad
        textField.autocorrectionType = .no
        textField.contentVerticalAlignment = .center
        textField.placeholder = NSLocalizedString("main.trackingButton.placeholder", comment: "")

        textField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("main.trackingButton.placeholder", comment: ""), attributes: [NSAttributedStringKey.foregroundColor: UPColor.lightGray, NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)])
            
        self.addSubview(tableView)
        tableView.backgroundColor = .clear
        tableView.separatorColor = UPColor.lightGray
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "someCell")

        self.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        
        self.addSubview(submitButton)
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.textField.pin.top(20).left(20).right(20).height(60)
        self.tableView.pin.below(of: textField).left(20).right(20).bottom(128)
        self.activityIndicator.pin.center(to: self.anchor.center)
        self.submitButton.pin.left().bottom(64).right().height(64)
    }
}
