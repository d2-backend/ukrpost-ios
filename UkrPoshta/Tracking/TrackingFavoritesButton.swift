//
//  TrackingFavoritesButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/26/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingFavoritesButton: UIButton {

    private let buttonLabel = UILabel()
    private let buttonImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {

        buttonImageView.image = buttonImageView.image?.withRenderingMode(.alwaysTemplate)
        buttonImageView.tintColor = .white
        
        self.addSubview(buttonImageView)
        
        buttonLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        buttonLabel.textColor = .white
        self.addSubview(buttonLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        buttonImageView.pin.left(20).vCenter().height(20).width(20)
        buttonLabel.pin.after(of: buttonImageView).marginLeft(10).vCenter().height(20).right()
    }
    
    override var isSelected: Bool {
        
        didSet {
            if isSelected {
                buttonLabel.text = NSLocalizedString("tracking.addToFavoritesButton.selected.title", comment: "")
                buttonImageView.image = UIImage(named: "favoritesIconSelected")

            } else {
                buttonLabel.text = NSLocalizedString("tracking.addToFavoritesButton.notSelected.title", comment: "")
                buttonImageView.image = UIImage(named: "favoritesIcon")

            }
        }
    }
}
