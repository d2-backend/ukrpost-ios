//
//  TrackingView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/24/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingView: UIView {

      private let headerView = UIView()
      let barcodeLabel = UILabel()
      let deliveryTypeLabel = UILabel()
      let nameButton = UIButton()
      private let headerSeparator = UIView()
      let addToFavoritesButton = TrackingFavoritesButton()

      let tableView = UITableView()

      override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
      }

      required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
      }

      private func commonInit() {
            self.backgroundColor = UPColor.whiteSmoke

            //self.headerView.layer.masksToBounds = false
            self.addSubview(headerView)
            headerView.layer.masksToBounds = false
            headerView.layer.cornerRadius = 20

            barcodeLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            barcodeLabel.textColor = UPColor.darkGray
            headerView.addSubview(barcodeLabel)

            deliveryTypeLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
            deliveryTypeLabel.textColor = UPColor.darkGray
            headerView.addSubview(deliveryTypeLabel)
            headerView.addSubview(nameButton)
            nameButton.titleLabel?.textColor = .white

            headerSeparator.backgroundColor = .white
            headerView.addSubview(headerSeparator)

            headerView.addSubview(addToFavoritesButton)

            self.addSubview(tableView)
            tableView.backgroundColor = .clear

            tableView.register(TrackingCell.self, forCellReuseIdentifier: "TrackingCell")
            tableView.separatorColor = .clear
      }

      override func layoutSubviews() {
            super.layoutSubviews()

            self.headerView.pin.top(20).right(20).left(20).height(150)
            let gradient = UPColor.yellowGradient(frame: CGRect(x: 0, y: 0, width: headerView.frame.size.width, height: headerView.frame.size.height))
            let roundPath = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: headerView.frame.size.width, height: headerView.frame.size.height), cornerRadius: 10)
            let maskLayer = CAShapeLayer()
            maskLayer.path = roundPath.cgPath
            layer.mask = maskLayer
            gradient.mask = maskLayer
            self.headerView.layer.addSublayer(gradient)


            nameButton.pin.top(20).right(20).height(32).width(100)
            self.barcodeLabel.pin.top(26).left(20).height(20).right(to: nameButton.edge.left)
            self.deliveryTypeLabel.pin.below(of: self.barcodeLabel).marginTop(9).left(20).right().height(20)

            self.headerSeparator.pin.top(100).left().right().height(1)

            self.addToFavoritesButton.pin.below(of: self.headerSeparator).left().right().bottom()

            tableView.pin.below(of: headerView).marginTop(10).left(20).right(20).bottom(74)

      }

}
