//
//  TrackingRequestViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/25/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingRequestViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

      private let mainView = TrackingRequestView()
      var barcodesList = [String]()
      var suggestedBarcodesList = [String]()
      var orderNumber = String()

      override func viewDidLoad() {
            super.viewDidLoad()
            print(orderNumber)
            if orderNumber != "" {
                  self.fetchStatusesWith(barcode: orderNumber)
                  navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))

            } else {
                  print("номер не найдет")
            }
            self.title = "Пошук вiдправлення"
            self.mainView.frame = self.view.frame
            self.view.addSubview(self.mainView)
            NotificationCenter.default.removeObserver(self)

            barcodesList = User.currentTrackingHistory()
            suggestedBarcodesList = barcodesList

            self.mainView.tableView.dataSource = self
            self.mainView.tableView.delegate = self
            self.mainView.submitButton.setTitle(NSLocalizedString("addressToIndex.submit.title", comment: ""), for: .normal)
            self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
      }

      override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            barcodesList = User.currentTrackingHistory()
            suggestedBarcodesList = barcodesList
            self.mainView.tableView.reloadData()
            self.mainView.textField.delegate = self
      }

      override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            self.mainView.textField.becomeFirstResponder()
      }

      @objc func cancelTapped() {
            let vc = MainViewController()
            vc.isAnimationNeeded = false
            let nc = UINavigationController(rootViewController: vc)
            nc.navigationBar.shadowImage = UIImage()
            nc.navigationBar.isTranslucent = false
            nc.navigationBar.tintColor = UPColor.darkGray
            self.present(nc, animated: true, completion: nil)
      }

      func fetchStatusesWith(barcode: String) {
            self.mainView.activityIndicator.startAnimating()
            UPAPIManager.getTrackingStatuses(barcode: barcode) { (result) in
                  switch result {
                  case .success(let statuses):
                        if statuses.count > 0 {
                              User.saveTrackingHistory(barcode: barcode)
                              self.navigateToTrackingList(list: statuses)
                        } else {
                              self.showAlertWith(text: NSLocalizedString("error.tracking.noData", comment: ""))
                        }

                        self.mainView.activityIndicator.stopAnimating()
                  case .partialSuccess(let string):
                        self.showAlertWith(text: string)
                        self.mainView.activityIndicator.stopAnimating()
                  case .failure(let error):
                        self.showAlertWith(text: error.localizedDescription)
                        self.mainView.activityIndicator.stopAnimating()

                  }
            }
      }

      func showAlertWith(text: String) {
            let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Добре", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
      }

      @objc func submitPressed() {
            fetchStatusesWith(barcode: self.mainView.textField.text!)
      }

      func navigateToTrackingList(list: [TrackingStatus]) {
            let vc = TrackingViewController()
            vc.statuses = list
            self.navigationController?.pushViewController(vc, animated: true)
      }

      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            fetchStatusesWith(barcode: textField.text!)
            return false
      }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if (textField.text?.count)! <= 12 {
                  textField.text = (textField.text! as NSString).replacingCharacters(in: range, with: string.uppercased())
                  if (textField.text?.count)! == 13 {
                        self.fetchStatusesWith(barcode: textField.text!)
                  }
                  return false
            } else {
                  textField.maxLength = 13
                  return true
            }
      }

      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return suggestedBarcodesList.count
      }

      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "someCell", for: indexPath as IndexPath)
            cell.textLabel!.text = "\(suggestedBarcodesList[indexPath.row])"
            cell.textLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
            cell.textLabel?.textColor = UPColor.lightGray
            cell.backgroundColor = .clear
            return cell

      }

      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let barcode = suggestedBarcodesList[indexPath.row]
            self.mainView.textField.text = barcode
            User.saveTrackingHistory(barcode: barcode)
            self.fetchStatusesWith(barcode: barcode)
            tableView.deselectRow(at: indexPath, animated: true)
      }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
      // this method handles row deletion
      func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
            
            if editingStyle == .delete {
                User.removeBarcode(barcode: barcodesList[indexPath.row])
                suggestedBarcodesList.remove(at: indexPath.row)
                tableView.reloadData()
            }
      }
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
      @IBInspectable var maxLength: Int {
            get {
                  guard let l = __maxLengths[self] else {
                        return 150
                  }
                  return l
            }
            set {
                  __maxLengths[self] = newValue
                  addTarget(self, action: #selector(fix), for: .editingChanged)
            }
      }
      @objc func fix(textField: UITextField) {
            let t = textField.text
            textField.text = t?.safelyLimitedTo(length: maxLength)
      }
}

extension String
{
      func safelyLimitedTo(length n: Int) -> String {
            if (self.count <= n) {
                  return self
            }
            return String(Array(self).prefix(upTo: n))
      }
}


