//
//  TrackingViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/24/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class TrackingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    private let mainView = TrackingView()
    var statuses:[TrackingStatus] = []
    private var currentFavorite: FavoriteItem?
    private var currentOrder: OrderItem?
      var order: DomesticShipment?
    private var isShrinked: Bool?

      private func addSharingButton() {
            let barButtonView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 44, height: 44)))
            let barButton = UIButton(frame: .zero)
            barButtonView.addSubview(barButton)
            barButton.pin.all(.zero)
            let image = UIImage(named: "shareIcon")
            barButton.setImage(image, for: .normal)
            barButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            barButton.addTarget(self, action: #selector(shareText), for: .touchUpInside)
            barButtonView.backgroundColor = .clear
            barButton.backgroundColor = .clear

            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButtonView)
      }

      @objc private func shareText() {
            let numberPost = statuses.last?.barcode?.description
            let statusPost = statuses.last?.eventName?.description
            let activityVC = UIActivityViewController(activityItems: ["Посилка з трек-номером: " + "\(numberPost!)" + " має статус: " + "\(statusPost!)"], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            
            self.present(activityVC, animated: true , completion: nil)
      }
      
    override func viewDidLoad() {
        super.viewDidLoad()
       addSharingButton()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.addToFavoritesButton.addTarget(self, action: #selector(addToFavoritesButtonPressed), for: .touchUpInside)
        self.mainView.nameButton.addTarget(self, action: #selector(namePressed), for: .touchUpInside)
        self.mainView.nameButton.setTitle(NSLocalizedString("tracking.nameButton.title", comment: ""), for: .normal)
        self.mainView.nameButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
            
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        if statuses.count > 2  {
            isShrinked = true
        } else {
            isShrinked = false
        }
        
        redrawData()
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isShrinked! {
            return 2
        } else {
            return statuses.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackingCell") as! TrackingCell
        var currentStatus: TrackingStatus!
        if isShrinked! {
            if indexPath.row == 0 {
                currentStatus = statuses.first
            } else {
                currentStatus = statuses.last
            }
        } else {
            currentStatus = statuses[indexPath.row]
        }
        
        let lastRowIndex = tableView.numberOfRows(inSection: 0) - 1
        if indexPath.row == 0 {
            cell.upperLine.isHidden = true
            cell.statusDot.image = UIImage(named: "TrackFirst")
            if isShrinked! {
                cell.bottomLine.isHidden = true
                cell.fakeBottomLine.isHidden = false
                cell.bottomDots.isHidden = false
            } else {
                cell.bottomLine.isHidden = false
                cell.fakeBottomLine.isHidden = true
                cell.bottomDots.isHidden = true
            }
            
        } else if indexPath.row == lastRowIndex {
            cell.statusDot.image = UIImage(named: "TrackLast")
            cell.bottomLine.isHidden = true
            cell.upperLine.isHidden = false
            cell.fakeBottomLine.isHidden = true
            cell.bottomDots.isHidden = true
        } else {
            cell.bottomLine.isHidden = false
            cell.upperLine.isHidden = false
            cell.statusDot.image = UIImage(named: "TrackMid")
            cell.fakeBottomLine.isHidden = true
            cell.bottomDots.isHidden = true
        }
        
        cell.statusLabel.text = currentStatus.eventName
        
        var neededAddressString = String()
        if currentStatus.index != nil {
            neededAddressString = currentStatus.index! + " "
        }
        if currentStatus.country != nil {
            neededAddressString = neededAddressString + currentStatus.country!
        }
        
        cell.addressUpperLabel.text = neededAddressString
        cell.addressBottomLabel.text = currentStatus.name
        cell.dateLabel.text = currentStatus.dateToDisplay()
        cell.statusLabel.sizeToFit()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let currentStatus = statuses[indexPath.row]

        let text = currentStatus.eventName!
        let attributetString = NSAttributedString(string: text, attributes: [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)])
        let frameSetter = CTFramesetterCreateWithAttributedString(attributetString)
        let neededHeight = CTFramesetterSuggestFrameSizeWithConstraints(frameSetter, CFRange(location: 0,length: 0), nil, CGSize(width: Double(self.view.frame.size.width - 60) , height: .greatestFiniteMagnitude), nil)
        
        
        return neededHeight.height + 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        expandTableView()
    }
      
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if isShrinked! {
            
            let footerButton = UPHorizontalButton(normalImageName: "chevron_down", selectedImageName: "chevron_down", imgAlignment: .right)
            footerButton.label.text = NSLocalizedString("tracking.expand.title", comment: "")
            footerButton.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40)
            footerButton.addTarget(self, action: #selector(expandTableView), for: .touchUpInside)
            footerButton.layer.masksToBounds = true
            footerButton.backgroundColor = .clear
            footerButton.label.textColor = UPColor.darkGray
            return footerButton
        } else {
            return UIView()
        }
    }
    
    @objc func expandTableView() {
        isShrinked = false
        mainView.tableView.reloadData()
    }
    
    @objc func addToFavoritesButtonPressed() {
        if self.mainView.addToFavoritesButton.isSelected == true {
            self.mainView.addToFavoritesButton.isSelected = false
            User.removeFromFavorites(item: self.currentFavorite!)
        } else {
            self.mainView.addToFavoritesButton.isSelected = true
            let item = FavoriteItem(barcode: (statuses.first?.barcode)!, type: kDeliveryTypes[(statuses.first?.mailType)!]!, name:nil)
            User.addToFavorites(item: item)
            print((statuses.first?.barcode)!)
            UPAPIManager.sendOrder(with: (statuses.first?.barcode)!, completion: nil)
            }
    }

    @objc func namePressed() {
        let alertController = UIAlertController(title: NSLocalizedString("tracking.nameAlert.title", comment: ""), message: statuses.first?.barcode, preferredStyle: .alert)
        alertController.view.tintColor = UPColor.darkGray
        alertController.addTextField { textField in
            textField.becomeFirstResponder()
        }
   
      let cancelAction = UIAlertAction(title: "Вiдмiнити", style: .default, handler: nil)
      alertController.addAction(cancelAction)
      present(alertController, animated: true, completion: nil)
      
      let confirmAction = UIAlertAction(title: "Зберегти", style: .cancel) { [weak alertController] _ in
      guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
      self.saveName(name: textField.text!)
        }
        alertController.addAction(confirmAction)
      
    }
    
    func saveName(name: String) {
        if self.currentFavorite != nil {
            let newItem = FavoriteItem(barcode: self.currentFavorite!.barcode, type: self.currentFavorite!.type, name: name)
            User.removeFromFavorites(item: self.currentFavorite!)
            User.addToFavorites(item: newItem)
            redrawData()
        }
    }
    
    func redrawData() {
        self.mainView.barcodeLabel.text = statuses.first?.barcode
        self.mainView.deliveryTypeLabel.text = kDeliveryTypes[(statuses.first?.mailType)!]
        self.mainView.addToFavoritesButton.isSelected = false
        let favoritesList = User.currentFavorites()
        for item in favoritesList {
            if item.barcode == statuses.first?.barcode {
                self.currentFavorite = item
                self.mainView.addToFavoritesButton.isSelected = true
                if item.name != nil && item.name != "" {
                    self.mainView.barcodeLabel.text = item.name
                }
                break
            }
        }
        if currentFavorite == nil {
            let newFavorite = FavoriteItem(barcode: statuses.first!.barcode!, type: kDeliveryTypes[(statuses.first?.mailType)!] ?? "", name: nil)
            self.currentFavorite = newFavorite
        }
    }
}
