//
//  CheckoutOptionsViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase
class CheckoutOptionsViewController: UPViewController, UITextViewDelegate{

    private let mainView = CheckoutOptionsView()
    private var textViewPlaceholder = String()
    var order: DomesticShipment?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
      var isExspress = Bool()

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }

      override func viewDidLoad() {
        super.viewDidLoad()
            if isExspress {
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 6",
            AnalyticsParameterItemName: "ОВ_УКР_ЕК_6",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_6"
            ])
            }
            if !isExspress {
                  Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                        AnalyticsParameterItemID: "id 6",
                        AnalyticsParameterItemName: "ОВ_УКР_SmartBox_6",
                        AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_6"
                        ])
            }
        self.title = "Оформити вiдправлення"
        hideBackNavigationButtonText()
        mainView.paidResipmentButtonLeft.isSelected = true
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        self.mainView.returnTypeButton.addTarget(self, action: #selector(optionsPressed(sender:)), for: .touchUpInside)
        self.mainView.returnTypeButton.isSelected = true
        self.mainView.storeAndReturnTypeButton.addTarget(self, action: #selector(optionsPressed(sender:)), for: .touchUpInside)
        self.mainView.storeAndReturnTypeButton.isSelected = false
        self.mainView.abandonTypeButton.addTarget(self, action: #selector(optionsPressed(sender:)), for: .touchUpInside)
        self.mainView.abandonTypeButton.isSelected = false
        
        self.mainView.descriptionField.delegate = self
        self.mainView.descriptionField.setPlaceholder(text: NSLocalizedString("checkout.options.description.placeholder", comment: ""))
        self.textViewPlaceholder = NSLocalizedString("checkout.options.description.placeholder", comment: "")
        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
      self.mainView.paidResipmentButtonLeft.addTarget(self, action: #selector(paidPressed), for: .touchUpInside)
      self.mainView.paidResipmentButtonRight.addTarget(self, action: #selector(paidPressed), for: .touchUpInside)

    }
    
            @objc func paidPressed() {
                  mainView.paidResipmentButtonLeft.isSelected = !mainView.paidResipmentButtonLeft.isSelected
                  mainView.paidResipmentButtonRight.isSelected = !mainView.paidResipmentButtonRight.isSelected
             
            }
//
//            @objc func paidPressedRight() {
//                  mainView.paidResipmentButtonRight.isSelected = !mainView.paidResipmentButtonRight.isSelected
//                   mainView.paidResipmentButtonLeft.isSelected = false
//
//            }
      
    @objc func submitPressed() {
        let vc = CheckoutPriceViewController()

        if self.mainView.returnTypeButton.isSelected {
            self.order?.onFailReceiveType = APIReturnBehaviour.returnToSender.rawValue
        } else if self.mainView.storeAndReturnTypeButton.isSelected {
            self.order?.onFailReceiveType = APIReturnBehaviour.storeAndReturn.rawValue
        } else if self.mainView.abandonTypeButton.isSelected {
            self.order?.onFailReceiveType = APIReturnBehaviour.abandon.rawValue
        }
        
        self.order?.description = self.mainView.descriptionField.text == NSLocalizedString("checkout.options.description.placeholder", comment: "") ? nil : self.mainView.descriptionField.text
        vc.order = self.order
        vc.addressTo = self.addressTo
        vc.addressFrom = self.addressFrom
        vc.paidResipment = mainView.paidResipmentButtonRight.isSelected
        vc.isExspress = self.isExspress
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func optionsPressed(sender: UIButton) {
        self.mainView.returnTypeButton.isSelected = false
        self.mainView.storeAndReturnTypeButton.isSelected = false
        self.mainView.abandonTypeButton.isSelected = false
        sender.isSelected = true
    }

    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        UIView.animate(withDuration: 0.25) {
            self.mainView.frame = CGRect(x:0, y: 0, width: self.mainView.frame.size.width, height: self.mainView.frame.size.height)
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UPColor.lightGray {
            textView.text = nil
            textView.textColor = UPColor.darkGray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {        
        if textView.text.isEmpty {
            textView.text = self.textViewPlaceholder
            textView.textColor = UPColor.lightGray
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
}
