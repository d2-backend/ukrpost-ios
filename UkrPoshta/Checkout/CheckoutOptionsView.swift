//
//  CheckoutOptionsView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CheckoutOptionsView: UIView {
      
    let paidHeader = UPHeader()
    let paidResipmentButtonLeft = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let paidResipmentButtonRight = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")

    let returnHeader = UPHeader()
    
    let returnTypeButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let storeAndReturnTypeButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let abandonTypeButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")

    private let separator = UIView()
    
    private let descriptionHeader = UPHeader()
    
    let descriptionField = UPTextView()
    
    let submitButton = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        self.addSubview(paidHeader)
        paidHeader.text = "Cплачує плату за відправлення:"
      
        self.addSubview(paidResipmentButtonLeft)
            paidResipmentButtonLeft.backgroundColor = .clear
            paidResipmentButtonLeft.label.text = "Відправник"
            paidResipmentButtonLeft.label.textAlignment = .left
      
      self.addSubview(paidResipmentButtonRight)
      paidResipmentButtonRight.backgroundColor = .clear
      paidResipmentButtonRight.label.text = "Одержувач"
      paidResipmentButtonRight.label.textAlignment = .left
      
        self.addSubview(returnHeader)
        returnHeader.text = NSLocalizedString("checkout.options.return.header", comment: "")
        
        self.addSubview(returnTypeButton)
        returnTypeButton.label.text = NSLocalizedString("checkout.options.return.return", comment: "")
        returnTypeButton.backgroundColor = .clear
        returnTypeButton.label.textAlignment = .left
        
        self.addSubview(storeAndReturnTypeButton)
        storeAndReturnTypeButton.label.text = NSLocalizedString("checkout.options.return.storeAndReturn", comment: "")
        storeAndReturnTypeButton.backgroundColor = .clear
        storeAndReturnTypeButton.label.textAlignment = .left

        self.addSubview(abandonTypeButton)
        abandonTypeButton.label.text = NSLocalizedString("checkout.options.return.abandon", comment: "")
        abandonTypeButton.backgroundColor = .clear
        abandonTypeButton.label.textAlignment = .left

        self.addSubview(separator)
        separator.backgroundColor = UPColor.separatorGray
        
        self.addSubview(descriptionHeader)
        descriptionHeader.text = NSLocalizedString("checkout.options.description.header", comment: "")
        
        self.addSubview(descriptionField)
        
        self.addSubview(submitButton)
        submitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        paidHeader.pin.top(20).left(20).right(20).height(15)
       paidResipmentButtonLeft.pin.top(to: paidHeader.edge.bottom).marginTop(15).left(20).width(UIScreen.main.bounds.size.width / 2.25).height(50)
       paidResipmentButtonRight.pin.top(to: paidHeader.edge.bottom).marginTop(15).right(20).width(UIScreen.main.bounds.size.width / 2.25).height(50)

      returnHeader.pin.top(to:paidResipmentButtonLeft.edge.bottom).marginTop(10).left(20).right(20).height(15)
        
        returnTypeButton.pin.below(of: returnHeader).marginTop(30).right(20).left(20).height(40)
        
        
        storeAndReturnTypeButton.pin.right(20).below(of: returnTypeButton).marginTop(10).left(20).height(40)
    
        
        abandonTypeButton.pin.below(of: storeAndReturnTypeButton).marginTop(10).right(20).left(20).height(40)

        separator.pin.below(of: abandonTypeButton).marginTop(20).left(20).right(20).height(1)
        
        descriptionHeader.pin.below(of: separator).marginTop(20).left(20).right(20).height(15)
        descriptionField.pin.below(of: descriptionHeader).marginTop(10).right(20).left(20).height(50)
        
        submitButton.pin.left().bottom(64).right().height(64)
        
    }

}
