//
//  CheckoutInputView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/11/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CheckoutInputView: UIScrollView {

    let viewA = UIView()
    let domesticTypeHeader = UILabel()
    let domesticExpressButton = UPHorizontalButton(normalImageName: "parcelIcon", selectedImageName: "parcelIconSelected")
    let domesticSmartBoxButton = UPHorizontalButton(normalImageName: "smartBox", selectedImageName: "smartBoxSelected")
    
    let weightHeader = UPHeader()
    let weightField = UPTextField()
    let sizeHeader = UPHeader()
    let sizeField = UPTextField()
    
    let uuidHeader = UPHeader()
    let uuidField = UPTextField()
    let availableBoxesButton = UPTextButton()
    let newBoxHeader = UPHeader()
    let newBoxField = UPTextField()
    
    let domesticSubmitButton = UPSubmitButton()

    let viewB = UIView()
    
    let countryHeader = UPHeader()
    let countryButton = UPTextButton()

    let typeHeader = UPHeader()
    let parcelButton = UPVerticalButton(normalImageName: "parcelIcon", selectedImageName: "parcelIconSelected")
    let letterButton = UPVerticalButton(normalImageName: "letterIcon", selectedImageName: "letterIconSelected")
    let bagButton = UPVerticalButton(normalImageName: "bagIcon", selectedImageName: "bagIconSelected")
    
    private let sizeInternHeader = UPHeader()
    private let sizesInternBackground = UIView()
    private let firstSeparator = UIView()
    private let secondSeparator = UIView()
    
    let lengthInternField = UPTextField()
    let heightInternField = UPTextField()
    let widthInternField = UPTextField()
    
    private let weightInternLabel = UILabel()
    let weightInternField = UPTextField()
    
    let submitInternButton = UPSubmitButton()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.isPagingEnabled = true
        self.backgroundColor = UPColor.whiteSmoke
        self.isScrollEnabled = false
        self.addSubview(viewA)
        viewA.backgroundColor = .clear
        
        viewA.addSubview(domesticTypeHeader)
        domesticTypeHeader.text = NSLocalizedString("calculator.type.header", comment: "")
        domesticTypeHeader.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        domesticTypeHeader.textColor = UPColor.lightGray
        
        viewA.addSubview(domesticExpressButton)
        domesticExpressButton.label.text = NSLocalizedString("checkout.expressButton.title", comment: "")

        viewA.addSubview(domesticSmartBoxButton)
        domesticSmartBoxButton.label.text = NSLocalizedString("calculator.type.smartBox", comment: "")

        viewA.addSubview(weightHeader)
        weightHeader.text = NSLocalizedString("calculator.weight.header", comment: "")
        
        viewA.addSubview(weightField)
        weightField.textAlignment = .right
        weightField.keyboardType = .decimalPad
        weightField.placeholder = "0.0"
        
        viewA.addSubview(sizeHeader)
        sizeHeader.text = NSLocalizedString("calculator.size.header", comment: "")
        
        viewA.addSubview(sizeField)
        sizeField.textAlignment = .right
        sizeField.keyboardType = .decimalPad
        sizeField.placeholder = "0.0"

        viewA.addSubview(uuidHeader)
        uuidHeader.text = NSLocalizedString("profile.bigView.title", comment: "")
        
        viewA.addSubview(uuidField)
        uuidField.placeholder = NSLocalizedString("checkout.smartBox.code.placeholder", comment: "")
        
        viewA.addSubview(availableBoxesButton)
        availableBoxesButton.titleLabel?.textAlignment = .center
        
        viewA.addSubview(newBoxHeader)
        newBoxHeader.text = NSLocalizedString("checkout.smartBox.code.header", comment: "")
        
        viewA.addSubview(newBoxField)
        newBoxField.placeholder = NSLocalizedString("checkout.smartBox.add.placeholder", comment: "")
        
        viewA.addSubview(domesticSubmitButton)
        domesticSubmitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        
        self.addSubview(viewB)
        viewB.backgroundColor = .clear
        
        viewB.addSubview(countryHeader)
        countryHeader.text = NSLocalizedString("calculator.country.header", comment: "")
        
        viewB.addSubview(countryButton)
        countryButton.setTitle(NSLocalizedString("calculator.country.placeholder", comment: "Краïна"), for: .normal)
        
        viewB.addSubview(typeHeader)
        typeHeader.text = NSLocalizedString("calculator.type.header", comment: "")
        
        viewB.addSubview(parcelButton)
        parcelButton.label.text = NSLocalizedString("calculator.type.parcel", comment: "")
        
        viewB.addSubview(letterButton)
        letterButton.label.text = NSLocalizedString("calculator.type.letter", comment: "")

        viewB.addSubview(bagButton)
        bagButton.label.text = NSLocalizedString("calculator.type.packed", comment: "")
        
        sizeInternHeader.text = NSLocalizedString("calculator.allSizes.header", comment: "")
        viewB.addSubview(sizeInternHeader)
        
        viewB.addSubview(sizesInternBackground)
        sizesInternBackground.backgroundColor = .white
        sizesInternBackground.layer.masksToBounds = false
        sizesInternBackground.layer.shadowColor = UPColor.darkGray.cgColor
        sizesInternBackground.layer.shadowOpacity = 0.1
        sizesInternBackground.layer.shadowOffset = .zero
        sizesInternBackground.layer.shadowRadius = 2
        sizesInternBackground.layer.cornerRadius = 3.0
        
        sizesInternBackground.addSubview(lengthInternField)
        lengthInternField.backgroundColor = .clear
        lengthInternField.textAlignment = .center
        lengthInternField.keyboardType = .numbersAndPunctuation
        
        sizesInternBackground.addSubview(heightInternField)
        heightInternField.backgroundColor = .clear
        heightInternField.textAlignment = .center
        heightInternField.keyboardType = .numbersAndPunctuation
        
        sizesInternBackground.addSubview(widthInternField)
        widthInternField.backgroundColor = .clear
        widthInternField.textAlignment = .center
        widthInternField.keyboardType = .numbersAndPunctuation
        
        sizesInternBackground.addSubview(firstSeparator)
        firstSeparator.backgroundColor = UPColor.separatorGray
        sizesInternBackground.addSubview(secondSeparator)
        secondSeparator.backgroundColor = UPColor.separatorGray
        
        viewB.addSubview(weightInternLabel)
        weightInternLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        weightInternLabel.textColor = UPColor.darkGray
        weightInternLabel.text = NSLocalizedString("calculator.details.weight.label", comment: "")
        
        viewB.addSubview(weightInternField)
        weightInternField.textAlignment = .center
        weightInternField.keyboardType = .numberPad
        
        viewB.addSubview(submitInternButton)
        submitInternButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let currentWidth = UIScreen.main.bounds.size.width
        let halfWidth = (currentWidth - 60) / 2
        self.contentSize = CGSize(width: currentWidth * 2, height: self.frame.size.height)
        
        viewA.pin.left().top().bottom().width(currentWidth)
        
        
        domesticTypeHeader.pin.top(20).left(20).right(20).height(10)
        domesticExpressButton.pin.below(of: domesticTypeHeader).marginTop(10).left(20).width(halfWidth).height(60)
        domesticSmartBoxButton.pin.below(of: domesticTypeHeader).marginTop(10).after(of: domesticExpressButton).marginLeft(20).right(20).height(60)

        weightHeader.pin.below(of: domesticSmartBoxButton).marginTop(20).left(20).width(halfWidth).height(15)
        weightField.pin.below(of: weightHeader).marginTop(10).left(20).height(60).width(halfWidth)
        
        sizeHeader.pin.below(of: domesticSmartBoxButton).marginTop(20).left(to: weightHeader.edge.right).marginLeft(20).height(15).width(halfWidth)
        sizeField.pin.below(of: weightHeader).marginTop(10).left(to: weightField.edge.right).marginLeft(20).height(60).width(halfWidth)

        
        uuidHeader.pin.below(of: domesticSmartBoxButton).marginTop(20).left(20).height(15).right(20)
        uuidField.pin.below(of: uuidHeader).marginTop(10).left(20).height(40).right(20)
        
        availableBoxesButton.pin.below(of: uuidField).marginTop(10).left(20).height(40).right(20)
        
        if availableBoxesButton.isHidden {
            newBoxHeader.pin.below(of: uuidField).marginTop(20).left(20).height(15).right(20)
        } else {
            newBoxHeader.pin.below(of: availableBoxesButton).marginTop(20).left(20).height(15).right(20)
        }
        
        newBoxField.pin.below(of: newBoxHeader).marginTop(10).left(20).right(20).height(40)
        
        domesticSubmitButton.pin.left().bottom(0).right(0).height(64)
        
        
        viewB.pin.left(to: viewA.edge.right).top().bottom().right().width(currentWidth)
        
        countryHeader.pin.top(20).left(20).right(20).height(15)
        countryButton.pin.below(of: countryHeader).marginTop(10).left(20).right(20).height(40)
        typeHeader.pin.top(to: countryButton.edge.bottom).marginTop(20).left(20).right(20).height(15)
        
        let typeWidth = (currentWidth - 40 - 20) / 3
        parcelButton.pin.top(to: typeHeader.edge.bottom).marginTop(10).left(20).height(80).width(typeWidth)
        letterButton.pin.top(to: typeHeader.edge.bottom).marginTop(10).left(to: parcelButton.edge.right).marginLeft(10).height(80).width(typeWidth)
        bagButton.pin.top(to: typeHeader.edge.bottom).marginTop(10).left(to: letterButton.edge.right).marginLeft(10).height(80).width(typeWidth)

        sizeInternHeader.pin.top(to: parcelButton.edge.bottom).marginTop(20).left(20).right(20).height(10)
        sizesInternBackground.pin.below(of: sizeInternHeader).marginTop(10).left(20).height(50).right(20)
        let tripleWidth = (currentWidth - 40) / 3
        
        lengthInternField.pin.top().left().bottom().width(tripleWidth)
        heightInternField.pin.left(to: lengthInternField.edge.right).top().bottom().width(tripleWidth)
        widthInternField.pin.top().bottom().left(to: heightInternField.edge.right).width(tripleWidth)
        firstSeparator.pin.top(5).bottom(5).left(to: heightInternField.edge.left).width(1)
        secondSeparator.pin.top(5).bottom(5).right(to: heightInternField.edge.right).width(1)
        
        weightInternField.pin.below(of: sizesInternBackground).marginTop(20).right(20).height(50).width(tripleWidth)
        weightInternLabel.pin.height(20).left(20).centerRight(to: weightInternField.anchor.centerLeft)
        
        submitInternButton.pin.left().bottom().right().height(64)
    }
}
