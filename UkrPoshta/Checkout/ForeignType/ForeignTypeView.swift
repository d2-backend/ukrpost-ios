//
//  ForeignTypeView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/28/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignTypeView: UIView {
    
    private let scroll = UIScrollView()
    private let header = UPHeader()
    let present = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected", imgAlignment: .left)
    let example = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected", imgAlignment: .left)
    let returning = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected", imgAlignment: .left)
    let docs = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected", imgAlignment: .left)
    let other = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected", imgAlignment: .left)
    let otherField = UPTextField()
    private let line = UIView()
    
    let detail = UIButton()
    private let detailTopTitle = UILabel()
    private let detailBotTitle = UILabel()
    let falseView = UIView()
    var isExpanded = false
    
    let licenseField = UPTextField()
    let certField = UPTextField()
    let accountField = UPTextField()
    let codeField = UPTextField()
    let countryField = UPTextField()

    let submit = UPSubmitButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        addSubview(scroll)
        submit.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        addSubview(submit)
        
        header.text = NSLocalizedString("checkout.tax.info.header", comment: "")
        scroll.addSubview(header)
        
        present.label.text = NSLocalizedString("checkout.tax.present", comment: "")
        present.label.textAlignment = .left
        present.backgroundColor = .clear
        scroll.addSubview(present)
        
        example.label.text = NSLocalizedString("checkout.tax.example", comment: "")
        example.label.textAlignment = .left
        example.backgroundColor = .clear
        scroll.addSubview(example)
        
        returning.label.text = NSLocalizedString("checkout.tax.returning", comment: "")
        returning.label.textAlignment = .left
        returning.backgroundColor = .clear
        scroll.addSubview(returning)
        
        docs.label.text = NSLocalizedString("checkout.tax.docs", comment: "")
        docs.label.textAlignment = .left
        docs.backgroundColor = .clear
        scroll.addSubview(docs)
        
        other.label.text = NSLocalizedString("checkout.tax.other", comment: "")
        other.label.textAlignment = .left
        other.backgroundColor = .clear
        scroll.addSubview(other)
        
        otherField.isHidden = true
        scroll.addSubview(otherField)
        
        line.backgroundColor = UPColor.separatorGray
        scroll.addSubview(line)
        
        detail.backgroundColor = .clear
        scroll.addSubview(detail)
        
        let aFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        let boldFont = UIFont(descriptor: aFont.fontDescriptor.withSymbolicTraits(.traitBold)!, size: aFont.pointSize)
        detailTopTitle.font = boldFont
        detailTopTitle.text = NSLocalizedString("checkout.tax.detail.top", comment: "")
        detailTopTitle.textColor = UPColor.darkGray
        detail.addSubview(detailTopTitle)
        
        detailBotTitle.font = aFont
        detailBotTitle.text = NSLocalizedString("checkout.tax.detail.bot", comment: "")
        detailBotTitle.numberOfLines = 0
        detailBotTitle.lineBreakMode = .byWordWrapping
        detailBotTitle.textColor = UPColor.lightGray
        detail.addSubview(detailBotTitle)
        
        falseView.backgroundColor = .clear
        scroll.addSubview(falseView)
        
        licenseField.placeholder = NSLocalizedString("checkout.tax.license", comment: "")
        falseView.addSubview(licenseField)
        
        certField.placeholder = NSLocalizedString("checkout.tax.cert", comment: "")
        falseView.addSubview(certField)
        
        accountField.placeholder = NSLocalizedString("checkout.tax.account", comment: "")
        falseView.addSubview(accountField)
        
        codeField.placeholder = NSLocalizedString("checkout.tax.code", comment: "")
        falseView.addSubview(codeField)
        
        countryField.placeholder = NSLocalizedString("checkout.tax.country", comment: "")
        falseView.addSubview(countryField)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        submit.pin.horizontally().bottom(64).height(64)
        scroll.pin.horizontally().top().bottom(to: submit.edge.top)
        
        header.pin.top(20).horizontally(20).height(20)
        present.pin.top(to: header.edge.bottom).marginTop(20).horizontally(20).height(30)
        example.pin.top(to: present.edge.bottom).marginTop(10).horizontally(20).height(30)
        returning.pin.top(to: example.edge.bottom).marginTop(10).horizontally(20).height(30)
        docs.pin.top(to: returning.edge.bottom).marginTop(10).horizontally(20).height(30)
        other.pin.top(to: docs.edge.bottom).marginTop(10).horizontally(20).height(30)
        if otherField.isHidden {
            line.pin.top(to: other.edge.bottom).marginTop(20).horizontally(20).height(1)
        } else {
            otherField.pin.top(to: other.edge.bottom).marginTop(10).horizontally(20).height(50)
            line.pin.top(to: otherField.edge.bottom).marginTop(20).horizontally(20).height(1)
        }
        
        if docs.isSelected {
            detail.isHidden = true
            falseView.isHidden = true
            scroll.contentSize = CGSize(width: frame.size.width, height: line.frame.maxY + 20)

        } else {
            detail.isHidden = isExpanded
            falseView.isHidden = !isExpanded
            if isExpanded {
                falseView.pin.top(to: line.edge.bottom).marginTop(20).horizontally(20).height(300)
                licenseField.pin.top().horizontally().height(50)
                certField.pin.top(to: licenseField.edge.bottom).marginTop(10).horizontally().height(50)
                accountField.pin.top(to: certField.edge.bottom).marginTop(10).horizontally().height(50)
                codeField.pin.top(to: accountField.edge.bottom).marginTop(10).horizontally().height(50)
                countryField.pin.top(to: codeField.edge.bottom).marginTop(10).horizontally().height(50)
                scroll.contentSize = CGSize(width: frame.size.width, height: falseView.frame.maxY + 20)
            } else {
                detail.pin.top(to: line.edge.bottom).marginTop(20).horizontally(20).height(80)
                detailTopTitle.pin.top(10).horizontally().height(20)
                detailBotTitle.pin.top(to: detailTopTitle.edge.bottom).horizontally(20).bottom()
                scroll.contentSize = CGSize(width: frame.size.width, height: detail.frame.maxY + 20)
                
            }
        }

        
    }

}
