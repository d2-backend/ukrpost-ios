//
//  ForeignTypeViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/28/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignTypeViewController: UIViewController, UITextFieldDelegate {

    private let mainView = ForeignTypeView()
    var country: Country?
    var order: DomesticShipment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))
        
        self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        mainView.present.isSelected = true
        mainView.example.isSelected = false
        mainView.returning.isSelected = false
        mainView.docs.isSelected = false
        mainView.other.isSelected = false

        mainView.present.addTarget(self, action: #selector(presentPressed), for: .touchUpInside)
        mainView.example.addTarget(self, action: #selector(examplePressed), for: .touchUpInside)
        mainView.returning.addTarget(self, action: #selector(returningPressed), for: .touchUpInside)
        mainView.docs.addTarget(self, action: #selector(docsPressed), for: .touchUpInside)
        mainView.other.addTarget(self, action: #selector(otherPressed), for: .touchUpInside)
        mainView.otherField.delegate = self
        mainView.detail.addTarget(self, action: #selector(detailPressed), for: .touchUpInside)

        mainView.licenseField.delegate = self
        mainView.certField.delegate = self
        mainView.accountField.delegate = self
        mainView.codeField.delegate = self
        mainView.countryField.delegate = self
        mainView.submit.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        redrawSubmit()
    }
    
    func redrawSubmit() {
        if mainView.other.isSelected == true && mainView.otherField.text!.trimmingCharacters(in: .whitespaces) == "" {
            mainView.submit.isEnabled = false
        } else {
            mainView.submit.isEnabled = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        redrawSubmit()
        return true
    }
    
    @objc func presentPressed() {
        mainView.present.isSelected = true
        mainView.example.isSelected = false
        mainView.returning.isSelected = false
        mainView.docs.isSelected = false
        mainView.other.isSelected = false
        mainView.otherField.isHidden = true
        mainView.submit.isEnabled = false
        redrawSubmit()
        mainView.layoutSubviews()
    }
    
    @objc func examplePressed() {
        mainView.present.isSelected = false
        mainView.example.isSelected = true
        mainView.returning.isSelected = false
        mainView.docs.isSelected = false
        mainView.other.isSelected = false
        mainView.otherField.isHidden = true
        redrawSubmit()
        mainView.layoutSubviews()

    }
    
    @objc func returningPressed() {
        mainView.present.isSelected = false
        mainView.example.isSelected = false
        mainView.returning.isSelected = true
        mainView.docs.isSelected = false
        mainView.other.isSelected = false
        mainView.otherField.isHidden = true
        redrawSubmit()
        mainView.layoutSubviews()

    }
    
    @objc func docsPressed() {
        mainView.present.isSelected = false
        mainView.example.isSelected = false
        mainView.returning.isSelected = false
        mainView.docs.isSelected = true
        mainView.other.isSelected = false
        mainView.otherField.isHidden = true
        redrawSubmit()
        mainView.layoutSubviews()

    }
    
    @objc func otherPressed() {
        mainView.present.isSelected = false
        mainView.example.isSelected = false
        mainView.returning.isSelected = false
        mainView.docs.isSelected = false
        mainView.other.isSelected = true
        mainView.otherField.isHidden = false
        redrawSubmit()
        mainView.layoutSubviews()

    }
    
    @objc func detailPressed() {
        mainView.isExpanded = true
        mainView.layoutSubviews()
    }
    
    
    @objc func closeTapped() {
        let vc = MainViewController()
        vc.isAnimationNeeded = false
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.shadowImage = UIImage()
        nc.navigationBar.isTranslucent = false
        nc.navigationBar.tintColor = UPColor.darkGray
        self.present(nc, animated: true, completion: nil)
    }
    
    @objc func submitPressed() {
        let vc = ForeignCalculatorViewController()
        var intData = InternationalData()
        if mainView.present.isSelected == true {
            intData.categoryType = "GIFT"
        } else if mainView.example.isSelected == true {
            intData.categoryType = "COMMERCIAL_SAMPLE"
        } else if mainView.returning.isSelected == true {
            intData.categoryType = "RETURNING_GOODS"
        } else if mainView.docs.isSelected == true {
            intData.categoryType = "DOCUMENTS"
        } else if mainView.other.isSelected == true {
            intData.categoryType = "MIXED_CONTENT"
            intData.Explanation = mainView.otherField.text
        }
        
        if mainView.licenseField.text != "" {
            intData.licenceNumber = mainView.licenseField.text
        }
        
        if mainView.certField.text != "" {
            intData.certificateNumber = mainView.certField.text
        }
        
        if mainView.accountField.text != "" {
            intData.invoiceNumber = mainView.accountField.text
        }
        
        if order?.packageType == APIPackageType.letter.rawValue || order?.packageType == APIPackageType.smallBag.rawValue {
            intData.tracked = true
        }
        
        order?.internationalData = intData
        
        vc.order = order
        vc.country = country
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
