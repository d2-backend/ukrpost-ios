//
//  CheckoutResultViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/16/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase
class CheckoutResultViewController: UIViewController {

    private let mainView = CheckoutResultView()
    var order: DomesticShipment?
   var isExpress = Bool()
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.mainView.downloadButton.isEnabled = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      var orderString = ""
      orderString = (order?.barcode?.description)!
      print(orderString)
      UPAPIManager.sendOrder(with: orderString, completion: nil)
      if isExpress {
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 8",
            AnalyticsParameterItemName: "ОВ_УКР_ЕК_8",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_8"
            ])
      }
      
      if !isExpress {
            Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                  AnalyticsParameterItemID: "id 8",
                  AnalyticsParameterItemName: "ОВ_УКР_SmartBox_8",
                  AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_8"
                  ])
      }
        self.title = "Оформити вiдправлення"
        hideBackNavigationButtonText()
        mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.navigationItem.setHidesBackButton(true, animated:false);
        
        let image = UIImage(named: "crossSmall")
        let rightBarButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(navigateToMain))
        rightBarButton.imageInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        mainView.id.text = self.order?.barcode
        
        self.mainView.downloadButton.addTarget(self, action: #selector(downloadPressed), for: .touchUpInside)
        
        let item = OrderItem(code: self.order!.barcode!, uuid: self.order!.uuid!, name: nil)
        User.addToOrders(item: item)
    }
    
    @objc func navigateToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func downloadPressed() {
        self.mainView.downloadButton.isEnabled = false
        let vc = StickerViewController()
        vc.orderCode = self.order?.barcode
        vc.uuid = self.order!.uuid!
        vc.isInternational = order?.type == "INTERNATIONAL" 
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}
