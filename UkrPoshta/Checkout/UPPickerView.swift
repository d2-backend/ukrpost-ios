//
//  UPPickerView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/15/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPPickerView: UIView {

    let tableView = UITableView()
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .clear
        self.addSubview(tableView)
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UPColor.whiteSmoke
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tableView.pin.all()
    }

}
