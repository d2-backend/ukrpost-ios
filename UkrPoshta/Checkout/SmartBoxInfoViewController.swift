//
//  SmartBoxInfoViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/31/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase

class SmartBoxInfoViewController: UIViewController, UITextFieldDelegate {

    private let mainView = SmartBoxInfoView()
    var currentBox: SmartBox?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    private var order: DomesticShipment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboard()
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 2",
            AnalyticsParameterItemName: "ОВ_УКР_SmartBox_2 ",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_2 "
            ])
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.boxLabel.text = currentBox?.smartBoxTariff?.description
        
        self.mainView.returnButton.addTarget(self, action: #selector(returnTypePressed(sender:)), for: .touchUpInside)
        self.mainView.returnButton.isSelected = true
        
        self.mainView.storeAndReturnButton.addTarget(self, action: #selector(returnTypePressed(sender:)), for: .touchUpInside)
        self.mainView.storeAndReturnButton.isSelected = false
        
        self.mainView.abandonButton.addTarget(self, action: #selector(returnTypePressed(sender:)), for: .touchUpInside)
        self.mainView.abandonButton.isSelected = false
        
        self.mainView.predefinedValueButton.addTarget(self, action: #selector(predefinedPressed), for: .touchUpInside)
        self.mainView.predefinedValueButton.isSelected = false
        
        self.mainView.predefinedValueField.delegate = self
        
        self.mainView.postpayButton.addTarget(self, action: #selector(postpayPressed), for: .touchUpInside)
        self.mainView.postpayButton.isSelected = false
        
        self.mainView.postpayField.delegate = self
        
        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        getBoxUUID()
        self.mainView.submitButton.isEnabled = false
    }
    
    @objc func returnTypePressed(sender: UIButton) {
        self.mainView.returnButton.isSelected = false
        self.mainView.storeAndReturnButton.isSelected = false
        self.mainView.abandonButton.isSelected = false
        
        sender.isSelected = true
    }
    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard) )
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.mainView.predefinedValueField.maxLength = 6
        self.mainView.postpayField.maxLength = 6
        if self.mainView.predefinedValueField.isEditing ||  self.mainView.postpayField.isEditing {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    
    @objc func predefinedPressed() {
        self.mainView.predefinedValueButton.isSelected = !self.mainView.predefinedValueButton.isSelected
    }
    
    @objc func postpayPressed() {
        self.mainView.postpayButton.isSelected = !self.mainView.postpayButton.isSelected
    }
    
    @objc func submitPressed() {

        if self.mainView.returnButton.isSelected {
            self.order?.onFailReceiveType = APIReturnBehaviour.returnToSender.rawValue
        } else if self.mainView.storeAndReturnButton.isSelected {
            self.order?.onFailReceiveType = APIReturnBehaviour.storeAndReturn.rawValue
        } else if self.mainView.abandonButton.isSelected {
            self.order?.onFailReceiveType = APIReturnBehaviour.abandon.rawValue
        }
        
        let predefinedInt = Int(self.mainView.predefinedValueField.text!)
        //let parcelItem = ParcelItem(weight: self.currentBox?.smartBoxTariff?.weight, length: self.currentBox?.smartBoxTariff?.length, declaredPrice: predefinedInt)
        let parcelItem = ParcelItem(name: nil, weight: self.currentBox?.smartBoxTariff?.weight, length: self.currentBox?.smartBoxTariff?.length, declaredPrice: predefinedInt, parcelItems: nil)

        self.order?.parcels = [parcelItem]
        self.order?.postPay = Int(mainView.postpayField.text ?? "0") ?? 0
        self.order?.declaredPrice = Int(mainView.predefinedValueField.text ?? "0") ?? 0
        let vc = SmartBoxRecepientViewController()
        vc.currentBox = self.currentBox
        vc.addressFrom = self.addressFrom
        vc.addressTo = self.addressTo
        vc.order = self.order
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getBoxUUID() {
        UPAPIManager.getBoxUUID(boxCode: currentBox!.code!) { (result) in
            switch result {
            case .success(let item):
                self.order = item
                self.mainView.submitButton.isEnabled = true
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
                
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }


}
