//
//  SenderInfoViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase
class SenderInfoViewController: UPViewController, UITextFieldDelegate {

    let mainView = SenderInfoView()
    var order: DomesticShipment?
    var currentUser: User?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var isExspress = Bool()
    var country: Country?
    var isHintNeeded = false
    
      override func viewDidLoad() {
            super.viewDidLoad()
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))
            if isExspress {
                  Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                        AnalyticsParameterItemID: "id 2",
                        AnalyticsParameterItemName: "ОВ_УКР_ЕК_2",
                        AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_2"
                  ])
            }
            if !isExspress {
                  Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                        AnalyticsParameterItemID: "id 2",
                        AnalyticsParameterItemName: "ОВ_УКР_SmartBox_2",
                        AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_2"
                  ])
            }
            self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
        self.mainView.isHintNeeded = (order?.international)!
            self.mainView.frame = self.view.frame
            self.view.addSubview(self.mainView)
            self.mainView.userTypeHeader.text = NSLocalizedString("checkout.sender.header", comment: "")
            self.mainView.uuidField.delegate = self
            self.mainView.isIndividualButton.addTarget(self, action: #selector(userTypePressed), for: .touchUpInside)
            self.mainView.isIndividualButton.isSelected = true
            self.mainView.isLegalButton.addTarget(self, action: #selector(userTypePressed), for: .touchUpInside)


            self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
            self.mainView.submitButton.isEnabled = false
            self.mainView.indPhoneField.text = UPPhoneHelper.displayStringFrom(rawString: "")
            self.mainView.corpPhoneField.text = UPPhoneHelper.displayStringFrom(rawString: "")

            let user: User = User.currentUser()
            self.currentUser = user
            self.fillDataFromUser()


            self.mainView.indNameField.delegate = self
            self.mainView.indSurnameField.delegate = self
            self.mainView.indMiddleNameField.delegate = self
            self.mainView.indPhoneField.delegate = self

            self.mainView.corpNameField.delegate = self
            self.mainView.corpPhoneField.delegate = self
            self.mainView.corpIDField.delegate = self
            self.mainView.corpBankIDField.delegate = self
            self.mainView.corpAccountField.delegate = self

      }

      func fetchToken(bearerUUID: String) {

            UPAPIManager.getToken(bearerUUID: bearerUUID) { (result) in
                  switch result {
                  case .success(let user):
                        User.save(user: user)
                        self.currentUser = user
                        self.fillDataFromUser()
                  case .partialSuccess(let string):
                        self.showAlertWith(text: string)

                  case .failure(let error):
                        self.showAlertWith(text: error.localizedDescription)
                  }
            }
      }

      func fillDataFromUser() {
            self.mainView.submitButton.isEnabled = false
            if self.currentUser != nil {
                  self.mainView.uuidField.text = self.currentUser?.uuid

                  if self.currentUser!.individual! {
                        self.mainView.isIndividualButton.isSelected = true
                        self.mainView.isLegalButton.isSelected = false
                        self.mainView.indSurnameField.text = self.currentUser?.lastName
                        self.mainView.indNameField.text = self.currentUser?.firstName
                        self.mainView.indMiddleNameField.text = self.currentUser?.middleName
                        self.mainView.indPhoneField.text = UPPhoneHelper.displayStringFrom(rawString: self.currentUser?.phoneNumber)

                  } else {
                        self.mainView.isIndividualButton.isSelected = false
                        self.mainView.isLegalButton.isSelected = true
                        self.mainView.corpNameField.text = self.currentUser?.name
                        self.mainView.corpPhoneField.text = UPPhoneHelper.displayStringFrom(rawString: self.currentUser?.phoneNumber)
                        self.mainView.corpIDField.text = self.currentUser?.edrpou
                        self.mainView.corpBankIDField.text = self.currentUser?.bankCode
                        self.mainView.corpAccountField.text = self.currentUser?.bankAccount
                  }

                  if currentUser!.addressId != nil && currentUser!.addressId != 0 && addressFrom == nil {
                        UPAPIManager.getAddress(id: currentUser!.addressId!) { (result) in
                              switch result {
                              case .success(let address):
                                    let newAddressFrom = CityDetails(regionID: nil, regionName: address.region, districtID: nil, districtName: address.district, cityID: nil, cityName: address.city, cityTypeID: nil, cityTypeName: nil, street: address.street, house: address.houseNumber, apartment: address.apartmentNumber, addressID: address.id, postcode: address.postcode)
                                    self.addressFrom = newAddressFrom
                              case .partialSuccess(let string):
                                    print(string)
                                    self.showAlertWith(text: string)
                              case .failure(let error):
                                    self.showAlertWith(text: error.localizedDescription)
                              }
                              self.redrawSubmitButton()

                        }
                  } else {
                        self.redrawSubmitButton()
                  }
            }
      }

      @objc func userTypePressed() {

            self.mainView.isIndividualButton.isSelected = !self.mainView.isIndividualButton.isSelected
            self.mainView.isLegalButton.isSelected = !self.mainView.isLegalButton.isSelected
            self.mainView.layoutSubviews()
            self.mainView.uuidField.text = ""
            self.currentUser = nil
            self.redrawSubmitButton()
      }


      @objc func submitPressed() {
            var aUser = User()

            if self.currentUser != nil {

                  aUser = self.currentUser!

            } else {
                  if self.mainView.isIndividualButton.isSelected {
                        aUser.firstName = self.mainView.indNameField.text
                        aUser.lastName = self.mainView.indSurnameField.text
                        aUser.middleName = self.mainView.indMiddleNameField.text
                        aUser.phoneNumber = UPPhoneHelper.rawStringFrom(displayString: self.mainView.indPhoneField.text)
                        aUser.type = APIUserType.individual.rawValue
                        aUser.individual = true
                        aUser.bankCode = nil
                        aUser.bankAccount = nil
                  } else {
                        aUser.name = self.mainView.corpNameField.text
                        aUser.phoneNumber = UPPhoneHelper.rawStringFrom(displayString: self.mainView.corpPhoneField.text)
                        aUser.bankCode = self.mainView.corpBankIDField.text
                        aUser.bankAccount = self.mainView.corpAccountField.text
                        if self.mainView.corpIDField.text?.count == 8 {
                              aUser.edrpou = self.mainView.corpIDField.text
                              aUser.type = APIUserType.company.rawValue
                        } else if self.mainView.corpIDField.text?.count == 10 {
                              aUser.tin = self.mainView.corpIDField.text
                              aUser.type = APIUserType.privateEnterpreneur.rawValue
                        }
                        aUser.individual = false

                  }
                  self.order?.sender = aUser
            }

            let vc = SenderAddressViewController()
            self.order?.sender = aUser
            vc.isExspress = self.isExspress
            vc.order = self.order
            vc.addressTo = self.addressTo
            vc.addressFrom = self.addressFrom
            vc.country = country
            self.navigationController?.pushViewController(vc, animated: true)

      }

      func showAlertWith(text: String) {
            let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))

            self.present(alert, animated: true, completion: nil)
      }

      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == self.mainView.uuidField {
                  self.fetchToken(bearerUUID: textField.text!)
            }
            textField.resignFirstResponder()
            return true
      }

      func textFieldDidEndEditing(_ textField: UITextField) {
            self.redrawSubmitButton()
      }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            switch textField {
            case mainView.uuidField:

                  return true

            case mainView.indPhoneField, mainView.corpPhoneField:
                  let oldString = textField.text
                  let newString = oldString! + string
                  if (newString.count <= 6 || newString.count > 19) {
                        return false
                  } else {
                        self.mainView.uuidField.text = ""
                        self.currentUser = nil
                        var oldRawString = UPPhoneHelper.rawStringFrom(displayString: oldString!)

                        switch string {
                        case "":
                              oldRawString!.removeLast()
                        case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
                              oldRawString = oldRawString! + string

                        default:
                              return false
                        }
                        textField.text = UPPhoneHelper.displayStringFrom(rawString: oldRawString)
                  }

                  return false
            case mainView.corpBankIDField:
                  switch string {
                  case "":
                        return true
                  case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
                        let newString = textField.text! + string
                        if newString.count < 7 {
                              textField.text = newString
                        }
                        return false
                  default:
                        return false
                  }
            case mainView.corpIDField:
                  switch string {
                  case "":
                        return true
                  case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
                        let newString = textField.text! + string
                        if newString.count < 11 {
                              textField.text = newString
                        }
                        return false
                  default:
                        return false
                  }
            case mainView.corpAccountField:
                  switch string {
                  case "":
                        return true
                  case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
                        let newString = textField.text! + string
                        if newString.count < 15 {
                              textField.text = newString
                        }
                        return false
                  default:
                        return false
                  }
            default:
                  self.mainView.uuidField.text = ""
                  self.currentUser = nil
                  return true
            }
      }

      @objc func closeTapped() {
            let vc = MainViewController()
            vc.isAnimationNeeded = false
            let nc = UINavigationController(rootViewController: vc)
            nc.navigationBar.shadowImage = UIImage()
            nc.navigationBar.isTranslucent = false
            nc.navigationBar.tintColor = UPColor.darkGray
            self.present(nc, animated: true, completion: nil)
      }

      func redrawSubmitButton() {
            if self.mainView.isIndividualButton.isSelected {
                  if self.mainView.indNameField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.indSurnameField.text!.trimmingCharacters(in: .whitespaces) != "" &&
                        //self.mainView.indMiddleNameField.text!.trimmingCharacters(in: .whitespaces) != "" &&
                        self.mainView.indPhoneField.text!.count == 19 {
                        self.mainView.submitButton.isEnabled = true
                  } else {
                        self.mainView.submitButton.isEnabled = false
                  }
            } else {
                  if self.mainView.corpNameField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpIDField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpBankIDField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpAccountField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpPhoneField.text!.count == 19 && (self.mainView.corpIDField.text!.count == 10 || self.mainView.corpIDField.text!.count == 8) {
                        self.mainView.submitButton.isEnabled = true
                  } else {
                        self.mainView.submitButton.isEnabled = false
                  }
            }
      }
}



