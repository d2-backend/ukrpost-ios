//
//  SenderInfoView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import PinLayout

class SenderInfoView: UIView {

    let scrollView = UIScrollView()
    
    let uuidHeader = UPHeader()
    let uuidField = UPTextField()
    
    let userTypeHeader = UPHeader()
    let isIndividualButton = UPHorizontalButton(normalImageName:"radioButton" , selectedImageName: "radioButtonSelected")
    let isLegalButton = UPHorizontalButton(normalImageName:"radioButton" , selectedImageName: "radioButtonSelected")

    let indSurnameField = UPTextField()
    let indNameField = UPTextField()
    let indMiddleNameField = UPTextField()
    let indPhoneHeader = UPHeader()
    let indPhoneField = UPTextField()

    let corpHeader = UPHeader()
    let corpNameField = UPTextField()
    let corpPhoneHeader = UPHeader()
    let corpPhoneField = UPTextField()
    let corpIDHeader = UPHeader()
    let corpIDField = UPTextField()
    let corpBankIDHeader = UPHeader()
    let corpBankIDField = UPTextField()
    let corpAccountHeader = UPHeader()
    let corpAccountField = UPTextField()

    let corpContactPersonSurnameHeader = UPHeader()
    let corpContactPersonSurnameField = UPTextField()

    let corpContactPersonNameHeader = UPHeader()
    let corpContactPersonNameField = UPTextField()

    let corpContactPersonMiddleNameHeader = UPHeader()
    let corpContactPersonMiddleNameField = UPTextField()
    
    let submitButton = UPSubmitButton()
    var isSenderCorp: Bool = false
    
    var isHintNeeded = false
    private let hintView = UIView()
    private let hintIcon = UIImageView()
    let hintLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(scrollView)
        scrollView.backgroundColor = .clear
        
        hintView.backgroundColor = UPColor.dimmingGray
        addSubview(hintView)
        
        hintIcon.image = UIImage(named: "InfoIcon")
        hintIcon.contentMode = .scaleAspectFit
        hintView.addSubview(hintIcon)
        
        hintLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        hintLabel.textColor = .black
        hintLabel.numberOfLines = 0
        hintLabel.lineBreakMode = .byWordWrapping
        hintLabel.text = NSLocalizedString("checkout.sender.hint", comment: "")
        hintView.addSubview(hintLabel)
        

        scrollView.addSubview(uuidHeader)
        uuidHeader.text = NSLocalizedString("profile.bigView.title", comment: "")
        
        scrollView.addSubview(uuidField)
        uuidField.placeholder = NSLocalizedString("checkout.uuid.placeholder", comment: "")
        
        scrollView.addSubview(userTypeHeader)
        // text in viewController
        
        scrollView.addSubview(isIndividualButton)
        isIndividualButton.label.text = NSLocalizedString("profile.edit.individualLabel", comment: "")
        isIndividualButton.backgroundColor = .clear
        isIndividualButton.label.textAlignment = .left
        
        scrollView.addSubview(isLegalButton)
        isLegalButton.label.text = NSLocalizedString("profile.edit.legalLabel", comment: "")
        isLegalButton.backgroundColor = .clear
        isLegalButton.label.textAlignment = .left
        
        self.addSubview(submitButton)
        submitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        
        // ind
        
        scrollView.addSubview(indSurnameField)
        indSurnameField.placeholder = NSLocalizedString("profile.edit.lastNameHeader", comment: "")
        
        scrollView.addSubview(indNameField)
        indNameField.placeholder = NSLocalizedString("profile.edit.nameHeader", comment: "")
        
        scrollView.addSubview(indMiddleNameField)
        indMiddleNameField.placeholder = NSLocalizedString("profile.edit.middleNameHeader", comment: "")
        
        scrollView.addSubview(indPhoneHeader)
        indPhoneHeader.text = NSLocalizedString("profile.phoneCell.header", comment: "")
        
        scrollView.addSubview(indPhoneField)
        indPhoneField.keyboardType = .numbersAndPunctuation
        // corp
        scrollView.addSubview(corpHeader)
        corpHeader.text = NSLocalizedString("checkout.corp.name.placeholder", comment: "")
        
        scrollView.addSubview(corpNameField)
        corpNameField.placeholder = NSLocalizedString("checkout.corp.name.placeholder", comment: "")
        
        scrollView.addSubview(corpPhoneHeader)
        corpPhoneHeader.text = NSLocalizedString("profile.phoneCell.header", comment: "")
        
        scrollView.addSubview(corpPhoneField)
        corpPhoneField.keyboardType = .numbersAndPunctuation


        scrollView.addSubview(corpIDHeader)
        corpIDHeader.text = NSLocalizedString("checkout.corp.ID.header", comment: "")

        scrollView.addSubview(corpIDField)
        corpIDField.placeholder = NSLocalizedString("checkout.corp.ID.header", comment: "")
        corpIDField.keyboardType = .numbersAndPunctuation

        scrollView.addSubview(corpBankIDHeader)
        corpBankIDHeader.text = NSLocalizedString("checkout.corp.BankID.header", comment: "")

        scrollView.addSubview(corpBankIDField)
        corpBankIDField.placeholder = NSLocalizedString("checkout.corp.BankID.header", comment: "")
        corpBankIDField.keyboardType = .numbersAndPunctuation
        
        scrollView.addSubview(corpAccountHeader)
        corpAccountHeader.text = NSLocalizedString("checkout.corp.account", comment: "")
        
        scrollView.addSubview(corpAccountField)
        corpAccountField.placeholder = NSLocalizedString("checkout.corp.account", comment: "")
        corpAccountField.keyboardType = .numbersAndPunctuation


        corpContactPersonSurnameHeader.text = NSLocalizedString("checkout.corp.contact.lastNameHeader", comment: "")
        scrollView.addSubview(corpContactPersonSurnameHeader)

        corpContactPersonSurnameField.placeholder = NSLocalizedString("checkout.corp.contact.lastNamePlaceHolder", comment: "")
        corpContactPersonSurnameField.keyboardType = .default
        scrollView.addSubview(corpContactPersonSurnameField)


        corpContactPersonNameHeader.text = NSLocalizedString("checkout.corp.contact.nameHeader", comment: "")
        scrollView.addSubview(corpContactPersonNameHeader)

        corpContactPersonNameField.placeholder = NSLocalizedString("checkout.corp.contact.nameHeader", comment: "")
        corpContactPersonNameField.keyboardType = .default
        scrollView.addSubview(corpContactPersonNameField)


        corpContactPersonMiddleNameHeader.text = NSLocalizedString("checkout.corp.contact.middleNameHeader", comment: "")
        scrollView.addSubview(corpContactPersonMiddleNameHeader)

        corpContactPersonMiddleNameField.placeholder = NSLocalizedString("checkout.corp.contact.middleNameHeader", comment: "")
        corpContactPersonMiddleNameField.keyboardType = .default
        scrollView.addSubview(corpContactPersonMiddleNameField)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width
        submitButton.pin.left().bottom(64).right().height(64)
        scrollView.pin.top().left().right().bottom(to: submitButton.edge.top)
        
        if isHintNeeded {
            hintView.pin.top().horizontally().height(40)
            hintIcon.pin.left(10).vertically().width(20)
            hintLabel.pin.left(to: hintIcon.edge.right).marginLeft(10).vertically().right(35)
        } else {
            hintView.pin.top().horizontally().height(0)
        }
        
        if uuidHeader.isHidden {
            userTypeHeader.pin.top(to: hintView.edge.bottom).marginTop(20).left(20).right(20).height(15)
        } else {
            uuidHeader.pin.top(to: hintView.edge.bottom).marginTop(20).left(20).right(20).height(15)
            uuidField.pin.top(to: uuidHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)
            userTypeHeader.pin.top(to: uuidField.edge.bottom).marginTop(20).left(20).right(20).height(15)
        }
        isIndividualButton.pin.top(to: userTypeHeader.edge.bottom).marginTop(10).left(20).right(20).height(30)
        isLegalButton.pin.top(to: isIndividualButton.edge.bottom).marginTop(5).left(20).right(20).height(30)

        if self.isIndividualButton.isSelected {
            corpHeader.isHidden = true
            corpNameField.isHidden = true
            corpPhoneHeader.isHidden = true
            corpPhoneField.isHidden = true
            corpIDHeader.isHidden = true
            corpIDField.isHidden = true
            corpBankIDHeader.isHidden = true
            corpBankIDField.isHidden = true
            corpAccountHeader.isHidden = true
            corpAccountField.isHidden = true

            corpContactPersonSurnameHeader.isHidden = true
            corpContactPersonSurnameField.isHidden = true
            corpContactPersonNameHeader.isHidden = true
            corpContactPersonNameField.isHidden = true
            corpContactPersonMiddleNameHeader.isHidden = true
            corpContactPersonMiddleNameField.isHidden = true
            
            indSurnameField.isHidden = false
            indNameField.isHidden = false
            indMiddleNameField.isHidden = false
            indPhoneHeader.isHidden = false
            indPhoneField.isHidden = false

            indSurnameField.pin.top(to: isLegalButton.edge.bottom).marginTop(20).left(20).right(20).height(50)
            indNameField.pin.top(to: indSurnameField.edge.bottom).marginTop(10).left(20).right(20).height(50)
            indMiddleNameField.pin.top(to: indNameField.edge.bottom).marginTop(10).left(20).right(20).height(50)
            indPhoneHeader.pin.top(to: indMiddleNameField.edge.bottom).marginTop(20).left(20).right(20).height(15)
            indPhoneField.pin.top(to: indPhoneHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)
            
            scrollView.contentSize = CGSize(width: currentWidth, height: indPhoneField.frame.maxY + 20)

        } else {
            corpHeader.isHidden = false
            corpNameField.isHidden = false
            corpPhoneHeader.isHidden = false
            corpPhoneField.isHidden = false
            corpIDHeader.isHidden = false
            corpIDField.isHidden = false
            corpBankIDHeader.isHidden = false
            corpBankIDField.isHidden = false
            corpAccountHeader.isHidden = false
            corpAccountField.isHidden = false
            
            indSurnameField.isHidden = true
            indNameField.isHidden = true
            indMiddleNameField.isHidden = true
            indPhoneHeader.isHidden = true
            indPhoneField.isHidden = true
            
            corpHeader.pin.top(to: isLegalButton.edge.bottom).marginTop(20).left(20).right(20).height(15)
            corpNameField.pin.top(to: corpHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)
            corpPhoneHeader.pin.top(to: corpNameField.edge.bottom).marginTop(20).left(20).right(20).height(15)
            corpPhoneField.pin.top(to: corpPhoneHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)

            if !isSenderCorp {
                showContactPersonsFields(show: false)

                corpIDHeader.pin.top(to: corpPhoneField.edge.bottom).marginTop(20).left(20).right(20).height(15)
                corpIDField.pin.top(to: corpIDHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)
                corpBankIDHeader.pin.top(to: corpIDField.edge.bottom).marginTop(20).left(20).right(20).height(15)
                corpBankIDField.pin.top(to: corpBankIDHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)
                corpAccountHeader.pin.top(to: corpBankIDField.edge.bottom).marginTop(20).left(20).right(20).height(15)
                corpAccountField.pin.top(to: corpAccountHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)

                scrollView.contentSize = CGSize(width: currentWidth, height: corpAccountField.frame.maxY + 20)
            } else {
                showContactPersonsFields(show: true)

                corpContactPersonSurnameHeader
                    .pin
                    .top(to: corpPhoneField.edge.bottom)
                    .marginTop(20).left(20).right(20).height(15)

                corpContactPersonSurnameField
                    .pin
                    .top(to: corpContactPersonSurnameHeader.edge.bottom)
                    .marginTop(10).left(20).right(20).height(50)

                corpContactPersonNameHeader
                    .pin
                    .top(to: corpContactPersonSurnameField.edge.bottom)
                    .marginTop(20).left(20).right(20).height(15)

                corpContactPersonNameField
                    .pin
                    .top(to: corpContactPersonNameHeader.edge.bottom)
                    .marginTop(10).left(20).right(20).height(50)

                corpContactPersonMiddleNameHeader
                    .pin
                    .top(to: corpContactPersonNameField.edge.bottom)
                    .marginTop(20).left(20).right(20).height(15)

                corpContactPersonMiddleNameField.pin.top(to: corpContactPersonMiddleNameHeader.edge.bottom).marginTop(10).left(20).right(20).height(50)

                scrollView.contentSize = CGSize(width: currentWidth, height: corpContactPersonMiddleNameField.frame.maxY + 20)
            }
        }
    }

    func showContactPersonsFields(show: Bool) {
        let isHidden = !show
        corpContactPersonSurnameHeader.isHidden = isHidden
        corpContactPersonSurnameField.isHidden = isHidden
        corpContactPersonNameHeader.isHidden = isHidden
        corpContactPersonNameField.isHidden = isHidden
        corpContactPersonMiddleNameHeader.isHidden = isHidden
        corpContactPersonMiddleNameField.isHidden = isHidden
    }

    var contactPersonName: String {
        return "\(corpContactPersonSurnameField.text ?? "") \(corpContactPersonNameField.text ?? "") \(corpContactPersonMiddleNameField.text ?? "")".condensedWhitespace
    }

}
