//
//  SenderAddressView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class SenderAddressView: UIView {
    
    let addressHeader = UPHeader()
    
    let indexField = UPTextField()
    let regionField = UPTextField()
    let districtField = UPTextField()
    let cityField = UPTextField()
    let streetField = UPTextField()
    let houseField = UPTextField()
    let appartmentField = UPTextField()
    
    let submitButton = UPSubmitButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
                
        self.addSubview(addressHeader)
        addressHeader.text = NSLocalizedString("checkout.sender.address.header", comment: "")
        
        self.addSubview(indexField)
        indexField.placeholder = NSLocalizedString("indexToAddressHeader.title", comment: "")
        indexField.keyboardType = .numbersAndPunctuation

        self.addSubview(regionField)
        regionField.placeholder = NSLocalizedString("addressToIndex.region.title", comment: "")
        
        self.addSubview(districtField)
        districtField.placeholder = NSLocalizedString("addressToIndex.district.title", comment: "")
        
        self.addSubview(cityField)
        cityField.placeholder = NSLocalizedString("addressToIndex.city.title", comment: "")
        
        self.addSubview(streetField)
        streetField.placeholder = NSLocalizedString("addressToIndex.street.title", comment: "")
        
        self.addSubview(houseField)
        houseField.placeholder = NSLocalizedString("addressToIndex.building.title", comment: "")
        houseField.keyboardType = .numbersAndPunctuation
        
        self.addSubview(appartmentField)
        appartmentField.placeholder = NSLocalizedString("addressToIndex.apartment.title", comment: "")
        appartmentField.keyboardType = .numbersAndPunctuation

        self.addSubview(submitButton)
        submitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width
        let fieldHeight: CGFloat = 50.0

        let smallWidth = (currentWidth - 50) / 2
        
        addressHeader.pin.top(20).left(20).right(20).height(10)
        indexField.pin.below(of: addressHeader).marginTop(10).right(20).left(20).height(fieldHeight)
        regionField.pin.below(of: indexField).marginTop(10).right(20).left(20).height(fieldHeight)
        districtField.pin.below(of: regionField).marginTop(10).left(20).right(20).height(fieldHeight)
        cityField.pin.below(of: districtField).marginTop(10).left(20).right(20).height(fieldHeight)
        streetField.pin.below(of: cityField).marginTop(10).left(20).right(20).height(fieldHeight)
        
        houseField.pin.below(of: streetField).marginTop(10).left(20).height(fieldHeight).width(smallWidth)
        appartmentField.pin.below(of: streetField).marginTop(10).right(20).height(fieldHeight).width(smallWidth)
        
        submitButton.pin.left().bottom(64).right().height(64)

    }

}
