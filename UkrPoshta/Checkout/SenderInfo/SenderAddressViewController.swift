//
//  SenderAddressViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase

protocol AddressViewProtocol: class {
    func showAlert(with text: String)
    func push(viewController: IndexSearchViewController)
}

class SenderAddressViewController: UPViewController, UITextFieldDelegate {
    
    private let mainView = SenderAddressView()
    private var savedAddress:Address?
    private var controller: AddressFormControllerProtocol?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var isExspress = Bool()
    var country: Country?
    var order: DomesticShipment?

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      if isExspress {
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 3",
            AnalyticsParameterItemName: "ОВ_УКР_ЕК_3",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_3"
            ])
      }
      if !isExspress {
            Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                  AnalyticsParameterItemID: "id 3",
                  AnalyticsParameterItemName: "ОВ_УКР_SmartBox_3",
                  AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_3"
                  ])
      }
        self.title = "Оформити вiдправлення"
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        self.mainView.submitButton.isEnabled = false
        
        controller = AddressFormController(with: mainView, and: addressFrom, viewController: self)
        controller?.viewDidLoad()
        
    }

    @objc func submitPressed() {
        if mainView.indexField.text == addressFrom?.postcode && addressFrom?.addressID == self.order?.sender?.addressId {
            navigateNext()
        } else {
            createAddress()
        }
    }
    
    func createAddress() {
        self.mainView.submitButton.isEnabled = false
        UPAPIManager.createAddress(postCode: self.mainView.indexField.text, region: self.mainView.regionField.text, district: self.mainView.districtField.text, city:  self.mainView.cityField.text, street: self.mainView.streetField.text, houseNumber: self.mainView.houseField.text, apartmentNumber: self.mainView.appartmentField.text) { (result) in
            switch result {
            case .success(let data):
                self.savedAddress = data
                let orderUser = self.order?.sender
                if orderUser?.uuid != "" {
                    UPAPIManager.addAddress(addressID: String(data.id!), isMain: true, UUID: orderUser!.uuid!, completion: { (result) in
                        switch result {
                        case .success(let user):
                            self.order?.sender = user
                            self.navigateNext()
                        case .partialSuccess(let string):
                            self.showAlert(with: string)
                        case .failure(let error):
                            self.showAlert(with: error.localizedDescription)
                        }
                    })
                } else {
                    self.order?.sender?.addressId = data.id
                    self.createUser()
                }
                
            case .partialSuccess(let string):
                self.showAlert(with: string)
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
            }
        }
    }
    
    func createUser() {
        
        UPAPIManager.createUser(user: (self.order?.sender)!) { (result) in
            switch result {
            case .success(let data):
                self.order?.sender = data
                self.navigateNext()
                
            case .partialSuccess(let string):
                self.showAlert(with: string)
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
            }
        }
    }


    func navigateNext() {
        if (self.order?.international)! {
            let vc = ForeignRecipientInfoViewController()
            vc.order = self.order
            vc.country = country
            self.navigationController?.pushViewController(vc, animated: true)

        } else {
            let vc = RecipientInfoViewController()
            vc.isExspress = self.isExspress
            vc.order = self.order
            if self.addressFrom != nil {
                vc.addressFrom = controller?.address
            } else {
                let postcode = mainView.indexField.text
                let newAddress = CityDetails(regionID: nil, regionName: nil, districtID: nil, districtName: nil, cityID: nil, cityName: nil, cityTypeID: nil, cityTypeName: nil, street: nil, house: nil, apartment: nil, addressID: nil, postcode: postcode)
                vc.addressFrom = newAddress
                vc.addressTo = self.addressTo
            }
            self.navigationController?.pushViewController(vc, animated: true)


        }
    }
}

extension SenderAddressViewController: AddressViewProtocol {
    func push(viewController: IndexSearchViewController) {
        view.endEditing(true)
        navigationController?.pushViewController(viewController, animated: true)
    }

    func hideKeyboard() {
        view.endEditing(true)
    }

    func showAlert(with text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))

        present(alert, animated: true, completion: nil)
        mainView.submitButton.isEnabled = true
    }
}
