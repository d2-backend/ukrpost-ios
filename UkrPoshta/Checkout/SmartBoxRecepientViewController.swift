//
//  SmartBoxRecepientViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/31/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase

class SmartBoxRecepientViewController: UIViewController, UITextFieldDelegate {

    var order: DomesticShipment?
    private var currentReceiver: User?
    var currentBox: SmartBox?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    
    private let mainView = SenderInfoView()

    override func viewDidLoad() {
        super.viewDidLoad()
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 3",
            AnalyticsParameterItemName: "ОВ_УКР_SmartBox_3 ",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_3 "
            ])
        hideBackNavigationButtonText()
        self.mainView.uuidHeader.isHidden = true
        self.mainView.uuidField.isHidden = true
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.userTypeHeader.text = NSLocalizedString("checkout.recipient.header", comment: "")

        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        self.mainView.submitButton.isEnabled = false
        
        self.mainView.isIndividualButton.addTarget(self, action: #selector(userTypePressed), for: .touchUpInside)
        self.mainView.isIndividualButton.isSelected = true
        self.mainView.isLegalButton.addTarget(self, action: #selector(userTypePressed), for: .touchUpInside)
        
        self.mainView.indPhoneField.text = UPPhoneHelper.displayStringFrom(rawString: "")
        self.mainView.corpPhoneField.text = UPPhoneHelper.displayStringFrom(rawString: "")
        self.mainView.indNameField.delegate = self
        self.mainView.indSurnameField.delegate = self
        self.mainView.indMiddleNameField.delegate = self
        self.mainView.indPhoneField.delegate = self
        
        self.mainView.corpNameField.delegate = self
        self.mainView.corpPhoneField.delegate = self
        self.mainView.corpIDField.delegate = self
        self.mainView.corpBankIDField.delegate = self
        self.mainView.corpAccountField.delegate = self
    }

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    @objc func userTypePressed() {
        self.mainView.isIndividualButton.isSelected = !self.mainView.isIndividualButton.isSelected
        self.mainView.isLegalButton.isSelected = !self.mainView.isLegalButton.isSelected
        self.mainView.layoutSubviews()
        self.redrawSubmitButton()
        
    }
    
    @objc func submitPressed() {
        var aUser = User()
        
        if self.currentReceiver != nil {
            aUser = self.currentReceiver!
        } else {
            if self.mainView.isIndividualButton.isSelected {
                aUser.firstName = self.mainView.indNameField.text
                aUser.lastName = self.mainView.indSurnameField.text
                aUser.middleName = self.mainView.indMiddleNameField.text
                aUser.phoneNumber = UPPhoneHelper.rawStringFrom(displayString:self.mainView.indPhoneField.text)
                aUser.type = APIUserType.individual.rawValue
                aUser.individual = true
                aUser.bankCode = nil
                aUser.bankAccount = nil
            } else {
                aUser.name = self.mainView.corpNameField.text
                aUser.phoneNumber = UPPhoneHelper.rawStringFrom(displayString: self.mainView.corpPhoneField.text)
                aUser.edrpou = self.mainView.corpIDField.text
                aUser.bankCode = self.mainView.corpBankIDField.text
                aUser.bankAccount = self.mainView.corpAccountField.text
                if self.mainView.corpIDField.text?.count == 8 {
                    aUser.edrpou = self.mainView.corpIDField.text
                    aUser.type = APIUserType.company.rawValue
                } else if self.mainView.corpIDField.text?.count == 10 {
                    aUser.tin = self.mainView.corpIDField.text
                    aUser.type = APIUserType.privateEnterpreneur.rawValue
                }
                aUser.individual = false
            }
        }
        
        let vc = SmartBoxRecepientAddressViewController()
        self.order?.recipient = aUser
        vc.order = self.order
        vc.addressTo = self.addressTo
        vc.addressFrom = self.addressFrom
        vc.currentBox = self.currentBox
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func redrawSubmitButton() {
        if self.mainView.isIndividualButton.isSelected {
            if self.mainView.indNameField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.indSurnameField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.indMiddleNameField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.indPhoneField.text!.count == 19 {
                self.mainView.submitButton.isEnabled = true
            } else {
                self.mainView.submitButton.isEnabled = false
            }
        } else {
            if self.mainView.corpNameField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpIDField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpBankIDField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpAccountField.text!.trimmingCharacters(in: .whitespaces) != "" && self.mainView.corpPhoneField.text!.count == 19 && (self.mainView.corpIDField.text!.count == 10 || self.mainView.corpIDField.text!.count == 8){
                self.mainView.submitButton.isEnabled = true
            } else {
                self.mainView.submitButton.isEnabled = false
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.redrawSubmitButton()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case mainView.uuidField:
            
            return true
            
        case  mainView.indPhoneField, mainView.corpPhoneField:
            let oldString = textField.text
            let newString = oldString! + string
            if (newString.count <= 6 || newString.count > 19) {
                return false
            } else {
                self.mainView.uuidField.text = ""
                self.currentReceiver = nil
                var oldRawString = UPPhoneHelper.rawStringFrom(displayString: oldString!)
                
                switch string {
                case "":
                    oldRawString!.removeLast()
                case "0","1","2","3","4","5","6","7","8","9" :
                    oldRawString = oldRawString! + string
                    
                default :
                    return false
                }
                textField.text = UPPhoneHelper.displayStringFrom(rawString: oldRawString)
            }
            
            return false
        case mainView.corpBankIDField :
            switch string {
            case "" :
                return true
            case "0","1","2","3","4","5","6","7","8","9" :
                let newString = textField.text! + string
                if newString.count < 7 {
                    textField.text = newString
                }
                return false
            default :
                return false
            }
        case mainView.corpIDField:
            switch string {
            case "" :
                return true
            case "0","1","2","3","4","5","6","7","8","9" :
                let newString = textField.text! + string
                if newString.count < 11 {
                    textField.text = newString
                }
                return false
            default :
                return false
            }
        case mainView.corpAccountField:
            switch string {
            case "" :
                return true
            case "0","1","2","3","4","5","6","7","8","9" :
                let newString = textField.text! + string
                if newString.count < 15 {
                    textField.text = newString
                }
                return false
            default :
                return false
            }
        default:
            self.mainView.uuidField.text = ""
            self.currentReceiver = nil
            return true
        }
    }
}
