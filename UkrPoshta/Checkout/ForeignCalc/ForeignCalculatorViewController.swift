//
//  ForeignCalculatorViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/29/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignCalculatorViewController: UIViewController, UITextFieldDelegate {

    private let mainView = ForeignCalculatorView()
    var country: Country?
    var order: DomesticShipment?
    private var currency: Currency? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))
        
        self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.aviaButton.addTarget(self, action: #selector(typePressed), for: .touchUpInside)
        self.mainView.aviaButton.isSelected = true
        
        self.mainView.carButton.addTarget(self, action: #selector(typePressed), for: .touchUpInside)
        self.mainView.carButton.isSelected = false
        
        mainView.waitNReturn.addTarget(self, action: #selector(returnTypePressed(sender:)), for: .touchUpInside)
        mainView.returnAtOnce.addTarget(self, action: #selector(returnTypePressed(sender:)), for: .touchUpInside)
        mainView.reject.addTarget(self, action: #selector(returnTypePressed(sender:)), for: .touchUpInside)
        mainView.waitNReturn.isSelected = false
        mainView.returnAtOnce.isSelected = false
        mainView.reject.isSelected = true
        
        mainView.predefinedValue.addTarget(self, action: #selector(predefinedPressed), for: .touchUpInside)
        mainView.predefinedValue.isSelected = false
        
        mainView.waitNReturnField.delegate = self
        mainView.predefinedValueField.delegate = self
        
        mainView.submit.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        getPrice()
    }
    
    @objc func predefinedPressed() {
        mainView.predefinedValue.isSelected = !mainView.predefinedValue.isSelected
        redrawSubmit()
    }
    
    @objc func returnTypePressed(sender: UIButton) {
        mainView.waitNReturn.isSelected = false
        mainView.returnAtOnce.isSelected = false
        mainView.reject.isSelected = false
        
        sender.isSelected = true
        redrawSubmit()
    }
    
    @objc func typePressed() {
        self.mainView.aviaButton.isSelected = !self.mainView.aviaButton.isSelected
        self.mainView.carButton.isSelected = !self.mainView.carButton.isSelected
        fetchPrice()
    }
    
    @objc func closeTapped() {
        let vc = MainViewController()
        vc.isAnimationNeeded = false
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.shadowImage = UIImage()
        nc.navigationBar.isTranslucent = false
        nc.navigationBar.tintColor = UPColor.darkGray
        self.present(nc, animated: true, completion: nil)
    }
    
    @objc func submitPressed() {
        
        if mainView.waitNReturn.isSelected {
            order?.internationalData?.daysForReturn = Int(mainView.waitNReturnField.text!) ?? 30
            order?.onFailReceiveType = "RETURN"
        } else if mainView.returnAtOnce.isSelected{
            order?.onFailReceiveType = "RETURN_IMMEDIATELY"
        } else if mainView.reject.isSelected {
            order?.onFailReceiveType = "PROCESS_AS_REFUSAL"
        }
        
        if mainView.predefinedValue.isSelected && mainView.predefinedValueField.text != "" {
            order?.declaredPrice = Int(mainView.predefinedValueField.text!) ?? 0
            var parcel = order?.parcels?.first
            parcel?.declaredPrice = Int(mainView.predefinedValueField.text!) ?? 0
            
            order?.parcels = [parcel!]
        }
        
        order?.internationalData?.transportType = mainView.aviaButton.isSelected ? TransportType.avia.rawValue : TransportType.ground.rawValue
        UPAPIManager.submitDomesticOrder(order: self.order!) { (result) in
            switch result {
            case .success(let data):
                if data.barcode != nil && data.uuid != nil {
                    let vc = CheckoutResultViewController()
                    vc.order = data
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showAlertWith(text:"")
                }
                
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
        
    }
    
    func getPrice() {
        UPAPIManager.getCurrency { (result) in
            switch result {
            case .success(let currency):
                self.currency = currency
                self.fetchPrice()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }

    func fetchPrice() {
        
        let info = DeliveryInternationalPrice(deliveryPrice: nil,
                                              weight: order?.weight,
                                              length: Float((order?.length!)!),
                                              calculationDescription: nil,
                                              packageType: order?.packageType,
                                              discountRate: nil,
                                              currencyExchangeRate: self.currency?.rate ,
                                              currencyCode: self.currency?.cc,
                                              recipientCountryIso3166: country?.code,
                                              declaredPrice: nil,
                                              deliveryPriceCurr: nil,
                                              recommended: false,
                                              bulky: false,
                                              fragile: false,
                                              transportType: mainView.aviaButton.isSelected ? TransportType.avia.rawValue : TransportType.ground.rawValue,
                                              withDeliveryNotification: false,
                                              byCourier: false,
                                              w2d: nil,
                                              cancelOrChange: nil,
                                              consignment: nil)
        
        UPAPIManager.getForeignPrices(info: info) { (result) in
            switch result {
            case .success(let price):
                print(price)
                self.mainView.resultLabel.text = "\(price.deliveryPrice ?? 0.0)" + " грн"
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func redrawSubmit() {
        if mainView.waitNReturn.isSelected && mainView.waitNReturnField.text == "" {
            mainView.submit.isEnabled = false
        } else if mainView.predefinedValue.isSelected && mainView.predefinedValueField.text == "" {
            mainView.submit.isEnabled = false
        } else {
            mainView.submit.isEnabled = true
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        redrawSubmit()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var oldString = textField.text
        var newString: String!
        
        switch string {
        case "":
            oldString!.removeLast()
            newString = oldString
        case "0","1","2","3","4","5","6","7","8","9" :
            newString = oldString! + string
        default :
            return false
        }
        
        if textField == mainView.waitNReturnField {
            let currentInt = Int(newString) ?? 0
            
            if currentInt > 30 {
                newString = "30"
            }
        }
        
        textField.text = newString
        return false
    }
    

}
