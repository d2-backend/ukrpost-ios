//
//  ForeignCalculatorView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/29/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignCalculatorView: UIView {

    private let header = UPHeader()
    let aviaButton = UPHorizontalButton(normalImageName: "avia", selectedImageName: "aviaSelected")
    let carButton = UPHorizontalButton(normalImageName: "car", selectedImageName: "carSelected")
    
    private let returnHeader = UPHeader()
    let waitNReturn = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    private let waitNReturnLabel = UILabel()
    let waitNReturnField = UPTextField()
    let returnAtOnce = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let reject = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    private let line = UIView()

    let predefinedValue = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let predefinedValueField = UPTextField()
    
    private let resultView = UIView()
    private let resultHeader = UILabel()
    let resultLabel = UILabel()
    
    let submit = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        header.text = NSLocalizedString("calculator.typeForeign.header", comment: "")
        addSubview(header)
        
        aviaButton.label.text = NSLocalizedString("calculator.type.avia", comment: "")
        addSubview(aviaButton)
        
        carButton.label.text = NSLocalizedString("calculator.type.ground", comment: "")
        addSubview(carButton)
        
        returnHeader.text = NSLocalizedString("checkout.options.return.header", comment: "")
        addSubview(returnHeader)
        
        waitNReturnField.textAlignment = .right
        addSubview(waitNReturnField)
        waitNReturn.label.text = NSLocalizedString("checkout.calc.waitNReturn", comment: "")
        waitNReturn.label.textAlignment = .left
        waitNReturn.backgroundColor = .clear
        addSubview(waitNReturn)
        waitNReturnLabel.font = UIFont.systemFont(ofSize: 10)
        waitNReturnLabel.textColor = UPColor.lightGray
        waitNReturnLabel.text = NSLocalizedString("checkout.calc.waitNReturn.label", comment: "")
        waitNReturnLabel.textAlignment = .center
        waitNReturnLabel.numberOfLines = 0
        waitNReturnLabel.lineBreakMode = .byWordWrapping
        addSubview(waitNReturnLabel)
        
        returnAtOnce.label.text = NSLocalizedString("checkout.calc.returnAtOnce", comment: "")
        returnAtOnce.label.textAlignment = .left
        returnAtOnce.backgroundColor = .clear
        addSubview(returnAtOnce)
        
        reject.label.text = NSLocalizedString("checkout.calc.reject", comment: "")
        reject.label.textAlignment = .left
        reject.backgroundColor = .clear
        addSubview(reject)
        
        line.backgroundColor = UPColor.separatorGray
        addSubview(line)
        
        predefinedValueField.textAlignment = .right
        addSubview(predefinedValueField)
        
        predefinedValue.label.text = NSLocalizedString("checkout.calc.predefined", comment: "")
        predefinedValue.label.textAlignment = .left
        predefinedValue.backgroundColor = .clear
        addSubview(predefinedValue)
        
        resultView.backgroundColor = .white
        addSubview(resultView)
        
        resultHeader.text = NSLocalizedString("checkout.price.result", comment: "")
        let aFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        let boldFont = UIFont(descriptor: aFont.fontDescriptor.withSymbolicTraits(.traitBold)!, size: aFont.pointSize)
        resultHeader.font = boldFont
        resultHeader.textColor = UPColor.darkGray
        resultView.addSubview(resultHeader)
        
        resultLabel.text = "..."
        resultLabel.font = boldFont
        resultLabel.textColor = UPColor.darkGray
        resultLabel.textAlignment = .right
        resultView.addSubview(resultLabel)

        submit.setTitle(NSLocalizedString("main.parcelRegistrationButton.title", comment: ""), for: .normal)
        addSubview(submit)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        header.pin.top(20).horizontally(20).height(15)
        let buttonWidth = (UIScreen.main.bounds.size.width - 50) / 2
        
        aviaButton.pin.top(to: header.edge.bottom).marginTop(10).left(20).height(60).width(buttonWidth)
        carButton.pin.top(to: header.edge.bottom).marginTop(10).right(20).height(60).width(buttonWidth)
        returnHeader.pin.top(to: aviaButton.edge.bottom).marginTop(10).horizontally(20).height(15)
        
        waitNReturnField.pin.top(to: returnHeader.edge.bottom).marginTop(10).right(20).height(60).width(80)
        waitNReturn.pin.top(to: returnHeader.edge.bottom).marginTop(10).left(10).right(to: waitNReturnField.edge.left).height(60)
        waitNReturnLabel.pin.right(to: waitNReturnField.edge.left).height(25).left(40).bottom(to: waitNReturn.edge.bottom).height(15)
        
        returnAtOnce.pin.top(to: waitNReturnField.edge.bottom).marginTop(10).horizontally(10).height(30)
        reject.pin.top(to: returnAtOnce.edge.bottom).marginTop(10).horizontally(10).height(30)
        
        line.pin.top(to: reject.edge.bottom).marginTop(10).horizontally(20).height(1)
        predefinedValueField.pin.top(to: line.edge.bottom).marginTop(10).right(20).height(60).width(80)
        predefinedValue.pin.top(to: line.edge.bottom).marginTop(10).left(10).height(60).right(to: predefinedValueField.edge.left)
        
        resultView.pin.horizontally().bottom(128).height(64)
        resultHeader.pin.vertically().left(20).width(150)
        resultLabel.pin.vertically().right(20).left(to: resultHeader.edge.right)
        
        submit.pin.horizontally().bottom(64).height(64)
    }


}
