//
//  ContentViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ContentViewController: UPViewController, UITextFieldDelegate {

    private let mainView = ContentView()

    private var items = [ContentItem]()
    var country: Country?
    var order: DomesticShipment?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))
        
        self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        mainView.buttonA.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
        mainView.buttonA.isSelected = true
        mainView.isFirstExpanded = true
        mainView.extendedViewA.nameField.delegate = self
        mainView.extendedViewA.nameField.delete.addTarget(self, action: #selector(deleteA), for: .touchUpInside)
        mainView.extendedViewA.weightField.delegate = self
        mainView.extendedViewA.valueField.delegate = self
        
        mainView.buttonB.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
        mainView.buttonB.isSelected = false
        mainView.extendedViewB.nameField.delegate = self
        mainView.extendedViewB.nameField.delete.addTarget(self, action: #selector(deleteB), for: .touchUpInside)

        mainView.extendedViewB.weightField.delegate = self
        mainView.extendedViewB.valueField.delegate = self
        
        mainView.buttonC.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
        mainView.extendedViewC.nameField.delegate = self
        mainView.extendedViewC.nameField.delete.addTarget(self, action: #selector(deleteC), for: .touchUpInside)

        mainView.extendedViewC.weightField.delegate = self
        mainView.extendedViewC.valueField.delegate = self
        mainView.buttonC.isSelected = false
        mainView.buttonC.isEnabled = false
        
        
        mainView.buttonD.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
        mainView.extendedViewD.nameField.delegate = self
        mainView.extendedViewD.nameField.delete.addTarget(self, action: #selector(deleteD), for: .touchUpInside)

        mainView.extendedViewD.weightField.delegate = self
        mainView.extendedViewD.valueField.delegate = self
        mainView.buttonD.isSelected = false
        mainView.buttonD.isEnabled = false

        mainView.submit.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        redrawSubmit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func submitPressed() {
        let vc = ForeignTypeViewController()
        vc.country = country

        
            var summ = 0
            for item in items {
                summ += item.value!
            }
        if order?.packageType == APIPackageType.parcel.rawValue || order?.packageType == APIPackageType.smallBag.rawValue {
            let parcel = ParcelItem(name: order?.packageType, weight: order?.weight, length: order?.length, declaredPrice: nil, parcelItems: items)
            order?.parcels = [parcel]

        } else {
            let parcel = ParcelItem(name: order?.packageType, weight: order?.weight, length: order?.length, declaredPrice: nil, parcelItems: nil)
            order?.parcels = [parcel]

        }
        
        vc.order = order
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func redrawSubmit() {
        if order?.packageType == APIPackageType.parcel.rawValue || order?.packageType == APIPackageType.smallBag.rawValue{
            if items.count > 0 {
                mainView.submit.isEnabled = true
            } else {
                mainView.submit.isEnabled = false
            }
        } else {
            mainView.submit.isEnabled = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case mainView.extendedViewA.valueField, mainView.extendedViewA.weightField, mainView.extendedViewB.valueField, mainView.extendedViewB.weightField, mainView.extendedViewC.valueField, mainView.extendedViewC.weightField, mainView.extendedViewD.valueField, mainView.extendedViewD.weightField :

            var oldString = textField.text
            var newString: String!
            switch string {
            case "":
                oldString!.removeLast()
                newString = oldString
            case "0","1","2","3","4","5","6","7","8","9" :
                newString = oldString! + string
            case ".", ",":
                if textField == mainView.extendedViewA.weightField || textField == mainView.extendedViewB.weightField || textField == mainView.extendedViewC.weightField || textField == mainView.extendedViewD.weightField{
                    if oldString?.range(of: ".") != nil {
                        newString = oldString
                    } else {
                        newString = oldString! + "."
                    }
                    if newString == "." {
                        newString = "0."
                    }
                } else {
                    return false
                }
            default :
                return false
            }

            textField.text = newString
            return false
        default:
            return true
        }
    }
    
    func weightTextToInt(_ text: String) -> Int {
        let float = Float(text)!
        let newFloat = float * 1000
        let result = Int(newFloat)
        return result
    }
    
    func weightIntToText(_ int: Int) -> String {
        let float = Float(int) / 1000
        let result = float.description
        return result
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case mainView.extendedViewA.nameField, mainView.extendedViewA.weightField, mainView.extendedViewA.valueField:
            if mainView.extendedViewA.nameField.text != "" && mainView.extendedViewA.weightField.text != "" {
                
                let firstItem = ContentItem(name: mainView.extendedViewA.nameField.text, description: mainView.extendedViewA.nameField.text, quantity: 1, weight: weightTextToInt(mainView.extendedViewA.weightField.text!), value: Int(mainView.extendedViewA.valueField.text!) ?? 0, countryOfOrigin: nil)
                self.items.append(firstItem)
                redrawItems()

            }
        case mainView.extendedViewB.nameField, mainView.extendedViewB.weightField, mainView.extendedViewB.valueField:
            if mainView.extendedViewB.nameField.text != "" && mainView.extendedViewB.weightField.text != "" {
                //mainView.buttonC.isSelected = true
                //mainView.buttonC.isEnabled = true
                let item = ContentItem(name: mainView.extendedViewB.nameField.text, description: mainView.extendedViewB.nameField.text, quantity: 1, weight: weightTextToInt(mainView.extendedViewB.weightField.text!), value: Int(mainView.extendedViewB.valueField.text!) ?? 0, countryOfOrigin: nil)
                self.items.append(item)
                redrawItems()

            }
        case mainView.extendedViewC.nameField, mainView.extendedViewC.weightField, mainView.extendedViewC.valueField:
            if mainView.extendedViewC.nameField.text != "" && mainView.extendedViewC.weightField.text != "" {
                //mainView.buttonD.isSelected = true
                //mainView.buttonD.isEnabled = true

                let item = ContentItem(name: mainView.extendedViewC.nameField.text, description: mainView.extendedViewC.nameField.text, quantity: 1, weight: weightTextToInt(mainView.extendedViewC.weightField.text!), value: Int(mainView.extendedViewC.valueField.text!) ?? 0, countryOfOrigin: nil)
                self.items.append(item)
                redrawItems()

            }
        case mainView.extendedViewD.nameField, mainView.extendedViewD.weightField, mainView.extendedViewD.valueField:
            if mainView.extendedViewD.nameField.text != "" && mainView.extendedViewD.weightField.text != "" {
                //mainView.buttonC.isSelected = true
                //mainView.buttonD.isEnabled = true
                let item = ContentItem(name: mainView.extendedViewD.nameField.text, description: mainView.extendedViewD.nameField.text, quantity: 1, weight: weightTextToInt(mainView.extendedViewD.weightField.text!), value: Int(mainView.extendedViewD.valueField.text!) ?? 0, countryOfOrigin: nil)
                self.items.append(item)
                redrawItems()

            }
        default:
            return
        }
        //mainView.layoutSubviews()
        redrawSubmit()
    }
    
    @objc func closeTapped() {
        let vc = MainViewController()
        vc.isAnimationNeeded = false
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.shadowImage = UIImage()
        nc.navigationBar.isTranslucent = false
        nc.navigationBar.tintColor = UPColor.darkGray
        self.present(nc, animated: true, completion: nil)
    }
    
    @objc func buttonPressed(button: UIButton) {
        if button.isSelected {
            mainView.expand(button: button)
        }
    }
    
    @objc func deleteA() {
        if items.count > 0 {
            items.remove(at: 0)
        }
        redrawItems()
    }
    
    @objc func deleteB() {
        if items.count > 1 {
            items.remove(at: 1)
        }
        redrawItems()
    }
    
    @objc func deleteC() {
        if items.count > 2 {
            items.remove(at: 2)
        }
        redrawItems()
    }
    
    @objc func deleteD() {
        if items.count > 3 {
            items.remove(at: 3)
        }
        redrawItems()
    }
    
    func redrawItems() {
        redrawSubmit()
        if items.count == 0 {
            mainView.extendedViewA.nameField.text = ""
            mainView.extendedViewA.weightField.text = ""
            mainView.extendedViewA.valueField.text = ""
            
            mainView.extendedViewB.nameField.text = ""
            mainView.extendedViewB.weightField.text = ""
            mainView.extendedViewB.valueField.text = ""
            
            mainView.extendedViewC.nameField.text = ""
            mainView.extendedViewC.weightField.text = ""
            mainView.extendedViewC.valueField.text = ""
            
            mainView.extendedViewD.nameField.text = ""
            mainView.extendedViewD.weightField.text = ""
            mainView.extendedViewD.valueField.text = ""
            
            mainView.buttonA.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            mainView.buttonB.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            mainView.buttonC.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            mainView.buttonD.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            
            mainView.isFirstExpanded = true
            mainView.isSecondExpanded = false
            mainView.isThirdExpanded = false
            mainView.isFourthExpanded = false
            
            mainView.buttonA.isSelected = true
            mainView.buttonB.isSelected = false
            mainView.buttonC.isSelected = false
            mainView.buttonC.isEnabled = false
            mainView.buttonD.isSelected = false
            mainView.buttonD.isEnabled = false
        }
        
        if items.count == 1 {

            mainView.extendedViewA.nameField.text = items.first?.name
            let weightA = weightIntToText((items.first?.weight)!)
            mainView.extendedViewA.weightField.text = "\(weightA)"
            let valueA = (items.first?.value)!
            mainView.extendedViewA.valueField.text = "\(valueA)"
            
            mainView.extendedViewB.nameField.text = ""
            mainView.extendedViewB.weightField.text = ""
            mainView.extendedViewB.valueField.text = ""
            
            mainView.extendedViewC.nameField.text = ""
            mainView.extendedViewC.weightField.text = ""
            mainView.extendedViewC.valueField.text = ""
            
            mainView.extendedViewD.nameField.text = ""
            mainView.extendedViewD.weightField.text = ""
            mainView.extendedViewD.valueField.text = ""
            
            let title = "\(mainView.extendedViewA.nameField.text ?? ""), \(mainView.extendedViewA.weightField.text ?? "") кг, \(mainView.extendedViewA.valueField.text ?? "") грн"
            mainView.buttonA.label.text = title

            mainView.buttonB.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            mainView.buttonC.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            mainView.buttonD.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            
            mainView.isFirstExpanded = true
            mainView.isSecondExpanded = false
            mainView.isThirdExpanded = false
            mainView.isFourthExpanded = false
            
            mainView.buttonA.isSelected = true
            mainView.buttonB.isSelected = true
            mainView.buttonC.isSelected = false
            mainView.buttonC.isEnabled = true
            mainView.buttonD.isSelected = false
            mainView.buttonD.isEnabled = false
        }
        
        if items.count == 2 {
            
            mainView.extendedViewA.nameField.text = items.first?.name
            let weightA = weightIntToText((items.first?.weight)!)
            mainView.extendedViewA.weightField.text = "\(weightA)"
            let valueA = (items.first?.value)!
            mainView.extendedViewA.valueField.text = "\(valueA)"
            
            mainView.extendedViewB.nameField.text = items[1].name
            let weightB = weightIntToText((items[1].weight)!)
            mainView.extendedViewB.weightField.text = "\(weightB)"
            let valueB = (items[1].value)!
            mainView.extendedViewB.valueField.text = "\(valueB)"
            
            mainView.extendedViewC.nameField.text = ""
            mainView.extendedViewC.weightField.text = ""
            mainView.extendedViewC.valueField.text = ""
            
            mainView.extendedViewD.nameField.text = ""
            mainView.extendedViewD.weightField.text = ""
            mainView.extendedViewD.valueField.text = ""
            
            let titleA = "\(mainView.extendedViewA.nameField.text ?? ""), \(mainView.extendedViewA.weightField.text ?? "") кг, \(mainView.extendedViewA.valueField.text ?? "") грн"
            mainView.buttonA.label.text = titleA
            
            let titleB = "\(mainView.extendedViewB.nameField.text ?? ""), \(mainView.extendedViewB.weightField.text ?? "") кг, \(mainView.extendedViewB.valueField.text ?? "") грн"
            mainView.buttonB.label.text = titleB
            mainView.buttonC.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            mainView.buttonD.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            
            mainView.buttonA.isSelected = true
            mainView.buttonB.isSelected = true
            mainView.buttonC.isSelected = true
            mainView.buttonC.isEnabled = true
            mainView.buttonD.isSelected = false
            mainView.buttonD.isEnabled = true
            
            mainView.isFirstExpanded = false
            mainView.isSecondExpanded = true
            mainView.isThirdExpanded = false
            mainView.isFourthExpanded = false
        }
        
        if items.count == 3 {
        /////
            mainView.extendedViewA.nameField.text = items.first?.name
            let weightA = weightIntToText((items.first?.weight)!)
            mainView.extendedViewA.weightField.text = "\(weightA)"
            let valueA = (items.first?.value)!
            mainView.extendedViewA.valueField.text = "\(valueA)"
            
            let titleA = "\(mainView.extendedViewA.nameField.text ?? ""), \(mainView.extendedViewA.weightField.text ?? "") кг, \(mainView.extendedViewA.valueField.text ?? "") грн"
            mainView.buttonA.label.text = titleA
        /////
            mainView.extendedViewB.nameField.text = items[1].name
            let weightB = weightIntToText((items[1].weight)!)
            mainView.extendedViewB.weightField.text = "\(weightB)"
            let valueB = (items[1].value)!
            mainView.extendedViewB.valueField.text = "\(valueB)"
            
            let titleB = "\(mainView.extendedViewB.nameField.text ?? ""), \(mainView.extendedViewB.weightField.text ?? "") кг, \(mainView.extendedViewB.valueField.text ?? "") грн"
            mainView.buttonB.label.text = titleB
       //////
            mainView.extendedViewC.nameField.text = items[2].name
            let weightC = weightIntToText((items[2].weight)!)
            mainView.extendedViewC.weightField.text = "\(weightC)"
            let valueC = (items[2].value)!
            mainView.extendedViewC.valueField.text = "\(valueC)"
            
            let titleC = "\(mainView.extendedViewC.nameField.text ?? ""), \(mainView.extendedViewC.weightField.text ?? "") кг, \(mainView.extendedViewC.valueField.text ?? "") грн"
            mainView.buttonC.label.text = titleC
        /////
            mainView.extendedViewD.nameField.text = ""
            mainView.extendedViewD.weightField.text = ""
            mainView.extendedViewD.valueField.text = ""
            
            mainView.buttonD.label.text = NSLocalizedString("checkout.add.content.button", comment: "")
            
            mainView.buttonA.isSelected = true
            mainView.buttonB.isSelected = true
            
            mainView.buttonC.isSelected = true
            mainView.buttonC.isEnabled = true
            
            mainView.buttonD.isSelected = true
            mainView.buttonD.isEnabled = true

            mainView.isFirstExpanded = false
            mainView.isSecondExpanded = false
            mainView.isThirdExpanded = true
            mainView.isFourthExpanded = false
        }
        
        
        if items.count == 4 {
            /////
            mainView.extendedViewA.nameField.text = items.first?.name
            let weightA = weightIntToText((items.first?.weight)!)
            mainView.extendedViewA.weightField.text = "\(weightA)"
            let valueA = (items.first?.value)!
            mainView.extendedViewA.valueField.text = "\(valueA)"
            
            let titleA = "\(mainView.extendedViewA.nameField.text ?? ""), \(mainView.extendedViewA.weightField.text ?? "") кг, \(mainView.extendedViewA.valueField.text ?? "") грн"
            mainView.buttonA.label.text = titleA
            /////
            mainView.extendedViewB.nameField.text = items[1].name
            let weightB = weightIntToText((items[1].weight)!)
            mainView.extendedViewB.weightField.text = "\(weightB)"
            let valueB = (items[1].value)!
            mainView.extendedViewB.valueField.text = "\(valueB)"
            
            let titleB = "\(mainView.extendedViewB.nameField.text ?? ""), \(mainView.extendedViewB.weightField.text ?? "") кг, \(mainView.extendedViewB.valueField.text ?? "") грн"
            mainView.buttonB.label.text = titleB
            //////
            mainView.extendedViewC.nameField.text = items[2].name
            let weightC = weightIntToText((items[2].weight)!)
            mainView.extendedViewC.weightField.text = "\(weightC)"
            let valueC = (items[2].value)!
            mainView.extendedViewC.valueField.text = "\(valueC)"
            
            let titleC = "\(mainView.extendedViewC.nameField.text ?? ""), \(mainView.extendedViewC.weightField.text ?? "") кг, \(mainView.extendedViewC.valueField.text ?? "") грн"
            mainView.buttonC.label.text = titleC
            /////
            mainView.extendedViewD.nameField.text = items[3].name
            let weightD = weightIntToText((items[3].weight)!)
            mainView.extendedViewD.weightField.text = "\(weightD)"
            let valueD = (items[3].value)!
            mainView.extendedViewD.valueField.text = "\(valueD)"
            
            let titleD = "\(mainView.extendedViewD.nameField.text ?? ""), \(mainView.extendedViewD.weightField.text ?? "") кг, \(mainView.extendedViewD.valueField.text ?? "") грн"
            mainView.buttonD.label.text = titleD
            
            mainView.buttonA.isSelected = true
            mainView.buttonB.isSelected = true
            
            mainView.buttonC.isSelected = true
            mainView.buttonC.isEnabled = true
            
            mainView.buttonD.isSelected = true
            mainView.buttonD.isEnabled = true
            
            mainView.isFirstExpanded = false
            mainView.isSecondExpanded = false
            mainView.isThirdExpanded = false
            mainView.isFourthExpanded = true
        }
        mainView.layoutSubviews()
    }

}
