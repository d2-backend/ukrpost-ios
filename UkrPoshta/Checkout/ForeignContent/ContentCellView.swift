//
//  ContentCellView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ContentCellView: UIView {
    
    private let header = UPHeader()
    let nameField = UPTextFieldWithDelete()
    private let weightLabel = UILabel()
    let weightField = UPTextField()
    
    private let valueLabel = UILabel()
    let valueField = UPTextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        //self.backgroundColor = UIColor.green
        
        header.text = NSLocalizedString("checkout.content.header", comment: "")
        addSubview(header)
        nameField.placeholder = NSLocalizedString("checkout.content.name", comment: "")
        nameField.addSubmitOnKeyboard()
        
        addSubview(nameField)
        
        weightLabel.text = NSLocalizedString("checkout.content.weight", comment: "")
        weightLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        weightLabel.textColor = UPColor.darkGray
        addSubview(weightLabel)
        
        weightField.keyboardType = .decimalPad
        weightField.addSubmitOnKeyboard()
        addSubview(weightField)
        
        valueLabel.text = NSLocalizedString("checkout.content.value", comment: "")
        valueLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        valueLabel.textColor = UPColor.darkGray
        addSubview(valueLabel)
        valueField.keyboardType = .phonePad
        valueField.addSubmitOnKeyboard()
        addSubview(valueField)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        header.pin.top(10).horizontally().height(15)
        nameField.pin.top(to: header.edge.bottom).marginTop(10).horizontally().height(50)
        weightField.pin.top(to: nameField.edge.bottom).marginTop(20).right().height(50).width(100)
        weightLabel.pin.top(to: nameField.edge.bottom).marginTop(20).left().right(to: weightField.edge.left).height(50)
        
        valueField.pin.top(to: weightField.edge.bottom).marginTop(10).right().height(50).width(100)
        valueLabel.pin.top(to: weightField.edge.bottom).marginTop(10).left().right(to: valueField.edge.left).height(50)
    }

}
