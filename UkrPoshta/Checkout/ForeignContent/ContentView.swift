//
//  ContentView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ContentView: UIView {

    private let scrollView = UIScrollView()
    
    let itemA = UIView()
    let itemB = UIView()
    let itemC = UIView()
    let itemD = UIView()

    
    let buttonA = UPHorizontalButton(normalImageName: "chevron_down", selectedImageName: "chevron_down", imgAlignment: .right)
    let buttonB = UPHorizontalButton(normalImageName: "chevron_down", selectedImageName: "chevron_down", imgAlignment: .right)
    let buttonC = UPHorizontalButton(normalImageName: "chevron_down", selectedImageName: "chevron_down", imgAlignment: .right)
    let buttonD = UPHorizontalButton(normalImageName: "chevron_down", selectedImageName: "chevron_down", imgAlignment: .right)

    var isFirstExpanded: Bool = false
    var isSecondExpanded: Bool = false
    var isThirdExpanded: Bool = false
    var isFourthExpanded: Bool = false
    
    let extendedViewA = ContentCellView()
    let extendedViewB = ContentCellView()
    let extendedViewC = ContentCellView()
    let extendedViewD = ContentCellView()
    
    private let lineA = UIView()
    private let lineB = UIView()
    private let lineC = UIView()
    
    let submit = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        addSubview(scrollView)

        scrollView.addSubview(itemA)
        scrollView.addSubview(itemB)
        scrollView.addSubview(itemC)
        scrollView.addSubview(itemD)

        let buttonPlaceholder = NSLocalizedString("checkout.add.content.button", comment: "")
        buttonA.label.text = buttonPlaceholder
        buttonA.label.textAlignment = .left
        buttonA.backgroundColor = .clear
        buttonA.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        itemA.addSubview(buttonA)
        lineA.backgroundColor = UPColor.separatorGray
        itemA.addSubview(lineA)
        
        buttonB.label.text = buttonPlaceholder
        buttonB.label.textAlignment = .left
        buttonB.backgroundColor = .clear
        buttonB.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)

        itemB.addSubview(buttonB)
        
        lineB.backgroundColor = UPColor.separatorGray
        itemB.addSubview(lineB)
        
        buttonC.label.text = buttonPlaceholder
        buttonC.label.textAlignment = .left
        buttonC.backgroundColor = .clear
        buttonC.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)

        itemC.addSubview(buttonC)
        lineC.backgroundColor = UPColor.separatorGray
        itemC.addSubview(lineC)
        
        buttonD.label.text = buttonPlaceholder
        buttonD.label.textAlignment = .left
        buttonD.backgroundColor = .clear
        buttonD.label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)

        itemD.addSubview(buttonD)
        
        itemA.addSubview(extendedViewA)
        itemB.addSubview(extendedViewB)
        itemC.addSubview(extendedViewC)
        itemD.addSubview(extendedViewD)
        
        submit.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        addSubview(submit)


    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        submit.pin.horizontally().bottom(64).height(64)
        scrollView.pin.top().bottom(to: submit.edge.top).marginBottom(5).horizontally()
//        let extendedViewHeight: CGFloat = 230
        //highlightCell()
        layoutCell()
//        let firstItem: UIView
//        if isFirstExpanded {
//            buttonA.isHidden = true
//            extendedViewA.isHidden = false
//
//            firstItem = extendedViewA
//            firstItem.pin.top(20).horizontally(20).height(extendedViewHeight)
//        } else {
//            buttonA.isHidden = false
//            extendedViewA.isHidden = true
//            firstItem = buttonA
//            firstItem.pin.top(20).horizontally(20).height(50)
//        }
//
//        let secondItem: UIView
//        if isSecondExpanded {
//            extendedViewB.isHidden = false
//            buttonB.isHidden = true
//            secondItem = extendedViewB
//            secondItem.pin.top(to: firstItem.edge.bottom).marginTop(10).horizontally(20).height(extendedViewHeight)
//        } else {
//            extendedViewB.isHidden = true
//            buttonB.isHidden = false
//            secondItem = buttonB
//            secondItem.pin.top(to: firstItem.edge.bottom).marginTop(10).horizontally(20).height(50)
//        }
//
//        let thirdItem: UIView
//        if isThirdExpanded {
//            extendedViewC.isHidden = false
//            buttonC.isHidden = true
//            thirdItem = extendedViewC
//            thirdItem.pin.top(to: secondItem.edge.bottom).marginTop(10).horizontally(20).height(extendedViewHeight)
//        } else {
//            extendedViewC.isHidden = true
//            buttonC.isHidden = false
//            thirdItem = buttonC
//            thirdItem.pin.top(to: secondItem.edge.bottom).marginTop(10).horizontally(20).height(50)
//        }
//
//        let fourthItem: UIView
//        if isFourthExpanded {
//            extendedViewD.isHidden = false
//            buttonD.isHidden = true
//            fourthItem = extendedViewD
//            fourthItem.pin.top(to: thirdItem.edge.bottom).marginTop(10).horizontally(20).height(extendedViewHeight)
//        } else {
//            extendedViewD.isHidden = true
//            buttonD.isHidden = false
//            fourthItem = buttonD
//            fourthItem.pin.top(to: thirdItem.edge.bottom).marginTop(10).horizontally(20).height(50)
//        }
        
//        if !buttonC.isEnabled {
//            thirdItem.isHidden = true
//            fourthItem.isHidden = true
//            scrollView.contentSize = CGSize(width: self.frame.size.width, height: secondItem.frame.maxY)
//        } else if !buttonD.isEnabled {
//            thirdItem.isHidden = false
//            fourthItem.isHidden = true
//            scrollView.contentSize = CGSize(width: self.frame.size.width, height: thirdItem.frame.maxY)
//        } else {
//            thirdItem.isHidden = false
//            fourthItem.isHidden = false
//            scrollView.contentSize = CGSize(width: self.frame.size.width, height: fourthItem.frame.maxY)
//        }
        

    }

    private func layoutCell() {
        let extendedViewHeight: CGFloat = 230
        let buttonHeight: CGFloat = 70

        if isFirstExpanded {
            buttonA.alpha = 0.0
            extendedViewA.alpha = 1.0
            itemA.pin.top(20).horizontally(20).height(extendedViewHeight)
            extendedViewA.pin.all()
        } else {
            buttonA.alpha = 1.0
            extendedViewA.alpha = 0.0
            itemA.pin.top(20).horizontally(20).height(buttonHeight)
            buttonA.pin.all()
        }
        lineA.pin.horizontally().bottom().height(1)
        
        if isSecondExpanded {
            extendedViewB.alpha = 1.0
            buttonB.alpha = 0
            itemB.pin.top(to: itemA.edge.bottom).marginTop(10).horizontally(20).height(extendedViewHeight)
            extendedViewB.pin.all()

        } else {
            extendedViewB.alpha = 0
            buttonB.alpha = 1

            itemB.pin.top(to: itemA.edge.bottom).marginTop(10).horizontally(20).height(buttonHeight)
            buttonB.pin.all()
        }
        lineB.pin.horizontally().bottom().height(1)
        
        if isThirdExpanded {
            extendedViewC.alpha = 1
            buttonC.alpha = 0

            itemC.pin.top(to: itemB.edge.bottom).marginTop(10).horizontally(20).height(extendedViewHeight)
            extendedViewC.pin.all()

        } else {
            extendedViewC.alpha = 0
            buttonC.alpha = 1
            itemC.pin.top(to: itemB.edge.bottom).marginTop(10).horizontally(20).height(buttonHeight)
            buttonC.pin.all()
        }
        lineC.pin.horizontally().bottom().height(1)

        if isFourthExpanded {
            extendedViewD.alpha = 1
            buttonD.alpha = 0
            itemD.pin.top(to: itemC.edge.bottom).marginTop(10).horizontally(20).height(extendedViewHeight)
            extendedViewD.pin.all()
        } else {
            extendedViewD.alpha = 0
            buttonD.alpha = 1
            itemD.pin.top(to: itemC.edge.bottom).marginTop(10).horizontally(20).height(buttonHeight)
            buttonD.pin.all()
        }
        
        if !buttonC.isEnabled {
            lineB.isHidden = true
            lineC.isHidden = true
            
            itemC.isHidden = true
            itemD.isHidden = true
            scrollView.contentSize = CGSize(width: self.frame.size.width, height: itemB.frame.maxY)
        } else if !buttonD.isEnabled {
            lineB.isHidden = false
            lineC.isHidden = true
            itemC.isHidden = false
            itemD.isHidden = true
            scrollView.contentSize = CGSize(width: self.frame.size.width, height: itemC.frame.maxY)
        } else {
            lineB.isHidden = false
            lineC.isHidden = false
            
            itemC.isHidden = false
            itemD.isHidden = false
            scrollView.contentSize = CGSize(width: self.frame.size.width, height: itemD.frame.maxY)
        }
    }
    
    
    func expand(button: UIButton) {
        if button == buttonA {
            isFirstExpanded = true
            isSecondExpanded = false
            isThirdExpanded = false
            isFourthExpanded = false
        } else if button == buttonB {
            isFirstExpanded = false
            isSecondExpanded = true
            isThirdExpanded = false
            isFourthExpanded = false
        } else if button == buttonC {
            isFirstExpanded = false
            isSecondExpanded = false
            isThirdExpanded = true
            isFourthExpanded = false
        } else if button == buttonD {
            isFirstExpanded = false
            isSecondExpanded = false
            isThirdExpanded = false
            isFourthExpanded = true
        }


            UIView.animate(withDuration: 0.3, animations: {
                self.layoutCell()

            }) { (_) in

                    print("😎😎😎")
            }

    }


}
