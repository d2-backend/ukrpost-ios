//
//  StickerViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 6/1/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import WebKit

class StickerViewController: UIViewController {

    var uuid: String?
    var orderCode: String?
    var isInternational = false
    private let webView = WKWebView()

    private var pdfURL: URL?

    override func viewDidLoad() {
        super.viewDidLoad()

        configWebView()
        loadContent()
        addSharingButton()
    }

    private func configWebView() {
        //webView.frame = view.frame
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let navbarHeight = self.navigationController?.navigationBar.frame.size.height
        
        webView.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.width, height: view.frame.size.height - statusBarHeight - navbarHeight!)
        view.addSubview(webView)
    }

    private func addSharingButton() {
        let barButtonView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 44, height: 44)))
        let barButton = UIButton(frame: .zero)
        barButtonView.addSubview(barButton)
        barButton.pin.all(.zero)
        let image = UIImage(named: "shareIcon")
        barButton.setImage(image, for: .normal)
        barButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        barButton.addTarget(self, action: #selector(sharePDF), for: .touchUpInside)
        barButtonView.backgroundColor = .clear
        barButton.backgroundColor = .clear

        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barButtonView)
    }

    @objc private func sharePDF() {
        guard let fileURL = pdfURL else {
            return
        }

        let title = "Замовлення з номером \(orderCode ?? "") прямує до Вас."
        let activityViewController = UIActivityViewController(
            activityItems: [title, fileURL],
            applicationActivities: nil)
        present(activityViewController, animated: true, completion: nil)
    }

    private func loadContent() {
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            var neededUrl: URL!
            for aURL in fileURLs {
                if aURL.absoluteString.range(of: orderCode!) != nil {
                    neededUrl = aURL
                    break
                }
            }
            if neededUrl != nil {
                pdfURL = neededUrl
                webView.loadFileURL(neededUrl, allowingReadAccessTo: neededUrl)
            } else {
                if isInternational {
                    UPAPIManager.getInternationalOrderSticker(shipmentUUID: uuid!) { (result) in
                        switch result {
                        case .success(let item):
                            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("\(self.orderCode!).pdf")
                            
                            do {
                                try item.write(to: fileURL, options: .atomic)
                                self.pdfURL = fileURL
                                self.webView.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
                                
                            } catch {
                                print(error)
                            }
                        case .partialSuccess(let string):
                            self.showAlertWith(text: string)
                            
                        case .failure(let error):
                            self.showAlertWith(text: error.localizedDescription)
                        }
                    }
                } else {
                    UPAPIManager.getOrderSticker(shipmentUUID: uuid!) { (result) in
                        switch result {
                        case .success(let item):
                            let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("\(self.orderCode!).pdf")
                            
                            do {
                                try item.write(to: fileURL, options: .atomic)
                                self.pdfURL = fileURL
                                self.webView.loadFileURL(fileURL, allowingReadAccessTo: fileURL)
                                
                            } catch {
                                print(error)
                            }
                        case .partialSuccess(let string):
                            self.showAlertWith(text: string)
                            
                        case .failure(let error):
                            self.showAlertWith(text: error.localizedDescription)
                        }
                    }
                }

            }

        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
    }

    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}





