//
//  SmartBoxInfoView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/31/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class SmartBoxInfoView: UIView {

    private let boxHeader = UPHeader()
    let boxLabel = UILabel()
    private let returnHeader = UPHeader()
    let returnButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let storeAndReturnButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let abandonButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    private let separator = UIView()
    let predefinedValueButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let predefinedValueField = UPTextField()
    
    let postpayButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let postpayField = UPTextField()
    let submitButton = UPSubmitButton()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(boxHeader)
        boxHeader.text = NSLocalizedString("checkout.box.type.header", comment: "")
        
        self.addSubview(boxLabel)
        boxLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        boxLabel.textColor = UPColor.darkGray
        
        self.addSubview(returnHeader)
        returnHeader.text = NSLocalizedString("checkout.options.return.header", comment: "")
        
        self.addSubview(returnButton)
        returnButton.label.text = NSLocalizedString("checkout.options.return.return", comment: "")
        returnButton.label.textAlignment = .left
        returnButton.backgroundColor = .clear
        
        self.addSubview(storeAndReturnButton)
        storeAndReturnButton.label.text = NSLocalizedString("checkout.options.return.storeAndReturn", comment: "")
        storeAndReturnButton.label.textAlignment = .left
        storeAndReturnButton.backgroundColor = .clear

        self.addSubview(abandonButton)
        abandonButton.label.text = NSLocalizedString("checkout.options.return.abandon", comment: "")
        abandonButton.label.textAlignment = .left
        abandonButton.backgroundColor = .clear
        
        self.addSubview(separator)
        separator.backgroundColor = UPColor.separatorGray
        
        self.addSubview(predefinedValueButton)
        predefinedValueButton.label.text = NSLocalizedString("calculator.predefinedValue.label", comment: "")
        predefinedValueButton.label.textAlignment = .left
        predefinedValueButton.backgroundColor = .clear

        self.addSubview(predefinedValueField)
        
        self.addSubview(postpayButton)
        postpayButton.label.text = NSLocalizedString("checkout.price.postpay", comment: "")
        postpayButton.label.textAlignment = .left
        postpayButton.backgroundColor = .clear

        self.addSubview(postpayField)
        
        self.addSubview(submitButton)
        submitButton.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //let currentWidth = UIScreen.main.bounds.size.width
        
        boxHeader.pin.top(20).left(20).right(20).height(15)
        boxLabel.pin.top(to: boxHeader.edge.bottom).marginTop(10).left(20).right(20).height(20)
        returnHeader.pin.top(to: boxLabel.edge.bottom).marginTop(20).left(20).right(20).height(15)
        returnButton.pin.top(to: returnHeader.edge.bottom).marginTop(20).left(20).right(20).height(20)
        storeAndReturnButton.pin.top(to: returnButton.edge.bottom).marginTop(15).right(20).left(20).height(20)
        abandonButton.pin.top(to: storeAndReturnButton.edge.bottom).marginTop(15).left(20).right(20).height(20)
        separator.pin.top(to: abandonButton.edge.bottom).marginTop(20).left(20).right(20).height(1)
        
        predefinedValueField.pin.top(to: separator.edge.bottom).marginTop(20).right(20).height(40).width(100)
        predefinedValueButton.pin.top(to: separator.edge.bottom).marginTop(20).left(20).height(40).right(to: predefinedValueField.edge.left)
        
        postpayField.pin.top(to: predefinedValueField.edge.bottom).marginTop(10).right(20).height(40).width(100)
        postpayButton.pin.top(to: predefinedValueButton.edge.bottom).marginTop(10).left(20).height(40).right(to: postpayField.edge.left)
        
        submitButton.pin.left().bottom(64).right().height(64)
        
    }

}
