//
//  CheckoutPriceViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/15/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase
class CheckoutPriceViewController: UPViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    private let mainView = CheckoutPriceView()
    private let popupView = UPPickerView()
    var isExspress = Bool()

    private let types = [NSLocalizedString("checkout.w2w.type", comment: ""), NSLocalizedString("checkout.d2w.type", comment: ""), NSLocalizedString("checkout.w2d.type", comment: ""), NSLocalizedString("checkout.d2d.type", comment: "")]
    private var selectedType = 0
    private let cellHeight: CGFloat = 50
    var order: DomesticShipment?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var paidResipment = Bool()
      
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
         navigationItem.rightBarButtonItem = UIBarButtonItem(title: "скасувати", style: .plain, target: self, action: #selector(closeTapped))
      if isExspress {
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 7",
            AnalyticsParameterItemName: "ОВ_УКР_ЕК_7",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_7"
            ])
      }
      if !isExspress {
            Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                  AnalyticsParameterItemID: "id 7",
                  AnalyticsParameterItemName: "ОВ_УКР_SmartBox_7",
                  AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_7"
                  ])
      }
        self.title = "Оформити вiдправлення"
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        self.mainView.typeButton.addTarget(self, action: #selector(typePressed), for: .touchUpInside)
        self.mainView.typeButton.setTitle(types[selectedType], for: .normal)
      //  self.mainView.typeButton.setImage(UIImage(named: "chevron_down"), for: .normal)
        
        popupView.tableView.delegate = self
        popupView.tableView.dataSource = self
        popupView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        
        mainView.valueButton.addTarget(self, action: #selector(valuePressed), for: .touchUpInside)
        mainView.valueField.delegate = self
        
        mainView.postPayButton.addTarget(self, action: #selector(postPayPressed), for: .touchUpInside)
        mainView.postPayField.delegate = self
        
        mainView.fragileButton.addTarget(self, action: #selector(fragilePressed), for: .touchUpInside)
        
        mainView.recomendedButton.addTarget(self, action: #selector(recomendedPressed), for: .touchUpInside)
        
        mainView.smsButton.addTarget(self, action: #selector(smsPressed), for: .touchUpInside)
        //mainView.paidResipmentButton.addTarget(self, action: #selector(paidPressed), for: .touchUpInside)
      
        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        self.mainView.submitButton.setTitle(NSLocalizedString("main.parcelRegistrationButton.title", comment: ""), for: .normal)
        
        self.fetchPrice()
    }
    
    @objc func submitPressed() {
      
        let deliveryType = getDeliveryType()
        self.order?.deliveryType = deliveryType.rawValue
        let predefinedValue = self.mainView.valueField.text
        
        let aDeclaredPrice = mainView.valueButton.isSelected ? Int(predefinedValue!) : nil
        
        //let parcelItem = ParcelItem(weight: self.order?.weight, length: self.order?.length, declaredPrice: aDeclaredPrice)
        let parcelItem = ParcelItem(name: nil, weight: self.order?.weight, length: self.order?.length, declaredPrice: aDeclaredPrice, parcelItems: nil)

        self.order?.parcels = [parcelItem]
        
        let postPay = self.mainView.postPayField.text
        self.order!.postPay = mainView.postPayButton.isSelected ? Int(postPay!) : nil
        
        self.order?.fragile = mainView.fragileButton.isSelected
        self.order?.recommended = mainView.recomendedButton.isSelected
        self.order?.sms = mainView.smsButton.isSelected
        self.order?.paidByRecipient = paidResipment
        
        UPAPIManager.submitDomesticOrder(order: self.order!) { (result) in
            switch result {
            case .success(let data):
                if data.barcode != nil && data.uuid != nil {
                    let vc = CheckoutResultViewController()
                    vc.order = data
                     vc.isExpress = self.isExspress
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    self.showAlertWith(text:"")
                }

            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                  self.showAlertWith(text: error.localizedDescription)
            }
        }
    }

    @objc func valuePressed() {
        mainView.valueButton.isSelected = !mainView.valueButton.isSelected
        self.fetchPrice()

    }

    @objc func postPayPressed() {
        mainView.postPayButton.isSelected = !mainView.postPayButton.isSelected
        self.fetchPrice()

    }
    
    @objc func fragilePressed() {
        mainView.fragileButton.isSelected = !mainView.fragileButton.isSelected
        self.fetchPrice()

    }
    
    @objc func recomendedPressed() {
        mainView.recomendedButton.isSelected = !mainView.recomendedButton.isSelected
        self.fetchPrice()
    }
    
    @objc func smsPressed() {
        mainView.smsButton.isSelected = !mainView.smsButton.isSelected
        self.fetchPrice()
    }
      
//      @objc func paidPressed() {
//            mainView.paidResipmentButton.isSelected = !mainView.paidResipmentButton.isSelected
//            print(mainView.paidResipmentButton.isSelected)
//            self.fetchPrice()
//      }
    
    @objc func typePressed() {
        let buttonFrame = self.mainView.typeButton.frame
        self.popupView.frame = CGRect(x: buttonFrame.origin.x, y: buttonFrame.origin.y + buttonFrame.size.height + 10, width: self.mainView.typeButton.frame.size.width, height: 0)
        //self.popupView.alpha = 0
        self.mainView.addSubview(self.popupView)
        
        let neededHeight = cellHeight * CGFloat(types.count)
        UIView.animate(withDuration: 1.0) {
            self.popupView.frame = CGRect(x: buttonFrame.origin.x, y: buttonFrame.origin.y + buttonFrame.size.height + 10, width: self.mainView.typeButton.frame.size.width, height: neededHeight)
            //self.popupView.alpha = 1
        }
    }
    
    func hideTypes() {
        let buttonFrame = self.mainView.typeButton.frame

        UIView.animate(withDuration: 1.0, animations: {
            self.popupView.frame = CGRect(x: buttonFrame.origin.x, y: buttonFrame.origin.y + buttonFrame.size.height + 10, width: self.mainView.typeButton.frame.size.width, height: 0)
            //self.popupView.alpha = 0
            
        }) { (result) in
            self.popupView.removeFromSuperview()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        cell.textLabel!.text = "\(types[indexPath.row])"
        if selectedType == indexPath.row {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedType = indexPath.row
        self.mainView.typeButton.setTitle(types[selectedType], for: .normal)
        popupView.tableView.deselectRow(at: indexPath, animated: true)
        popupView.tableView.reloadData()
        hideTypes()
        fetchPrice()
        
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view?.isDescendant(of: popupView.tableView) == true {
            return false
        }
        return true
    }
    
    func fetchPrice() {
        self.mainView.resultValue.text = "..."
        
        let predefinedValueFloat = mainView.valueButton.isSelected ? Float(self.mainView.valueField.text!) : nil
        let deliveryType = getDeliveryType()
    
        
        UPAPIManager.getDeliveryPrice(postcodeFrom: self.addressFrom!.postcode, postcodeTo: self.addressTo!.postcode, weight: String(self.order!.weight!), length: String(self.order!.length!), type: SendType.express, deliveryType: deliveryType, declaredValue: predefinedValueFloat, withSMS: mainView.smsButton.isSelected, isRecomended: mainView.recomendedButton.isSelected) { (result) in
            switch result {
            case .success(let data):
                let price = data
                self.mainView.resultValue.text = String(price.deliveryPrice!) + " грн."
  
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    func getDeliveryType() -> DeliveryType {
        var deliveryType: DeliveryType
        switch selectedType {
        case 0:
            deliveryType = DeliveryType.warehouseToWarehouse
        case 1:
            deliveryType = DeliveryType.doorToWarehouse
        case 2:
            deliveryType = DeliveryType.warehouseToDoor
        case 3:
            deliveryType = DeliveryType.doorToDoor
        default:
            deliveryType = DeliveryType.warehouseToWarehouse
        }
        return deliveryType
    }

      @objc func closeTapped() {
            let vc = MainViewController()
            vc.isAnimationNeeded = false
            let nc = UINavigationController(rootViewController: vc)
            nc.navigationBar.shadowImage = UIImage()
            nc.navigationBar.isTranslucent = false
            nc.navigationBar.tintColor = UPColor.darkGray
            self.present(nc, animated: true, completion: nil)
      }
      
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
