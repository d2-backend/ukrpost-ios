//
//  CheckoutViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/11/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase

class CheckoutViewController: UPViewController, CountrySearchDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let mainView = CheckoutView()
    private var countryTo: Country?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var sendType: SendType?
    var weight: Int?
    private var weightRestrictionParc: Float = 30.0
    private var weightRestrictionBag: Float = 2.0
    private var weightRestrictionLtr: Float = 2.0

    var length: String?
    private var user: User?
    private var availableSmartBoxes: [SmartBox]?
    private var selectedSmartBoxIndex: Int = 0
    private let popupView = UPPickerView()
    var isExspress = Bool()
      
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }

    override func viewDidLoad() {
      super.viewDidLoad()
      hideBackNavigationButtonText()
      self.title = "Оформити вiдправлення"
      self.mainView.frame = self.view.frame
      self.view.addSubview(self.mainView)
      fetchSmartBoxesForUser()
        
        self.mainView.domesticButton.addTarget(self, action: #selector(domesticPressed), for: .touchUpInside)
        self.mainView.domesticButton.isSelected = true
        self.mainView.domesticButton.isHidden = true
        
        self.mainView.internationalButton.addTarget(self, action: #selector(internationalPressed), for: .touchUpInside)
        self.mainView.internationalButton.isSelected = false
        //self.mainView.internationalButton.isEnabled = false
        self.mainView.internationalButton.isHidden = true
        
        self.mainView.checkoutInputView.domesticExpressButton.addTarget(self, action: #selector(typePressed), for: .touchUpInside)
        self.mainView.checkoutInputView.domesticSmartBoxButton.addTarget(self, action: #selector(typePressed), for: .touchUpInside)
      mainView.checkoutInputView.weightField.text = weight == nil ? "" : "\(Float(self.weight!) / 1000)"
      self.mainView.checkoutInputView.sizeField.text = length == nil ? "" : length
        if sendType != nil {
            if sendType == .express {
                self.mainView.checkoutInputView.domesticExpressButton.isSelected = true
                self.mainView.checkoutInputView.domesticSmartBoxButton.isSelected = false

            } else {
                self.mainView.checkoutInputView.domesticExpressButton.isSelected = false
                self.mainView.checkoutInputView.domesticSmartBoxButton.isSelected = true
            }
        } else {
            self.mainView.checkoutInputView.domesticExpressButton.isSelected = true
            self.mainView.checkoutInputView.domesticSmartBoxButton.isSelected = false
        }
        
        self.mainView.checkoutInputView.weightField.delegate = self
        self.mainView.checkoutInputView.sizeField.delegate = self

        self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = true
        
        self.mainView.checkoutInputView.availableBoxesButton.addTarget(self, action: #selector(smartBoxPressed), for: .touchUpInside)
        self.mainView.checkoutInputView.uuidField.delegate = self
        self.mainView.checkoutInputView.newBoxField.delegate = self
        self.mainView.checkoutInputView.domesticSubmitButton.addTarget(self, action: #selector(domesticSubmitPressed), for: .touchUpInside)
        self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = false
        ////// viewB
        
        self.mainView.checkoutInputView.countryButton.addTarget(self, action:  #selector(countryPressed), for: .touchUpInside)
        self.mainView.checkoutInputView.parcelButton.addTarget(self, action:  #selector(typeInternPressed(sender:)), for: .touchUpInside)
        self.mainView.checkoutInputView.parcelButton.isSelected = true

        self.mainView.checkoutInputView.letterButton.addTarget(self, action:  #selector(typeInternPressed(sender:)), for: .touchUpInside)
        self.mainView.checkoutInputView.bagButton.addTarget(self, action:  #selector(typeInternPressed(sender:)), for: .touchUpInside)
        self.mainView.checkoutInputView.lengthInternField.delegate = self
        self.mainView.checkoutInputView.heightInternField.delegate = self
        self.mainView.checkoutInputView.widthInternField.delegate = self
        self.mainView.checkoutInputView.weightInternField.delegate = self
        
        self.mainView.checkoutInputView.submitInternButton.addTarget(self, action: #selector(internSubmitPressed), for: .touchUpInside)
        self.mainView.checkoutInputView.submitInternButton.isEnabled = false
        redrawDomesticView()

        drawCheckBoxView()
    }
    
    func fetchSmartBoxesForUser() {
        
        if User.currentUser().uuid != "" {
            self.availableSmartBoxes = nil
            UPAPIManager.getUserSmartBoxes(userUUID: User.currentUser().uuid!) { (result) in
                
                switch result {
                case .success(let data):
                    self.availableSmartBoxes = data
                    
                case .partialSuccess(let string):
                    print(string)
                case .failure(let error):
                    print(error)
                }
                self.drawCheckBoxView()
                self.redrawSubmitDomesticButton()

            }
        }
    }
    
    @objc func smartBoxPressed() {
        if self.availableSmartBoxes == nil {
            let alert = UIAlertController(title: "Помилка", message: "Для вiдображення доступних для вас смартбоксiв введiть будь ласка Ваш ID користувача у профайлi.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Добре", style: .default, handler: nil))
            self.present(alert, animated: true)
            return
        }
        
        if self.availableSmartBoxes!.count > 1 {
            let buttonFrame = self.mainView.checkoutInputView.availableBoxesButton.frame
            
            self.popupView.frame = CGRect(x: buttonFrame.origin.x, y: buttonFrame.origin.y + buttonFrame.size.height + 10, width: buttonFrame.size.width, height: 0)
            self.mainView.addSubview(self.popupView)
            self.popupView.tableView.delegate = self
            self.popupView.tableView.dataSource = self
            self.popupView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
            self.popupView.tableView.rowHeight = 40
            
            var neededHeight:CGFloat
            
            if self.availableSmartBoxes!.count > 4 {
                neededHeight = 160
            } else {
                neededHeight = 40 * CGFloat(self.availableSmartBoxes!.count)
            }
                UIView.animate(withDuration: 1.0) {
                self.popupView.frame = CGRect(x: buttonFrame.origin.x, y: buttonFrame.origin.y + buttonFrame.size.height + 10, width: buttonFrame.size.width, height: neededHeight)
            }
        }
    }
    
    func hideTypes() {
        let buttonFrame = self.mainView.checkoutInputView.availableBoxesButton.frame
        
        UIView.animate(withDuration: 1.0, animations: {
            self.popupView.frame = CGRect(x: buttonFrame.origin.x, y: buttonFrame.origin.y + buttonFrame.size.height + 10, width: buttonFrame.size.width, height: 0)
            
        }) { (result) in
            self.popupView.removeFromSuperview()
        }
    }
    
    @objc func domesticPressed() {
        self.mainView.internationalButton.isSelected = false
        self.mainView.domesticButton.isSelected = true
        self.mainView.checkoutInputView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @objc func internationalPressed() {
        let width = UIScreen.main.bounds.size.width
        self.mainView.internationalButton.isSelected = true
        self.mainView.domesticButton.isSelected = false
        self.mainView.checkoutInputView.setContentOffset(CGPoint(x: width, y: 0), animated: true)
    }
    
    @objc func typePressed() {        
        self.mainView.checkoutInputView.domesticExpressButton.isSelected = !self.mainView.checkoutInputView.domesticExpressButton.isSelected
        self.mainView.checkoutInputView.domesticSmartBoxButton.isSelected = !self.mainView.checkoutInputView.domesticSmartBoxButton.isSelected
        redrawDomesticView()
    }
    
    func redrawDomesticView() {
        if self.mainView.checkoutInputView.domesticExpressButton.isSelected {
            isExspress = true
            self.mainView.checkoutInputView.weightHeader.isHidden = false
            self.mainView.checkoutInputView.weightField.isHidden = false
            self.mainView.checkoutInputView.sizeHeader.isHidden = false
            self.mainView.checkoutInputView.sizeField.isHidden = false
        } else {
            isExspress = false
            self.mainView.checkoutInputView.weightHeader.isHidden = true
            self.mainView.checkoutInputView.weightField.isHidden = true
            self.mainView.checkoutInputView.sizeHeader.isHidden = true
            self.mainView.checkoutInputView.sizeField.isHidden = true
        }
        self.drawCheckBoxView()
        self.mainView.checkoutInputView.layoutSubviews()
        redrawSubmitDomesticButton()
    }
    
    func redrawSubmitDomesticButton() {
        self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = false
        if self.mainView.checkoutInputView.domesticExpressButton.isSelected {
            if self.mainView.checkoutInputView.weightField.text != "" && self.mainView.checkoutInputView.sizeField.text != "" {
                self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = true
            }
        } else {
            if User.currentUser().uuid != "" && self.availableSmartBoxes != nil && self.availableSmartBoxes!.count > 0 {
                self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = true
            } else {
                self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = false
            }
        }
        
    }
    
    func redrawSubmitInternButton() {
        self.mainView.checkoutInputView.submitInternButton.isEnabled = false
        if self.mainView.checkoutInputView.heightInternField.text != "" && self.mainView.checkoutInputView.widthInternField.text != "" && self.mainView.checkoutInputView.lengthInternField.text != "" && self.mainView.checkoutInputView.weightInternField.text != "" && self.countryTo != nil {
            self.mainView.checkoutInputView.submitInternButton.isEnabled = true
        }
    }
    
    @objc func domesticSubmitPressed () {
        if self.mainView.checkoutInputView.domesticExpressButton.isSelected {
            if self.mainView.checkoutInputView.weightField.text != "" || self.mainView.checkoutInputView.sizeField.text != "" {
                let vc = SenderInfoViewController()
                vc.isExspress = self.isExspress
                Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                      AnalyticsParameterItemID: "id 1",
                      AnalyticsParameterItemName: "ОВ_УКР_ЕК_1",
                      AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_1"
                      ])

                var order = DomesticShipment()
                order.type = SendType.express.rawValue
                order.weight = Int(Float(self.mainView.checkoutInputView.weightField.text!)! * 1000)
                order.length = Int(self.mainView.checkoutInputView.sizeField.text!)
                vc.order = order
                vc.addressTo = self.addressTo
                vc.addressFrom = self.addressFrom
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                AnalyticsParameterItemID: "id 1",
                AnalyticsParameterItemName: "ОВ_УКР_SmartBox_1 ",
                AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_1 "
                ])
            let vc = SmartBoxInfoViewController()
            vc.currentBox = self.availableSmartBoxes![self.selectedSmartBoxIndex]
            vc.addressFrom = self.addressFrom
            vc.addressTo = self.addressTo
            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    @objc func countryPressed() {
        let vc = CountryViewController()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func setCountry(country: Country) {
        self.countryTo = country
        self.mainView.checkoutInputView.countryButton.setTitle(country.name, for: .normal)
        self.mainView.checkoutInputView.countryButton.isSelected = true
        redrawSubmitInternButton()
        getCountryRestriction()
    }
    
    func getCountryRestriction() {
        let countryCode = self.countryTo?.code
        UPAPIManager.getRestrictionFor(country: countryCode!) { (result) in
            switch result {
            case .success(let item):
                print(item)
                self.weightRestrictionParc = fminf(item.maxWeightAviaParcel, item.maxWeightGroundParcel)
                self.checkWeightRestriction()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
                
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
            self.mainView.checkoutInputView.newBoxField.text = ""
        }
    }
    
    func checkWeightRestriction() {
        if mainView.checkoutInputView.parcelButton.isSelected {
            let currentWeight = Float(mainView.checkoutInputView.weightInternField.text!) ?? 0
            if currentWeight > weightRestrictionParc {
                mainView.checkoutInputView.weightInternField.text = "\(weightRestrictionParc)"
                showAlertWith(text: String(format: NSLocalizedString("checkout.weight.hint", comment: ""), weightRestrictionParc))
            }
        }
    }
    
    @objc func typeInternPressed(sender: UIButton) {
        self.mainView.checkoutInputView.parcelButton.isSelected = false
        self.mainView.checkoutInputView.letterButton.isSelected = false
        self.mainView.checkoutInputView.bagButton.isSelected = false
        self.mainView.checkoutInputView.heightInternField.text = ""
        self.mainView.checkoutInputView.widthInternField.text = ""
        self.mainView.checkoutInputView.lengthInternField.text = ""
        self.mainView.checkoutInputView.weightInternField.text = ""

        sender.isSelected = true
    }
    
    @objc func internSubmitPressed () {
        let vc = SenderInfoViewController()
        var order = DomesticShipment()
        order.type = "INTERNATIONAL"
        order.weight = Int(Float(self.mainView.checkoutInputView.weightInternField.text!)! * 1000)
        let intA = Int(self.mainView.checkoutInputView.lengthInternField.text!)!
        let intB = Int(self.mainView.checkoutInputView.widthInternField.text!)!
        let intC = Int(self.mainView.checkoutInputView.heightInternField.text!)!
        var sizes = [intA, intB, intC].sorted { ($0 > $1) }
        
        order.length = sizes[0]
        order.width = sizes[1]
        order.height = sizes[2]
        
        order.international = true
        if mainView.checkoutInputView.parcelButton.isSelected {
            order.packageType = APIPackageType.parcel.rawValue
        } else if mainView.checkoutInputView.letterButton.isSelected {
            order.packageType = APIPackageType.letter.rawValue
        } else {
            order.packageType = APIPackageType.smallBag.rawValue
        }
        order.deliveryType = DeliveryType.warehouseToWarehouse.rawValue
        vc.order = order
        vc.country = countryTo
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if self.view.frame.origin.y != 64 {
            UIView.animate(withDuration: 0.25) {
                self.view.frame = CGRect(x:self.view.frame.origin.x , y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        redrawSubmitInternButton()
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.mainView.checkoutInputView.weightField || textField == self.mainView.checkoutInputView.sizeField {
            self.mainView.checkoutInputView.domesticSubmitButton.isEnabled = false
            textField.resignFirstResponder()
            if textField == self.mainView.checkoutInputView.uuidField {
                self.fetchToken(bearerUUID: textField.text!)
            } else if textField == self.mainView.checkoutInputView.newBoxField {
                self.addBoxToUser(uuid: User.currentUser().uuid!, boxCode: textField.text!)
            }
            return true
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == self.mainView.checkoutInputView.lengthInternField || textField == self.mainView.checkoutInputView.heightInternField || textField == self.mainView.checkoutInputView.widthInternField {
            let sizeLimit: Int!
            if self.mainView.checkoutInputView.bagButton.isSelected == true || self.mainView.checkoutInputView.letterButton.isSelected == true {
                sizeLimit = 90
            } else {
                sizeLimit = 200
            }
            
            if self.mainView.checkoutInputView.lengthInternField.text != "" &&  self.mainView.checkoutInputView.heightInternField.text != "" && self.mainView.checkoutInputView.widthInternField.text != "" {
                guard let heightStr = self.mainView.checkoutInputView.heightInternField.text else {return }
                let heightInt = Int(heightStr) ?? 0
                guard let widthStr = self.mainView.checkoutInputView.widthInternField.text else {return }
                let widthInt = Int(widthStr) ?? 0
                guard let lengthStr = self.mainView.checkoutInputView.lengthInternField.text else {return }
                let lengthInt = Int(lengthStr) ?? 0
                if heightInt + widthInt + lengthInt > sizeLimit {
                    self.mainView.checkoutInputView.heightInternField.text = ""
                    self.mainView.checkoutInputView.widthInternField.text = ""
                    self.mainView.checkoutInputView.lengthInternField.text = ""
                    let alert = UIAlertController(title: "Помилка", message: "Сума сторiн не має перевищувати \(sizeLimit!) см.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ок", style: .default, handler: nil))
                    self.present(alert, animated: true)
                }
            }
            redrawSubmitInternButton()
        }
    }

      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //if textField == self.mainView.checkoutInputView.weightField || textField == self.mainView.checkoutInputView.sizeField {
            var oldString = textField.text
            var newString: String!
            switch string {
            case "":
                oldString!.removeLast()
                newString = oldString
            case "0","1","2","3","4","5","6","7","8","9" :
                newString = oldString! + string
            case ".", ",":
                if textField == self.mainView.checkoutInputView.weightField || textField == self.mainView.checkoutInputView.weightInternField {
                    if oldString?.range(of: ".") != nil {
                        newString = oldString
                    } else {
                        newString = oldString! + "."
                    }
                    if newString == "." {
                        newString = "0."
                    }
                } else {
                    return false
                }
            default :
                return false
            }
            if newString == "" {
                textField.text = newString
                redrawSubmitDomesticButton()
                return false
            }
            
            if textField == self.mainView.checkoutInputView.weightField {
                let arr = newString.components(separatedBy: ".")
                if Double(newString!)! > 30.0 {
                    textField.text = "30"
                } else if arr.count > 1 && arr[1].count > 3 {
                    return false
                } else {
                    textField.text = newString
                }
            } else if textField == self.mainView.checkoutInputView.sizeField {
                if Double(newString!)! > 200.0 {
                    textField.text = "200"
                } else {
                    textField.text = newString
                }
            }
        
        let sizeLimitIntern: Int
        if self.mainView.checkoutInputView.bagButton.isSelected == true || self.mainView.checkoutInputView.letterButton.isSelected == true{
            sizeLimitIntern = 60
        } else {
            sizeLimitIntern = 105
        }
        if textField == mainView.checkoutInputView.weightInternField {
            let arr = newString.components(separatedBy: ".")
            var currentRestriction: Float = 0.0
            if mainView.checkoutInputView.parcelButton.isSelected {
                currentRestriction = weightRestrictionParc
            } else if mainView.checkoutInputView.bagButton.isSelected {
                currentRestriction = weightRestrictionBag
            } else {
                currentRestriction = weightRestrictionLtr
            }
            
            if Float(newString)! > currentRestriction {
                textField.text = "\(currentRestriction)"
            } else if arr.count > 1 && arr[1].count > 3 {
                return false
            } else {
                textField.text = newString
            }
        } else if textField == mainView.checkoutInputView.lengthInternField || textField == mainView.checkoutInputView.heightInternField || textField == mainView.checkoutInputView.widthInternField {
            if Int(newString)! > sizeLimitIntern {
                textField.text = "\(sizeLimitIntern)"
            } else {
                textField.text = newString
            }
        }
            redrawSubmitDomesticButton()
            return false
//        } else {
//            return true
//        }
    }
    
    func drawCheckBoxView() {
        if self.mainView.checkoutInputView.domesticSmartBoxButton.isSelected {
            
            self.mainView.checkoutInputView.uuidHeader.isHidden = false
            self.mainView.checkoutInputView.uuidField.isHidden = false
            self.mainView.checkoutInputView.uuidField.text = User.currentUser().uuid
      
            if  User.currentUser().uuid == "" {
                self.mainView.checkoutInputView.uuidField.placeholder = "Введiть ваш ID у профiлi"
                self.mainView.checkoutInputView.availableBoxesButton.isHidden = true
                self.mainView.checkoutInputView.newBoxHeader.isHidden = true
                self.mainView.checkoutInputView.newBoxField.isHidden = true
            } else {
                if self.availableSmartBoxes != nil && self.availableSmartBoxes!.count > 0 {
                    self.mainView.checkoutInputView.availableBoxesButton.isHidden = false
                    let selectedBox = self.availableSmartBoxes![self.selectedSmartBoxIndex]
                    self.mainView.checkoutInputView.availableBoxesButton.setTitle(selectedBox.smartBoxTariff?.description, for: .normal)
                    self.mainView.checkoutInputView.newBoxHeader.isHidden = false
                    self.mainView.checkoutInputView.newBoxField.isHidden = false
                } else {
                    self.mainView.checkoutInputView.availableBoxesButton.isHidden = true
                    self.mainView.checkoutInputView.newBoxHeader.isHidden = false
                    self.mainView.checkoutInputView.newBoxField.isHidden = false
                }
            }

            self.mainView.checkoutInputView.layoutSubviews()
        } else {
            self.mainView.checkoutInputView.uuidHeader.isHidden = true
            self.mainView.checkoutInputView.uuidField.isHidden = true
            self.mainView.checkoutInputView.availableBoxesButton.isHidden = true
            self.mainView.checkoutInputView.newBoxHeader.isHidden = true
            self.mainView.checkoutInputView.newBoxField.isHidden = true
        }
        
    }
    
    func fetchToken(bearerUUID: String) {
        
        UPAPIManager.getToken(bearerUUID: bearerUUID) { (result) in
            switch result {
            case .success(let user):
                User.save(user: user)
                self.fetchSmartBoxesForUser()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
                
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func addBoxToUser(uuid: String, boxCode: String) {
        
        UPAPIManager.addSmartBoxToUser(userUUID: uuid, boxCode: boxCode) { (result) in
            switch result {
            case .success(let item):
                print(item)
                self.fetchSmartBoxesForUser()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
                
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
            self.mainView.checkoutInputView.newBoxField.text = ""
        }
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.availableSmartBoxes!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        let currentBox = availableSmartBoxes![indexPath.row]
        
        cell.textLabel!.text = currentBox.smartBoxTariff?.description
        if selectedSmartBoxIndex == indexPath.row {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedSmartBoxIndex = indexPath.row
        self.mainView.checkoutInputView.availableBoxesButton.setTitle(availableSmartBoxes![self.selectedSmartBoxIndex].smartBoxTariff?.description, for: .normal)
        popupView.tableView.deselectRow(at: indexPath, animated: true)
        popupView.tableView.reloadData()
        hideTypes()
        
    }
    
}
