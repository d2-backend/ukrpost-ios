//
//  CheckoutResultView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/16/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CheckoutResultView: UIView {

    private let icon = UIImageView()
    private let header = UILabel()
    private let idHeader = UPHeader()
    let id = UITextView()
    private let info = UPHeader()
    let downloadButton = UIButton()
    private let downloadIcon = UIImageView()
    private let downloadLabel = UILabel()
    let shareButton = UIButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(icon)
        icon.image = UIImage(named: "yellowBox")
        icon.contentMode = .scaleAspectFit
        
        self.addSubview(header)
        header.text = NSLocalizedString("checkout.result.header", comment: "")
        header.textColor = UPColor.darkGray
        let headerFont = UIFont.preferredFont(forTextStyle: UIFontTextStyle.title2)
        let boldHeaderFont = UIFont(descriptor: headerFont.fontDescriptor.withSymbolicTraits(.traitBold)!, size: headerFont.pointSize)
        header.font = boldHeaderFont
        header.numberOfLines = 0
        header.lineBreakMode = .byWordWrapping
        
        self.addSubview(idHeader)
        idHeader.text = NSLocalizedString("checkout.result.idHeader", comment: "")
        
        self.addSubview(id)
        id.textAlignment = .center
        id.isEditable = false
        id.backgroundColor = .clear
        id.tintColor = UPColor.darkGray
        let font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        id.font = font
        id.textColor = UPColor.darkGray
        
        self.addSubview(info)
        info.text = NSLocalizedString("checkout.result.info", comment: "")
        info.numberOfLines = 0
        info.lineBreakMode = .byWordWrapping
        
        self.addSubview(downloadButton)
        downloadButton.backgroundColor = .white
        downloadButton.layer.masksToBounds = false
        downloadButton.layer.shadowColor = UPColor.darkGray.cgColor
        downloadButton.layer.shadowOpacity = 0.1
        downloadButton.layer.shadowOffset = .zero
        downloadButton.layer.shadowRadius = 2
        
        downloadButton.addSubview(downloadIcon)
        downloadIcon.isUserInteractionEnabled = false
        downloadIcon.image = UIImage(named: "download")
        downloadIcon.backgroundColor = UPColor.separatorGray
        
        downloadButton.addSubview(downloadLabel)
        downloadLabel.isUserInteractionEnabled = false
        downloadLabel.text = NSLocalizedString("checkout.result.download", comment: "")
        downloadLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        downloadLabel.textColor = UPColor.darkGray

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //let currentWidth = UIScreen.main.bounds.size.width
        
        icon.pin.top(30).left(20).width(100).height(100)
        header.pin.below(of: icon).marginTop(20).left(20).right(20).height(70)
        idHeader.pin.below(of: header).marginTop(20).left(20).right(20).height(15)
        id.pin.below(of: idHeader).marginTop(10).left(20).right(20).height(40)
        info.pin.below(of: id).marginTop(20).left(20).right(20)
        info.sizeToFit()
        
        downloadButton.pin.left().bottom(64).right().height(64)
        downloadIcon.pin.vCenter().left(20).height(20).width(20)
        downloadLabel.pin.after(of: downloadIcon).vCenter(to: downloadIcon.edge.vCenter).right(to: downloadButton.edge.right).height(of: downloadButton).marginHorizontal(10)
    }

}
