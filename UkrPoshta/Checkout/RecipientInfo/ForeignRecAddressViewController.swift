//
//  ForeignRecAddressViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignRecAddressViewController: UPViewController, UITextFieldDelegate {

    private let mainView = ForeignRecAddressView()
    var country: Country?
    var order: DomesticShipment?

    override func viewDidLoad() {
        super.viewDidLoad()

        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))
        
        self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        mainView.countryLabel.text = country?.name ?? ""
        mainView.indexField.delegate = self
        mainView.cityField.delegate = self
        mainView.streetField.delegate = self
        mainView.houseField.delegate = self
        mainView.apartmentField.delegate = self

        mainView.submit.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        redrawSubmit()
    }

    func redrawSubmit() {
        if mainView.indexField.text != "" && mainView.cityField.text != "" && mainView.streetField.text != "" && mainView.houseField.text != "" {
            mainView.submit.isEnabled = true
        } else {
            mainView.submit.isEnabled = false
        }
    }
    @objc func closeTapped() {
        let vc = MainViewController()
        vc.isAnimationNeeded = false
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.shadowImage = UIImage()
        nc.navigationBar.isTranslucent = false
        nc.navigationBar.tintColor = UPColor.darkGray
        self.present(nc, animated: true, completion: nil)
    }

    @objc func submitPressed() {
        UPAPIManager.createIntAddress(country: country?.code, postCode: mainView.indexField.text, city: mainView.cityField.text, street: mainView.streetField.text, houseNumber: mainView.houseField.text, apartmentNumber: mainView.apartmentField.text) { (result) in
            switch result {
            case .success(let address):
                self.order?.recipientAddressId = address.id
                self.order?.recipient?.addressId = address.id
                self.createUser()
            case .partialSuccess(let string):
                print(string)
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
        
    }
    
    func createUser() {
        UPAPIManager.createUser(user: (self.order?.recipient)!) { (result) in
            switch result {
            case .success(let data):
                self.order?.recipient = data
                //self.order?.recipient?.addressId = self.savedAddress?.id
                self.navigateNext()
                
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
    func navigateNext() {
        let vc = ContentViewController()
        vc.order = order
        vc.country = country
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        redrawSubmit()
        return true
    }
}
