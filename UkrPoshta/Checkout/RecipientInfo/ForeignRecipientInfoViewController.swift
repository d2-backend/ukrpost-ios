//
//  ForeignRecipientInfoViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignRecipientInfoViewController: UPViewController, UITextFieldDelegate {

    var order: DomesticShipment?
    private var currentReceiver: User?
    
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var country: Country?
    
    private let mainView = ForeignRecipientInfoView()

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Скасувати", style: .plain, target: self, action: #selector(closeTapped))

        self.navigationController?.navigationBar.topItem?.title = "Оформити вiдправлення"
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        mainView.nameField.delegate = self
        mainView.lastNameField.delegate = self
        mainView.phoneField.delegate = self
        mainView.phoneField.text = UPPhoneHelper.foreignDisplayStringFrom(rawString: "")
        mainView.submit.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        redrawSubmit()
    }
    
    @objc func closeTapped() {
        let vc = MainViewController()
        vc.isAnimationNeeded = false
        let nc = UINavigationController(rootViewController: vc)
        nc.navigationBar.shadowImage = UIImage()
        nc.navigationBar.isTranslucent = false
        nc.navigationBar.tintColor = UPColor.darkGray
        self.present(nc, animated: true, completion: nil)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case mainView.phoneField:
            let oldString = textField.text
            let newString = oldString! + string
            if (newString.count > 19) {
                return false
            } else {
                var oldRawString = UPPhoneHelper.foreignRawStringFrom(displayString: oldString!)
                
                switch string {
                case "":
                    oldRawString!.removeLast()
                case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9":
                    oldRawString = oldRawString! + string
                    
                default:
                    return false
                }
                textField.text = UPPhoneHelper.foreignDisplayStringFrom(rawString: oldRawString)
            }

            return false
            
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        redrawSubmit()
        textField.resignFirstResponder()
        return true
    }
    
    @objc func submitPressed() {
        let vc = ForeignRecAddressViewController()
        var recipient = User()
        recipient.firstName = mainView.nameField.text
        recipient.lastName = mainView.lastNameField.text
        recipient.phoneNumber = UPPhoneHelper.foreignRawStringFrom(displayString: mainView.phoneField.text)
        recipient.type = APIUserType.individual.rawValue
        order?.recipient = recipient
        order?.recipientPhone = UPPhoneHelper.foreignRawStringFrom(displayString: mainView.phoneField.text)
        vc.order = order
        vc.country = country
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func redrawSubmit() {
        if mainView.lastNameField.text != "" && mainView.nameField.text != "" && mainView.phoneField.text?.count == 19 {
            mainView.submit.isEnabled = true
        } else {
            mainView.submit.isEnabled = false
        }
    }
}
