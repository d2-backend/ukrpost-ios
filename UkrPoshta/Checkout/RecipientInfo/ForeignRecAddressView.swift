//
//  ForeignRecAddressView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignRecAddressView: UIView {

    let countryLabel = UILabel()
    private let line = UIView()
    
    private let header = UPHeader()
    let indexField = UPTextField()
    let cityField = UPTextField()
    let streetField = UPTextField()
    let houseField = UPTextField()
    let apartmentField = UPTextField()
    let submit = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke

        countryLabel.textColor = UPColor.darkGray
        countryLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        addSubview(countryLabel)
        
        line.backgroundColor = UPColor.separatorGray
        self.addSubview(line)
        
        header.text = NSLocalizedString("checkout.recipient.address.header", comment: "")
        addSubview(header)
        
        indexField.placeholder = NSLocalizedString("indexToAddressHeader.title", comment: "")
        addSubview(indexField)
        
        cityField.placeholder = NSLocalizedString("addressToIndex.city.title", comment: "")
        addSubview(cityField)
        
        streetField.placeholder = NSLocalizedString("addressToIndex.street.title", comment: "")
        addSubview(streetField)
        
        houseField.placeholder = NSLocalizedString("addressToIndex.building.title", comment: "")
        addSubview(houseField)
        
        apartmentField.placeholder = NSLocalizedString("addressToIndex.apartment.title", comment: "")
        addSubview(apartmentField)
        
        submit.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        addSubview(submit)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        countryLabel.pin.top(15).left(20).right(20).height(10)
        line.pin.below(of: countryLabel).marginTop(15).left(20).right(20).height(1)
        header.pin.below(of: line).marginTop(15).left(20).right(20).height(10)
        indexField.pin.top(to: header.edge.bottom).marginTop(10).horizontally(20).height(50)
        cityField.pin.top(to: indexField.edge.bottom).marginTop(10).horizontally(20).height(50)
        streetField.pin.top(to: cityField.edge.bottom).marginTop(10).horizontally(20).height(50)
        
        let width = UIScreen.main.bounds.size.width
        let halfWidth = (width - 50) / 2
        
        houseField.pin.top(to: streetField.edge.bottom).left(20).height(50).width(halfWidth).marginTop(10)
        apartmentField.pin.top(to: streetField.edge.bottom).left(to: houseField.edge.right).marginLeft(10).height(50).width(halfWidth).marginTop(10)
        submit.pin.horizontally().bottom(64).height(64)

    }

}
