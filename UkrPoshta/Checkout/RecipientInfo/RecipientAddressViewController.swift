//
//  RecipientAddressViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase
class RecipientAddressViewController: UIViewController, UITextFieldDelegate {

    private let mainView = SenderAddressView()
    var order: DomesticShipment?
    private var savedAddress:Address?
    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var controller: AddressFormControllerProtocol?
    var isExspress = Bool()

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      print(isExspress)
      if isExspress {
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 5",
            AnalyticsParameterItemName: "ОВ_УКР_ЕК_5",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_ЕК_5"
            ])
      }
      if !isExspress {
            Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
                  AnalyticsParameterItemID: "id 5",
                  AnalyticsParameterItemName: "ОВ_УКР_SmartBox_5",
                  AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_5"
                  ])
      }
        self.title = "Оформити вiдправлення"
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        self.mainView.addressHeader.text = NSLocalizedString("checkout.recipient.address.header", comment: "")
        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        controller = AddressFormController(with: mainView, and: addressTo, viewController: self)
        controller?.viewDidLoad()
    }
    
    @objc func submitPressed() {
        createAddress()
    }
    
    func createAddress() {
        self.mainView.submitButton.isEnabled = false
        UPAPIManager.createAddress(postCode: self.mainView.indexField.text, region: self.mainView.regionField.text, district: self.mainView.districtField.text, city:  self.mainView.cityField.text, street: self.mainView.streetField.text, houseNumber: self.mainView.houseField.text, apartmentNumber: self.mainView.appartmentField.text) { (result) in
            switch result {
            case .success(let data):
                self.savedAddress = data
                self.order?.recipient?.addressId = data.id
                self.createUser()
                
            case .partialSuccess(let string):
                self.showAlert(with: string)
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
            }
        }
    }
    
    func createUser() {
        UPAPIManager.createUser(user: (self.order?.recipient)!) { (result) in
            switch result {
            case .success(let data):
                self.order?.recipient = data
                self.order?.recipient?.addressId = self.savedAddress?.id
                self.navigateNext()
                
            case .partialSuccess(let string):
                self.showAlert(with: string)
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
            }
        }
    }
    
    func navigateNext() {
        let vc = CheckoutOptionsViewController()
        vc.order = self.order
        if let address = self.controller?.address {
            vc.addressTo = address
        } else {
            let newAddress = CityDetails(regionID: nil, regionName: nil, districtID: nil, districtName: nil, cityID: nil, cityName: nil, cityTypeID: nil, cityTypeName: nil, street: nil, house: nil, apartment: nil, addressID: nil, postcode: self.mainView.indexField.text)
            
            vc.addressTo = newAddress
        }
        vc.addressFrom = self.addressFrom
        vc.isExspress = self.isExspress
        self.navigationController?.pushViewController(vc, animated: true)
        
    }

}

extension RecipientAddressViewController: AddressViewProtocol {
    func push(viewController: IndexSearchViewController) {
        view.endEditing(true)
        navigationController?.pushViewController(viewController, animated: true)
    }

    func showAlert(with text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)
        self.mainView.submitButton.isEnabled = true

    }
}
