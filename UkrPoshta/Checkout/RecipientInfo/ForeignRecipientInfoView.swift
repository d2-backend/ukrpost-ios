//
//  ForeignRecipientInfoView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ForeignRecipientInfoView: UIView {

    private let senderHeader = UPHeader()
    let lastNameField = UPTextField()
    let nameField = UPTextField()
    
    private let phoneHeader = UPHeader()
    let phoneField = UPTextField()
    
    let submit = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        senderHeader.text = NSLocalizedString("checkout.recipient.header", comment: "")
        addSubview(senderHeader)
        
        lastNameField.placeholder = NSLocalizedString("profile.edit.lastNameHeader", comment: "")
        addSubview(lastNameField)
        nameField.placeholder = NSLocalizedString("profile.edit.nameHeader", comment: "")
        addSubview(nameField)
        
        phoneHeader.text = NSLocalizedString("profile.phoneCell.header", comment: "")
        addSubview(phoneHeader)
        phoneField.keyboardType = .phonePad
        addSubview(phoneField)
        
        submit.setTitle(NSLocalizedString("calculator.nextButton.title", comment: ""), for: .normal)
        addSubview(submit)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        senderHeader.pin.top(20).horizontally(20).height(15)
        lastNameField.pin.top(to: senderHeader.edge.bottom).marginTop(10).horizontally(20).height(50)
        nameField.pin.top(to: lastNameField.edge.bottom).marginTop(10).horizontally(20).height(50)
        
        phoneHeader.pin.top(to: nameField.edge.bottom).marginTop(20).horizontally(20).height(15)
        phoneField.pin.top(to: phoneHeader.edge.bottom).marginTop(10).horizontally(20).height(50)
        submit.pin.horizontally().bottom(64).height(64)
        
    }

}
