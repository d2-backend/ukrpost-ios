//
//  AddressFormController.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/4/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import UIKit

protocol AddressFormControllerProtocol {
    func viewDidLoad()
    var address: CityDetails? {get}
}

final class AddressFormController: NSObject, AddressFormControllerProtocol {
    private var region: Region?
    private var district: District?
    private var city: City?
    private var street: Street?
      
    private weak var view: SenderAddressView?
    public private (set) var address: CityDetails?
    private weak var viewController: AddressViewProtocol?
    
    init(with view: SenderAddressView, and address: CityDetails?, viewController: AddressViewProtocol) {
        self.view = view
        self.address = address
        self.viewController = viewController
    }

    func viewDidLoad() {
        configView()
    }

    private func configView() {
        view?.indexField.delegate = self
        view?.regionField.delegate = self
        view?.districtField.delegate = self
        view?.cityField.delegate = self
        view?.streetField.delegate = self
        view?.houseField.delegate = self
        view?.appartmentField.delegate = self
        if address != nil {
            view?.indexField.text = address?.postcode
            view?.regionField.text = address?.regionName
            view?.districtField.text = address?.districtName
            view?.cityField.text = address?.cityName
            view?.streetField.text = address?.street
            view?.houseField.text = address?.house
            view?.appartmentField.text = address?.apartment
        }
        redrawSubmitButton()
    }

    private func fetchData(by postCode: String) {

        UPAPIManager.getPostOffice(by: postCode) { [weak self] result in
            switch result {
            case .success(let postOffice):
                self?.fillFieldsBased(on: postOffice)
            case .partialSuccess(let string):
                print(string)
                self?.viewController?.showAlert(with: string)
            case .failure(let error):
                print(error.localizedDescription)
                 self?.viewController?.showAlert(with: error.localizedDescription)
            }
        }
    }

    private func fillFieldsBased(on postOffice: PostOffice) {
        region = Region(regionID: postOffice.regionID, regionUA: postOffice.regionUA)
      district = District(districtUA: postOffice.districtUA, districtID: postOffice.districtID, regionID: postOffice.regionID, regionUA: postOffice.regionUA)
        city = City(regionID: postOffice.regionID, regionUA: postOffice.regionUA,
                    districtID: postOffice.districtID, districtUA: postOffice.districtUA,
                    cityID: postOffice.cityID, cityUA: postOffice.cityUA, cityTypeUA: postOffice.shortCityTypeUA, shortCityTypeUA: postOffice.shortCityTypeUA, nameUA: postOffice.cityUA)
        redrawUI()
    }

    func tryToFindIndex() {
        if canISearchIndex {
            fetchIndexesByStreet()
        }
    }

}

extension AddressFormController: UITextFieldDelegate {
    func redrawSubmitButton() {
        if view?.indexField.text != "" && view?.regionField.text != "" && view?.cityField.text != "" && view?.streetField.text != "" && view?.houseField.text != "" {

            view?.submitButton.isEnabled = true
        } else {
            view?.submitButton.isEnabled = false
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == view?.indexField {
            switch string {
            case "" :
                return true
            case "0","1","2","3","4","5","6","7","8","9" :
                let newString = textField.text! + string
                if newString.count < 6 {
                    textField.text = newString
                }
                if newString.count == 5 {
                    fetchData(by: newString)
                }

                return false
            default :
                return false
            }
        }

        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == view?.regionField {
            regionSearch()
            return false
        } else if textField == view?.districtField {
            districtSearch()
            return false
        } else if textField == view?.cityField {
            citySearch()
            return false
        } else if textField == view?.streetField {
            streetSearch()
            return false
        }

        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == view?.appartmentField {
            tryToFindIndex()
        } else if textField == view?.houseField {
            tryToFindIndex()
        }
        self.redrawSubmitButton()
    }
}

extension AddressFormController: IndexSearchDelegate {
    private func redrawUI() {
        view?.regionField.text = region?.regionUA
        view?.districtField.text = district?.districtUA
        view?.cityField.text = city?.cityUA
        view?.streetField.text = street?.streetUA
        tryToFindIndex()
    }

    private func regionSearch() {
        region = nil
        district = nil
        city = nil
        street = nil

        let vc = IndexSearchViewController(type: .region, region: region, district: district, city: city, street: street)
        vc.delegate = self
        viewController?.push(viewController: vc)
    }

    private func districtSearch() {

        district = nil
        city = nil
        street = nil

        let vc = IndexSearchViewController(type: .district, region: region, district: district, city: city, street: street)
        vc.delegate = self
        viewController?.push(viewController: vc)
    }

    private func citySearch() {
        self.city = nil
        self.street = nil

        let vc = IndexSearchViewController(type: .city, region: self.region, district: self.district, city: self.city, street: self.street)
        vc.delegate = self
        viewController?.push(viewController: vc)
    }

    private func streetSearch() {
        self.street = nil

        let vc = IndexSearchViewController(type: .street, region: region, district: district, city: city, street: street)
        vc.delegate = self
        viewController?.push(viewController: vc)
    }

    var canISearchIndex: Bool {
        return region != nil && district != nil && city != nil && street != nil && view?.houseField.text != nil && view?.houseField.text != ""
    }


    func fetchIndexesByStreet() {
        view?.submitButton.isEnabled = false
        UPAPIManager.getIndexesList(streetID: self.street?.streetID) { [weak self] (result) in
            switch result {
            case .success(let data):
                self?.filterIndexesWithHouse(indexesAndHouses: data)

            case .partialSuccess(let string):
                self?.viewController?.showAlert(with: string)

            case .failure(let error):
                self?.viewController?.showAlert(with: error.localizedDescription)
            }
            self!.redrawSubmitButton()
        }
    }

    func filterIndexesWithHouse(indexesAndHouses: [IndexAndHouse]) {
        var postcode: String? = nil
        for index: IndexAndHouse in indexesAndHouses {
            print("\(index.houseNumber) ____ \(index.postcode)")
            if  index.houseNumber == view?.houseField.text {
                view?.indexField.text = index.postcode
                postcode = index.postcode
                break
            }
        }
        if postcode == nil {
            viewController?.showAlert(with: NSLocalizedString("calculator.error.noHouse", comment: ""))
            view?.houseField.text = ""
            view?.appartmentField.text = ""
        }
    }


    func setRegion(region: Region) {
        self.region = region
        redrawUI()
    }

    func setDistrict(district: District) {
        self.district = district
      self.region = Region(regionID: district.regionID, regionUA: district.regionUA)
        redrawUI()
    }

    func setCity(city: City) {
        self.city = city
      self.region = Region(regionID: city.regionID, regionUA: city.regionUA)
      self.district = District(districtUA: city.districtUA, districtID: city.districtID, regionID: city.regionID, regionUA: city.regionUA)
        redrawUI()
    }

    func setStreet(street: Street) {
        self.street = street

        redrawUI()
    }
}
