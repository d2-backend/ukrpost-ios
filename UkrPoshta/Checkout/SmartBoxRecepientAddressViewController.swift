//
//  SmartBoxRecepientAddressViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/31/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import Firebase

class SmartBoxRecepientAddressViewController: UIViewController, UITextFieldDelegate {

    private let mainView = SenderAddressView()
    var order: DomesticShipment?
    private var savedAddress:Address?
    var currentBox: SmartBox?

    var addressFrom: CityDetails?
    var addressTo: CityDetails?
    var controller: AddressFormControllerProtocol?

    
    override func viewDidLoad() {
        super.viewDidLoad()
      Analytics.logEvent(AnalyticsEventLevelStart, parameters: [
            AnalyticsParameterItemID: "id 4",
            AnalyticsParameterItemName: "ОВ_УКР_SmartBox_4 ",
            AnalyticsParameterContentType: "пользователь зашел в ОВ_УКР_SmartBox_4 "
            ])
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        
        self.mainView.addressHeader.text = NSLocalizedString("checkout.recipient.address.header", comment: "")

        self.mainView.submitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
        
        controller = AddressFormController(with: mainView, and: addressTo, viewController: self)
        controller?.viewDidLoad()

    }

    @objc func submitPressed() {
        createAddress()
    }
    
    func createAddress() {
        self.mainView.submitButton.isEnabled = false
        UPAPIManager.createAddress(postCode: self.mainView.indexField.text, region: self.mainView.regionField.text, district: self.mainView.districtField.text, city:  self.mainView.cityField.text, street: self.mainView.streetField.text, houseNumber: self.mainView.houseField.text, apartmentNumber: self.mainView.appartmentField.text) { (result) in
            switch result {
            case .success(let data):
                self.savedAddress = data
                self.order?.recipient?.addressId = data.id
                self.createUser()
                
            case .partialSuccess(let string):
                self.showAlert(with: string)
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
            }
        }
    }
    
    func createUser() {
        UPAPIManager.createUser(user: (self.order?.recipient)!) { (result) in
            switch result {
            case .success(let data):
                self.order?.recipient = data
                self.order?.recipient?.addressId = self.savedAddress?.id
                self.navigateNext()
                
            case .partialSuccess(let string):
                self.showAlert(with: string)
            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
            }
        }
    }

    
    func navigateNext() {
        
        UPAPIManager.submitSmartBoxOrder(order: self.order!, boxUUID: self.order!.uuid!) { (result) in
            switch result {
            case .success(let data):
                let vc = CheckoutResultViewController()
                vc.order = data
                self.navigationController?.pushViewController(vc, animated: true)
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }
        
    }

}

extension SmartBoxRecepientAddressViewController: AddressViewProtocol {
    func showAlert(with text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        self.mainView.submitButton.isEnabled = true

    }

    func push(viewController: IndexSearchViewController) {
        view.endEditing(true)
        navigationController?.pushViewController(viewController, animated: true)
    }

}
