//
//  CheckoutPriceView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/15/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class CheckoutPriceView: UIView {
    
    private let header = UPHeader()
    
    let typeButton = UIButton()
    let bottomArrow = UIImageView()
      
    private let separator = UILabel()
    let valueButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let valueField = UPTextField()
    
    let postPayButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    let postPayField = UPTextField()
    
    let fragileButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    
    let recomendedButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    
    let smsButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
    
   // let paidResipmentButton = UPHorizontalButton(normalImageName: "checkbox", selectedImageName: "checkboxSelected")
      
    private let resultView = UIView()
    private let resultLabel = UILabel()
    let resultValue = UILabel()
    
    let submitButton = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.addSubview(header)
        header.text = NSLocalizedString("checkout.price.header", comment: "")
        
        self.addSubview(typeButton)
        typeButton.backgroundColor = .clear
        typeButton.setTitleColor(UPColor.darkGray, for: .normal)
        typeButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        typeButton.contentHorizontalAlignment = .left
        typeButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
      
      self.addSubview(bottomArrow)
      bottomArrow.image = UIImage(named: "chevron_down")
      bottomArrow.contentMode = .scaleAspectFit
      
        self.addSubview(separator)
        separator.backgroundColor = UPColor.separatorGray
        
        self.addSubview(valueButton)
        valueButton.backgroundColor = .clear
        valueButton.label.text = NSLocalizedString("checkout.price.value", comment: "")
        valueButton.isSelected = false
        valueButton.label.textAlignment = .left
        self.addSubview(valueField)
        
        self.addSubview(postPayButton)
        postPayButton.backgroundColor = .clear
        postPayButton.label.text = NSLocalizedString("checkout.price.postpay", comment: "")
        postPayButton.isSelected = false
        postPayButton.label.textAlignment = .left
        self.addSubview(postPayField)
        
        self.addSubview(fragileButton)
        fragileButton.backgroundColor = .clear
        fragileButton.label.text = NSLocalizedString("checkout.price.fragile", comment: "")
        fragileButton.isSelected = false
        fragileButton.label.textAlignment = .left

        self.addSubview(recomendedButton)
        recomendedButton.backgroundColor = .clear
        recomendedButton.label.text = NSLocalizedString("checkout.price.recomended", comment: "")
        recomendedButton.isSelected = false
        recomendedButton.label.textAlignment = .left

        self.addSubview(smsButton)
        smsButton.backgroundColor = .clear
        smsButton.label.text = NSLocalizedString("checkout.price.sms", comment: "")
        smsButton.isSelected = false
        smsButton.label.textAlignment = .left
      
 //     self.addSubview(paidResipmentButton)
//      paidResipmentButton.backgroundColor = .clear
//      paidResipmentButton.label.text = "Оплата одержувачем"
//      paidResipmentButton.isSelected = false
//      paidResipmentButton.label.textAlignment = .left
        
        self.addSubview(resultView)
        resultView.backgroundColor = .white
        resultView.addSubview(resultLabel)
        resultLabel.text = NSLocalizedString("checkout.price.result", comment: "")
        resultLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        resultLabel.textColor = UPColor.darkGray
        resultView.addSubview(resultValue)
        resultValue.text = "..."
        resultValue.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        resultValue.textColor = UPColor.darkGray
        
        self.addSubview(submitButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //let currentWidth = UIScreen.main.bounds.size.width
        
        header.pin.top(20).left(20).right(20).height(15)
        typeButton.pin.below(of: header).marginTop(10).left(20).right(20).height(40)
         bottomArrow.pin.below(of: header).marginTop(10).right(50).height(25).width(25)
        separator.pin.below(of: typeButton).marginTop(10).right(20).left(20).height(1)
        valueField.pin.below(of: separator).marginTop(10).right(20).height(50).width(120)
        valueButton.pin.left(20).height(50).right(to: valueField.edge.left).vCenter(to: valueField.edge.vCenter)
        postPayField.pin.below(of: valueField).marginTop(10).right(20).height(50).width(120)
        postPayButton.pin.left(20).height(50).right(to: postPayField.edge.left).vCenter(to: postPayField.edge.vCenter)

        fragileButton.pin.left(20).height(30).right(20).top(to: postPayField.edge.bottom).marginTop(10)
        
        recomendedButton.pin.left(20).height(30).right(20).top(to: fragileButton.edge.bottom).marginTop(10)
        
        smsButton.pin.left(20).height(30).right(20).top(to: recomendedButton.edge.bottom).marginTop(10)
        //paidResipmentButton.pin.left(20).height(30).right(20).top(to: smsButton.edge.bottom).marginTop(10)
      
        submitButton.pin.left().bottom(64).right().height(64)
        resultView.pin.above(of: submitButton).left().right().height(64)
        
        resultValue.pin.vCenter().right(20).width(80).height(of: resultView)
        resultLabel.pin.vCenter().left(20).before(of: resultValue).height(of: resultView)
        
    }


}
