//
//  OnboardView.swift
//  UkrPoshta
//
//  Created by Zhekon on 14.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class OnboardView: UIView {
    
    let onboardScrollView = UIScrollView()
    let pageScroll = UIPageControl()
    let mainViewFirst = OnboardSingleView()
    let mainViewSecond = OnboardSingleView()
    let mainViewThird = OnboardSingleView()
    let mainViewFourth = OnboardSingleView()
    let mainViewFifth = OnboardSingleView()
    let skipButton = UIButton()
    let nextButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        addSubview(onboardScrollView)
        onboardScrollView.backgroundColor = UPColor.whiteSmoke
        onboardScrollView.isPagingEnabled = true
    
        mainViewFirst.infoLabel.text = NSLocalizedString("infoLabelTitleOne", comment: "")
        mainViewFirst.infoLabelSubtitle.text = NSLocalizedString("infoLabelSubTitleOne", comment: "")
        mainViewFirst.infoImage.image = UIImage(named: "30")
        skipButton.setTitle(NSLocalizedString("buttonNamed", comment: ""), for: .normal)
        skipButton.setTitleColor(UPColor.lightGray, for: .normal)
        mainViewFirst.addSubview(skipButton)
        onboardScrollView.addSubview(mainViewFirst)

        mainViewSecond.infoLabel.text = NSLocalizedString("infoLabelTitleTwo", comment: "")
        mainViewSecond.infoLabelSubtitle.text = NSLocalizedString("infoLabelSubTitleTwo", comment: "")
        mainViewSecond.infoImage.image = UIImage(named: "31")
        onboardScrollView.addSubview(mainViewSecond)
        
        mainViewThird.infoLabel.text = NSLocalizedString("infoLabelTitleThree", comment: "")
        mainViewThird.infoLabelSubtitle.text = NSLocalizedString("infoLabelSubTitleThree", comment: "")
        mainViewThird.infoImage.image = UIImage(named: "32")
        onboardScrollView.addSubview(mainViewThird)
        
        mainViewFourth.infoLabel.text = NSLocalizedString("infoLabelTitleFour", comment: "")
        mainViewFourth.infoLabelSubtitle.text = NSLocalizedString("infoLabelSubTitleFour", comment: "")
        mainViewFourth.infoImage.image = UIImage(named: "33")
        onboardScrollView.addSubview(mainViewFourth)
        
        mainViewFifth.infoLabel.text = NSLocalizedString("infoLabelTitleFive", comment: "")
        mainViewFifth.infoLabelSubtitle.text = NSLocalizedString("infoLabelSubTitleFive", comment: "")
        mainViewFifth.infoImage.image = UIImage(named: "34")
        onboardScrollView.addSubview(mainViewFifth)
        
        self.addSubview(pageScroll)
        pageScroll.numberOfPages = 5
        pageScroll.currentPage = 0
        pageScroll.pageIndicatorTintColor = UPColor.darkGray
        pageScroll.currentPageIndicatorTintColor = UPColor.headerLightYellow
        pageScroll.isEnabled = false
        nextButton.setTitle(NSLocalizedString("buttonNamedTwo", comment: ""), for: .normal)
        nextButton.setTitleColor(UPColor.lightGray, for: .normal)
        self.addSubview(nextButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let height = self.frame.size.height
        onboardScrollView.contentSize = CGSize(width: self.frame.size.width * 5, height: self.frame.size.height - 25)
        onboardScrollView.pin.left().top().bottom().right()
        mainViewFirst.pin.top().left().bottom().width(self.frame.size.width)
        mainViewSecond.pin.left(to: mainViewFirst.edge.right).top().bottom().width(self.frame.size.width)
        mainViewThird.pin.left(to: mainViewSecond.edge.right).top().bottom().width(self.frame.size.width)
        mainViewFourth.pin.left(to: mainViewThird.edge.right).top().bottom().width(self.frame.size.width)
        mainViewFifth.pin.left(to: mainViewFourth.edge.right).top().bottom().width(self.frame.size.width)
        skipButton.pin.top(10).left(10).width(120).height(40)
        pageScroll.pin.left().right().bottom(height * 0.16).height(10)
        nextButton.pin.left().right().bottom(height * 0.08).height(20)
    }
}




