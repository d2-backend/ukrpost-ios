//
//  OnboardViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 14.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

public let kUserDefault = UserDefaults.standard
class OnboardViewController: UIViewController, UIScrollViewDelegate {
    
    let mainView = OnboardView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showStartAnimation()
        self.mainView.frame = self.view.frame
        self.view.addSubview(self.mainView)
        self.mainView.onboardScrollView.delegate = self
        self.navigationController?.isNavigationBarHidden = true
        self.mainView.skipButton.addTarget(self, action: #selector(startApplicationPressed), for: .touchUpInside)
        self.mainView.nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
    }
    
    @objc func startApplicationPressed() {
        navigateToMain()
    }
    
    @objc func nextButtonPressed() {
        self.mainView.nextButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.33) {
            self.mainView.nextButton.isEnabled = true}
        if self.mainView.pageScroll.currentPage <= 3 {
            let frame = UIScreen.main.bounds
            let newX = CGFloat(self.mainView.pageScroll.currentPage + 1) * CGFloat(frame.width)
            let rect = CGRect(x:newX, y: 0, width: frame.width, height: frame.height)
            self.mainView.onboardScrollView.scrollRectToVisible(rect, animated: true)
            self.mainView.onboardScrollView.contentOffset.x = CGFloat(UIScreen.main.bounds.size.width) * CGFloat(self.mainView.pageScroll.currentPage)
            self.mainView.pageScroll.currentPage += 1
            nextPushButtonText()
        } else {
            navigateToMain()
        }
    }
    
    func navigateToMain() {
        let vc = MainViewController()
        vc.isAnimationNeeded = false
        let navigationController = UINavigationController(rootViewController: vc)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.tintColor = UPColor.darkGray
        self.navigationController?.present(navigationController, animated: true, completion: nil)
        kUserDefault.set(true, forKey: "isCheck")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let point = scrollView.contentOffset.x
        self.mainView.pageScroll.currentPage = Int(point / UIScreen.main.bounds.width)
        nextPushButtonText()
    }
    
    func nextPushButtonText(){
        if  self.mainView.pageScroll.currentPage != 4 {
            self.mainView.nextButton.setTitle("Продовжити", for: .normal)
        }
        if  self.mainView.pageScroll.currentPage == 4 {
            self.mainView.nextButton.setTitle("Розпочати роботу", for: .normal)
        }
    }
    
    func showStartAnimation()  {
        let startView = UIView()
        let startIcon = UIImageView()

        UIApplication.shared.keyWindow?.addSubview(startView)
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        startView.frame = CGRect(x: 0, y: 0, width: width, height: height)
        startView.backgroundColor = UPColor.headerDarkYellow
        startIcon.frame.size = CGSize(width: 120, height: 160)
        startIcon.center = CGPoint(x: width/2, y: height/2)
        startIcon.image = UIImage(named: "logo_light_gray")
        startIcon.contentMode = .scaleAspectFit
        startView.addSubview(startIcon)
        
        UIView.animate(withDuration: 1.5, delay: 0, options: [ .preferredFramesPerSecond60], animations: {
            startIcon.transform = CGAffineTransform(scaleX: 0.35, y: 0.35)
            startIcon.layer.speed = 1.75
        }) { (bool) in
            UIView.animate(withDuration: 1.75, delay: 0, options: [ .preferredFramesPerSecond60], animations: {
                startIcon.transform = CGAffineTransform(scaleX: 10, y: 10)
                startIcon.layer.opacity = 0.0
                startView.backgroundColor = .clear
            }) { (bool) in
                startView.removeFromSuperview()
            }
        }
    }
}
