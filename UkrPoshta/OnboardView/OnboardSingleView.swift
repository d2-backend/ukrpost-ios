//
//  OnboardSingleView.swift
//  UkrPoshta
//
//  Created by Zhekon on 14.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class OnboardSingleView: UIView {
    
    let infoLabel = UILabel()
    let infoLabelSubtitle = UILabel()
    var infoImage = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        infoLabel.numberOfLines = 0
        infoLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        infoLabel.textColor = UPColor.darkGray
        infoLabel.textAlignment = .center
        infoImage.contentMode = .scaleAspectFill
        infoLabelSubtitle.numberOfLines = 0
        infoLabelSubtitle.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        infoLabelSubtitle.textColor = UPColor.lightGray
        infoLabelSubtitle.textAlignment = .center
        
        addSubview(infoLabel)
        addSubview(infoLabelSubtitle)
        addSubview(infoImage)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let height = self.frame.size.height
        infoImage.pin.left().right().height(height * 0.3).top(height * 0.15)
        infoLabel.pin.below(of: infoImage).marginTop(height * 0.1).left().right().height(30)
        infoLabelSubtitle.pin.below(of: infoLabel).marginTop(5).left().right().height(40)
    }
}






