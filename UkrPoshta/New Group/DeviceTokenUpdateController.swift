//
//  DeviceTokenUpdateController.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/5/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

final class DeviceTokenUpdateController {
    let newToken: String
    var countRequestTryes = 0
    
    init(with newToken: String) {
        self.newToken = newToken
        checkToken()
    }
    
    private func checkToken() {
        guard let oldToken = User.currentDeviceToken() else {
            User.saveDeviceToken(token: newToken)
            return
        }
        if oldToken == newToken {
            return
        }
        updateTokens(newToken: newToken, oldToken: oldToken)
    }
    
    private func updateTokens(newToken: String, oldToken: String) {
        UPAPIManager.refreshDeviceToken(newDeviceToken: newToken, oldDeviceToken: oldToken,
                                        completion: { [weak self] flag in
                                            if !flag {
                                                gloabalThread(after: 30, completion: {
                                                    if self?.countRequestTryes ?? 0 < 5 {
                                                        self?.updateTokens(newToken: newToken, oldToken: oldToken)
                                                    }
                                                })
                                            } else {
                                                User.saveDeviceToken(token: newToken)
                                            }
        })
    }
}

