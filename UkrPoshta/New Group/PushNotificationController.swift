//
//  PushNotificationController.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/5/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

final class PushNotificationController {
    let orderId: String
    var countRequestTryes = 0
    init(with orderId: String) {
        self.orderId = orderId
    }
    
    func sendTokenToServerSide() {
        countRequestTryes += 1
        UPAPIManager.sendOrder(with: orderId) { [weak self] flag in
            if !flag {
                gloabalThread(after: 30, completion: {
                    if self?.countRequestTryes ?? 0 < 5 {
                        self?.sendTokenToServerSide()
                    }
                })
            }
        }
    }
}
