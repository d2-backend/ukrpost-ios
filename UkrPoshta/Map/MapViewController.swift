//
//  StampViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 31.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//
//

import UIKit
import GoogleMaps
import GooglePlaces
import ANLoader
struct CoordinatesOnMap {
      let latitude: String
      let longtitude: String
      let nameOfStamp: String
      let street: String
      let postindex : String
      let city : String
}

class MapViewController: UIViewController, CLLocationManagerDelegate , GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource {
      
      @IBOutlet weak var googleMapsView: GMSMapView!
      @IBOutlet weak var officeInfoMapOutlet: UIButton!
      @IBOutlet weak var streetOutlet: UILabel!
      @IBOutlet weak var nameOfficeOutlet: UILabel!
      @IBOutlet weak var numberOutlet: UILabel!
      @IBAction func infoButtonPressed(_ sender: UIButton) {
          showDetailViewController()
      }
      // для отслеживания текущего местоположения
      var locationManager = CLLocationManager()
      var latitudeMy : Float  = 0.0
      var longtitudeMy : Float = 0.0
      var myCoordinatesList: [CoordinatesOnMap] = []
      var street = String()
      var nameOffice = String()
      var city = String()
      var index = String()
      var latitude = Double()
      var longitude = Double()
      var streetForTableViewArray = [String]()
      var nameStampForTableViewArray = [String]()
      var citysForTableViewArray = [String]()
      var indexesForTableViewArray = [String]()
      var longitudeForTableViewArray = [Double]()
      var latitudeForTableViewArray = [Double]()


      let MBGestureView = UIView()
      let mapTableView = UITableView()
      let SwipeBUtt = UIButton()
      let SwipeGestureImage = UIImageView()
      let cityNameLabel = UILabel()
      var cityCount = UILabel()
      var firstX = CGFloat()
      var firstY = CGFloat()
      var pan = UIPanGestureRecognizer()
      var Tab = UITapGestureRecognizer()
      
      
      func hideBackNavigationButtonText() {
            let imgBack = UIImage(named: "ic_back")
            navigationController?.navigationBar.backIndicatorImage = imgBack
            navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
            navigationItem.leftItemsSupplementBackButton = true
            navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
      }
      
      func showDetailViewController(){
            let vc = DetailMapViewController()
            vc.mainView.nameOffice.text = nameOffice
            vc.mainView.streetLabel.text = "\(street)"+", "+"\(city)"+", "+"\(index)"
            vc.longitude = self.longitude
            vc.latitude = self.latitude
            self.navigationController?.pushViewController(vc, animated: true)
      }
      
      override func viewDidLoad() {
            super.viewDidLoad()
            self.view.backgroundColor = UPColor.whiteSmoke
            hideBackNavigationButtonText()
            officeInfoMapOutlet.isHidden = true
            streetOutlet.isHidden = true
            nameOfficeOutlet.isHidden = true
            numberOutlet.isHidden = true
            
            officeInfoMapOutlet.backgroundColor = .white
            officeInfoMapOutlet.layer.shadowColor = UPColor.darkGray.cgColor
            officeInfoMapOutlet.layer.shadowOpacity = 0.2
            officeInfoMapOutlet.layer.shadowOffset = .zero
            officeInfoMapOutlet.layer.shadowRadius = 2
            officeInfoMapOutlet.layer.cornerRadius = 3
            
            locationManager.delegate = self
            checkUserAutorize()
            initGoogleMaps()
            tableViewSwipe()
      }
      
      // работа с table view логикой
      func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.streetForTableViewArray.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapCell") as! MapCell
            cell.street.text = streetForTableViewArray[indexPath.row]
            cell.postIndex.text = nameStampForTableViewArray[indexPath.row]
            return cell
      }
      
      func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
      {
            let vc = DetailMapViewController()
            vc.mainView.nameOffice.text = nameStampForTableViewArray[indexPath.row]
            vc.mainView.streetLabel.text = "\(streetForTableViewArray[indexPath.row])"+", "+"\(citysForTableViewArray[indexPath.row])"+", "+"\(indexesForTableViewArray[indexPath.row])"
            vc.longitude = longitudeForTableViewArray[indexPath.row]
            vc.latitude = latitudeForTableViewArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
      }
      
      // проверка при открытии приложения
      func checkUserAutorize() {
            if CLLocationManager.locationServicesEnabled() {
                  switch CLLocationManager.authorizationStatus() {
                  case .notDetermined, .restricted, .denied:
                        print("No access")
                        
                  case  .authorizedWhenInUse, .authorizedAlways:
                        locationManager.startUpdatingLocation()
                        locationManager.startMonitoringSignificantLocationChanges()
                        latitudeMy = Float((locationManager.location?.coordinate.latitude)!)
                        longtitudeMy = Float((locationManager.location?.coordinate.longitude)!)
                        self.currentLocationOfficesParse()
                  }
            } else {
                  let locationManager = CLLocationManager()
                  locationManager.requestWhenInUseAuthorization()
            }
      }
      
      
      func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            checkUserAutorize() //здесь проверка статуса авторизации при старте
      }
      
      func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
            print("Error while get location \(error)")
      }
      
      func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            let location = locations.last
            let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 13.0)
            self.googleMapsView.animate(to: camera)
            self.locationManager.stopUpdatingLocation()
      }
      
      
      //работа с гугл картами
      func initGoogleMaps() {
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(latitudeMy), longitude: CLLocationDegrees(longtitudeMy), zoom: 6.0)
            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
            mapView.isMyLocationEnabled = true
            self.googleMapsView.camera = camera
            self.googleMapsView.delegate = self
            self.googleMapsView.isMyLocationEnabled = true
            self.googleMapsView.settings.myLocationButton = true
            self.googleMapsView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
      }
      
      // MARK: GMSMapview делегат
      func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
            self.googleMapsView.isMyLocationEnabled = true
      }
      func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
            self.googleMapsView.isMyLocationEnabled = true
            if (gesture) {
                  mapView.selectedMarker = nil
            }
      }
      
      func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
            // по этому нажатию нужно прятать кнопку с инфой
            officeInfoMapOutlet.isHidden = true
            streetOutlet.isHidden = true
            nameOfficeOutlet.isHidden = true
            numberOutlet.isHidden = true
            self.googleMapsView.settings.myLocationButton = true
            mapTableView.isHidden = false
            SwipeGestureImage.isHidden = false
            SwipeBUtt.isHidden = false
            cityNameLabel.isHidden = false
            cityCount.isHidden = false
      }
      
      func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
            let markerLatitude =  NSString(format:"%.4f", marker.position.latitude)
            let markerLongitude =  NSString(format:"%.4f", marker.position.longitude)
            latitude = marker.position.latitude
            longitude = marker.position.longitude
            officeInfoMapOutlet.isHidden = false
            streetOutlet.isHidden = false
            nameOfficeOutlet.isHidden = false
            numberOutlet.isHidden = false
            mapTableView.isHidden = true
            SwipeGestureImage.isHidden = true
            SwipeBUtt.isHidden = true
            cityNameLabel.isHidden = true
            cityCount.isHidden = true
            for information in myCoordinatesList {
                  let infoLatitude = Double(information.latitude);let infoLatitudeFinally =  NSString(format:"%.4f", infoLatitude!)
                  let infoLongitude = Double(information.longtitude);let infoLongitudeFinally =  NSString(format:"%.4f", infoLongitude!)
                  if markerLatitude == infoLatitudeFinally && markerLongitude ==  infoLongitudeFinally{
                        self.streetOutlet.text = information.street
                        self.nameOfficeOutlet.text = information.nameOfStamp
                        street = information.street
                        nameOffice = information.nameOfStamp
                        city = information.city
                        index = information.postindex
                        self.googleMapsView.settings.myLocationButton = false
                        break
                  }
            }
            return true
      }
      
      // MARK:делегат автозаполнения
      func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
            let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
            self.googleMapsView.camera = camera
            self.dismiss(animated: true, completion: nil)
            latitudeMy = Float(place.coordinate.latitude)
            longtitudeMy = Float(place.coordinate.longitude)
            nameStampForTableViewArray = []
            streetForTableViewArray = []
            indexesForTableViewArray = []
            citysForTableViewArray = []
            parseAllOffices()
            ANLoader.hide()
      }
      
      func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
            print("ERROR AUTO COMPLETE \(error)")
      }
      
      func wasCancelled(_ viewController: GMSAutocompleteViewController) {
            self.dismiss(animated: true, completion: nil) // when cancel search
      }
      
      @IBAction func openSearchAddress(_ sender: UIBarButtonItem) {
            let autoCompleteController = GMSAutocompleteViewController()
            autoCompleteController.delegate = self
            
            self.locationManager.startUpdatingLocation()
            self.present(autoCompleteController, animated: true, completion: nil)
      }
      
      
      // парс данных
      func currentLocationOfficesParse(){
            ANLoader.showLoading()
            let roundLatitude = Float(round(10000*(locationManager.location?.coordinate.latitude)!)/10000)
            let roundLongitude = Float(round(10000*(locationManager.location?.coordinate.longitude)!)/10000)
            UPAPIManager.getCoordinates(long: roundLongitude  , lat: roundLatitude ) { (result) in
                  switch result {
                  case .success(let data):
                        var cityCounter = 0
                        let coordinatesList = data
                        for coordMap in coordinatesList {
                              cityCounter += 1
                              self.myCoordinatesList = coordinatesList
                              let position = CLLocationCoordinate2D(latitude: (Double(coordMap.latitude))!, longitude: Double(coordMap.longtitude)!)
                              let marker = GMSMarker(position: position)
                              marker.title = nil
                              marker.snippet = nil
                              marker.map = self.googleMapsView
                              marker.icon = UIImage(named: "map_marker")
                               self.cityNameLabel.text = coordMap.city
                              self.cityCount.text = "Показати всi (\(cityCounter))"
                              print(cityCounter)
                              self.streetForTableViewArray.append(coordMap.street)
                              self.nameStampForTableViewArray.append(coordMap.nameOfStamp)
                              self.citysForTableViewArray.append(coordMap.city)
                              self.indexesForTableViewArray.append(coordMap.postindex)
                              self.latitudeForTableViewArray.append(Double(coordMap.latitude)!)
                              self.longitudeForTableViewArray.append(Double(coordMap.longtitude)!)
                              self.mapTableView.reloadData()
                              ANLoader.hide()
                        }
                  case .partialSuccess(let string):
                        print(string)
                        ANLoader.hide()
                  case .failure(let error):
                        print(error)
                        ANLoader.hide()
                  }
            }
      }
      
      func parseAllOffices(){
            ANLoader.showLoading()
            UPAPIManager.getCoordinates(long: longtitudeMy, lat: latitudeMy ) { (result) in
                  switch result {
                  case .success(let data):
                        let coordinatesList = data
                        self.myCoordinatesList = coordinatesList
                        var cityCounter = 0
                        for coordMap in coordinatesList {
                              let position = CLLocationCoordinate2D(latitude: (Double(coordMap.latitude))!, longitude: Double(coordMap.longtitude)!)
                              cityCounter += 1
                              let marker = GMSMarker(position: position)
                              marker.title = nil
                              marker.snippet = nil
                              marker.map = self.googleMapsView
                              marker.icon = UIImage(named: "map_marker")
                              self.cityNameLabel.text = coordMap.city
                              self.streetForTableViewArray.append(coordMap.street)
                              self.nameStampForTableViewArray.append(coordMap.nameOfStamp)
                              self.citysForTableViewArray.append(coordMap.city)
                              self.indexesForTableViewArray.append(coordMap.postindex)
                              self.cityCount.text = "Показати всi (\(cityCounter))"
                              self.mapTableView.reloadData()
                              ANLoader.hide()
                        }
                  case .partialSuccess(let string):
                        print(string)
                        ANLoader.hide()
                  case .failure(let error):
                        print(error)
                        ANLoader.hide()
                  }
            }
      }
      
      // отрисовка и поведение tableView
      func tableViewSwipe() {
            self.MBGestureView.frame = CGRect(x: 10,y: (self.view.frame.size.height - 114), width: self.view.frame.size.width  - 20, height: self.view.frame.size.height)
            
            self.MBGestureView.backgroundColor =  UIColor.black
            
            self.SwipeBUtt.frame = CGRect(x: 0, y: 0, width: self.MBGestureView.frame.size.width,height: 50)
            self.SwipeBUtt.backgroundColor = .clear
            
            self.SwipeGestureImage.frame = CGRect(x: (self.SwipeBUtt.frame.size.width/2 - 50/2), y: 0, width: 50,height: 50)
            self.cityNameLabel.frame = CGRect(x:10, y: 0, width: 150,height: 50)
            self.cityNameLabel.textAlignment = .left
            self.cityNameLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
            
            self.cityCount.frame = CGRect(x:self.MBGestureView.frame.size.width - 150, y: 0, width: 150,height: 50)
            self.cityCount.textAlignment = .right
            self.cityCount.font = UIFont.boldSystemFont(ofSize: 14)
            
            self.SwipeGestureImage.image = UIImage.init(named: "up")
            self.SwipeBUtt.addSubview(self.SwipeGestureImage)
            
            self.mapTableView.frame = CGRect(x: 0, y: self.SwipeBUtt.frame.size.height + 1,width: self.MBGestureView.frame.size.width,height: (self.MBGestureView.frame.size.height - (self.SwipeBUtt.frame.size.height * 3)))
            self.MBGestureView.backgroundColor = UIColor.clear
            self.mapTableView.layer.cornerRadius = 6
            self.MBGestureView.addSubview(self.cityCount)
            self.MBGestureView.addSubview(self.cityNameLabel)
            self.MBGestureView.addSubview(self.SwipeBUtt)
            self.MBGestureView.addSubview(self.mapTableView)
            self.view.addSubview(self.MBGestureView)
            
            self.mapTableView.register(UINib.init(nibName: "DatingPersonCell", bundle: nil), forCellReuseIdentifier: "MapCell")
            self.mapTableView.estimatedRowHeight = 72
            
            self.mapTableView.delegate = self
            self.mapTableView.dataSource = self
            self.pan.delegate = self
            self.pan = UIPanGestureRecognizer(target: self, action: #selector(self.move))
            self.pan.minimumNumberOfTouches = 1
            self.pan.maximumNumberOfTouches = 1
            self.MBGestureView.addGestureRecognizer(self.pan)
            self.Tab.delegate = self
            self.Tab = UITapGestureRecognizer(target: self, action: #selector(self.HandleTab))
            self.SwipeBUtt.addGestureRecognizer(self.Tab)
            self.mapTableView.separatorColor = UIColor.black
      }
      
      @objc func HandleTab(_ sender: UITapGestureRecognizer) {
            self.actofGesture(self)
      }
      
      @objc func move(_ sender: AnyObject) {
            //   Pan Gesture
            self.view.bringSubview(toFront: (sender as! UIPanGestureRecognizer).view!)
            var translatedPoint = (sender as! UIPanGestureRecognizer).translation(in: self.view)
            if (sender as! UIPanGestureRecognizer).state == .began {
                  firstX = sender.view.center.x
                  firstY = sender.view.center.y
            }
            translatedPoint = CGPoint(x: firstX, y: firstY + translatedPoint.y)
            sender.view.center = translatedPoint
            if (sender as! UIPanGestureRecognizer).state == .ended {
                  let velocityX: CGFloat = (0.2 * (sender as! UIPanGestureRecognizer).velocity(in: self.view).x)
                  let finalX: CGFloat = firstX
                  var finalY: CGFloat = translatedPoint.y + (0.35 * (sender as! UIPanGestureRecognizer).velocity(in: self.view).y)
                  
                  let screenSize = UIScreen.main.bounds.size
                  let height = screenSize.height
                  var  UPHeight = CGFloat()
                  var  DownHeight = CGFloat()
                  if height == 568
                  {
                        UPHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) + 300
                        DownHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) - 20
                  }
                  else if height == 667
                  {
                        UPHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) + 378
                        DownHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) - 20
                  }
                  else if height == 736
                  {
                        UPHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) + 445
                        DownHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) - 20
                  }
                  if translatedPoint.y < UPHeight {
                        finalY = DownHeight
                        self.SwipeGestureImage.image = UIImage.init(named: "up")
                        googleMapsView.isHidden = true
                  }
                  else if translatedPoint.y > DownHeight {
                        finalY = UPHeight;
                        self.SwipeGestureImage.image = UIImage.init(named: "up")
                        googleMapsView.isHidden = false

                  }
                  let animationDuration: CGFloat = (abs(velocityX) * 0.0002) + 0.2
                  print("finalX = \(finalX) , finalY = \(finalY)")
                  UIView.beginAnimations(nil, context: nil)
                  UIView.setAnimationDuration(TimeInterval(animationDuration))
                  UIView.setAnimationCurve(.easeOut)
                  UIView.setAnimationDelegate(self)
                  //            UIView.setAnimationDidStopSelector(#selector(self.animationDidFinish))
                  sender.view.center = CGPoint(x: finalX, y: finalY)
                  UIView.commitAnimations()
            }
      }
      
      @IBAction func actofGesture(_ sender: AnyObject)
      {
            // Tab Gesture
            var finalY: CGFloat
            let finalX: CGFloat = self.MBGestureView.center.x
            print("\(self.MBGestureView.center.y)")
            var  UPHeight = CGFloat()
            var  DownHeight = CGFloat()
            let screenSize = UIScreen.main.bounds.size
            let height = screenSize.height
            if height == 568
            {
                  UPHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) + 400
                  DownHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) - 20
            }
            else if height == 667
            {
                  UPHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) + 478
                  DownHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) - 20
            }
            else if height == 736
            {
                  UPHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) + 545
                  DownHeight = (self.view.frame.size.height - self.view.frame.size.height / 3) - 20
            }
            if self.MBGestureView
                  .center.y >= UPHeight {
                  finalY = DownHeight
                  self.SwipeGestureImage.image = UIImage.init(named: "up")
                  googleMapsView.isHidden = true
            }
            else {
                  finalY = UPHeight
                  self.SwipeGestureImage.image = UIImage.init(named: "up")
                  googleMapsView.isHidden = false
            }
            
            UIView.beginAnimations(nil, context: nil)
            UIView.setAnimationDuration(TimeInterval( 0.4))
            UIView.setAnimationCurve(.easeOut)
            UIView.setAnimationDelegate(self)
            self.MBGestureView.center = CGPoint(x: finalX, y: finalY)
            UIView.commitAnimations()
      }
}

