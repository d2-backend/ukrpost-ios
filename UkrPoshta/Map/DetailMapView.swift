//
//  DetailMap.swift
//  UkrPoshta
//
//  Created by Zhekon on 01.06.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetailMapView: UIView {

    let streetLabel = UILabel()
    let numberButton = UIButton()
    let numberLabel = UILabel()
    let nameOfficeOutlet = UILabel()
    let infoImage = UIImageView()
    let nameOffice = UILabel()
    let phoneImage = UIImageView()
    let locationImage = UIImageView()
    let paveTheWayButton = UIButton()
    let paveTheWayLabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        streetLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        streetLabel.textColor = UPColor.darkGray
        streetLabel.backgroundColor = UPColor.whiteSmoke
        streetLabel.numberOfLines = 0

        numberButton.backgroundColor = UPColor.whiteSmoke
        
        numberLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        numberLabel.textColor = UPColor.darkGray
        numberLabel.backgroundColor = UPColor.whiteSmoke
        numberLabel.text = "0 800 300-545"
        numberLabel.textAlignment = .left
        
        nameOffice.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        nameOffice.textColor = UPColor.darkGray
        nameOffice.backgroundColor = .white
        
        nameOfficeOutlet.backgroundColor = .white
        nameOfficeOutlet.layer.shadowColor = UPColor.darkGray.cgColor
        nameOfficeOutlet.layer.shadowOpacity = 0.2
        nameOfficeOutlet.layer.shadowOffset = .zero
        nameOfficeOutlet.layer.shadowRadius = 2
        nameOfficeOutlet.layer.cornerRadius = 3
        nameOfficeOutlet.textColor = UPColor.darkGray
        nameOfficeOutlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        nameOfficeOutlet.contentMode = .center
        phoneImage.contentMode = .scaleAspectFill
        phoneImage.image = UIImage(named:"icon_phone")
        locationImage.contentMode = .scaleAspectFill
        locationImage.image = UIImage(named:"icon_pin_location")
        infoImage.contentMode = .scaleAspectFill
        infoImage.image = UIImage(named:"cover_know_more_about_smartbox")
        
        paveTheWayButton.backgroundColor = UPColor.dimmingGray
        paveTheWayButton.layer.cornerRadius = 15
        
        paveTheWayButton.addSubview(paveTheWayLabel)
        paveTheWayLabel.text = "Прокласти маршрут"
        paveTheWayLabel.textColor = UPColor.darkGray
        paveTheWayLabel.textAlignment = .center
        paveTheWayLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)

        addSubview(streetLabel)
        addSubview(infoImage)
        addSubview(numberButton)
        numberButton.addSubview(numberLabel)
        addSubview(nameOfficeOutlet)
        addSubview(nameOffice)
        addSubview(phoneImage)
        addSubview(locationImage)
        addSubview(paveTheWayButton)
        
      }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        infoImage.pin.left(0).right(0).height(225).top(0)
        nameOfficeOutlet.pin.below(of: infoImage).marginTop(0).left(0).right(0).height(60)
        nameOffice.pin.below(of: infoImage).marginTop(0).left(50).right().height(60)
        numberButton.pin.below(of: nameOfficeOutlet).marginTop(10).left(50).right(50).height(25)
        numberLabel.pin.all()
        phoneImage.pin.below(of: nameOfficeOutlet).marginTop(10).left(20).right(to: numberButton.edge.left).marginRight(0).bottom().height(25).width(13)
        streetLabel.pin.below(of: numberButton).marginTop(10).left(50).right(0).height(60)
        paveTheWayButton.pin.below(of: streetLabel).marginTop(10).horizontally(50).height(40)
        paveTheWayLabel.pin.all()
        locationImage.pin.below(of: phoneImage).marginTop(30).left(20).right(to: streetLabel.edge.left).marginRight(0).bottom().height(25).width(13)
    }
}
