//
//  DetailMapViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 01.06.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetailMapViewController: UIViewController {

    let mainView = DetailMapView()
    var latitude = Double()
    var longitude = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(mainView)
        mainView.numberButton.addTarget(self, action: #selector(phoneNumberTapped), for: .touchUpInside)
        mainView.paveTheWayButton.addTarget(self, action: #selector(getDirection), for: .touchUpInside)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mainView.frame = self.view.bounds
    }

    @objc func phoneNumberTapped() {
        let number = "0800300545"
        if let url = URL(string: "tel://\(number)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    @objc func getDirection() {
        if (UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!)) {
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://" + "?daddr=\(latitude),\(longitude)&zoom=12&directionsmode=driving")!)
        } else {
            let alertController = UIAlertController(title: "Повiдомлення", message: "Встановiть будь ласка додаток 'Google Map'", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "закрити", style: .cancel) { (action:UIAlertAction) in
            }
            alertController.addAction(actionCancel)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}
