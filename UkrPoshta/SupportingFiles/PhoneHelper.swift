//
//  PhoneHelper.swift
//  UkrPoshta
//
//  Created by Zhekon on 22.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import UIKit

func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    switch textField {
    case is UITextField: // напиши сюда свой текстфилд
        let oldString = textField.text
        let newString = oldString! + string
        if (newString.count <= 6 || newString.count > 19) {
            return false
        } else {
            var oldRawString = rawStringFrom(displayString: oldString!)
            
            switch string {
            case "":
                oldRawString.removeLast()
            case "0","1","2","3","4","5","6","7","8","9" :
                oldRawString = oldRawString + string
                
            default :
                return false
            }
            
            textField.text = displayStringFrom(rawString: oldRawString)
        }
        return false
    default:
        return true
    }
}

func rawStringFrom(displayString: String) -> String {
    let arrayOfDigits = displayString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
    let result = arrayOfDigits.joined(separator: "")
    
    return result
}

func displayStringFrom(rawString: String?) -> String {
    if rawString != nil {
        var resultString = rawString
        if (resultString?.count)! >= 1 {
            resultString!.insert("+", at: resultString!.startIndex)
        }
        if (resultString?.count)! >= 3 {
            resultString!.insert(" ", at: resultString!.index(resultString!.startIndex, offsetBy: 3))
            resultString!.insert("(", at: resultString!.index(resultString!.startIndex, offsetBy: 4))
            
        }
        if (resultString?.count)! >= 8 {
            resultString!.insert(")", at: resultString!.index(resultString!.startIndex, offsetBy: 8))
            resultString!.insert(" ", at: resultString!.index(resultString!.startIndex, offsetBy: 9))
        }
        
        if (resultString?.count)! >= 13 {
            resultString!.insert("-", at: resultString!.index(resultString!.startIndex, offsetBy: 13))
        }
        
        if (resultString?.count)! >= 16 {
            resultString!.insert("-", at: resultString!.index(resultString!.startIndex, offsetBy: 16))
        }
        
        return resultString!
    } else {
        return "+38 (0"
    }
}
