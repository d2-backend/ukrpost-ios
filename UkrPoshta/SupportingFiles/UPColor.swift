//
//  UPColor.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/11/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPColor: NSObject {
    
    static let lightGray: UIColor = UIColor(red: 153/255, green: 162/255, blue: 170/255, alpha: 1.0)
    static let darkGray: UIColor = UIColor(red: 61/255, green: 78/255, blue: 93/255, alpha: 1.0)
    static let whiteSmoke: UIColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
    static let dimmingGray: UIColor = UIColor(red: 231/255, green: 236/255, blue: 241/255, alpha: 0.7)
    static let green: UIColor = UIColor(red: 150/255, green: 212/255, blue: 43/255, alpha: 1.0)

    static let headerLightYellow: UIColor = UIColor(red: 1.0, green: 0.89, blue: 0.44, alpha: 1.0)
    static let headerDarkYellow: UIColor = UIColor(red: 0.99, green: 0.76, blue: 0.24, alpha: 1.0)

    static let sunglow: UIColor = UIColor(red: 253/255, green: 193/255, blue: 61/255, alpha: 1.0)
    static let separatorGray: UIColor = UIColor(red: 231/255, green: 236/255, blue: 241/255, alpha: 1.0)

    static func yellowGradient(frame: CGRect) -> CAGradientLayer {
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = [UPColor.headerLightYellow.cgColor, UPColor.headerDarkYellow.cgColor]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.zPosition = -1
        return gradient
    }
}
