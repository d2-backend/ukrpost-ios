//
//  UPPhoneHelper.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/22/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPPhoneHelper {

   static func rawStringFrom(displayString: String?) -> String? {
        if let aString = displayString {
            if aString != "" {
                let arrayOfDigits = aString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
                let result = arrayOfDigits.joined(separator: "")
                
                return result
            } else {
                return "380"
            }
        } else {
            return "380"
        }
    }
    
    static func displayStringFrom(rawString: String?) -> String {
        if let trueRawString = self.rawStringFrom(displayString: rawString) {
        
            var resultString = trueRawString
            if resultString.count >= 1 {
                resultString.insert("+", at: resultString.startIndex)
            }
            if resultString.count >= 3 {
                resultString.insert(" ", at: resultString.index(resultString.startIndex, offsetBy: 3))
                resultString.insert("(", at: resultString.index(resultString.startIndex, offsetBy: 4))
                
            }
            if resultString.count >= 8 {
                resultString.insert(")", at: resultString.index(resultString.startIndex, offsetBy: 8))
                resultString.insert(" ", at: resultString.index(resultString.startIndex, offsetBy: 9))
            }
            
            if resultString.count >= 13 {
                resultString.insert("-", at: resultString.index(resultString.startIndex, offsetBy: 13))
            }
            
            if resultString.count >= 16 {
                resultString.insert("-", at: resultString.index(resultString.startIndex, offsetBy: 16))
            }
            
            return resultString
        } else {
            return "+38 (0"
        }

    }
    
    static func foreignRawStringFrom(displayString: String?) -> String? {
        if let aString = displayString {
            if aString != "" {
                let arrayOfDigits = aString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
                let result = arrayOfDigits.joined(separator: "")
                
                return result
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    static func foreignDisplayStringFrom(rawString: String?) -> String {
        if let trueRawString = self.foreignRawStringFrom(displayString: rawString) {
            
            var resultString = trueRawString
            
            if resultString.count  >= 1 {
                resultString.insert("+", at: resultString.startIndex)
            }
            if resultString.count >= 3 {
                resultString.insert(" ", at: resultString.index(resultString.startIndex, offsetBy: 3))
                resultString.insert("(", at: resultString.index(resultString.startIndex, offsetBy: 4))
                
            }
            if resultString.count >= 8 {
                resultString.insert(")", at: resultString.index(resultString.startIndex, offsetBy: 8))
                resultString.insert(" ", at: resultString.index(resultString.startIndex, offsetBy: 9))
            }
            
            if resultString.count >= 13 {
                resultString.insert("-", at: resultString.index(resultString.startIndex, offsetBy: 13))
            }
            
            if resultString.count >= 16 {
                resultString.insert("-", at: resultString.index(resultString.startIndex, offsetBy: 16))
            }
            
            return resultString
        } else {
            return "+"
        }
        
    }
}
