//
//  Helper.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/4/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

func mainThread(_ completion: @escaping () -> ()) {
    DispatchQueue.main.async(execute: completion)
}

func gloabalThread(after: Double, completion: @escaping () -> ()) {
    DispatchQueue.global().asyncAfter(deadline: .now() + after) {
        completion()
    }
}

