//
//  UIViewController+extensions.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/2/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func hideTextBackNavigationButton() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "Розрахунок вартостi", style: .plain, target: self, action: nil)
    }
}
