//
//  String+extensions.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/2/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }

    var districtExtension: String {
        return districtIsACity ? self: "\(self) р-н."
    }

    var districtIsACity: Bool {
        return contains("(місто)")
    }

    var condensedWhitespace: String {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        return components.filter { !$0.isEmpty }.joined(separator: " ")
    }
}
