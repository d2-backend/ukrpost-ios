//
//  IndexesInputView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class IndexesInputView: UIScrollView {

    let viewA = UIView()
    let viewB = UIView()
    
    let headerAddresses = UILabel()
    let regionButton = UPTextButton()
    let districtButton = UPTextButton()
    let cityButton = UPTextButton()
    let streetButton = UPTextButton()
    let houseField = UPTextField()

    let getIndexButton = UPSubmitButton()
    let getAddressButton = UPSubmitButton()
    let toolBarButton = UPSubmitButton()

    let headerIndexes = UILabel()
    let indexTextField = UPTextField()

    let tableView = UITableView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.isPagingEnabled = true
        self.backgroundColor = UPColor.whiteSmoke
        self.isScrollEnabled = false
        self.addSubview(viewA)
        
        self.addSubview(viewB)
        
        viewA.addSubview(headerAddresses)
        headerAddresses.text = NSLocalizedString("addressToIndexHeader.title", comment: "")
        headerAddresses.textColor = UPColor.lightGray
        headerAddresses.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        
        viewA.addSubview(regionButton)
        regionButton.setTitle(NSLocalizedString("addressToIndex.region.title", comment: ""), for: .normal)
      
        viewA.addSubview(districtButton)
        districtButton.setTitle(NSLocalizedString("addressToIndex.district.title", comment: ""), for: .normal)

        viewA.addSubview(cityButton)
        cityButton.setTitle(NSLocalizedString("addressToIndex.city.title", comment: ""), for: .normal)
        
        viewA.addSubview(streetButton)
        streetButton.setTitle(NSLocalizedString("addressToIndex.street.title", comment: ""), for: .normal)
        
        viewA.addSubview(houseField)
        houseField.placeholder = NSLocalizedString("addressToIndex.building.short", comment: "")
        houseField.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        houseField.keyboardType = .numbersAndPunctuation
        houseField.textAlignment = .center
                
        viewA.addSubview(getIndexButton)
        getIndexButton.setTitle(NSLocalizedString("addressToIndex.submit.title", comment: ""), for: .normal)

        viewB.addSubview(headerIndexes)
        headerIndexes.text = NSLocalizedString("indexToAddressHeader.title", comment: "")
        headerIndexes.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        headerIndexes.textColor = UPColor.lightGray

        viewB.addSubview(indexTextField)
        indexTextField.placeholder = NSLocalizedString("indexToAddressHeader.title", comment: "")
        indexTextField.keyboardType = .numberPad
        
        let toolView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        toolBarButton.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        toolBarButton.setTitle(NSLocalizedString("addressToIndex.submit.title", comment: ""), for: .normal)
        toolView.addSubview(toolBarButton)
        indexTextField.inputAccessoryView = toolView
        

        viewB.addSubview(tableView)
        tableView.backgroundColor = UPColor.whiteSmoke
        
        viewB.addSubview(getAddressButton)
        getAddressButton.setTitle(NSLocalizedString("addressToIndex.submit.title", comment: ""), for: .normal)


    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let currentWidth = UIScreen.main.bounds.size.width
        self.contentSize = CGSize(width: currentWidth * 2, height: self.frame.size.height)
        
        viewA.pin.left().top().bottom().width(currentWidth)
        viewB.pin.left(to: viewA.edge.right).top().bottom().right().width(currentWidth)
        
        headerAddresses.pin.top(10).left(20).right(20).height(20)
        
        regionButton.pin.top(to: headerAddresses.edge.bottom).marginTop(5).left(20).right(20).height(55)
        districtButton.pin.top(to: regionButton.edge.bottom).marginTop(10).left(20).right(20).height(55)
        cityButton.pin.top(to: districtButton.edge.bottom).marginTop(10).left(20).right(20).height(55)
        
        houseField.pin.top(to: cityButton.edge.bottom).marginTop(10).right(20).height(55).width(80)
        streetButton.pin.top(to: cityButton.edge.bottom).marginTop(10).left(20).height(55).right(to: houseField.edge.left).marginRight(10)
        
        getIndexButton.pin.left().right().bottom(0).height(64)
        
        headerIndexes.pin.top(20).left(20).right(20).height(20)
        indexTextField.pin.top(to: headerIndexes.edge.bottom).marginTop(10).left(20).right(20).height(55)
        tableView.pin.top(to: indexTextField.edge.bottom).marginTop(10).left(0).right(0).bottom(to: getAddressButton.edge.top)
        getAddressButton.pin.left().right().bottom().height(64)

    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }
}
