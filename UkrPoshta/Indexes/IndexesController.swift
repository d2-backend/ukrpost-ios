//
//  IndexesController.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/3/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import UIKit

protocol IndexesControllerProtocol: class {
    func fetchMixedAddress(for postcode: String)
    func config(tableView: UITableView)
}

final class IndexesController: NSObject, IndexesControllerProtocol {
    
    private weak var view: IndexesViewProtocol?
    private var addresses = [MixedAddress]()
    init(with view: IndexesViewProtocol) {
        self.view = view
    }
    
    func fetchMixedAddress(for postcode: String) {
        if postcode.count != 5 {
            clearTable()
            return
        }
        
        
        clearTable()
        UPAPIManager.getAddressBy(postcode: postcode) { [weak self] response in
            switch response {
            case .success(let addresses):
                guard let strongSelf = self else {
                    self?.view?.stopAnimation()
                    return
                }
                
                strongSelf.addresses = strongSelf.genrateMixedAddresses(for: addresses)
                strongSelf.view?.reloadTableView()
                
            case .partialSuccess(let string):
                self?.clearTable()
                self?.view?.showAlert(with: string)
            case .failure(let error):
                print(error.localizedDescription)
                self?.clearTable()
                self?.view?.showAlert(with: error.localizedDescription)
            }
            
            self?.view?.stopAnimation()
        }
    }
    
    private func clearTable() {
        addresses = [MixedAddress]()
        view?.reloadTableView()
    }
    
    private func genrateMixedAddresses(for addresses: [AddressByIndex]) -> [MixedAddress] {
        var mixedAddresses = [String : MixedAddress]()
        addresses.forEach { address in
            if mixedAddresses[address.streetID] != nil {
                mixedAddresses[address.streetID]?.houseNumbers.append(address.houseNumber)
            } else {
                mixedAddresses[address.streetID] = MixedAddress.makeMixedAddress(from: address)
            }
        }
        let array =  mixedAddresses.map {$0.1}
        return array
    }
    
    func config(tableView: UITableView) {
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.estimatedRowHeight = 100
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        tableView.rowHeight = UITableViewAutomaticDimension
        let mixedAddressCellNib = UINib.init(nibName: "MixedAddressCell", bundle: nil)
        tableView.register(mixedAddressCellNib, forCellReuseIdentifier: "MixedAddressCell")
        tableView.dataSource = self
    }
}

extension IndexesController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addresses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MixedAddressCell", for: indexPath) as? MixedAddressCell else {
            fatalError("Can't create MixedAddressCell")
        }
        config(cell, at: indexPath)
        
        return cell
    }
    
    private func config(_ cell: MixedAddressCell, at index: IndexPath) {
        if index.row >= addresses.count {
            return
        }
        
        let address = addresses[index.row]
        cell.streetName.text = address.streetNameDescription
        cell.buildingNumbers.text = address.buildingsDescription
        cell.locationInfo.text = address.locationInfoDescription
    }
    
}
