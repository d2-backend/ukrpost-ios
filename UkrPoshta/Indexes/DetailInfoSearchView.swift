//
//  DetailInfoSearchView.swift
//  UkrPoshta
//
//  Created by Zhekon on 15.06.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetailInfoSearchView: UIView {
      
      let indexHeaderLabel = UILabel()
      let indexLabel = UILabel()
      let betweenView = UIView()
      let areaLabel = UILabel()
      let streetLabel = UILabel()
      let copyButton = UIButton()
      
      override init(frame: CGRect) {
            super.init(frame: frame)
            commonInit()
      }
      
      required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            commonInit()
      }
      
      private func commonInit() {
            self.backgroundColor = UPColor.whiteSmoke
            self.addSubview(indexHeaderLabel)
            indexHeaderLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
            indexHeaderLabel.textColor = UPColor.darkGray
            indexHeaderLabel.text = "Iндекс"
            
            self.addSubview(indexLabel)
            indexLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            indexLabel.textColor = UPColor.darkGray
            self.addSubview(betweenView)
            betweenView.backgroundColor = UPColor.separatorGray
            
            self.addSubview(areaLabel)
            areaLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            areaLabel.textColor = UPColor.darkGray
            self.addSubview(streetLabel)
            streetLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            streetLabel.textColor = UPColor.darkGray
            
            self.addSubview(copyButton)
            copyButton.setImage(UIImage(named: "icon_copy"), for: .normal)
      }
      
      override func layoutSubviews() {
            super.layoutSubviews()
            indexHeaderLabel.pin.top(25).left(20).width(100).height(20)
            indexLabel.pin.below(of: indexHeaderLabel).marginTop(10).left(20).width(100).height(25)
            betweenView.pin.top(to: indexLabel.edge.bottom).marginTop(10).left(20).right(15).height(2)
            copyButton.pin.above(of: betweenView).marginBottom(10).right(20).width(40).height(40)
            areaLabel.pin.top(to: betweenView.edge.bottom).marginTop(15).left(20).width(self.frame.size.width).height(20)
            streetLabel.pin.top(to: areaLabel.edge.bottom).marginTop(0).left(20).width(self.frame.size.width).height(20)
      }
}
