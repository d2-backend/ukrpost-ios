//
//  IndexTabButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class IndexTabButton: UIButton {

    let bottomLine = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.addSubview(bottomLine)
        self.backgroundColor = .white
        self.setTitleColor(UPColor.darkGray, for: .selected)
        self.setTitleColor(UPColor.lightGray, for: .normal)
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let height: CGFloat = 2.0
        bottomLine.pin.left().bottom().right().height(height)
        let gradient = UPColor.yellowGradient(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: height))
        self.bottomLine.layer.addSublayer(gradient)

        
    }
    
    override var isSelected: Bool {
        
        didSet {
            bottomLine.isHidden = !isSelected
        }
    }

}
