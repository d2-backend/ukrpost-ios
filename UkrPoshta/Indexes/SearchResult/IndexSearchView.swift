//
//  IndexSearchView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class IndexSearchView: UIView {

    let searchField = UPTextField()
    let tableView = UITableView()
    let errorMessageLabel = UILabel()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {

        backgroundColor = UPColor.whiteSmoke
        
        addSubview(searchField)
        searchField.clearButtonMode = .always
        searchField.keyboardType = .namePhonePad
        
        addSubview(tableView)
        tableView.register(SuggestionCell.self, forCellReuseIdentifier: "SuggestionCell")
        tableView.register(SuggestionExtendedCell.self, forCellReuseIdentifier: "SuggestionExtendedCell")
        tableView.rowHeight = 40
        tableView.backgroundColor = .clear
        tableView.tableFooterView = UIView(frame: .zero)

        addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true

        addSubview(errorMessageLabel)
        configMessageLabel()
    }

    private func configMessageLabel() {
        errorMessageLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
        errorMessageLabel.textColor = UPColor.lightGray
        errorMessageLabel.numberOfLines = 0
        errorMessageLabel.isHidden = true
        errorMessageLabel.lineBreakMode = .byWordWrapping
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        searchField.pin.left(20).top(20).right(20).height(60)
        activityIndicator.pin.hCenter().top(200)
        errorMessageLabel.pin.left(20).below(of: searchField).marginTop(10).right(20).height(40)
        tableView.pin.left(5).below(of: searchField).marginTop(10).right(20).bottom(74)
    }
}
