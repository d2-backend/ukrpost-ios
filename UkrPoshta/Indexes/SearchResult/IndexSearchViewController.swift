//
//  IndexSearchViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

enum IndexSearchType {
    case region
    case district
    case city
    case street
}

extension IndexSearchType {
    var title: String {
        switch self {
        case .region:
            return NSLocalizedString("addressToIndex.region.title", comment: "")
        case .district:
            return NSLocalizedString("addressToIndex.district.title", comment: "")
        case .city:
            return NSLocalizedString("addressToIndex.city.title", comment: "")
        case .street:
            return NSLocalizedString( "addressToIndex.street.title", comment: "")
        }
    }
}

protocol IndexSearchDelegate {
    func setRegion(region: Region)
    func setDistrict(district: District)
    func setCity(city: City)
    func setStreet(street: Street)
}

class IndexSearchViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    var delegate: IndexSearchDelegate?
    let mainView = IndexSearchView()
    
    var type: IndexSearchType
    
    private var region: Region?
    private var district: District?
    private var city: City?
    private var street: Street?

    private var resultToDisplay:[Any] = []
    private var rawResult:[Any] = []
    
    init(type: IndexSearchType, region: Region?, district: District?, city: City?, street: Street?) {
        self.type = type
        self.region = region
        self.district = district
        self.city = city
        self.street = street
        super.init(nibName: nil, bundle: nil)
        
        switch type {
        case .region:
            self.mainView.searchField.placeholder = NSLocalizedString("addressToIndex.region.placeholder", comment: "")
        case .district:
            self.mainView.searchField.placeholder = NSLocalizedString("addressToIndex.district.placeholder", comment: "")
        case .city:
            self.mainView.searchField.placeholder = NSLocalizedString("addressToIndex.city.placeholder", comment: "")
        case .street:
            self.mainView.searchField.placeholder = NSLocalizedString("addressToIndex.street.placeholder", comment: "")
  
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        self.title = type.title
        hideBackNavigationButtonText()
        super.viewDidLoad()
        self.mainView.frame = self.view.bounds
        self.view.addSubview(mainView)
        self.view.backgroundColor = UPColor.whiteSmoke

        self.mainView.searchField.delegate = self
        
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        
        if type == .region {
            self.mainView.activityIndicator.startAnimating()
            UPAPIManager.getRegionList { (result) in
                switch result {
                case .success(let data):
                    print(data)
                    self.resultToDisplay = data
                    self.rawResult = data
                    self.mainView.tableView.reloadData()
                case .partialSuccess(let string):
                    self.showAlertWith(text: string)
                case .failure(let error):
                    self.showAlertWith(text: error.localizedDescription)
                }
                self.mainView.activityIndicator.stopAnimating()

            }
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mainView.frame = self.view.bounds

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//
//    @objc func keybordWasShown(notification: NSNotification) {
//        let mainFrame = self.view.frame
//        let keyboardRect: CGRect = notification.userInfo!["UIKeyboardBoundsUserInfoKey"] as! CGRect
//        let keyboardHeight = keyboardRect.size.height
//
//        let newHeight = mainFrame.size.height - keyboardHeight + 64
//        self.mainView.frame = CGRect(x: 0, y: 0, width: mainFrame.width, height: newHeight)
//        self.mainView.layoutSubviews()
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            return false
        }
        self.mainView.searchField.textColor = UPColor.darkGray
        self.resultToDisplay = []
        self.mainView.tableView.reloadData()
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }
        
        if self.type == .region {
            if newString != "" {
                for region: Region in self.rawResult as! [Region] {
                    if region.regionUA.lowercased().range(of: newString.lowercased()) != nil {
                        resultToDisplay.append(region)
                    }
                }
            } else {
                self.resultToDisplay = rawResult
            }
            
            self.mainView.tableView.reloadData()
        } else {
            
            if newString.count > 2 {
                self.fetchData(for: newString)
            }
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if type == .region {
            self.resultToDisplay = self.rawResult
        } else {
            self.resultToDisplay = []
        }
        self.mainView.tableView.reloadData()

        return true
    }
    
    func fetchData(for string: String) {
        self.mainView.activityIndicator.stopAnimating()
        self.mainView.activityIndicator.startAnimating()
        if type == .district {
            UPAPIManager.getDistrictList(string: string, regionID: self.region?.regionID) { (result) in
                switch result {
                case .success(let data):
                    self.resultToDisplay = data
                    self.rawResult = data
                    self.mainView.tableView.reloadData()
                case .partialSuccess(let string):
                    print(string)
                    self.mainView.searchField.textColor = .red
                case .failure(let error):
                    print(error)
                    self.mainView.searchField.textColor = .red
                }
                self.mainView.activityIndicator.stopAnimating()
            }
        } else if type == .city {
            UPAPIManager.getCityList(string: string, regionID: self.region?.regionID, districtID: self.district?.districtID) { (result) in
                switch result {

                case .success(let data):
                    self.resultToDisplay = data
                    self.rawResult = data
                    self.mainView.tableView.reloadData()

                case .partialSuccess(_):
                    self.mainView.searchField.textColor = .red
                case .failure(_):
                    self.mainView.searchField.textColor = .red
                }
                self.mainView.activityIndicator.stopAnimating()
            }
        } else if type == .street {
            UPAPIManager.getStreetList(string: string, regionID: self.region?.regionID, districtID: self.district?.districtID, cityID: self.city?.cityID) { (result) in
                switch result {
                    
                case .success(let data):
                    self.resultToDisplay = data
                    self.rawResult = data
                    self.mainView.tableView.reloadData()
                    
                case .partialSuccess(let string):
                    print(string)
                    self.mainView.searchField.textColor = .red

                case .failure(let error):
                    print(error)
                    self.mainView.searchField.textColor = .red
                }
                self.mainView.activityIndicator.stopAnimating()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SuggestionCell") as! SuggestionCell

        if type == .region {
            let currentRegion = resultToDisplay[indexPath.row] as! Region
            cell.mainLabel.text = currentRegion.regionUA
        } else if type == .district {
            let currentDistrict = resultToDisplay[indexPath.row] as! District
            cell.mainLabel.text = currentDistrict.districtUA
        } else if type == .city {
            let currentCity = resultToDisplay[indexPath.row] as! City
            let displayString = currentCity.cityTypeUA + " " + currentCity.cityUA
            cell.mainLabel.text = displayString
        } else if type == .street {
            let currentStreet = resultToDisplay[indexPath.row] as! Street
            let displayString = currentStreet.streetTypeUA + " " + currentStreet.streetUA
            cell.mainLabel.text = displayString
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.type == .region {
            let region = self.resultToDisplay[indexPath.row] as! Region
            delegate?.setRegion(region: region)
        } else if  self.type == .district {
            let district = self.resultToDisplay[indexPath.row] as! District
            delegate?.setDistrict(district: district)
        } else if self.type == .city {
            let city = self.resultToDisplay[indexPath.row] as! City
            delegate?.setCity(city: city)
        } else if self.type == .street {
            let street = self.resultToDisplay[indexPath.row] as! Street
            delegate?.setStreet(street: street)
        }
        
        self.navigationController?.popViewController(animated: true)
    }


    
    func copyToPasteboardAndPop(string: String!) {
        UIPasteboard.general.string = string
        self.navigationController?.popViewController(animated: true)

    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}
