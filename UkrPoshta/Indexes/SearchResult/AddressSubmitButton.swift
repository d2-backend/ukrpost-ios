//
//  AddressSubmitButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/7/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class AddressSubmitButton: UIButton {

    let upperLabel = UILabel()
    let bottomLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)
        self.addSubview(upperLabel)
        upperLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        upperLabel.textColor = UPColor.darkGray
        
        self.addSubview(bottomLabel)
        bottomLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption1)
        bottomLabel.textColor = UPColor.lightGray
        bottomLabel.textAlignment = .right
        
        self.layer.masksToBounds = false
        self.layer.borderColor = UPColor.lightGray.cgColor
        self.layer.borderWidth = 2
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        
        self.layer.cornerRadius = 3.0
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        upperLabel.pin.top(20).left(10).right(10).height(20)
        bottomLabel.pin.below(of: upperLabel).marginTop(5).left(10).right(10).height(15)
        
    }
}
