//
//  SuggestionCell.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/2/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class SuggestionCell: UITableViewCell {

    let mainLabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(mainLabel)
        mainLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        mainLabel.textColor = UPColor.darkGray
        self.backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        mainLabel.pin.left(20).right(20).top(10).height(20)
    }

}
