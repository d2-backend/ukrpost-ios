//
//  DetainInfoViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 15.06.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class DetainInfoViewController: UIViewController {
      
      var index = String()
      var street = String()
      var area = String()
      var house = String()
      
      private let detailMainView = DetailInfoSearchView()
      
      override func viewDidLoad() {
            super.viewDidLoad()
            self.view.addSubview(detailMainView)
            self.detailMainView.frame = self.view.frame
            
            self.detailMainView.copyButton.addTarget(self, action: #selector(copyPressed), for: .touchUpInside)
            
            self.detailMainView.indexLabel.text = index
            self.detailMainView.streetLabel.text = street
            self.detailMainView.areaLabel.text = area + house
      }
      
      @objc func copyPressed() {
            UIPasteboard.general.string = index
            self.navigationController?.popViewController(animated: true)
      }
}




