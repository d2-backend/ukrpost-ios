//
//  IndexesContentView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class IndexesContentView: UIView {

    let contentView = UIView()
    let addressToIndexButton = IndexTabButton()
    let indexToAddressButton = IndexTabButton()
    
    let indexesInputView = IndexesInputView()
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        
        self.contentView.backgroundColor = .darkGray
        self.addSubview(contentView)
        
        contentView.addSubview(indexesInputView)
        
        self.addSubview(addressToIndexButton)
        addressToIndexButton.isSelected = true
        addressToIndexButton.setTitle(NSLocalizedString("addressToIndexButton.title", comment: ""), for: .normal)

        self.addSubview(indexToAddressButton)
        indexToAddressButton.isSelected = false
        indexToAddressButton.setTitle(NSLocalizedString("indexToAddressButton.title", comment: ""), for: .normal)
        
        self.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true


    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let currentWidth = UIScreen.main.bounds.size.width

        self.addressToIndexButton.pin.top().left().height(40).width(currentWidth / 2)
        self.indexToAddressButton.pin.after(of: addressToIndexButton).top().right().height(40).width(currentWidth / 2)
        
        self.contentView.pin.top(to: self.addressToIndexButton.edge.bottom).left().right().bottom(64)
        
        indexesInputView.pin.top().left().bottom().right()
        activityIndicator.pin.center()

    }
    
}
