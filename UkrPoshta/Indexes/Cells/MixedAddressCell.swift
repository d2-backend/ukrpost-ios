//
//  MixedAddressCell.swift
//  UkrPoshta
//
//  Created by Max Tymchii on 6/3/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import UIKit

final class MixedAddressCell: UITableViewCell {
    
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet weak var buildingNumbers: UILabel!
    @IBOutlet weak var locationInfo: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
    }

    private func commonInit() {
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        streetName.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        streetName.textColor = UPColor.darkGray
        streetName.backgroundColor = .clear
        streetName.numberOfLines = 0
        streetName.lineBreakMode = .byWordWrapping

        buildingNumbers.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.subheadline)
        buildingNumbers.textColor = UPColor.darkGray
        buildingNumbers.backgroundColor = .clear
        buildingNumbers.numberOfLines = 0
        buildingNumbers.lineBreakMode = .byWordWrapping

        locationInfo.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        locationInfo.textColor = UPColor.darkGray
        locationInfo.backgroundColor = .clear
        locationInfo.numberOfLines = 0
        locationInfo.lineBreakMode = .byWordWrapping

    }
}
