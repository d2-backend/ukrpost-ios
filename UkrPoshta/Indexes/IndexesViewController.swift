//
//  IndexesViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

protocol IndexesViewProtocol: class {
    func startAnimation()
    func stopAnimation()
    func showAlert(with text: String)
    func reloadTableView()

}

protocol IndexMainDelegate {
    func setCityDetails(cityDetails: CityDetails)
}

class IndexesViewController: UIViewController, UIScrollViewDelegate, IndexSearchDelegate, UITextFieldDelegate {
    
    var delegate: IndexMainDelegate?
    private let mainView = IndexesContentView()
    private let detailMainView = DetailInfoSearchView()
    private var region: Region?
    private var district: District?
    private var city: City?
    private var street: Street?
    private var indexesAndHouses: [IndexAndHouse]?
    private var indexAndHouse: IndexAndHouse?
    private var cityDetails: CityDetails?
    private var controller: IndexesController?

    init() {
        super.init(nibName: nil, bundle: nil)
        controller = IndexesController(with: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Iндекси"
        hideBackNavigationButtonText()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)

        self.mainView.indexesInputView.delegate = self
        self.mainView.addressToIndexButton.addTarget(self, action: #selector(addressToIndexPressed), for: .touchUpInside)
        self.mainView.indexToAddressButton.addTarget(self, action: #selector(indexToAddressPressed), for: .touchUpInside)

        self.mainView.indexesInputView.regionButton.addTarget(self, action: #selector(regionButtonPressed), for: .touchUpInside)
        self.mainView.indexesInputView.districtButton.addTarget(self, action: #selector(districtButtonPressed), for: .touchUpInside)
        self.mainView.indexesInputView.cityButton.addTarget(self, action: #selector(cityButtonPressed), for: .touchUpInside)
        self.mainView.indexesInputView.streetButton.addTarget(self, action: #selector(streetButtonPressed), for: .touchUpInside)
        self.mainView.indexesInputView.getIndexButton.addTarget(self, action: #selector(getIndexButtonPressed), for: .touchUpInside)

        self.mainView.indexesInputView.indexTextField.delegate = self
        self.mainView.indexesInputView.getAddressButton.addTarget(self, action: #selector(fetchAddressByIndex), for: .touchUpInside)
        self.mainView.indexesInputView.toolBarButton.addTarget(self, action: #selector(fetchAddressByIndex), for: .touchUpInside)
        self.mainView.indexesInputView.houseField.delegate = self

        controller?.config(tableView: mainView.indexesInputView.tableView)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
  //      NotificationCenter.default.removeObserver(self)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      //  NotificationCenter.default.addObserver(self, selector:#selector(keybordWasShown) , name: NSNotification.Name.UIKeyboardDidShow, object: nil)

        if self.region != nil {
            self.mainView.indexesInputView.regionButton.setTitle(self.region?.regionUA, for: .selected)
            self.mainView.indexesInputView.regionButton.isSelected = true
        } else {
            self.mainView.indexesInputView.regionButton.isSelected = false
        }
        
        if self.district != nil {
            self.mainView.indexesInputView.districtButton.setTitle(self.district?.districtUA, for: .selected)
            self.mainView.indexesInputView.districtButton.isSelected = true
        } else {
            self.mainView.indexesInputView.districtButton.isSelected = false
        }
        
        if self.city != nil {
            self.mainView.indexesInputView.cityButton.setTitle(self.city?.cityUA, for: .selected)
            self.mainView.indexesInputView.cityButton.isSelected = true
        } else {
            self.mainView.indexesInputView.cityButton.isSelected = false
        }
        
        if self.street != nil {
            self.mainView.indexesInputView.streetButton.setTitle(self.street?.streetUA, for: .selected)
            self.mainView.indexesInputView.streetButton.isSelected = true
        } else {
            self.mainView.indexesInputView.streetButton.isSelected = false
        }
        
    }

    @objc func keybordWasShown(notification: NSNotification) {
//
//        let keyboardRect: CGRect = notification.userInfo!["UIKeyboardBoundsUserInfoKey"] as! CGRect
//        let keyboardHeight = keyboardRect.size.height
//
//        let getAddressButtonFrame = self.mainView.indexesInputView.getAddressButton.frame
//        let getIndexButtonFrame = self.mainView.indexesInputView.getIndexButton.frame
//
//        let deltaHeight = getAddressButtonFrame.origin.y - keyboardHeight
//
//        UIView.animate(withDuration: 0.25) {
//            self.mainView.indexesInputView.getAddressButton.frame = CGRect(x:getAddressButtonFrame.origin.x, y: deltaHeight, width: getAddressButtonFrame.size.width, height: getAddressButtonFrame.size.height)
//            self.mainView.indexesInputView.getIndexButton.frame = CGRect(x:getIndexButtonFrame.origin.x, y: deltaHeight, width: getIndexButtonFrame.size.width, height: getIndexButtonFrame.size.height)
//        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        let getAddressButtonFrame = self.mainView.indexesInputView.getAddressButton.frame
        let getIndexButtonFrame = self.mainView.indexesInputView.getIndexButton.frame

        UIView.animate(withDuration: 0.25) {
            self.mainView.indexesInputView.getAddressButton.frame = CGRect(x:getAddressButtonFrame.origin.x, y:self.mainView.indexesInputView.frame.size.height - 64, width: getAddressButtonFrame.size.width, height: getAddressButtonFrame.size.height)
            self.mainView.indexesInputView.getIndexButton.frame = CGRect(x:getIndexButtonFrame.origin.x, y: self.mainView.indexesInputView.frame.size.height - 64, width: getIndexButtonFrame.size.width, height: getIndexButtonFrame.size.height)
        }
        return true
    }
      
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return false
      }
    
    @objc func addressToIndexPressed() {
        self.mainView.indexToAddressButton.isSelected = false
        self.mainView.addressToIndexButton.isSelected = true
        self.mainView.indexesInputView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.view.endEditing(true)
    }
    
    @objc func indexToAddressPressed() {
        let width = UIScreen.main.bounds.size.width
        self.mainView.addressToIndexButton.isSelected = false
        self.mainView.indexToAddressButton.isSelected = true
        self.mainView.indexesInputView.setContentOffset(CGPoint(x: width, y: 0), animated: true)
        self.view.endEditing(true)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let point = scrollView.contentOffset.x
        let currentViewIndex = Int(point / UIScreen.main.bounds.width)
        
        if currentViewIndex == 0 {
            self.mainView.indexToAddressButton.isSelected = false
            self.mainView.addressToIndexButton.isSelected = true
        } else {
            self.mainView.addressToIndexButton.isSelected = false
            self.mainView.indexToAddressButton.isSelected = true
        }
    }
    
    @objc func regionButtonPressed() {
      
        self.region = nil
        self.district = nil
        self.city = nil
        self.street = nil

        let vc = IndexSearchViewController(type: .region, region: self.region, district: self.district, city: self.city, street: self.street)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func districtButtonPressed() {

        self.district = nil
        self.city = nil
        self.street = nil
        
        let vc = IndexSearchViewController(type: .district, region: self.region, district: self.district, city: self.city, street: self.street)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func cityButtonPressed() {

        self.city = nil
        self.street = nil
        
        let vc = IndexSearchViewController(type: .city, region: self.region, district: self.district, city: self.city, street: self.street)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func streetButtonPressed() {

        self.street = nil

        let vc = IndexSearchViewController(type: .street, region: self.region, district: self.district, city: self.city, street: self.street)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func getIndexButtonPressed () {
        self.mainView.indexesInputView.houseField.resignFirstResponder()
        var alertString = "Заповніть поля:\n"
        var alertNeeded = false
        
        if self.city == nil {
            alertString += "Населений пункт\n"
            alertNeeded = true
        }
        if self.street == nil {
            alertString += "Вулиця\n"
            alertNeeded = true
        }
        if (self.mainView.indexesInputView.houseField.text?.isEmpty)! {
            alertString += "Номер будинку\n"
            alertNeeded = true
        }
        if alertNeeded {
            self.showAlert(with: alertString)
        } else {
            self.fetchIndexesByStreet()
        }
    }
    
    func setRegion(region: Region) {
        self.region = region
    }
    
    func setDistrict(district: District) {
        self.district = district
    }

    func setCity(city: City) {
        self.city = city
    }
    
    func setStreet(street: Street) {
        self.street = street
    }
    
    func setIndexAndHouse() {
        if self.delegate != nil {
            let regionID = self.street?.regionId
            let regionName = self.street?.regionUA
            let districtID = self.street?.districtID
            let districtName = self.street?.districtUA
            let cityID = self.street?.cityID
            let cityName = self.street?.cityUA
            let cityType = self.city?.shortCityTypeUA
            let postcode = self.indexAndHouse?.postcode

            let newCityDetails = CityDetails(regionID: regionID!,
                                             regionName: regionName!,
                                             districtID: districtID!,
                                             districtName: districtName!,
                                             cityID: cityID!,
                                             cityName: cityName!,
                                             cityTypeID: "",
                                             cityTypeName: cityType!,
                                             street: nil,
                                             house: nil,
                                             apartment: nil,
                                             addressID: nil,
                                             postcode: postcode!)
            
            self.delegate?.setCityDetails(cityDetails: newCityDetails)
            self.navigationController?.popViewController(animated: true)
        } else {
            self.showIndexAlert(index: self.indexAndHouse?.postcode)
        }
    }
    

    @objc func submitAddressPressed() {
        if self.delegate != nil {
            self.delegate?.setCityDetails(cityDetails: self.cityDetails!)
            self.navigationController?.popViewController(animated: true)
        } else {
            self.showIndexAlert(index: self.cityDetails?.postcode)
        }
    }
    
    
    
    func showIndexAlert (index: String!) {
        let alert = UIAlertController(title: NSLocalizedString("addressToIndex.result.header", comment: ""), message: index, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("addressToIndex.result.button.title", comment: ""), style: .default, handler: {(alert: UIAlertAction!) in UIPasteboard.general.string = index}))

        self.present(alert, animated: true, completion: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let oldString = textField.text
        let newString = oldString! + string
        if newString.count > 5 {
            return false
        } else {
            return true
        }
    }
    
    @objc func fetchAddressByIndex() {
        mainView.indexesInputView.indexTextField.resignFirstResponder()

        let stringToSubmit = mainView.indexesInputView.indexTextField.text

        if let postCode = stringToSubmit {
            controller?.fetchMixedAddress(for: postCode)
        }
    }

    func fetchIndexesByStreet() {
        self.mainView.activityIndicator.startAnimating()
        UPAPIManager.getIndexesList(streetID: self.street?.streetID) { (result) in
            switch result {
            case .success(let data):
                print(data)
                self.indexesAndHouses = data
                self.filterIndexesWithHouse()
                let vc = DetainInfoViewController()
                vc.index = (self.indexAndHouse?.postcode)!
                vc.area = (self.street?.streetUA)! + ", "
                vc.street = (self.city?.cityUA)!
                vc.house = self.mainView.indexesInputView.houseField.text!
                self.navigationController?.pushViewController(vc, animated: true)
                self.mainView.activityIndicator.stopAnimating()

            case .partialSuccess(let string):
                self.showAlert(with: string)
                self.mainView.activityIndicator.stopAnimating()

            case .failure(let error):
                self.showAlert(with: error.localizedDescription)
                self.mainView.activityIndicator.stopAnimating()

            }
        }
    }
    
    func filterIndexesWithHouse() {
        for index: IndexAndHouse in self.indexesAndHouses! {
            if  index.houseNumber == self.mainView.indexesInputView.houseField.text {
                  
                let cityType = self.city?.shortCityTypeUA ?? "no"
                let cityName = self.city?.cityUA ?? "none"
                let postcode = index.postcode
                let regionName = self.city?.regionUA ?? "none"
                self.indexAndHouse = index
              
                break
            }
        }
      if self.indexAndHouse == nil {
            self.indexAndHouse = self.indexesAndHouses?[0]
      }
        self.mainView.activityIndicator.stopAnimating()
    }
}

extension IndexesViewController: IndexesViewProtocol {

    func reloadTableView() {
        mainView.indexesInputView.tableView.reloadData()
    }
    
    func startAnimation() {
        mainView.activityIndicator.startAnimating()
    }

    func stopAnimation() {
        mainView.activityIndicator.stopAnimating()
    }

    func showAlert(with text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))

        present(alert, animated: true, completion: nil)
    }

}
