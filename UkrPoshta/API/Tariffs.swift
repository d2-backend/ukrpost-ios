//
//  Tariffs.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/9/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

struct Tariff: Decodable {
    let id: Int?
    let weight: Float?
    let length: Float?
    let shipmentType: String?
    let w2wVariation: String?
    let price: Float?
    let fromDate: String?
}


private class SingletonSetupHelper {
    var list:[Tariff]?
}

class TariffsStored {
    static let shared = TariffsStored()
    private static let setup = SingletonSetupHelper()
    
    class func setup(list:[Tariff]){
        TariffsStored.setup.list = list
    }
    
    private init() {
        let param = TariffsStored.setup.list
        guard param != nil else {
            fatalError("Error - you must call setup before accessing MySingleton.shared")
        }
        
    }
}


struct DeliveryPrice: Codable {
    let deliveryPrice: Float?
    let calculationDescription: String?
    let validate: Bool?
    let addressFrom: TariffAddress?
    let addressTo: TariffAddress?
    let type: String?
    let deliveryType: String?
    let weight: Float?
    let length: Float?
    let width: Float?
    let height: Float?
    let declaredPrice: Float?
    let discountRate: Float?
    let sms: Bool?
    let recommended: Bool?
}

struct DeliveryInternationalPrice: Codable {
    let deliveryPrice: Float?
    let weight: Int?
    let length: Float?
    let calculationDescription: String?
    let packageType: String?
    let discountRate: Float?
    let currencyExchangeRate: Float?
    let currencyCode: String?
    let recipientCountryIso3166: String?
    let declaredPrice: Float?
    let deliveryPriceCurr: Float?
    let recommended: Bool?
    let bulky: Bool?
    let fragile: Bool?
    let transportType: String
    let withDeliveryNotification: Bool?
    let byCourier: Bool?
    let w2d: Bool?
    let cancelOrChange: Bool?
    let consignment: Consignment?
}

struct Consignment: Codable{
    let pricePerUnit: Float?
    let pricePerKg: Float?
    let forFragile: Float?
    let forBulkyPercent: Float?
}

struct TariffAddress: Codable{
    let postcode: String?
    let conglomerationPostcode: String?
}

enum SendType: String {
    case standart = "STANDARD"
    case express = "EXPRESS"
    case smartBox = "SMARTBOX"
}

enum TransportType: String {
    case avia = "AVIA"
    case ground = "GROUND"
}

enum DeliveryType: String {
    case warehouseToDoor = "W2D"
    case warehouseToWarehouse = "W2W"
    case doorToWarehouse = "D2W"
    case doorToDoor = "D2D"

}

enum APIPackageType: String {
    case letter = "LETTER"
    case banderole = "BANDEROLE"
    case smallBag = "SMALL_BAG"
    case parcel = "PARCEL"
    
}

struct Currency: Codable {
    let r030: Float?
    let txt: String?
    let rate: Float?
    let cc: String?
    let exchangeDate: String?
}

struct StampOrder: Codable {
    let lbError: String?
    let id: String?
    let PayData: StampPayData?
    let PayHost: String?
    let UpcBody: String?
    let lbSuccess: String?
}

struct StampPayData: Codable {
    let Version: String?
    let MerchantID: String?
    let TerminalID: String?
    let PurchaseTime: String?
    let OrderID: String?
    let Currency: String?
    let TotalAmount: String?
    let SD: String?
    let Signature: String?
    let locale: String?
    let PurchaseDesc: String?
}

enum APIReturnBehaviour: String {
    case returnToSender = "RETURN"
    case storeAndReturn = "RETURN_AFTER_FREE_STORAGE"
    case abandon = "PROCESS_AS_REFUSAL"
}

struct ParcelItem: Codable {
    var name: String?
    
    var weight: Int?
    var length: Int?
    var declaredPrice: Int?
    var parcelItems: [ContentItem]?
}

struct ContentItem: Codable {
    let name: String?
    let description: String?
    let quantity: Int?
    let weight: Int?
    let value: Int?
    let countryOfOrigin: String?
}

struct InternationalData: Codable {
    var transportType: String?
    var withDeliveryNotification: Bool?
    var byCourier: Bool?
    var categoryType: String?
    var w2d: Bool?
    var cancelOrChange: Bool?
    var licenceNumber: String?
    var certificateNumber: String?
    var invoiceNumber: String?
    var Explanation: String?
    var comments: String?
    var parcelQuantity: Int?
    var certificateAndInvoiceQuantity: Int?
    var declaredPriceSDR: Float?
    var Taxes: Float?
    var aviaReturn: Bool?
    var daysForReturn: Int?
    var addressForReturnId: Int?
    var consignment: Bool?
    var onlyPrintedIssues: Bool?
    var tracked: Bool?
}


struct DomesticShipment: Codable {
    let uuid: String?
    var type: String?
    var sender: User?
    var recipient: User?
    var recipientPhone: String?
    var recipientEmail: String?
    var recipientAddressId: Int?
    var returnAddressId: Int?
    var shipmentGroupUuid: String?
    var externalId: String?
    var deliveryType: String?
    var packageType: String?
    var onFailReceiveType: String?
    var barcode: String?
    var weight: Int?
    var length: Int?
    var width: Int?
    var height: Int?
    var declaredPrice: Int?
    let deliveryPrice: Float?
    var postPay: Int?
    let currencyCode: String?
    let postPayCurrencyCode: String?
    let currencyExchangeRate: Float?
    let discountPerClient: String?
    var contactPersonName: String?
    let lastModified: String?
    var description: String?
    var parcels: [ParcelItem]?
    let statusTrackings: String?
    let toReturnToSender: Bool?
    var paidByRecipient: Bool?
    let nonCashPayment: Bool?
    let bulky: Bool?
    var fragile: Bool?
    let bees: Bool?
    var recommended: Bool?
    var sms: Bool?
    var international: Bool
    var internationalData: InternationalData?
    let status: String?
    let postPayDeliveryPrice: Int?
    var postPayPaidByRecipient: Bool?
    let calculationDescription: String?
    let documentBack: Bool?
    let checkOnDelivery: Bool?
    let transferPostPayToBankAccount: Bool?
//    let deliveryPricePaid: Bool?
//    let postPayPaid: Bool?
//    let postPayDeliveryPricePaid: Bool?
    
    init() {
        self.uuid = nil
        self.type = nil
        self.sender = nil
        self.recipient = nil
        self.recipientPhone = nil
        self.recipientEmail = nil
        self.recipientAddressId = nil
        self.returnAddressId = nil
        self.shipmentGroupUuid = nil
        self.externalId = nil
        self.deliveryType = nil
        self.packageType = nil
        self.onFailReceiveType = nil
        self.barcode = nil
        self.weight = nil
        self.length = nil
        self.width = nil
        self.height = nil
        self.declaredPrice = nil
        self.deliveryPrice = nil
        self.postPay = nil
        self.currencyCode = nil
        self.postPayCurrencyCode = nil
        self.currencyExchangeRate = nil
        self.discountPerClient = nil
        self.lastModified = nil
        self.description = nil
        self.parcels = nil
        self.statusTrackings = nil
        self.toReturnToSender = nil
        self.paidByRecipient = nil
        self.nonCashPayment = nil
        self.bulky = nil
        self.fragile = nil
        self.bees = nil
        self.recommended = nil
        self.sms = nil
        self.international = false
        self.status = nil
        self.postPayDeliveryPrice = nil
        self.postPayPaidByRecipient = nil
        self.calculationDescription = nil
        self.documentBack = nil
        self.checkOnDelivery = nil
        self.transferPostPayToBankAccount = nil
        self.contactPersonName = nil
    }
}

struct SmartBox: Codable {
    let uuid: String?
    let smartBoxTariff: SmartBoxTariff?
    let code: String?
    let senderUuid: String?
    let buyerUuid: String?
    let postcode: String?
    let description: String?
    let activated: String?
    let activationExpired: String?
    let used: String?
    let useExpired: String?
    let shipmentsExpired: String?
}

struct SmartBoxTariff: Codable {
    let id: Int?
    let quantity: Int?
    let price: Float?
    let weight: Int?
    let length: Int?
    let description: String?
    let type: String?
}

struct AddSmartBoxSuccess: Decodable {
    let message: String?
}

struct CountryConstraints: Codable {
    let iso3166: String?
    let declaredPriceSDRMaxGround: Float
    let declaredPriceSDRMaxAvia: Float
    let customsDeclarationCountGround: Int
    let customsDeclarationCountAvia: Int
    let aviaWithCourierParcelAllowed: Bool
    let maxWeightGroundParcel: Float
    let maxWeightAviaParcel: Float
    let declaredPriceSDRMaxGroundParcel: Float
    let declaredPriceSDRMaxAviaParcel: Float
    let customsDeclarationCountGroundParcel: Int
    let customsDeclarationAviaParcel: Int
    let fragileAviaParcelAllowed: Bool
    let bulkyAviaParcelAllowed: Bool
    let declaredPriceAviaParcelAllowed: Bool
    let postpayAviaParcelAllowed: Bool
    let fragileGroundParcelAllowed: Bool
    let bulkyGroundParcelAllowed: Bool
    let declaredPriceGroundParcelAllowed: Bool
    let postpayGroundParcelAllowed: Bool
    let groundWithCourierParcelAllowed: Bool
    let declaredPriceAviaAllowed: Bool
    let personalHangingAviaAllowed: Bool
    let aviaWithCourierAllowed: Bool
    let declaredPriceGroundAllowed: Bool
    let personalHangingGroundAllowed: Bool
    let groundWithCourierAllowed: Bool
    let restanteGroundParcelAllowed: Bool
    let restanteAviaParcelAllowed: Bool
    let currencyAviaAllowed: Bool
    let declaredPricePostpayGroundAllowed: Bool
    let declaredPricePostpayAviaAllowed: Bool
    let currencyGroundAllowed: Bool
}




