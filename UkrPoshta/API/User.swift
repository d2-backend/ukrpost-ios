//
//  User.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/16/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation

private let kUserDataKey = "UserData"
private let kBarcodesKey = "barcodeHistory"
private let kFavoritesKey = "FavoritesList"
private let kOrdersKey = "OrdersList"
private let kDeviceKey = "deviceKey"

struct User: Codable {
    var uuid: String?
    var name: String?
    var firstName: String?
    var middleName: String?
    var lastName: String?
    var postId: String?
    var externalId: String?
    var uniqueRegistrationNumber: String?
    var counterpartyUuid: String?
    var addressId: Int?
    var addresses: [AddressCode]
    var phoneNumber: String?
    var phones: [Phone]
    var email: String?
    var emails: [Email]
    var type: String?
    var individual: Bool?
    var edrpou: String?
    var bankCode: String?
    var bankAccount: String?
    var tin: String?
    var contactPersonName: String?
    var resident: Bool?
    
    init() {
        self.uuid = ""
        self.name = ""
        self.firstName = ""
        self.middleName = ""
        self.lastName = ""
        self.postId = ""
        self.externalId = ""
        self.uniqueRegistrationNumber = ""
        self.counterpartyUuid = ""
        self.addressId = 0
        self.addresses = []
        self.phoneNumber = ""
        self.phones = []
        self.email = ""
        self.emails = []
        self.type = ""
        self.individual = true
        self.edrpou = nil
        self.bankCode = nil
        self.bankAccount = nil
        self.tin = nil
        self.contactPersonName = nil
        self.resident = true
    }
    
    static func currentUser() -> User {
        if UserDefaults.standard.value(forKey: kUserDataKey) != nil {
            let data: Data = UserDefaults.standard.value(forKey: kUserDataKey) as! Data
            let user = try! JSONDecoder().decode(User.self, from: data)
            return user
        } else {
            let newUser = self.init()
            self.save(user: newUser)
            return newUser
        }
    }
    
    static func save (user: User) {
        let encoder = JSONEncoder()
        let data = try! encoder.encode(user)
        UserDefaults.standard.setValue(data, forKey: kUserDataKey)
        UserDefaults.standard.synchronize()
    }
    
    static func currentDeviceToken() -> String? {
      return UserDefaults.standard.string(forKey: kDeviceKey)
    }
    
    static func saveDeviceToken (token: String) {
        UserDefaults.standard.setValue(token, forKey: kDeviceKey)
        UserDefaults.standard.synchronize()
    }
    
    static func removeUser(){
        let emptyTestUser = User()
        let encoder = JSONEncoder()
        let data = try! encoder.encode(emptyTestUser)
        UserDefaults.standard.setValue(data, forKey: kUserDataKey)
        UserDefaults.standard.setValue(nil, forKey: kBarcodesKey)
        UserDefaults.standard.setValue(nil, forKey: kFavoritesKey)
        UserDefaults.standard.setValue(nil, forKey: kOrdersKey)
        self.clearDocs()
        UserDefaults.standard.synchronize()
    }
    
    static func clearDocs() {
        let fileManager = FileManager.default
        let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let fileURLs = try fileManager.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            for aURL in fileURLs {
                if aURL.absoluteString.range(of: "pdf") != nil {
                    try fileManager.removeItem(at: aURL)
                }
            }
            
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
        }
    }
    
    static func saveTrackingHistory(barcode: String){
        
        var barcodesList: [String] = UserDefaults.standard.value(forKey: kBarcodesKey) as? [String] ?? []
        
        if barcodesList.contains(barcode) {
            let index = barcodesList.index(of: barcode)
            barcodesList.remove(at: index!)
            barcodesList.insert(barcode, at: 0)
        } else {
            if barcodesList.count < 20 {
                barcodesList.insert(barcode, at: 0)
            } else {
                barcodesList.removeLast()
                barcodesList.insert(barcode, at: 0)
            }
        }
        UserDefaults.standard.setValue(barcodesList, forKey: kBarcodesKey)
        UserDefaults.standard.synchronize()
    }
    
    static func currentTrackingHistory() -> [String] {
        if UserDefaults.standard.value(forKey: kBarcodesKey) == nil {
            UserDefaults.standard.setValue([], forKey: kBarcodesKey)
            UserDefaults.standard.synchronize()
            return []
        }
        return UserDefaults.standard.value(forKey: kBarcodesKey) as! [String]
    }
    
    static func addToFavorites(item: FavoriteItem) {

        if UserDefaults.standard.value(forKey: kFavoritesKey) == nil {
            UserDefaults.standard.setValue([], forKey: kFavoritesKey)
            UserDefaults.standard.synchronize()
        }

        var favoritesList = UserDefaults.standard.value(forKey: kFavoritesKey) as! [Data]
        
        for itemData in favoritesList {
            let currentItem = try! JSONDecoder().decode(FavoriteItem.self, from: itemData)
            if currentItem.barcode == item.barcode {
                return
            }
        }
        
        let itemData = try! JSONEncoder().encode(item)
        
        favoritesList.append(itemData)
        UserDefaults.standard.set(favoritesList, forKey: kFavoritesKey)
        UserDefaults.standard.synchronize()
    }
    
    static func currentFavorites() -> [FavoriteItem] {
        if UserDefaults.standard.value(forKey: kFavoritesKey) == nil {
            UserDefaults.standard.setValue([], forKey: kFavoritesKey)
            UserDefaults.standard.synchronize()
            return []
        }
        
        let favoritesDataList = UserDefaults.standard.value(forKey: kFavoritesKey) as! [Data]
        var favoritesItemList:[FavoriteItem] = []
        for data in favoritesDataList {
            let item = try! JSONDecoder().decode(FavoriteItem.self, from: data)
            favoritesItemList.append(item)
        }
        return favoritesItemList
    }
    
    static func removeFromFavorites(item: FavoriteItem) {
        let favoritesItemList = self.currentFavorites()
        var indexToRemove: Int?
        for (index, currentItem) in favoritesItemList.enumerated() {
            if currentItem.barcode == item.barcode {
                indexToRemove = index
                break
            }
        }
        if indexToRemove != nil {
            var dataList = UserDefaults.standard.value(forKey: kFavoritesKey) as! [Data]
            dataList.remove(at: indexToRemove!)
            UserDefaults.standard.setValue(dataList, forKey: kFavoritesKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    static func removeOrder(order: OrderItem) {
        let currentItem = self.currentOrders()
        var indexToRemove: Int?
        for (index, currentItem) in currentItem.enumerated(){
            if currentItem.code == order.code {
                indexToRemove = index
                break
            }
        }
          if indexToRemove != nil {
            var dataList = UserDefaults.standard.value(forKey: kOrdersKey) as! [Data]
            dataList.remove(at: indexToRemove!)
            UserDefaults.standard.setValue(dataList, forKey: kOrdersKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    static func removeBarcode(barcode: String) {
        let currentItem = self.currentFavorites()
        var indexToRemove: Int?
        for (index, currentItem) in currentItem.enumerated(){
            if currentItem.barcode == barcode {
                indexToRemove = index
                break
            }
        }
        if indexToRemove != nil {
            var dataList = UserDefaults.standard.value(forKey: kBarcodesKey) as! [String]
            dataList.remove(at: indexToRemove!)
            UserDefaults.standard.setValue(dataList, forKey: kBarcodesKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    
    static func addToOrders(item: OrderItem) {
        let pushNotificationController = PushNotificationController.init(with: item.code)
        pushNotificationController.sendTokenToServerSide()

        if UserDefaults.standard.value(forKey: kOrdersKey) == nil {
            UserDefaults.standard.setValue([], forKey: kOrdersKey)
            UserDefaults.standard.synchronize()
        }

        var ordersList = UserDefaults.standard.value(forKey: kOrdersKey) as! [Data]
        
        let itemData = try! JSONEncoder().encode(item)
        
        ordersList.append(itemData)
        UserDefaults.standard.set(ordersList, forKey: kOrdersKey)
        UserDefaults.standard.synchronize()
    }
    
    static func currentOrders() -> [OrderItem] {
        if UserDefaults.standard.value(forKey: kOrdersKey) == nil {
            UserDefaults.standard.setValue([], forKey: kOrdersKey)
            UserDefaults.standard.synchronize()
            return []
        }
        
        let ordersDataList = UserDefaults.standard.value(forKey: kOrdersKey) as! [Data]
        var ordersItemList:[OrderItem] = []
        for data in ordersDataList {
            let item = try! JSONDecoder().decode(OrderItem.self, from: data)
            ordersItemList.append(item)
        }
        return ordersItemList
    }
}

enum APIUserType: String {
    case individual = "INDIVIDUAL"
    case privateEnterpreneur = "PRIVATE_ENTREPRENEUR"
    case company = "COMPANY"
}

struct FavoriteItem: Codable{
    let barcode: String
    let type: String?
    let name: String?
}

struct OrderItem: Codable{
    let code: String
    let uuid: String
    let name: String?
}

struct AddressCode: Codable{
    let uuid: String?
    let addressId: Int?
    let type: String?
    let main: Bool?
}

struct Phone: Codable{
    let uuid: String?
    let phoneNumber: String?
    let type: String?
    let main: Bool?
}

struct Email: Codable{
    let uuid: String?
    let email: String?
    let type: String?
    let main: Bool?
}

struct EditedUser: Codable {
    
    let firstName: String?
    let lastName: String?
    let middleName: String?
    let phoneNumber: String?
    let email: String?
    let individual: Bool?
}

struct TokenError: Codable {
    let code: String?
    let message: String?
}
