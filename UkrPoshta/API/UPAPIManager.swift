//
//  UPAPIManager.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/16/18.
//  Copyright © 2018 D2. All rights reserved.
//

import Foundation
import SwiftyXMLParser

enum Result<Value> {
    case success(Value)
    case partialSuccess(String)
    case failure(Error)
}

/// sandbox
let sandBoxHeader = "Bearer b8124b6e-0733-3f01-bc17-3cb6ab6af50b"
let sendBoxTrackingHeader = "Bearer 7ea193bd-7276-3e38-b28e-aaeb14fbf79b"
let sendBoxAddressesHeader = "Bearer 5e6f10df-7f9e-31bb-81ef-ed77615005e2"

let sandBoxApiToken = "7b8deceb-b238-431c-9625-7719b8b82243"

let sendBoxUUID = "3bb150d5-d98c-48d3-b894-8c79e0b42919"

let sendBoxPriceCalculation = "Bearer 2a836f5f-c5d2-36b3-a957-3102bd5c2950"
// prod
let prodHeader = "Bearer 63b58e46-4c16-3ba6-a4aa-47f4b1522989"
let prodTrackingHeader = "Bearer c3e02b53-3b1d-386e-b676-141ffa054c57"
let prodAddressesHeader = "Bearer 01c616d9-89b7-3163-a88e-ed7b2db4eb61"

let prodApiToken = "6c211a36-3de5-4af6-bf12-73118ef8dfe0"
let prodTestUUID = "961c6d96-ba2e-47b8-b7e0-5ed7c4c6a7fd"

let prodPriceCalculation = "Bearer 14d62611-195f-37c3-ade5-9e245900db9f"
let pushHeaderToken = "BrgGTicNc6bA9WsmQiAAI0DqPag0jSlTVLXqmhLB2wphnZtwu1D7RELHQPA0LlVJr2mly9hZFxXLqHe6"
let pushHeader = "X-SECRET"
///
let isSendBox = false

let eComHeader = isSendBox ? sandBoxHeader : prodHeader
let eComToken = isSendBox ? sandBoxApiToken : prodApiToken

let trackingHeader = isSendBox ? sendBoxTrackingHeader : prodTrackingHeader
let addressesHeader = isSendBox ? sendBoxAddressesHeader : prodAddressesHeader

let priceCalculationHeader = isSendBox ? sendBoxPriceCalculation : prodPriceCalculation

class UPAPIManager: NSObject {
    
    
    
    class func getToken(bearerUUID: String, completion: ((Result<User>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/clients/" + bearerUUID
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let user = try decoder.decode(User.self, from: jsonData)
                        completion?(.success(user))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getAddress(id: Int, completion: ((Result<Address>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/addresses/" + "\(id)"
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let address = try decoder.decode(Address.self, from: jsonData)
                        completion?(.success(address))
                    } catch {
                        completion?(.failure(error))
                    }
                }
            }
        }
        task.resume()
    }
    
    class func createAddress(postCode: String!, region: String?, district: String?, city: String?, street: String?, houseNumber: String?, apartmentNumber: String?, completion: ((Result<Address>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/addresses"
        //urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        let dic = NSMutableDictionary()
        
        dic.setValue(postCode, forKey: "postcode")
        //queryList.append(URLQueryItem(name: "postcode", value: postCode))
        
        if region != nil {
            dic.setValue(region, forKey: "region")
            
            // queryList.append(URLQueryItem(name: "region", value: region))
        }
        if district != nil {
            dic.setValue(district, forKey: "district")
            
            //queryList.append(URLQueryItem(name: "district", value: district))
        }
        if city != nil {
            dic.setValue(city, forKey: "city")
            
            //queryList.append(URLQueryItem(name: "city", value: city))
        }
        if street != nil {
            dic.setValue(street, forKey: "street")
            
            //queryList.append(URLQueryItem(name: "street", value: street))
        }
        if houseNumber != nil {
            dic.setValue(houseNumber, forKey: "houseNumber")
            
            //queryList.append(URLQueryItem(name: "houseNumber", value: houseNumber))
        }
        if apartmentNumber != nil {
            dic.setValue(apartmentNumber, forKey: "apartmentNumber")
            
            //            queryList.append(URLQueryItem(name: "apartmentNumber", value: apartmentNumber))
        }
        
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: dic, options: []) {
            request.httpBody = theJSONData
        }
        
        //        let dataToPass: Data = try JSONSerialization.data(withJSONObject: dic, options: [.prettyPrinted]) {
        //            print("shoto")
        //        }
        
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let address = try decoder.decode(Address.self, from: jsonData)
                        completion?(.success(address))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func createIntAddress(country: String!, postCode: String!, city: String!, street: String?, houseNumber: String?, apartmentNumber: String?, completion: ((Result<Address>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/addresses"
        //urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        let dic = NSMutableDictionary()
        dic.setValue(postCode, forKey: "postcode")
        dic.setValue(country, forKey: "country")
        dic.setValue(city, forKey: "city")
        dic.setValue(street, forKey: "street")
        dic.setValue(houseNumber, forKey: "houseNumber")
        dic.setValue(apartmentNumber, forKey: "apartmentNumber")
        
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: dic, options: []) {
            request.httpBody = theJSONData
        }
        
        //        let dataToPass: Data = try JSONSerialization.data(withJSONObject: dic, options: [.prettyPrinted]) {
        //            print("shoto")
        //        }
        
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let address = try decoder.decode(Address.self, from: jsonData)
                        completion?(.success(address))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    class func addAddress(addressID: String!, isMain: Bool, UUID: String, completion: ((Result<User>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/clients/" + "\(UUID)"
        //urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        let dictionary = ["addresses": [["addressId": addressID, "main": isMain]]]
        
        
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "PUT"
        
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: dictionary, options: []) {
            request.httpBody = theJSONData
        }
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let user = try decoder.decode(User.self, from: jsonData)
                        completion?(.success(user))
                    } catch {
                        completion?(.failure(error))
                    }
                }
            }
        }
        task.resume()
    }
    
    class func deleteAddress(addressUUID: String!, completion: ((Result<Any>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/client-addresses/" + addressUUID
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "DELETE"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else {
                    let resultString = String(data: responseData!, encoding: .utf8)
                    if resultString == "" {
                        completion?(.success("ok"))
                    } else {
                        completion?(.partialSuccess(resultString!))
                        
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    class func saveUser(user: String, isIndividual: Bool,firstName: String?, lastName: String?, middleName: String?, phone: String?, email: String? , completion: ((Result<User>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/clients/" + user
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let editedUser = EditedUser(firstName: firstName, lastName: lastName, middleName: middleName, phoneNumber: phone, email: email, individual: isIndividual)
        let encodedData = try? JSONEncoder().encode(editedUser)
        
        request.httpBody = encodedData
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let address = try decoder.decode(User.self, from: jsonData)
                        completion?(.success(address))
                    } catch {
                        completion?(.failure(error))
                    }
                }
            }
        }
        task.resume()
    }

    class func createUser(user: User , completion: ((Result<User>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/clients"
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let encodedData = try? JSONEncoder().encode(user)
        
        request.httpBody = encodedData
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let address = try decoder.decode(User.self, from: jsonData)
                        completion?(.success(address))
                        
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
        //https://push.ukrposhta.d2.digital/push/register-package
    class func sendOrder(with orderID: String, completion: ((Bool) -> Void)?) {
        guard let APNSToken = User.currentDeviceToken() else {return}
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "push.ukrposhta.d2.digital"
        urlComponents.path = "/push/register-package"

        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue(pushHeaderToken, forHTTPHeaderField: pushHeader)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let dataString = "deviceToken=\(APNSToken)&orderId=\(orderID)"
        request.httpBody = dataString.data(using: .utf8)
        print(dataString)
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let _ = error {
                    completion?(false)
                    return
                }
                guard let httpResponse = response as? HTTPURLResponse else {
                    completion?(false)
                    return
                }
                if httpResponse.statusCode != 200 {
                    completion?(false)
                    return
                }
                completion?(true)
            }
        }
        task.resume()
    }

    //https://push.ukrposhta.d2.digital/push/change-token
    class func refreshDeviceToken(newDeviceToken: String, oldDeviceToken: String,
                                  completion: ((Bool) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "push.ukrposhta.d2.digital"
        urlComponents.path = "/push/change-token"

        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue(pushHeaderToken, forHTTPHeaderField: pushHeader)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let dataString = "newToken=\(newDeviceToken)&oldToken=\(oldDeviceToken)"
        request.httpBody = dataString.data(using: .utf8)
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let _ = error {
                    completion?(false)
                    return
                }

                guard let httpResponse = response as? HTTPURLResponse else {
                    completion?(false)
                    return
                }
                if httpResponse.statusCode != 200 {
                    completion?(false)
                    return
                }
                completion?(true)
            }
        }
        task.resume()
    }
    
    
    class func getTrackingStatuses(barcode: String, completion: ((Result<[TrackingStatus]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/status-tracking/0.0.1/statuses"
        //urlComponents.queryItems = [URLQueryItem(name: "barcode", value: "0500700032887")]
        urlComponents.queryItems = [URLQueryItem(name: "barcode", value: barcode)]
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(trackingHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let statuses = try decoder.decode([TrackingStatus].self, from: jsonData)
                        completion?(.success(statuses))
                    } catch {
                        
                        if String(data: jsonData, encoding: .utf8) == "Shipment not found" {
                            completion?(.partialSuccess("Данi про відправлення на даний час відсутні"))
                        } else {
                            completion?(.failure(error))
                        }
                        
                    }
                }
            }
        }
        task.resume()
    }
    
    //MARK: For google maps
    class func getCoordinates(long: Float!, lat: Float!, completion: ((Result<[CoordinatesOnMap]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_postoffices_by_geolocation"
        var queryList = [URLQueryItem]()
        
        queryList.append(URLQueryItem(name: "long", value: String(long)))
        queryList.append(URLQueryItem(name: "lat", value: String(lat)))
        queryList.append(URLQueryItem(name: "maxdistance", value: String(4)))
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"] 
                    let items = xml[path]
                    var coordinateMap = [CoordinatesOnMap]()
                    for item in items {
                        let coordinate = CoordinatesOnMap(latitude: item["LATITUDE"].text!, longtitude: item["LONGITUDE"].text!, nameOfStamp: item["POSTFILIALNAME"].text!, street: item["ADDRESS"].text!, postindex: item["POSTINDEX"].text!, city: item["CITYNAME"].text!)
                        coordinateMap.append(coordinate)
                    }
                    if coordinateMap.count > 0 {
                        completion?(.success(coordinateMap))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                }
            }
        }
        task.resume()
    }
    
    
    class func getRegionList(completion: ((Result<[Region]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_regions_by_region_ua"
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let items = xml[path]
                    print(items)
                    
                    var regions = [Region]()
                    for item in items {
                        let region = Region(regionID: item["REGION_ID"].text!, regionUA: item["REGION_UA"].text!)
                        regions.append(region)
                    }
                    if regions.count > 0 {
                        completion?(.success(regions))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                }
            }
        }
        task.resume()
    }
    
    
    class func getDistrictList(string: String?, regionID: String?, completion: ((Result<[District]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_districts_by_region_id_and_district_ua"
        
        var queryList = [URLQueryItem]()
        
        if regionID != nil {
            queryList.append(URLQueryItem(name: "region_id", value: regionID))
        }
        if string != nil {
            queryList.append(URLQueryItem(name: "district_ua", value: string))
        }
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let items = xml[path]
                    print(items)
                    
                    var districts = [District]()
                    for item in items {
                        let district = District(districtUA: item[0,"DISTRICT_UA"].text!, districtID: item[0,"DISTRICT_ID"].text!, regionID: item[0,"REGION_ID"].text!, regionUA: item[0,"REGION_UA"].text! )
                        districts.append(district)
                    }
                    if districts.count > 0 {
                        completion?(.success(districts))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    class func getCityList(string: String?, regionID: String?, districtID: String?, completion: ((Result<[City]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_city_by_region_id_and_district_id_and_city_ua"
        
        var queryList = [URLQueryItem]()
        
        if regionID != nil {
            queryList.append(URLQueryItem(name: "region_id", value: regionID))
        }
        if districtID != nil {
            queryList.append(URLQueryItem(name: "district_id", value: districtID))
        }
        if string != nil {
            queryList.append(URLQueryItem(name: "city_ua", value: string))
        }
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let items = xml[path]
                    print(items)
                    
                    var cities = [City]()
                    for item in items {
                        let city = City(regionID: item["REGION_ID"].text!,
                                        regionUA: item["REGION_UA"].text!,
                                        districtID: item["DISTRICT_ID"].text!,
                                        districtUA: item["DISTRICT_UA"].text!,
                                        cityID: item["CITY_ID"].text!,
                                        cityUA: item["CITY_UA"].text!,
                                        cityTypeUA: item["CITYTYPE_UA"].text!,
                                        shortCityTypeUA: item["SHORTCITYTYPE_UA"].text!,
                                        nameUA: item["NAME_UA"].text!)
                        cities.append(city)
                    }
                    if cities.count > 0 {
                        completion?(.success(cities))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    
    class func getStreetList(string: String?, regionID: String?, districtID: String?, cityID: String?, completion: ((Result<[Street]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_street_by_region_id_and_district_id_and_city_id_and_street_ua"
        
        var queryList = [URLQueryItem]()
        
        if regionID != nil {
            queryList.append(URLQueryItem(name: "region_id", value: regionID))
        }
        if districtID != nil {
            queryList.append(URLQueryItem(name: "district_id", value: districtID))
        }
        if cityID != nil {
            queryList.append(URLQueryItem(name: "city_id", value: cityID))
        }
        if string != nil {
            queryList.append(URLQueryItem(name: "street_ua", value: string))
        }
        
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let items = xml[path]
                    print(items)
                    
                    var streets = [Street]()
                    for item in items {
                        let street = Street(regionId: item["REGION_ID"].text!,
                                            regionUA: item["REGION_UA"].text!,
                                            districtID: item["DISTRICT_ID"].text!,
                                            districtUA: item["DISTRICT_UA"].text!,
                                            cityUA: item["CITY_UA"].text!,
                                            cityID: item["CITY_ID"].text!,
                                            streetUA: item["STREET_UA"].text!,
                                            streetID: item["STREET_ID"].text!,
                                            streetTypeUA: item["STREETTYPE_UA"].text!)
                        streets.append(street)
                    }
                    if streets.count > 0 {
                        completion?(.success(streets))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    class func getIndexesList(streetID: String?, completion: ((Result<[IndexAndHouse]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_addr_house_by_street_id"
        
        var queryList = [URLQueryItem]()
        
        if streetID != nil {
            queryList.append(URLQueryItem(name: "street_id", value: streetID))
            print(queryList)
        }
        
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let items = xml[path]
                    print(items)
                    
                    var list = [IndexAndHouse]()
                    for item in items {
                        let indexAndHouse = IndexAndHouse(streetID: item["STREET_ID"].text!, postcode: item["POSTCODE"].text!, houseNumber: item["HOUSENUMBER_UA"].text!)
                        list.append(indexAndHouse)
                    }
                    if list.count > 0 {
                        completion?(.success(list))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getAddressBy(postcode: String?, completion: ((Result<[AddressByIndex]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_address_by_postcode"
        
        var queryList = [URLQueryItem]()
        
        if postcode != nil {
            queryList.append(URLQueryItem(name: "postcode", value: postcode))
        }
        queryList.append(URLQueryItem(name: "lang", value: "UA"))
        
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let items = xml[path]
                    print(items)
                    
                    var list = [AddressByIndex]()
                    for item in items {
                        let addressByIndex = AddressByIndex(regionID: item["REGION_ID"].text!,
                                                            regionName: item["REGION_NAME"].text!,
                                                            districtID: item["DISTRICT_ID"].text!,
                                                            districtName: item["DISTRICT_NAME"].text!,
                                                            cityID: item["CITY_ID"].text!,
                                                            cityName: item["CITY_NAME"].text!,
                                                            cityTypeID: item["CITYTYPE_ID"].text!,
                                                            cityTypeName: item["CITYTYPE_NAME"].text!,
                                                            streetID: item["STREET_ID"].text!,
                                                            streetName: item["STREET_NAME"].text!,
                                                            streetTypeID: item["STREETTYPE_ID"].text!,
                                                            streetTypeName: item["STREETTYPE_NAME"].text!,
                                                            houseNumber: item["HOUSENUMBER"].text!,
                                                            postcode: item["POSTCODE"].text!)
                        
                        list.append(addressByIndex)
                    }
                    if list.count > 0 {
                        completion?(.success(list))
                    } else {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    class func getCityDetailsBy(postcode: String?, completion: ((Result<CityDetails>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_city_details_by_postcode"
        
        var queryList = [URLQueryItem]()
        
        if postcode != nil {
            queryList.append(URLQueryItem(name: "postcode", value: postcode))
        }
        queryList.append(URLQueryItem(name: "lang", value: "UA"))
        
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let item = xml[path]
                    
                    if item["REGION_ID"].text != nil {
                        let city:CityDetails = CityDetails(regionID: item["REGION_ID"].text!,
                                                           regionName: item["REGION_NAME"].text!,
                                                           districtID: item["DISTRICT_ID"].text!,
                                                           districtName: item["DISTRICT_NAME"].text!,
                                                           cityID: item["CITY_ID"].text!,
                                                           cityName: item["CITY_NAME"].text!,
                                                           cityTypeID: item["CITYTYPE_ID"].text!,
                                                           cityTypeName: item["CITYTYPE_NAME"].text!,
                                                           street: nil,
                                                           house: nil,
                                                           apartment: nil,
                                                           addressID: nil,
                                                           postcode: item["POSTCODE"].text!)
                        

                        completion?(.success(city))
                    } else if item.error != nil {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                        
                    }
                    
                }
            }
        }
        task.resume()
    }


    class func getPostOffice(by postcode: String, completion: ((Result<PostOffice>) -> Void)?) {

        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_postoffices_by_postindex"

        var queryList = [URLQueryItem]()
        queryList.append(URLQueryItem(name: "pi", value: postcode))
        queryList.append(URLQueryItem(name: "lang", value: "UA"))

        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }

        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }

        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)


        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)

                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let item = xml[path]

                    if item["POREGION_ID"].text != nil {
                        let postOffice: PostOffice = PostOffice(postCode: item["POSTINDEX"].text ?? "",
                                                                regionUA: item["REGION_UA"].text ?? "",
                                                                regionID: item["POREGION_ID"].text ?? "",
                                                                districtUA: item["DISTRICT_UA"].text ?? "",
                                                                districtID: item["PODISTRICT_ID"].text ?? "",
                                                                shortCityTypeUA: item["SHORTCITYTYPE_UA"].text ?? "",
                                                                cityUA: item["CITY_UA"].text ?? "",
                                                                cityID: item["POCITY_ID"].text ?? "",
                                                                cityTypeUA: item["CITYTYPE_UA"].text ?? "",
                                                                streetUA: item["STREET_UA"].text ?? "",
                                                                streetID: item["POSTREET_ID"].text ?? "",
                                                                housenumber: item["HOUSENUMBER"].text ?? "")
                        completion?(.success(postOffice))
                    } else if item.error != nil {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))

                    }

                }
            }
        }
        task.resume()
    }


    class func getCityDetailsFromPostOfficeBy(cityID: String, regionID: String?, districtID: String?, completion: ((Result<CityDetails>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/address-classifier-ws/get_postoffices_by_city_id"
        
        var queryList = [URLQueryItem]()
        
        queryList.append(URLQueryItem(name: "city_id", value: cityID))
        
        if regionID != nil {
            queryList.append(URLQueryItem(name: "region_id", value: regionID))
        }
        
        if districtID != nil {
            queryList.append(URLQueryItem(name: "district_id", value: districtID))
        }
        
        queryList.append(URLQueryItem(name: "lang", value: "UA"))
        
        if queryList.count > 0 {
            urlComponents.queryItems = queryList
        }
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(addressesHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let receivedData = responseData {
                    print(receivedData.description)
                    
                    let xml = XML.parse(receivedData)
                    let path = ["Entries", "Entry"]
                    let item = xml[path].first
                    
                    print(receivedData.description)
                    
                    if item["REGION_ID"].text != nil {
                        let city:CityDetails = CityDetails(regionID: item["REGION_ID"].text!,
                            regionName: "", districtID: "", districtName: "", cityID: "", cityName: "", cityTypeID: "", cityTypeName: "", street: "", house: "", apartment: "", addressID: nil, postcode: item["POSTINDEX"].text!)
                        
                        completion?(.success(city))
                    } else if item.error != nil {
                        completion?(.partialSuccess(NSLocalizedString("error.tracking.noData", comment: "")))
                        
                    }
                    
                }
            }
        }
        task.resume()
    }
    
    class func getDeliveryPrice(postcodeFrom: String?, postcodeTo: String?, weight: String?, length: String, type: SendType, deliveryType: DeliveryType, declaredValue: Float?, withSMS: Bool, isRecomended: Bool?, completion: ((Result<DeliveryPrice>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/domestic/delivery-price"
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let json = DeliveryPrice(deliveryPrice: nil,
                                 calculationDescription: nil,
                                 validate: nil,
                                 addressFrom: TariffAddress(postcode: postcodeFrom, conglomerationPostcode: nil),
                                 addressTo: TariffAddress(postcode: postcodeTo, conglomerationPostcode: nil),
                                 type: type.rawValue,
                                 deliveryType: deliveryType.rawValue,
                                 weight: weight != nil ? Float(weight!) : nil,
                                 length: Float(length),
                                 width: nil,
                                 height: nil,
                                 declaredPrice: declaredValue,
                                 discountRate: nil,
                                 sms: withSMS,
                                 recommended: isRecomended)
        let encodedData = try? JSONEncoder().encode(json)
        
        request.httpBody = encodedData
        
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let price = try decoder.decode(DeliveryPrice.self, from: jsonData)
                        completion?(.success(price))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getTarifs(completion: ((Result<[Tariff]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/calculation/dictionaries/tariffs"
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let tariffs = try decoder.decode([Tariff].self, from: jsonData)
                        completion?(.success(tariffs))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getCurrency(completion: ((Result<Currency>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "bank.gov.ua"
        urlComponents.path = "/NBUStatService/v1/statdirectory/exchange"
        urlComponents.query = "json"
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let currencies = try decoder.decode([Currency].self, from: jsonData)
                        var currencyFound = false
                        
                        for aCurency in currencies {
                            if aCurency.cc == "USD" {
                                completion?(.success(aCurency))
                                currencyFound = true
                                break
                            }
                        }
                        if !currencyFound {
                            completion?(.partialSuccess("currency error"))
                        }
                        
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getForeignPrices(info: DeliveryInternationalPrice?, completion: ((Result<DeliveryInternationalPrice>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/calculation/international/delivery-price"
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let encodedData = try? JSONEncoder().encode(info)
        request.httpBody = encodedData
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let price = try decoder.decode(DeliveryInternationalPrice.self, from: jsonData)
                        completion?(.success(price))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getStampAddress(completion: ((Result<[StampRegion]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "services.ukrposhta.com"
        urlComponents.path = "/courier/kd_api.ashx"
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let object = try decoder.decode(StampRegionRaw.self, from: jsonData)
                        var resultArray = [StampRegion]()
                        
                        for (aKey, aValue) in object.ddlRegion! {
                            if aKey == "selected" || aKey == "item0" {
                                continue
                            } else {
                                let aRegion = StampRegion(name: aValue, code: aKey, apiID: nil)
                                resultArray.append(aRegion)
                            }
                        }
                        completion?(.success(resultArray))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getStampDistrict(region:StampRegion, completion: ((Result<[StampDistrict]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "services.ukrposhta.com"
        urlComponents.path = "/courier/kd_api.ashx"
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonDic = ["ddlRegion" : ["selected" : region.code]]
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: jsonDic, options: []) {
            request.httpBody = theJSONData
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let object = try decoder.decode(StampDistrictsRaw.self, from: jsonData)
                        var resultArray = [StampDistrict]()
                        
                        for (aKey, aValue) in object.ddlDistrict! {
                            if aKey == "selected" || aKey == "item0" || aKey == "item1" {
                                continue
                            } else {
                                let aDistrict = StampDistrict(name: aValue, code: aKey, APIID: nil)
                                resultArray.append(aDistrict)
                            }
                        }
                        completion?(.success(resultArray))
                        
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getStampCity(region:StampRegion, district: StampDistrict, completion: ((Result<[StampCity]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "services.ukrposhta.com"
        urlComponents.path = "/courier/kd_api.ashx"
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonDic = ["ddlRegion" : ["selected" : region.code], "ddlDistrict" : ["selected": district.code]]
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: jsonDic, options: []) {
            request.httpBody = theJSONData
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let object = try decoder.decode(StampCityRaw.self, from: jsonData)
                        var resultArray = [StampCity]()
                        
                        for (aKey, aValue) in object.ddlCity! {
                            if aKey == "selected" || aKey == "item0" || aKey == "item1" {
                                continue
                            } else {
                                let aCity = StampCity(name: aValue, code: aKey, APIID: nil)
                                resultArray.append(aCity)
                            }
                        }
                        completion?(.success(resultArray))
                        
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getStampStreet(region:StampRegion, district: StampDistrict, city: StampCity, completion: ((Result<[StampStreet]>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "services.ukrposhta.com"
        urlComponents.path = "/courier/kd_api.ashx"
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonDic = ["ddlRegion" : ["selected" : region.code], "ddlDistrict" : ["selected": district.code], "ddlCity" : ["selected" : city.code ]]
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: jsonDic, options: []) {
            request.httpBody = theJSONData
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let object = try decoder.decode(StampStreetRaw.self, from: jsonData)
                        var resultArray = [StampStreet]()
                        
                        for (aKey, aValue) in object.ddlIndex! {
                            if aKey == "selected" || aKey == "item0" || aKey == "item1" {
                                continue
                            } else {
                                let aStreet = StampStreet(code: aKey, index: aValue, name: nil, APIID: nil)
                                resultArray.append(aStreet)
                            }
                        }
                        completion?(.success(resultArray))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func createOrder(name: String, addressFull: String, phone: String, count:Int, listID: Int, paySum: String, comment: String, deliverType: Int, images:[UIImage], ddlRegion:String?, ddlDistrict:String?, ddlCity:String?, ddlIndex:String?, completion: ((Result<StampOrder>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "services.ukrposhta.com"
        urlComponents.path = "/ownmark/OwnMarkAPI.ashx"
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        var imgDic:[String:String] = [String:String]()
        for (index, anImage) in images.enumerated() {
            let imageData = UIImagePNGRepresentation(anImage)
            let imageBase64 = imageData?.base64EncodedString(options: .lineLength64Characters)
            imgDic["item" + "\(index)"] = imageBase64
        }
        
        let jsonDic:[String: Any] = ["action" : "CreateOrder",
                                     "count": count,
                                     "list_id" : listID,
                                     "PaySum": paySum,
                                     "comment": comment,
                                     "deliver_type": 1,
                                     "ddlRegion" : ["selected" : ddlRegion],
                                     "ddlDistrict" : ["selected": ddlDistrict],
                                     "ddlCity" : ["selected" : ddlCity ],
                                     "ddlIndex" : ["selected" : ddlIndex],
                                     "images": imgDic,
                                     "txtFullName": name,
                                     "txtAddress": addressFull,
                                     "txtPhone": phone]
        if let theJSONData = try?  JSONSerialization.data(withJSONObject: jsonDic, options: []) {
            request.httpBody = theJSONData
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let object = try decoder.decode(StampOrder.self, from: jsonData)
                        
                        if object.lbError != "" {
                            completion?(.partialSuccess(object.lbError!))
                        } else {
                            completion?(.success(object))
                        }
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    //сюда
    class func submitDomesticOrder(order: DomesticShipment, completion: ((Result<DomesticShipment>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/shipments"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        if let theJSONData = try? JSONEncoder().encode(order) {
            request.httpBody = theJSONData
        }
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let data = try decoder.decode(DomesticShipment.self, from: jsonData)
                        completion?(.success(data))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getUserSmartBoxes(userUUID: String, completion: ((Result<[SmartBox]>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/clients/" + userUUID + "/smartBoxes"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    do {
                        let data = try decoder.decode([SmartBox].self, from: jsonData)
                        completion?(.success(data))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func addSmartBoxToUser(userUUID: String, boxCode: String, completion: ((Result<AddSmartBoxSuccess>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/smart-boxes/" + boxCode + "/use-with-sender/" + userUUID + "/without-group"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    print(jsonData)
                    let decoder = JSONDecoder()
                    do {
                        let data = try decoder.decode(AddSmartBoxSuccess.self, from: jsonData)
                        if data.message?.range(of: "not found!") != nil {
                            completion?(.partialSuccess("Введений код не знайдено"))
                        } else {
                            completion?(.success(data))
                        }
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func submitSmartBoxOrder(order: DomesticShipment, boxUUID: String, completion: ((Result<DomesticShipment>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/shipments/" + boxUUID
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "PUT"

        let smartOrder: [String: Any] =
            ["recipient": ["uuid" : order.recipient?.uuid ?? ""],
             "recipientAddressId": order.recipient?.addressId ?? -1,
             "postPay": order.postPay ?? 0,
             "parcels":[["declaredPrice": order.declaredPrice ?? 0]]
        ]

        if let theJSONData = try? JSONSerialization.data(withJSONObject: smartOrder, options: []) {
            request.httpBody = theJSONData
        }
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let data = try decoder.decode(DomesticShipment.self, from: jsonData)
                        completion?(.success(data))
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getBoxUUID(boxCode: String, completion: ((Result<DomesticShipment>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/smart-boxes/" + boxCode + "/shipments/next"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else if let jsonData = responseData {
                    let decoder = JSONDecoder()
                    
                    do {
                        let data = try decoder.decode(DomesticShipment.self, from: jsonData)
                        if data.uuid != nil {
                            completion?(.success(data))
                        } else {
                            do {
                                let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                                completion?(.partialSuccess(tokenError.message!))
                            } catch {
                                completion?(.failure(error))
                            }
                        }
                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: jsonData)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    class func getOrderSticker(shipmentUUID: String, completion: ((Result<Data>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/shipments/" + shipmentUUID + "/sticker"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else {
                    completion?(.success(responseData! as Data))
                    
                }
            }
        }
        task.resume()
    }
    
    class func getInternationalOrderSticker(shipmentUUID: String, completion: ((Result<Data>) -> Void)?) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/international/shipments/" + shipmentUUID + "/forms"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken)]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else {
                    completion?(.success(responseData! as Data))
                    
                }
            }
        }
        task.resume()
    }
    
    class func getRestrictionFor(country: String, completion: ((Result<CountryConstraints>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.ukrposhta.ua"
        urlComponents.path = "/ecom/0.0.1/constraints-per-countries/by-parameters"
        
        urlComponents.queryItems = [URLQueryItem(name: "token", value: eComToken),URLQueryItem(name: "countryISO3166", value: country) ]
        
        guard let url = urlComponents.url else {
            print("__error: Could not create URL from components")
            return
        }
        
        var request = URLRequest(url: url)
        request.setValue(eComHeader, forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { (responseData, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion?(.failure(error))
                } else {
                    let decoder = JSONDecoder()
                    
                    do {
                        let data = try decoder.decode(CountryConstraints.self, from: responseData!)
                        completion?(.success(data))

                    } catch {
                        do {
                            let tokenError = try decoder.decode(TokenError.self, from: responseData!)
                            completion?(.partialSuccess(tokenError.message!))
                        } catch {
                            completion?(.failure(error))
                        }
                    }
                }
            }
        }
        task.resume()
    }
}

