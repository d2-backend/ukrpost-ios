//
//  TrackingStatus.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/24/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

enum CodingKeys:String, CodingKey {
    
    case barcode
    case step
    case date
    case index
    case name
    case event
    case eventName
    case country
    case eventReason
    case eventReasonId = "eventReason_id"
    case mailType
    case indexOrder
}

struct TrackingStatus: Codable {
    let barcode: String?
    let step: Int?
    let date: String?
    let index: String?
    let name: String?
    let event: String?
    let eventName: String?
    let country: String?
    let eventReason: String?
    let eventReasonId: Int?
    let mailType: Int?
    let indexOrder: Int?
    
    func dateToDisplay() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let date = dateFormatter.date(from: self.date!) else {
            fatalError("TrackingStatus: failed to load self.date to formatter")
        }
        
        let dateToStringFormatter = DateFormatter()
        dateToStringFormatter.dateFormat = "dd.MM.yyyy, HH:mm"
        let resultString = dateToStringFormatter.string(from: date)
        return resultString
    }
}

