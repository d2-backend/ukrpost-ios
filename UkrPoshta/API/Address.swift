//
//  Address.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/20/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

struct Address: Codable {
    let id: Int?
    let postcode: String?
    let region: String?
    let district: String?
    let city: String?
    let street: String?
    let houseNumber: String?
    let apartmentNumber: String?
    let description: String?
    let countryside: Bool
    let foreignStreetHouseApartment: String?
    let detailedInfo: String?
    let country:String?
}

struct Region {
    let regionID: String
    let regionUA: String
}



struct District {
          let districtUA: String
          let districtID: String
      let regionID: String
      let regionUA: String
}

struct City {
    let regionID: String
    let regionUA: String
    let districtID: String
    let districtUA: String
    let cityID: String
    let cityUA: String
    let cityTypeUA: String
    let shortCityTypeUA: String
    let nameUA: String
}

extension City {
    var name: String {
        return "\(shortCityTypeUA).\(cityUA)"
    }
    
    var descriptionName: String {
        return "\(regionUA) обл, \(districtUA.districtExtension)"
    }
}

struct Street {
    let regionId: String
    let regionUA: String
    let districtID: String
    let districtUA: String
    let cityUA: String
    let cityID: String
    let streetUA: String
    let streetID: String
    let streetTypeUA: String
}

struct IndexAndHouse {
    let streetID: String
    let postcode: String
    let houseNumber: String
}

struct AddressByIndex {
    let regionID: String
    let regionName: String
    let districtID: String
    let districtName: String
    let cityID: String
    let cityName: String
    let cityTypeID: String
    let cityTypeName: String
    let streetID: String
    let streetName: String
    let streetTypeID: String
    let streetTypeName: String
    let houseNumber: String
    let postcode: String
}

struct PostOffice: Codable {
    let postCode: String
    let regionUA: String
    let regionID: String
    let districtUA: String
    let districtID: String
    let shortCityTypeUA: String
    let cityUA: String
    let cityID: String
    let cityTypeUA: String
    let streetUA: String
    let streetID: String
    let housenumber: String
}

struct MixedAddress {
    let regionID: String
    let regionName: String
    let districtID: String
    let districtName: String
    let cityID: String
    let cityName: String
    let cityTypeID: String
    let cityTypeName: String
    let streetID: String
    let streetName: String
    let streetTypeID: String
    let streetTypeName: String
    var houseNumbers: [String]
    let postcode: String
}

extension MixedAddress {
    static func makeMixedAddress(from address: AddressByIndex) -> MixedAddress {
        return MixedAddress(regionID: address.regionID,
                            regionName: address.regionName,
                            districtID: address.districtID,
                            districtName: address.districtName,
                            cityID: address.cityID,
                            cityName: address.cityName,
                            cityTypeID: address.cityTypeID,
                            cityTypeName: address.cityTypeName,
                            streetID: address.streetID,
                            streetName: address.streetName,
                            streetTypeID: address.streetTypeID,
                            streetTypeName: address.streetTypeName,
                            houseNumbers: [address.houseNumber],
                            postcode: address.postcode)
    }

    var streetNameDescription: String {
        return "\(streetTypeName) \(streetName)"
    }

    var buildingsDescription: String {
        return houseNumbers.joined(separator: ", ")
    }

    var locationInfoDescription: String {
        let cityFullName = "\(cityTypeName). \(cityName)"
        let string = "\(regionName) обл., "
        let tail = districtName.districtIsACity ? cityFullName : "\(districtName.districtExtension), \(cityFullName)"
        return string.appending(tail)
    }
}

struct CityDetails {
    var regionID: String?
    var regionName: String?
    var districtID: String?
    var districtName: String?
    var cityID: String?
    var cityName: String?
    var cityTypeID: String?
    var cityTypeName: String?
    var street: String?
    var house: String?
    var apartment: String?
    var addressID: Int?
    var postcode: String?
}

extension CityDetails {
    mutating func update(with city: City) {
        regionID = city.regionID
        regionName = city.regionUA
        districtID = city.districtID
        districtName = city.districtUA
        cityID = city.cityID
        cityName = city.cityUA
        cityTypeID = city.cityTypeUA
        cityTypeName = city.shortCityTypeUA
    }
}

extension CityDetails {
    var name: String {
        return "\(postcode ?? "") \(cityTypeName ?? "").\(cityName ?? "")"
    }

    var descriptionName: String {
        return "\(regionName ?? "") обл, \(districtName?.districtExtension ?? "")"
    }
}

struct StampRegionRaw: Codable {
    let Action: String?
    let ch1: String?
    let ddlRegion: [String: String]?
}

struct StampDistrictsRaw: Codable {
    let ddlDistrict: [String: String]?
}

struct StampCityRaw: Codable {
    let ddlCity: [String: String]?
}

struct StampStreetRaw: Codable {
    let ddlIndex: [String: String]?
}
