//
//  UPVerticalButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/23/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPVerticalButton: UIButton {

    private let icon = UIImageView()
    private var normalImage = UIImage()
    private var selectedImage = UIImage()
    
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //commonInit(str)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //commonInit()
    }
    
    required init(normalImageName: String, selectedImageName: String) {
        super.init(frame: .zero)
        
        self.normalImage = UIImage(named: normalImageName)!
        self.selectedImage = UIImage(named: selectedImageName)!
        self.backgroundColor = .white
        self.addSubview(icon)
        
        icon.image = normalImage
        
        self.addSubview(label)
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        self.layer.cornerRadius = 3.0
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        icon.pin.top(20).hCenter().height(20).width(20)
        label.pin.top(to: icon.edge.bottom).marginTop(10).right().left().height(20)
    }
    
    override var isSelected: Bool {
        
        didSet {
            if isSelected {
                icon.image = selectedImage
                label.textColor = UPColor.darkGray
            } else {
                icon.image = normalImage
                label.textColor = UPColor.lightGray
                
            }
        }
    }
}
