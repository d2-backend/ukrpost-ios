//
//  UPTextButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPTextButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white
        self.contentHorizontalAlignment = .left
        self.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0)
        self.setTitleColor(UPColor.lightGray, for: .normal)
        self.setTitleColor(UPColor.lightGray, for: .disabled)
        self.setTitleColor(UPColor.darkGray, for: .selected)
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        self.layer.cornerRadius = 3.0
    }
    override var isEnabled: Bool {
        
        didSet {
            if !isEnabled {
                self.backgroundColor = UPColor.separatorGray
            } else {
                self.backgroundColor = .white
            }
        }
    }
}
