//
//  UPTextFieldWithDelete.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 8/28/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPTextFieldWithDelete: UITextField {
    
    let delete = UIButton()

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override  func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func commonInit(){
        self.backgroundColor = .white
        self.tintColor = UPColor.lightGray
        self.font = UIFont.preferredFont(forTextStyle: .body)
        self.textColor = UPColor.darkGray
        self.autocorrectionType = .no
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        
        self.layer.cornerRadius = 3.0
        
        delete.setImage(UIImage(named: "Trash")?.withRenderingMode(.alwaysTemplate), for: .normal)
        delete.imageView?.tintColor = UPColor.lightGray
        delete.imageView?.contentMode = .scaleAspectFit
        addSubview(delete)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        delete.pin.vertically().right(10).width(20)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20 , y: bounds.origin.y, width: bounds.size.width - 60, height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20 , y: bounds.origin.y, width: bounds.size.width - 60, height: bounds.size.height)
        
    }

}
