//
//  UPHorizontalButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/8/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

enum ImgAlignment {
    case left
    case right
}

class UPHorizontalButton: UIButton {

    private let icon = UIImageView()
    private var normalImage = UIImage()
    private var selectedImage = UIImage()
    private var alignment:ImgAlignment?

    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //commonInit(str)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        //commonInit()
    }
    
    required init(normalImageName: String, selectedImageName: String, imgAlignment: ImgAlignment = .left) {
        self.alignment = imgAlignment
        super.init(frame: .zero)
        
        self.normalImage = UIImage(named: normalImageName)!
        self.selectedImage = UIImage(named: selectedImageName)!
        self.backgroundColor = .white
        self.addSubview(icon)
        
        icon.image = normalImage

        self.addSubview(label)
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2
        
        self.layer.cornerRadius = 3.0

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if self.alignment == .left {
            icon.pin.left(10).vCenter().height(20).width(20)
            label.pin.after(of: icon).marginLeft(10).right(10).top().bottom()
        } else {
            icon.pin.right(10).vCenter().height(20).width(20)
            label.pin.before(of: icon).marginRight(10).left(10).top().bottom()
        }

    }

    override var isSelected: Bool {
        
        didSet {
            if isSelected {
                icon.image = selectedImage
                label.textColor = UPColor.darkGray
            } else {
                icon.image = normalImage
                label.textColor = UPColor.lightGray
                
            }
        }
    }
}
