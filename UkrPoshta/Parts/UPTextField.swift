//
//  UPTextField.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/12/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPTextField: UITextField {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override  func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func commonInit(){
        self.backgroundColor = .white
        self.tintColor = UPColor.lightGray
        self.font = UIFont.preferredFont(forTextStyle: .body)
        self.textColor = UPColor.darkGray
        self.autocorrectionType = .no
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UPColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.1
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2

        self.layer.cornerRadius = 3.0

    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20 , y: bounds.origin.y, width: bounds.size.width - 40, height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 20 , y: bounds.origin.y, width: bounds.size.width - 40, height: bounds.size.height)

    }
}

extension UITextField{
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    func addSubmitOnKeyboard() {
        
        let toolView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let toolBarButton = UPSubmitButton()
        toolBarButton.addTarget(self, action: #selector(self.doneButtonAction), for: .touchUpInside)
        toolBarButton.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50)
        toolBarButton.setTitle(NSLocalizedString("common.next", comment: ""), for: .normal)
        toolView.addSubview(toolBarButton)
        self.inputAccessoryView = toolView

    }
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}
