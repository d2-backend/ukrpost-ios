//
//  UPRadioButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/14/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPRadioButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.setImage(UIImage(named: "radioButtonSelected"), for: .selected)
        self.setImage(UIImage(named: "radioButton"), for: .normal)
        
    }
}
