//
//  UPTextView.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/15/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPTextView: UITextView {

    private var placeholder = String()

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    required override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.commonInit()
    }
    
    func commonInit(){
        self.backgroundColor = .white
        self.tintColor = UPColor.lightGray
        self.font = UIFont.preferredFont(forTextStyle: .footnote)
        self.textColor = UPColor.darkGray
        self.autocorrectionType = .no

        
        self.layer.cornerRadius = 3.0
    }
    
    func setPlaceholder(text:String) {
        self.text = text
        self.textColor = UPColor.lightGray
        self.placeholder = text
    }
    
}
