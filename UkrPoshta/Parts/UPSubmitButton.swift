//
//  UPSubmitButton.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 4/30/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPSubmitButton: UIButton {

    private let dimmingView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.setTitleColor(UPColor.darkGray, for: .normal)
        self.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        self.addSubview(dimmingView)
        dimmingView.backgroundColor = UPColor.dimmingGray
        dimmingView.isUserInteractionEnabled = false
        dimmingView.isHidden = true

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let yellowGradient = UPColor.yellowGradient(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))

        self.layer.insertSublayer(yellowGradient, at: 0)
        
        dimmingView.pin.all()
    }
    
    override var isEnabled: Bool {

        didSet {
            if isEnabled {
                self.dimmingView.isHidden = true
            } else {
                self.dimmingView.isHidden = false
            }
        }
    }


}
