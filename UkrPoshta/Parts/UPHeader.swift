//
//  UPHeader.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/12/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPHeader: UILabel {

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    override  func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func commonInit(){
        self.textColor = UPColor.lightGray
        self.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.caption2)
    }
    
}
