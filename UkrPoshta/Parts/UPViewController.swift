//
//  UPViewController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/12/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UPViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()

        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)


    }

    @objc func dismissKeyboard() {
            self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
