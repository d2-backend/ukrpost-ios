//
//  UserInfoVC.swift
//  UkrPoshta
//
//  Created by Zhekon on 21.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UserInfoView: UIView {
    
    let sername = UPTextField()
    let name = UPTextField()
    let patronymic = UPTextField()
    let numberPhoneLabel = UPHeader()
    let numberPhone = UPTextField()
    let emailLabel = UPHeader()
    let emailTextField = UPTextField()
    let nextButton = UPSubmitButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        sername.placeholder = "Прiзвище"
        sername.isEnabled = true
        self.addSubview(sername)
        name.placeholder = "Iм'я"
        self.addSubview(name)
        patronymic.placeholder = "По батьковi"
        self.addSubview(patronymic)
        numberPhoneLabel.text = "Номер телефону"
        self.addSubview(numberPhoneLabel)
        numberPhone.keyboardType = .phonePad
        self.addSubview(numberPhone)
        emailLabel.text = "E-mail"
        self.addSubview(emailLabel)
        emailTextField.placeholder = "E-mail"
        self.addSubview(emailTextField)
        nextButton.setTitle("Продовжити", for: UIControlState .normal)
        nextButton.setTitleColor(.black, for: .normal)
        self.addSubview(nextButton)
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let size = UIScreen.main.bounds.size
        sername.pin.top( size.height - size.height + 15).right(15).left(15).height(50)
        name.pin.top(to: sername.edge.bottom).marginTop(15).left(15).right(15).height(50)
        patronymic.pin.top(to: name.edge.bottom).marginTop(15).left(15).right(15).height(50)
        numberPhoneLabel.pin.top(to: patronymic.edge.bottom).marginTop(30).left(15).right(15).height(25)
        numberPhone.pin.top(to: numberPhoneLabel.edge.bottom).marginTop(15).left(15).right(15).height(50)
        emailLabel.pin.top(to: numberPhone.edge.bottom).marginTop(30).left(15).right(15).height(25)
        emailTextField.pin.top(to: emailLabel.edge.bottom).marginTop(15).left(15).right(15).height(50)
        nextButton.pin.left(0).right(0).bottom(64).height(64)
    }
}










