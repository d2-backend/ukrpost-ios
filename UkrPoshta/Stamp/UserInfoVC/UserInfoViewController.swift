//
//  UserInfoViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 21.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController, UITextFieldDelegate {
    //MARK: ссылаюсь сюда для передачи
    var sheetsCount: Int?
    var comment = String()
    var cropedImagesArray:[UIImageView]?
    var sheetId : String?
    var markCost: Int?
      var countLetter : Int?

    let mainView = UserInfoView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Власна марка"
       hideBackNavigationButtonText()
        mainView.nextButton.isEnabled = false
        mainView.numberPhone.delegate = self
        mainView.name.delegate = self
        mainView.sername.delegate = self
        mainView.patronymic.delegate = self
        mainView.emailTextField.delegate = self
        mainView.numberPhone.text = displayStringFrom(rawString: nil)
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.nextButton.addTarget(self, action: #selector(nextButtonPressed), for: .touchUpInside)
        hideKeyboard()
    }
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case self.mainView.emailTextField :
            if !isValidEmail(testStr: self.mainView.emailTextField.text!)  {
                self.mainView.emailTextField.textColor = .red
            }
        default:
            break
        }
        
        if self.mainView.sername.text == "" || self.mainView.name.text == "" || self.mainView.numberPhone.text?.count != 19 || self.mainView.emailTextField.textColor == .red  {
            mainView.nextButton.isEnabled = false
        } else {
            mainView.nextButton.isEnabled = true
        }
        if !isValidEmail(testStr: self.mainView.emailTextField.text!)  {
           self.mainView.emailTextField.textColor = .red
        }
        

    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard) )
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case self.mainView.emailTextField : self.mainView.emailTextField.textColor = UPColor.darkGray
        case  mainView.numberPhone:
            let oldString = textField.text
            let newString = oldString! + string
            if (newString.count <= 6 || newString.count > 19) {
                return false
            } else {
                var oldRawString = rawStringFrom(displayString: oldString!)
                
                switch string {
                case "":
                    oldRawString.removeLast()
                case "0","1","2","3","4","5","6","7","8","9" :
                    oldRawString = oldRawString + string
                    
                default :
                    return false
                }
                textField.text = displayStringFrom(rawString: oldRawString)
            }
            return false
        default:
            return true
        }
        return true
    }
    
    func rawStringFrom(displayString: String) -> String {
        let arrayOfDigits = displayString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
        let result = arrayOfDigits.joined(separator: "")
        
        return result
    }
    
    func displayStringFrom(rawString: String?) -> String {
        if rawString != nil {
            var resultString = rawString
            if (resultString?.count)! >= 1 {
                resultString!.insert("+", at: resultString!.startIndex)
            }
            if (resultString?.count)! >= 3 {
                resultString!.insert(" ", at: resultString!.index(resultString!.startIndex, offsetBy: 3))
                resultString!.insert("(", at: resultString!.index(resultString!.startIndex, offsetBy: 4))
            }
            if (resultString?.count)! >= 8 {
                resultString!.insert(")", at: resultString!.index(resultString!.startIndex, offsetBy: 8))
                resultString!.insert(" ", at: resultString!.index(resultString!.startIndex, offsetBy: 9))
            }
            if (resultString?.count)! >= 13 {
                resultString!.insert("-", at: resultString!.index(resultString!.startIndex, offsetBy: 13))
            }
            if (resultString?.count)! >= 16 {
                resultString!.insert("-", at: resultString!.index(resultString!.startIndex, offsetBy: 16))
            }
            return resultString!
        } else {
            return "+38 (0"
        }
    }
    
    @objc func nextButtonPressed() {
     // let vc = IssueViewController()
        let vc = StampInfoViewController()
        vc.sheetsCount = self.sheetsCount
        vc.comment = self.comment
        vc.cropedImagesArray = self.cropedImagesArray
        vc.name = self.mainView.name.text!
        vc.serName = self.mainView.sername.text!
        vc.patronomyc = self.mainView.patronymic.text!
        vc.phoneNumber = self.mainView.numberPhone.text!
        vc.email = self.mainView.emailTextField.text!
        vc.sheetId = self.sheetId!
        vc.markCost = self.markCost!
        vc.countLetter = self.countLetter
        self.navigationController?.pushViewController(vc, animated: true)
    }
}






