//
//  DetailCropNineVC.swift
//  UkrPoshta
//
//  Created by Zhekon on 17.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit


class DetailCropNineVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var cropedImagesArray:[UIImageView]?
    var croppedImage: UIImage!
    var sheetName: String?
    var markCost: Int?
    var countLetter : Int?
    var  imageArrayNamed = ["template_nine_stamp_0","template_nine_stamp_2","template_nine_stamp_3","template_nine_stamp_4","template_nine_stamp_5","template_nine_stamp_6","template_nine_stamp_7","template_nine_stamp_8","template_nine_stamp_9", "template_stamp_nine_para"]
    private var places = [UIImageView]()

    @IBOutlet weak var bigStampImage: UIImageView!
    @IBOutlet weak var bottomCollectionView: UICollectionView!
    @IBOutlet weak var croppedImage1: UIImageView!
    @IBOutlet weak var croppedImage2: UIImageView!
    @IBOutlet weak var croppedImage3: UIImageView!
    @IBOutlet weak var croppedImage4: UIImageView!
    @IBOutlet weak var croppedImage5: UIImageView!
    @IBOutlet weak var croppedImage6: UIImageView!
    @IBOutlet weak var croppedImage7: UIImageView!
    @IBOutlet weak var croppedImage8: UIImageView!
    @IBOutlet weak var croppedImage9: UIImageView!
    @IBAction func nextButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        places = [croppedImage1, croppedImage2, croppedImage3,croppedImage4,croppedImage5,croppedImage6,croppedImage7,croppedImage8,croppedImage9]
        viewConfigurations()
        navigationBarConfigurations()
    }
    
    private func navigationBarConfigurations() {
        let customTopButton = UIButton()
        customTopButton.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
        customTopButton.setTitleColor(UPColor.darkGray, for: .normal)
        customTopButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        customTopButton.setTitle("додати ще фото: \(cropedImagesArray!.count.description)", for: .normal)
        if (cropedImagesArray?.count)! >= 9{
            customTopButton.setTitle("розпочати спочатку", for: .normal)
            let alertController = UIAlertController(title: "повiдомлення", message: "Максимальна кiлькiсть фото: 9шт.", preferredStyle: .alert)
            let actionCancel = UIAlertAction(title: "закрити", style: .cancel) { (action:UIAlertAction) in
            }
            alertController.addAction(actionCancel)
            self.present(alertController, animated: true, completion: nil)
        }
        customTopButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        let customBarButtonItem = UIBarButtonItem()
        customBarButtonItem.customView = customTopButton
        self.navigationItem.rightBarButtonItem = customBarButtonItem
    }
    
    @objc func backButtonPressed() {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    private func viewConfigurations(){
        for index in 0...(places.count - 1) {
            let neededImageIndex = index % (cropedImagesArray?.count)!
            let currentHole = places[index]
            let pictureForHole = cropedImagesArray![neededImageIndex]
            currentHole.image = pictureForHole.image
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArrayNamed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as!  ImageCollectionViewCell
        if collectionView == self.bottomCollectionView {
        cell.imgImage.image = UIImage(named: imageArrayNamed[indexPath.row])
            sheetName = imageArrayNamed[indexPath.row]
        }
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row <= imageArrayNamed.count {
            bigStampImage.image = UIImage(named: imageArrayNamed[indexPath.row])
            sheetName = imageArrayNamed[indexPath.row]
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailNineToSheet" {
            let detailVC = segue.destination as? QuantityOfSheetViewController
            detailVC?.cropedImagesArray = self.cropedImagesArray
            detailVC?.sheetName =  sheetName!
            detailVC?.markCost = self.markCost
            detailVC?.countLetter = self.countLetter
        }
    }
}
