//
//  DetailCropEightVC.swift
//  UkrPoshta
//
//  Created by Zhekon on 19.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit


    class DetailCropEightVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
        var sheetName: String?
        var markCost: Int?
        var countLetter : Int?
        var cropedImagesArray:[UIImageView]?
        var croppedImage:UIImage!
        var  imageArrayNamed = ["template_eight_stamp_0"]
        private var places = [UIImageView]()

        @IBOutlet weak var bigStampImage: UIImageView!
        @IBOutlet weak var bottomCollectionView: UICollectionView!
        @IBOutlet weak var croppedImage1: UIImageView!
        @IBOutlet weak var croppedImage2: UIImageView!
        @IBOutlet weak var croppedImage3: UIImageView!
        @IBOutlet weak var croppedImage4: UIImageView!
        @IBOutlet weak var croppedImage5: UIImageView!
        @IBOutlet weak var croppedImage6: UIImageView!
        @IBOutlet weak var croppedImage7: UIImageView!
        @IBOutlet weak var croppedImage8: UIImageView!
        @IBAction func nextButton(_ sender: Any) {
            let vc = QuantityOfSheetViewController()
            vc.cropedImagesArray = self.cropedImagesArray
            
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            places = [croppedImage1, croppedImage2, croppedImage3,croppedImage4,croppedImage5,croppedImage6,croppedImage7,croppedImage8]
            viewConfigurations()
            navigationBarConfigurations()
        }
        
        private func navigationBarConfigurations() {
            let customTopButton = UIButton()
            customTopButton.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            customTopButton.setTitleColor(UPColor.darkGray, for: .normal)
            customTopButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            customTopButton.setTitle("додати ще фото: \(cropedImagesArray!.count.description)", for: .normal)
            if (cropedImagesArray?.count)! >= 8 {
                customTopButton.setTitle("розпочати спочатку", for: .normal)
                let alertController = UIAlertController(title: "повiдомлення", message: "Максимальна кiлькiсть фото: 8шт.", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "закрити", style: .cancel) { (action:UIAlertAction) in
                }
                alertController.addAction(actionCancel)
                self.present(alertController, animated: true, completion: nil)
            }
            customTopButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
            let customBarButtonItem = UIBarButtonItem()
            customBarButtonItem.customView = customTopButton
            self.navigationItem.rightBarButtonItem = customBarButtonItem
        }
        
        @objc func backButtonPressed() {
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
        private func viewConfigurations(){
            for index in 0...(places.count - 1) {
                let neededImageIndex = index % (cropedImagesArray?.count)!
                let currentHole = places[index]
                let pictureForHole = cropedImagesArray![neededImageIndex]
                currentHole.image = pictureForHole.image
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return imageArrayNamed.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as!  ImageCollectionViewCell
            if collectionView == self.bottomCollectionView {
                cell.imgImage.image = UIImage(named: imageArrayNamed[indexPath.row])
                sheetName = imageArrayNamed[indexPath.row]
            }
            return cell
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if indexPath.row <= imageArrayNamed.count {
                bigStampImage.image = UIImage(named: imageArrayNamed[indexPath.row])
                sheetName = imageArrayNamed[indexPath.row]
            }
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "DetailEightToSheet" {
                let detailVC = segue.destination as? QuantityOfSheetViewController
                detailVC?.cropedImagesArray = self.cropedImagesArray
                detailVC?.sheetName =  sheetName!
                detailVC?.markCost = self.markCost
                detailVC?.countLetter = self.countLetter
            }
        }
    }


