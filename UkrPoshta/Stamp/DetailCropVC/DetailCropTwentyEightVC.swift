//
//  DetailCropTwentyEightVC.swift
//  UkrPoshta
//
//  Created by Zhekon on 19.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

    class DetailCropTwentyEightVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
        var cropedImagesArray:[UIImageView]?
        var croppedImage:UIImage!
        var  imageArrayNamed = ["template_twenty_eight_stamp_0"]
        private var places = [UIImageView]()
        var sheetName: String?
        var markCost: Int?
        var countLetter : Int?
        @IBOutlet weak var bigStampImage: UIImageView!
        @IBOutlet weak var bottomCollectionView: UICollectionView!
        @IBOutlet weak var croppedImage1: UIImageView!
        @IBOutlet weak var croppedImage2: UIImageView!
        @IBOutlet weak var croppedImage3: UIImageView!
        @IBOutlet weak var croppedImage4: UIImageView!
        @IBOutlet weak var croppedImage5: UIImageView!
        @IBOutlet weak var croppedImage6: UIImageView!
        @IBOutlet weak var croppedImage7: UIImageView!
        @IBOutlet weak var croppedImage8: UIImageView!
        @IBOutlet weak var croppedImage9: UIImageView!
        @IBOutlet weak var croppedImage10: UIImageView!
        @IBOutlet weak var croppedImage11: UIImageView!
        @IBOutlet weak var croppedImage12: UIImageView!
        @IBOutlet weak var croppedImage13: UIImageView!
        @IBOutlet weak var croppedImage14: UIImageView!
        @IBOutlet weak var croppedImage15: UIImageView!
        @IBOutlet weak var croppedImage16: UIImageView!
        @IBOutlet weak var croppedImage17: UIImageView!
        @IBOutlet weak var croppedImage18: UIImageView!
        @IBOutlet weak var croppedImage19: UIImageView!
        @IBOutlet weak var croppedImage20: UIImageView!
        @IBOutlet weak var croppedImage21: UIImageView!
        @IBOutlet weak var croppedImage22: UIImageView!
        @IBOutlet weak var croppedImage23: UIImageView!
        @IBOutlet weak var croppedImage24: UIImageView!
        @IBOutlet weak var croppedImage25: UIImageView!
        @IBOutlet weak var croppedImage26: UIImageView!
        @IBOutlet weak var croppedImage27: UIImageView!
        @IBOutlet weak var croppedImage28: UIImageView!
        @IBAction func nextButton(_ sender: Any) {
            let vc = QuantityOfSheetViewController()
            vc.cropedImagesArray = self.cropedImagesArray
        }
    
                override func viewDidLoad() {
            super.viewDidLoad()
            places = [croppedImage1, croppedImage2, croppedImage3,croppedImage4,croppedImage5,croppedImage6,croppedImage7,croppedImage8,croppedImage9,croppedImage10,croppedImage11,croppedImage12,croppedImage13,croppedImage14,croppedImage15,croppedImage16,croppedImage17,croppedImage18,croppedImage19,croppedImage20,croppedImage21,croppedImage22,croppedImage23,croppedImage24,croppedImage25,croppedImage26,croppedImage27,croppedImage28]
            viewConfigurations()
            navigationBarConfigurations()
        }
        
        private func navigationBarConfigurations() {
            let customTopButton = UIButton()
            customTopButton.frame = CGRect(x: 0, y: 0, width: 200, height: 30)
            customTopButton.setTitleColor(UPColor.darkGray, for: .normal)
            customTopButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
            customTopButton.setTitle("додати ще фото: \(cropedImagesArray!.count.description)", for: .normal)
            if (cropedImagesArray?.count)! >= 28 {
                customTopButton.setTitle("розпочати спочатку", for: .normal)
                let alertController = UIAlertController(title: "повiдомлення", message: "Максимальна кiлькiсть фото: 28шт.", preferredStyle: .alert)
                let actionCancel = UIAlertAction(title: "закрити", style: .cancel) { (action:UIAlertAction) in
                }
                alertController.addAction(actionCancel)
                self.present(alertController, animated: true, completion: nil)
            }
            customTopButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
            let customBarButtonItem = UIBarButtonItem()
            customBarButtonItem.customView = customTopButton
            self.navigationItem.rightBarButtonItem = customBarButtonItem
        }
        
        @objc func backButtonPressed() {
            let _ = self.navigationController?.popViewController(animated: true)
        }
        private func viewConfigurations(){
            for index in 0...(places.count - 1) {
                let neededImageIndex = index % (cropedImagesArray?.count)!
                let currentHole = places[index]
                let pictureForHole = cropedImagesArray![neededImageIndex]
                currentHole.image = pictureForHole.image
            }
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return imageArrayNamed.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as!  ImageCollectionViewCell
            if collectionView == self.bottomCollectionView {
                cell.imgImage.image = UIImage(named: imageArrayNamed[indexPath.row])
                sheetName = imageArrayNamed[indexPath.row]            }
            return cell
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if indexPath.row <= imageArrayNamed.count {
                bigStampImage.image = UIImage(named: imageArrayNamed[indexPath.row])
                sheetName = imageArrayNamed[indexPath.row]            }
        }
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "DetailTwentyEightToSheet" {
                let detailVC = segue.destination as? QuantityOfSheetViewController
                detailVC?.cropedImagesArray = self.cropedImagesArray
                detailVC?.sheetName =  sheetName!
                detailVC?.markCost = self.markCost
                detailVC?.countLetter = self.countLetter
            }
        }
}
