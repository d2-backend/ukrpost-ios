//
//  StampCityViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 24.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

protocol StampCityDelegate {
    func setCity(city: StampCity)
}

struct StampCity {
    let name: String
    let code: String
    var APIID: String?
}

class StampCityViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {

    private var region: StampRegion?
    private var district: StampDistrict?

    let mainView = IndexSearchView()
    var delegate: StampCityDelegate?
    private var cityRaw:[StampCity]?
    private var cityForDisplay:[StampCity]?
    private var selectedCity: StampCity?
    init(region:StampRegion, district: StampDistrict) {
        self.region = region
        self.district = district
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        self.mainView.searchField.delegate = self
        self.mainView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        fetchCitys()
    }

    func fetchCitys() {
      self.mainView.searchField.isUserInteractionEnabled = false
        UPAPIManager.getStampCity(region:self.region!, district: self.district!) { (result) in
            switch result {
            case .success(let result):
                self.cityRaw = result.sorted(by: self.sortByUkrainianName)
                self.cityForDisplay = self.cityRaw
                self.mainView.tableView.reloadData()
                  self.mainView.searchField.isUserInteractionEnabled = true
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
   
    func sortByUkrainianName(first:StampCity, second:StampCity) -> Bool {
        let defaultCompareString = " аАбБвВгГґҐдДеЕєЄёЁжЖзЗиИіІїЇйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩьЬъЪыЫэЭюЮяЯaAbBcCdDeEfFgGhHiIkKlLmMnNoOpPqQrRsStTvVxXyYzZ"
        for (index, char) in first.name.enumerated() {
            
            var indexOfLeftWord: Int = 9999
            if let indexL = defaultCompareString.index(of: char) {
                let distanceL = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexL)
                indexOfLeftWord = distanceL
            }
            
            let indexOfRight = second.name.index (second.name.startIndex, offsetBy: index)
            let rightChar = second.name[indexOfRight]
            var indexOfRightWord: Int = 9999
            
            if let indexR = defaultCompareString.index(of: rightChar) {
                let distanceR = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexR)
                indexOfRightWord = distanceR
            }
            if indexOfLeftWord > indexOfRightWord || indexOfLeftWord < indexOfRightWord {
                return indexOfLeftWord < indexOfRightWord
            } else {
                return false
            }
        }
        return false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.mainView.searchField.textColor = UPColor.darkGray
        self.cityForDisplay = []
        self.mainView.tableView.reloadData()
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }
        // релоад при каждом изменении в текстфилде
        if newString != "" {
            for aCity in self.cityRaw! {
                if aCity.name.lowercased().range(of: newString.lowercased()) != nil {
                    cityForDisplay!.append(aCity)
                }
            }
        } else {
            self.cityForDisplay = self.cityRaw
        }
        self.mainView.tableView.reloadData()
        return true
    }
    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cityForDisplay?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        let aCity = cityForDisplay![indexPath.row]
        
        cell.textLabel!.text = "\(aCity.name)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mainView.tableView.deselectRow(at: indexPath, animated: true)
        let aSelectedCity = self.cityForDisplay![indexPath.row]
        self.selectedCity = aSelectedCity
        getFullCityData()
    }
    
    func getFullCityData() {
        UPAPIManager.getCityList(string: self.selectedCity?.name, regionID: self.region?.apiID, districtID: self.district?.APIID) { (result) in
            switch result {
            case .success(let data):
                let cityList = data
                for aCity in cityList {
                    if aCity.cityUA == self.selectedCity!.name {
                        self.selectedCity?.APIID = aCity.cityID
                        self.navigateBack()
                        break
                    }
                }
                
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }

    }
    
    func navigateBack() {
        delegate?.setCity(city: self.selectedCity!)
        self.navigationController?.popViewController(animated: true)
    }
}













