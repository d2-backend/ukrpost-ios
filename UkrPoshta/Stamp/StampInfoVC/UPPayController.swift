//
//  UPPayController.swift
//  UkrPoshta
//
//  Created by A.Shamanskyi MAC on 5/27/18.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import WebKit

class UPPayController: UIViewController {
    private var webView = WKWebView()
    var order: StampOrder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlToBrowser = NSURL(string: order!.PayHost! + "?" + order!.UpcBody!)!
        UIApplication.shared.openURL(urlToBrowser as URL)
        self.navigationController?.popToRootViewController(animated: true)
        //   let url = URL(string: order!.PayHost! + "?" + order!.UpcBody!)
        //  var request = URLRequest(url: url!)
        //  request.httpMethod = "POST"
        //   request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        //if let theJSONData = try?  JSONEncoder().encode(order?.PayData) {
        // request.httpBody = theJSONData
        //  }
        //   webView.load(request)
        //   webView.allowsBackForwardNavigationGestures = true
        //     view = webView
        //        let image = UIImage(named: "crossSmall")
        //        let rightBarButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(navigateToMain))
        //        self.navigationItem.rightBarButtonItem = rightBarButton
    }
//    @objc func navigateToMain() {
//        self.navigationController?.popToRootViewController(animated: true)
//    }
    
}
