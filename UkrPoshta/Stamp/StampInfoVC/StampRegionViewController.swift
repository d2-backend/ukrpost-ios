//
//  StampRegionViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 23.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

protocol StampRegionDelegate {
    func setRegion(region: StampRegion)
}

struct StampRegion {
    let name: String
    let code: String
    var apiID: String?
}

class StampRegionViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    private let mainView = IndexSearchView()
    var delegate: StampRegionDelegate?
    private var regions:[StampRegion]?
    private var regionsForDisplay:[StampRegion]?
    private var selectedRegion: StampRegion?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchRegions()
      self.view.backgroundColor = UPColor.lightGray
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        self.mainView.searchField.becomeFirstResponder()
        self.mainView.searchField.delegate = self
    }
  
    func fetchRegions() {
        UPAPIManager.getStampAddress { (result) in
            switch result {
            case .success(let result):
                self.regions = result.sorted(by: self.sortByUkrainianName)
                self.regionsForDisplay = self.regions
                self.mainView.tableView.reloadData()
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    //при ошибке парса
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.regionsForDisplay?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        let aRegion = regionsForDisplay![indexPath.row]
        
        cell.textLabel!.text = "\(aRegion.name)"

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mainView.tableView.deselectRow(at: indexPath, animated: true)
        let aSelectedRegion = self.regionsForDisplay![indexPath.row]
        self.selectedRegion = aSelectedRegion
        getFullRegionData(region: aSelectedRegion)
    }
    // сортировка массива
    func sortByUkrainianName(first:StampRegion, second:StampRegion) -> Bool {
        let defaultCompareString = " аАбБвВгГґҐдДеЕєЄёЁжЖзЗиИіІїЇйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩьЬъЪыЫэЭюЮяЯaAbBcCdDeEfFgGhHiIkKlLmMnNoOpPqQrRsStTvVxXyYzZ"
        for (index, char) in first.name.enumerated() {
            
            var indexOfLeftWord: Int = 9999
            if let indexL = defaultCompareString.index(of: char) {
                let distanceL = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexL)
                indexOfLeftWord = distanceL
            }
            
            let indexOfRight = second.name.index (second.name.startIndex, offsetBy: index)
            let rightChar = second.name[indexOfRight]
            var indexOfRightWord: Int = 9999
            
            if let indexR = defaultCompareString.index(of: rightChar) {
                let distanceR = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexR)
                indexOfRightWord = distanceR
            }
            if indexOfLeftWord > indexOfRightWord || indexOfLeftWord < indexOfRightWord {
                return indexOfLeftWord < indexOfRightWord
            } else {
                return false
            }
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.mainView.searchField.textColor = UPColor.darkGray
        self.regionsForDisplay = []
        self.mainView.tableView.reloadData()
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }
        // релоад при каждом изменении в текстфилде
        if newString != "" {
            for aRegion in self.regions! {
                if aRegion.name.lowercased().range(of: newString.lowercased()) != nil {
                    regionsForDisplay!.append(aRegion)
                }
            }
        } else {
            self.regionsForDisplay = self.regions
        }
        self.mainView.tableView.reloadData()
        return true
    }
    
    func getFullRegionData(region: StampRegion) {
        UPAPIManager.getRegionList { (result) in
            switch result {
            case .success(let data):
                let regionList = data
                for aRegion in regionList {
                    if aRegion.regionUA == region.name {
                        self.selectedRegion?.apiID = aRegion.regionID
                        self.navigateBack()
                        break
                    }
                }
                
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func navigateBack() {
            delegate?.setRegion(region: self.selectedRegion!)
            self.navigationController?.popViewController(animated: true)
    }
}
