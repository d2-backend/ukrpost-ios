//
//  StampInfoViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 21.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class StampInfoViewController: UIViewController, UITextFieldDelegate, StampRegionDelegate, StampDistrictDelegate, StampCityDelegate, StampStreetDelegate {
  
    //MARK: ссылаюсь сюда для передачи
    var sheetsCount:Int?
      var countLetter : Int?

    var comment = String()
    var name = String()
    var serName = String()
    var patronomyc = String()
    var phoneNumber = String()
    var email = String()
    var cropedImagesArray:[UIImageView]?
    var sheetId : String?
    var markCost: Int?
    //MARK: переменные для оформления созданной марки
    var index = String()
    var area = String()
    var locality = String()
    var house = String()
    var flat = String()
    var entrance = String()
    var floor = String()
    var regionDDL = String()
    var citys = String()
    var ddlIndex = String()

    let mainView = StampInfoView()
    var activeTextField: UITextField?
    private var region: StampRegion?
    private var district: StampDistrict?
    private var city: StampCity?
    private var street: Street?
    private var streetsDLL: [StampStreet]?
    private var selectedIndexAndHouse: IndexAndHouse?
    private var selectedStreetDLL: String?
      var fullName = String()
      var addressStr = String()
      var deliveryType = Int()
      var delivaryCost = Float()
      var room = String()
      
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Власна марка"
        hideBackNavigationButtonText()
      self.view.backgroundColor = UPColor.lightGray
        self.mainView.frame = self.view.frame
        self.mainView.infoScrollView.isScrollEnabled = false
        mainView.indexTextField.delegate = self
        mainView.houseTextField.delegate = self
        mainView.roomTextField.delegate = self
        mainView.letterButton.isSelected = true
        self.mainView.issueButtonFirst.isEnabled = false

        self.view.addSubview(mainView)
        self.mainView.letterButton.addTarget(self, action: #selector(letterPressed), for: .touchUpInside)
        self.mainView.pickupButton.addTarget(self, action: #selector(pickUpPressed), for: .touchUpInside)
        self.mainView.issueButtonFirst.addTarget(self, action: #selector(issuePressed), for: .touchUpInside)
        self.mainView.submitPickupButton.addTarget(self, action: #selector(issuePressed), for: .touchUpInside)
        self.mainView.regionButton.addTarget(self, action: #selector(regionPressed), for: .touchUpInside)
        self.mainView.districtButton.addTarget(self, action: #selector(districtPressed), for: .touchUpInside)
        self.mainView.cityButton.addTarget(self, action: #selector(cityPressed), for: .touchUpInside)
        self.mainView.streetButton.addTarget(self, action: #selector(streetPressed), for: .touchUpInside)

        
        self.mainView.districtButton.isEnabled = false
        
        self.mainView.cityButton.isEnabled = false

        self.mainView.streetButton.isEnabled = false
        
    }
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func checkNillInfo(){
      if self.mainView.indexTextField.text == "" || self.mainView.houseTextField.text == "" || self.mainView.districtButton.isEnabled == false || self.mainView.cityButton.isEnabled == false  || self.mainView.regionButton.isEnabled == false || self.mainView.streetButton.isEnabled == false ||  self.selectedStreetDLL == nil {
            self.mainView.issueButtonFirst.isEnabled = false
        } else {
            self.mainView.issueButtonFirst.isEnabled = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector:#selector(keybordWasShown) , name: NSNotification.Name.UIKeyboardDidShow, object: nil)
       checkNillInfo()
    }
    
     func textFieldDidEndEditing(_ textField: UITextField) {
        checkNillInfo()
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        super.viewWillDisappear(animated)
    }
    
    @objc func regionPressed() {
        let vc = StampRegionViewController()
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @objc func districtPressed() {
        let vc = StampDistrictViewController(region: self.region!)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func cityPressed() {
        let vc = StampCityViewController(region: self.region!, district: self.district!)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func streetPressed() {
        let vc = StampStreetViewController(region: self.region!, district: self.district!, city: self.city!)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func letterPressed() {
        self.mainView.pickupButton.isSelected = false
        self.mainView.letterButton.isSelected = true
        self.mainView.infoScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.mainView.letterIcon.image = UIImage(named: "radioButtonSelected")
        self.mainView.pickupIcon.image = UIImage(named: "radioButton")
    }
    
    @objc func pickUpPressed() {
        let width = UIScreen.main.bounds.size.width
        self.mainView.pickupButton.isSelected = true
        self.mainView.letterButton.isSelected = false
        self.mainView.infoScrollView.setContentOffset(CGPoint(x: width, y: 0), animated: true)
        self.mainView.letterIcon.image = UIImage(named: "radioButton")
        self.mainView.pickupIcon.image = UIImage(named: "radioButtonSelected")
    }
      
    //сюда
      @objc func issuePressed() {
            let vc = IssueViewController()
            vc.sheetsCount = self.sheetsCount!
            vc.comment = self.comment
            vc.fullName = self.serName + " " + self.name + " " + self.patronomyc
            vc.deliveryType =  self.mainView.letterButton.isSelected ? 1 : 2
            vc.regionDDL = self.mainView.letterButton.isSelected ?  (self.region?.code)! : "item500000009"
            vc.city = self.mainView.letterButton.isSelected ? (self.city?.code)! : "item7711429808"
            vc.ddlIndex = self.mainView.letterButton.isSelected ? self.selectedStreetDLL! : "item1001"
            vc.cropedImagesArray = self.cropedImagesArray
            vc.sheetId  = self.sheetId
            vc.markCost = self.markCost!
            vc.phoneNumber = self.phoneNumber
            vc.delivaryCost = self.delivaryCost
            vc.countLetter = self.countLetter!
            vc.ddlDistrict = self.mainView.letterButton.isSelected ? (self.district?.code)! : "item0"
            if self.mainView.roomTextField.text == "" {
            vc.flat = ""
                  }
            if self.mainView.roomTextField.text != "" {
             vc.flat = self.mainView.roomTextField.text!
                  }
           
            if self.mainView.letterButton.isSelected {
                  let cityName = self.city!.name
                  let streetName = self.street?.streetUA
                  let houseStr = self.selectedIndexAndHouse?.houseNumber
                  let index = self.selectedIndexAndHouse?.postcode
                  addressStr = "\(streetName!) \(houseStr!), \(cityName), \(index ?? "")"
            } else {
                  addressStr = "Хрещатик 22, Київ, 01001"
            }
            vc.addressStr = addressStr

            self.navigationController?.pushViewController(vc, animated: true)
      }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if self.view.frame.origin.y != 64 {
            UIView.animate(withDuration: 0.25) {
                self.view.frame = CGRect(x:self.view.frame.origin.x , y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        if textField == self.mainView.houseTextField && self.mainView.houseTextField.text != ""{
            self.getData(house: self.mainView.houseTextField.text!)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func keybordWasShown(notification: NSNotification) {
        let textFieldBottom = self.activeTextField?.frame.maxY
        let visibleHeight = self.view.bounds.height - textFieldBottom!
        let keyboardRect: CGRect = notification.userInfo!["UIKeyboardBoundsUserInfoKey"] as! CGRect
        let keyboardHeight = keyboardRect.size.height
        
        if keyboardHeight > visibleHeight {
            let heightToAnimate = keyboardHeight - visibleHeight
            UIView.animate(withDuration: 0.25) {
                self.view.frame = CGRect(x:self.view.frame.origin.x , y: -heightToAnimate, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
    }
    
    func setRegion(region: StampRegion) {
        self.region = region
        self.mainView.regionButton.setTitle(region.name, for: .selected)
        self.mainView.regionButton.isSelected = true

        if self.region?.apiID == "10" {
            self.district = StampDistrict(name: "Київ", code: "item0", APIID: "581")
            self.mainView.districtButton.setTitle(self.district!.name, for: .selected)

            self.mainView.districtButton.isEnabled = true
            self.mainView.districtButton.isSelected = true
            self.city = StampCity(name: "Київ", code: "item7711429808", APIID: "1")
            self.mainView.cityButton.setTitle(self.city!.name, for: .selected)
            self.mainView.cityButton.isEnabled = true
            self.mainView.cityButton.isSelected = true
            self.mainView.streetButton.isEnabled = true
        }
        self.mainView.districtButton.isEnabled = true
    }
    
    func setDistrict(district: StampDistrict) {
        self.district = district
        self.mainView.districtButton.setTitle(district.name, for: .selected)
        self.mainView.districtButton.isSelected = true
        
        self.mainView.cityButton.isEnabled = true
    }
    
    func setCity(city: StampCity)
    {
        self.city = city
        self.mainView.cityButton.setTitle(city.name, for: .selected)
        self.mainView.cityButton.isSelected = true
        
        self.mainView.streetButton.isEnabled = true
    }
    
    func setStreet(street: Street, streetsDLL: [StampStreet]) {
        self.street = street
        self.streetsDLL = streetsDLL
        self.mainView.streetButton.setTitle(street.streetUA, for: .selected)
        self.mainView.streetButton.isSelected = true
        
    }
    
    func getData(house:String)  {
        UPAPIManager.getIndexesList(streetID: self.street?.streetID) { (result) in
            switch result {
            case .success(let data):
                let list = data
                if list.count > 0 {
                    for item in list {
                        if item.houseNumber == house {
                            self.selectedIndexAndHouse = item
                            for aDLL in self.streetsDLL! {
                                if aDLL.index == item.postcode {
                                    self.selectedStreetDLL = aDLL.code
                                }
                            }
                              if self.selectedStreetDLL == nil {
                                    self.selectedStreetDLL = self.streetsDLL?[0].code
                              }
                            break
                        }
                    }
                    if self.selectedIndexAndHouse == nil {
                        self.mainView.houseTextField.textColor = .red
                        self.mainView.issueButtonFirst.isEnabled = false
                    } else {
                        self.mainView.houseTextField.textColor = UPColor.darkGray
                        self.mainView.issueButtonFirst.isEnabled = true
                    }
                } else {
                    self.showAlertWith(text: "Нажаль данної комбінації вудиця-будинок не знайдено")
                    self.street = nil
                    self.streetsDLL = nil
                    self.mainView.streetButton.setTitle("", for: .selected)
                    self.mainView.streetButton.isSelected = false
                    self.mainView.houseTextField.text = ""
                }
 
            case .partialSuccess(let string):
                self.showAlertWith(text: string)
            case .failure(let error):
                self.showAlertWith(text: error.localizedDescription)
            }
        }
    }
    
  
      
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}






