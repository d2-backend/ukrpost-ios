//
//  StampDistrictViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 24.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

protocol StampDistrictDelegate {
    func setDistrict(district: StampDistrict)
}

struct StampDistrict {
    let name: String
    let code: String
    var APIID:String?
}

class StampDistrictViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {
    
    private var region: StampRegion?
    private let mainView = IndexSearchView()
    var delegate: StampDistrictDelegate?
    private var districtsRaw:[StampDistrict]?
    private var districtsForDisplay:[StampDistrict]?
    private var selectedDistrict: StampDistrict?
    init(region:StampRegion) {
        self.region = region
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        self.mainView.searchField.delegate = self
        self.mainView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        fetchDistricts()
        
    }
    
    func fetchDistricts() {
        UPAPIManager.getStampDistrict(region: self.region!) { (result) in
            switch result {
            case .success(let result):
                self.districtsRaw = result.sorted(by: self.sortByUkrainianName)
                self.districtsForDisplay = self.districtsRaw
                self.mainView.tableView.reloadData()
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
  
    func sortByUkrainianName(first:StampDistrict, second:StampDistrict) -> Bool {
        let defaultCompareString = " аАбБвВгГґҐдДеЕєЄёЁжЖзЗиИіІїЇйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩьЬъЪыЫэЭюЮяЯaAbBcCdDeEfFgGhHiIkKlLmMnNoOpPqQrRsStTvVxXyYzZ"
        for (index, char) in first.name.enumerated() {
            
            var indexOfLeftWord: Int = 9999
            if let indexL = defaultCompareString.index(of: char) {
                let distanceL = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexL)
                indexOfLeftWord = distanceL
            }
            
            let indexOfRight = second.name.index (second.name.startIndex, offsetBy: index)
            let rightChar = second.name[indexOfRight]
            var indexOfRightWord: Int = 9999
            
            if let indexR = defaultCompareString.index(of: rightChar) {
                let distanceR = defaultCompareString.distance(from: defaultCompareString.startIndex, to: indexR)
                indexOfRightWord = distanceR
            }
            if indexOfLeftWord > indexOfRightWord || indexOfLeftWord < indexOfRightWord {
                return indexOfLeftWord < indexOfRightWord
            } else {
                return false
            }
        }
        return false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.mainView.searchField.textColor = UPColor.darkGray
        self.districtsForDisplay = []
        self.mainView.tableView.reloadData()
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }
        // релоад при каждом изменении в текстфилде
        if newString != "" {
            for aDistrict in self.districtsRaw! {
                if aDistrict.name.lowercased().range(of: newString.lowercased()) != nil {
                    districtsForDisplay!.append(aDistrict)
                }
            }
        } else {
            self.districtsForDisplay = self.districtsRaw
        }
        
        self.mainView.tableView.reloadData()
        
        return true
    }
    //при ошибке парса
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.districtsForDisplay?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        let aDistrict = districtsForDisplay![indexPath.row]
        
        cell.textLabel!.text = "\(aDistrict.name)"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mainView.tableView.deselectRow(at: indexPath, animated: true)
        let aSelectedDistrict = self.districtsForDisplay![indexPath.row]
        self.selectedDistrict = aSelectedDistrict
        self.getFulldistrictData()
//        delegate?.setDistrict(district: selectedDistrict)
//        self.navigationController?.popViewController(animated: true)
    }
    
    func getFulldistrictData() {
        UPAPIManager.getDistrictList(string: self.selectedDistrict?.name, regionID: self.region?.apiID) { (result) in
            switch result {
            case .success(let data):
                let districtList = data
                for aDistrict in districtList {
                    if aDistrict.districtUA == self.selectedDistrict!.name {
                        self.selectedDistrict?.APIID = aDistrict.districtID
                        self.navigateBack()
                        break
                    }
                }
                
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func navigateBack() {
        delegate?.setDistrict(district: self.selectedDistrict!)
        self.navigationController?.popViewController(animated: true)
    }
}
