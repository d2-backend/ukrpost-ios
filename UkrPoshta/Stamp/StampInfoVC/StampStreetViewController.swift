//
//  StampStreetViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 24.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
protocol StampStreetDelegate {
    func setStreet(street: Street, streetsDLL: [StampStreet])
}

struct StampStreet {
    let code: String
    let index: String
    var name: String?
    var APIID: String?
}
class StampStreetViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {

    private var region: StampRegion?
    private var district: StampDistrict?
    private var city: StampCity?
    
    private let mainView = IndexSearchView()
    var delegate: StampStreetDelegate?
    private var streetRaw:[StampStreet]?
    private var streetForDisplay:[Street]?
    private var selectedStreet: Street?
    
    init(region:StampRegion, district: StampDistrict, city: StampCity) {
        self.region = region
        self.district = district
        self.city = city
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        self.mainView.searchField.delegate = self
        self.mainView.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")
        fetchStreets()
        fetchStreetsWithName(string: "")
    }
    
    func fetchStreets() {
        UPAPIManager.getStampStreet(region:self.region!, district: self.district!, city: self.city!) { (result) in
            switch result {
            case .success(let result):
                self.streetRaw = result
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    
    func showAlertWith(text: String) {
        let alert = UIAlertController(title: NSLocalizedString("error.default.title", comment: ""), message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("error.default.ok", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.streetForDisplay?.count ?? 0
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)
        let aStreet = streetForDisplay![indexPath.row]
        
        cell.textLabel!.text = "\(aStreet.streetUA)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.mainView.tableView.deselectRow(at: indexPath, animated: true)
        if self.streetRaw != nil {
            let selectedStreet = self.streetForDisplay![indexPath.row]
            delegate?.setStreet(street: selectedStreet,  streetsDLL: self.streetRaw!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func fetchStreetsWithName(string: String) {
        UPAPIManager.getStreetList(string: string, regionID: self.region?.apiID, districtID: self.district?.APIID, cityID: self.city?.APIID) { (result) in
            switch result {
            case .success(let data):
                    self.streetForDisplay = data
                self.mainView.tableView.reloadData()
                if data.count == 0 {
                    self.mainView.searchField.textColor = .red
                }
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }

    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n" {
            return false
        }
        self.mainView.searchField.textColor = UPColor.darkGray
        self.streetForDisplay = []
        self.mainView.tableView.reloadData()
        let oldString = textField.text
        var newString: String
        if string != "" {
            newString = oldString! + string
        } else {
            newString = oldString!
            newString.removeLast()
        }

        self.fetchStreetsWithName(string: newString)
        return true
    }

}







