//
//  StampInfoView.swift
//  UkrPoshta
//
//  Created by Zhekon on 21.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class StampInfoView: UIView {
    
    let letterButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let pickupButton = UPHorizontalButton(normalImageName: "radioButton", selectedImageName: "radioButtonSelected")
    let letterIcon = UIImageView()
    let pickupIcon = UIImageView()

    let infoScrollView = UIScrollView()
    let adressLabel = UPHeader()
    
    let indexTextField = UPTextField()
    let regionButton = UPTextButton()
    let districtButton = UPTextButton()
    let cityButton = UPTextButton()
    let streetButton = UPTextButton()
    let houseTextField = UPTextField()
    let roomTextField = UPTextField()

    let issueButtonFirst = UPSubmitButton()
    private let leftView = UIView()
    private let rightView = UIView()
    let submitPickupButton = UPSubmitButton()
    let infoButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
      
       letterButton.label.text = "Лист"
        addSubview(letterButton)
        pickupButton.label.text = "Самовивiз"
        addSubview(pickupButton)
        
        issueButtonFirst.setTitle("Оформити", for: .normal)
        leftView.addSubview(issueButtonFirst)
        adressLabel.text = "Адреса"
        leftView.addSubview(adressLabel)
        indexTextField.placeholder = "Iндекс"
        leftView.addSubview(indexTextField)
        
        regionButton.setTitle("Область", for: .normal)
        leftView.addSubview(regionButton)
        
        districtButton.setTitle("Район", for: .normal)
        leftView.addSubview(districtButton)
        
        cityButton.setTitle("Населенный пункт", for: .normal)
        leftView.addSubview(cityButton)
        
        streetButton.setTitle("вулиця", for: .normal)
        leftView.addSubview(streetButton)
        
        houseTextField.placeholder = "будинок"
        leftView.addSubview(houseTextField)
        
        roomTextField.placeholder = "квартира"
        leftView.addSubview(roomTextField)
        infoScrollView.isPagingEnabled = true
        addSubview(infoScrollView)
        infoScrollView.addSubview(leftView)
        infoScrollView.addSubview(rightView)
        submitPickupButton.setTitle("Оформити", for: UIControlState .normal)
        infoButton.setTitle("Головпоштамт, Хрещатик 22 Киïв, 01001", for: UIControlState .normal)
        infoButton.setTitleColor(UPColor.darkGray, for: .normal)
        infoButton.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        rightView.addSubview(submitPickupButton)
        infoButton.backgroundColor = .white
        infoButton.layer.shadowColor = UPColor.darkGray.cgColor
        infoButton.layer.shadowOpacity = 0.2
        infoButton.layer.shadowOffset = .zero
        infoButton.layer.shadowRadius = 2
        infoButton.layer.cornerRadius = 3
        rightView.addSubview(infoButton)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let size = UIScreen.main.bounds.size
     
        letterButton.pin.top().right().left().height(50).width(size.width / 2 )
        pickupButton.pin.top(0).right().left(to: letterButton.edge.right).marginLeft(0).height(50).width(size.width / 2 )
        infoScrollView.contentSize = CGSize(width: size.width * 2, height: size.height - 100)
        infoScrollView.pin.below(of: letterButton).marginTop(0).left().bottom().right()
        leftView.pin.top().left().bottom().width(size.width)
        rightView.pin.left(to: leftView.edge.right).top().bottom().width(size.width)
        adressLabel.pin.top(10).right(20).left(20).height(25)
        indexTextField.pin.top(to: adressLabel.edge.bottom).marginTop(5).left(15).right(15).height(45)
        regionButton.pin.top(to: indexTextField.edge.bottom).marginTop(7.5).left(15).right(15).height(45)
        districtButton.pin.top(to: regionButton.edge.bottom).marginTop(7.5).left(15).right(15).height(45)
        cityButton.pin.top(to: districtButton.edge.bottom).marginTop(7.5).left(15).right(15).height(45)
        streetButton.pin.top(to: cityButton.edge.bottom).marginTop(7.5).left(15).right(15).height(45)
        houseTextField.pin.top(to: streetButton.edge.bottom).marginTop(7.5).left(15).right(size.width / 2 + 2.5).height(45)
        roomTextField.pin.top(to: streetButton.edge.bottom).marginTop(7.5).left(to: houseTextField.edge.right).marginLeft(5).right(15).height(45)
        issueButtonFirst.pin.left(0).right(0).bottom(64).height(64)
        
        submitPickupButton.pin.bottom(64).left().right().height(64)
        infoButton.pin.top(20).left(20).right(20).height(64)
    }
}






