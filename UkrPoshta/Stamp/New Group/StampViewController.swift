//
//  StampViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 16.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//
//

import UIKit
import Photos
import AVFoundation

  enum TypeStamp {
    case eight
    case nine
    case twentyEight
}

class StampViewController: UIViewController {
    
    let mainView = StampView()
    var selectedType :TypeStamp?
    let pickerController = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Власна марка"
        self.view.addSubview(mainView)
        pickerController.sourceType = UIImagePickerControllerSourceType.camera
        pickerController.delegate = self
        
        self.mainView.frame = self.view.frame
        self.mainView.firstButton.addTarget(self, action: #selector(nineStampPressed), for: .touchUpInside)
        self.mainView.secondButton.addTarget(self, action: #selector(eightStampPressed), for: .touchUpInside)
        self.mainView.thirdButton.addTarget(self, action: #selector(twentyeightStampPressed), for: .touchUpInside)

    }
    
    func navigateToGallery() {
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "GeneralCropVC") as! UINavigationController
        let firstVC = controller.viewControllers[0] as! GeneralCropVC
        firstVC.selectedType = self.selectedType
        self.navigationController?.pushViewController(firstVC, animated: true)
    }
    
    func checkCameraIsUsed(){
      if AVCaptureDevice.authorizationStatus(for: .video) == .authorized || AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined{
            self.present(self.pickerController, animated: true, completion: nil)
      }
      
      if  AVCaptureDevice.authorizationStatus(for: .video) == .restricted || AVCaptureDevice.authorizationStatus(for: .video) == .denied {
            let alertController = UIAlertController (title: "Помилка", message: "Перейдiть до налаштувань, та надайте доступ до галареï", preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "До налаштувань", style: .default) { (_) -> Void in
                  guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                        return
                  }
                  if UIApplication.shared.canOpenURL(settingsUrl) {
                        if #available(iOS 10.0, *) {
                              UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                              })
                        }
                  }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Закрити", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
      }
}
    
    func checkStatusPhotoLibrary(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.denied) {
            let alertController = UIAlertController (title: "Помилка", message: "Перейдiть до налаштувань, та надайте доступ до галареï", preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "До налаштувань", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        })
                    }
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Закрити", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func showActionSheet(){
        let optionMenu = UIAlertController(title: nil, message: "оберiть опцiю", preferredStyle: .actionSheet)
        optionMenu.view.tintColor = UPColor.darkGray
        let openCamera = UIAlertAction(title: "вiдкрити камеру", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.checkCameraIsUsed()
        })
        
        let openGallery = UIAlertAction(title: "вiдкрити галерею", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.checkStatusPhotoLibrary()
            self.navigateToGallery()
        })
            
        let cancelAction = UIAlertAction(title: "закрити", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(openGallery)
        optionMenu.addAction(openCamera)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @objc func nineStampPressed() {
        selectedType = .nine
        showActionSheet()
    }
    
    @objc func eightStampPressed() {
        selectedType = .eight
        showActionSheet()
    }
    @objc func twentyeightStampPressed() {
        selectedType = .twentyEight
        showActionSheet()
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            pickerController.dismiss(animated: true, completion: {
            self.navigateToGallery()
            })
        }
    }
}

extension StampViewController : UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        UIImageWriteToSavedPhotosAlbum(info[UIImagePickerControllerOriginalImage] as! UIImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        
    }
}

extension StampViewController : UINavigationControllerDelegate {
    
}


