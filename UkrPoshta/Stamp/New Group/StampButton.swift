//
//  StampButton.swift
//  UkrPoshta
//
//  Created by Zhekon on 16.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class StampButton: UIButton {
    
    let imageStamp = UIImageView()
    let stampLabel = UILabel()
    let descriptionStampLabel = UILabel()
    let costStampLabel = UILabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .white
        
        stampLabel.numberOfLines = 0
        stampLabel.font  = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        stampLabel.textColor = UPColor.darkGray
        
        descriptionStampLabel.numberOfLines = 0
        descriptionStampLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        descriptionStampLabel.textColor = UPColor.lightGray
        
        costStampLabel.numberOfLines = 0
        costStampLabel.font  = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        costStampLabel.textColor = UPColor.darkGray
      
        imageStamp.contentMode = .scaleAspectFit
        
        self.addSubview(imageStamp)
        self.addSubview(stampLabel)
        self.addSubview(descriptionStampLabel)
        self.addSubview(costStampLabel)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let iconSize  =  self.frame.height - 30
        imageStamp.pin.top(15).left(15).bottom(15).width(iconSize).height(iconSize)
        stampLabel.pin.top(15).left(to: imageStamp.edge.right).marginLeft(10) .right(to: costStampLabel.edge.left).marginRight(15).height(30).width(80)
        costStampLabel.pin.left(to:stampLabel.edge.right).marginLeft(0).right(0).top(15).height(30).width(65)
        descriptionStampLabel.pin.top().left(to: stampLabel.edge.left).marginLeft(0).right(0).bottom()
    }
}








