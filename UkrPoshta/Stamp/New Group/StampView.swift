//
//  StampView.swift
//  UkrPoshta
//
//  Created by Zhekon on 16.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class StampView: UIView {

    let firstButton = StampButton()
    let secondButton = StampButton()
    let thirdButton = StampButton()
    let typeStamp = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        typeStamp.text = "Тип аркуша"
        typeStamp.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        typeStamp.textColor = UPColor.lightGray
        
        firstButton.costStampLabel.text = "165 грн"
        firstButton.costStampLabel.font = UIFont.boldSystemFont(ofSize: 16)
        firstButton.descriptionStampLabel.text = "В аркуші 9 марок з додатковим оформленням полів"
        firstButton.stampLabel.text = "9 марок"
        firstButton.imageStamp.image = UIImage(named: "picture_nine_stamps")
        
        secondButton.costStampLabel.text = "165 грн"
        secondButton.costStampLabel.font = UIFont.boldSystemFont(ofSize: 16)
        secondButton.descriptionStampLabel.text = "В аркуші 8 марок"
        secondButton.stampLabel.text = "8 марок"
        secondButton.imageStamp.image = UIImage(named: "picture_eight_stamps")

        thirdButton.costStampLabel.text = "390 грн"
        thirdButton.costStampLabel.font = UIFont.boldSystemFont(ofSize: 15)
        thirdButton.descriptionStampLabel.text = "В аркуші 28 марок"
        thirdButton.stampLabel.text = "28 марок"
        thirdButton.imageStamp.image = UIImage(named: "picture_twenty_eight_stamps")
        
        self.addSubview(firstButton)
        self.addSubview(secondButton)
        self.addSubview(thirdButton)
        self.addSubview(typeStamp)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let size = UIScreen.main.bounds.size
        let heightButton = (size.height / 3) - 55
        let widthButton = size.width - 40
        typeStamp.pin.top().bottom(to: firstButton.edge.top).marginTop(15).left(20).right().width(90).height(20)
        firstButton.pin.top(size.height / 15).left(20).right(20).height(heightButton).width(widthButton)
        secondButton.pin.top(to: firstButton.edge.bottom).marginTop(15).left(20).right(20).height(heightButton).width(widthButton)
        thirdButton.pin.top(to: secondButton.edge.bottom).marginTop(15).left(20).right(20).height(heightButton).width(widthButton)

    }
}











