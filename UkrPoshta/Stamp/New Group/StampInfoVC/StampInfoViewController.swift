//
//  StampInfoViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 21.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class StampInfoViewController: UIViewController, UITextFieldDelegate {
    
    let mainView = StampInfoView()
    var activeTextField: UITextField?
    
    override func viewDidLoad() {
        mainView.areaTextField.delegate = self
        mainView.entranceTextField.delegate = self
        mainView.floorTextField.delegate = self
        mainView.houseTextField.delegate = self
        mainView.indexTextField.delegate = self
        mainView.localityTextField.delegate = self
        mainView.regionTextField.delegate = self
        mainView.roomTextField.delegate = self
        mainView.streetTextField.delegate = self
        
        super.viewDidLoad()
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        self.mainView.letter.addTarget(self, action: #selector(letterPressed), for: .touchUpInside)
        self.mainView.pickup.addTarget(self, action: #selector(pickUpPressed), for: .touchUpInside)
        
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        NotificationCenter.default.addObserver(self, selector:#selector(keybordWasShown) , name: NSNotification.Name.UIKeyboardDidShow, object: nil)
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        NotificationCenter.default.removeObserver(self)
//    }

    @objc func letterPressed() {
        self.mainView.pickup.isSelected = false
        self.mainView.letter.isSelected = true
        self.mainView.infoScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @objc func pickUpPressed() {
        let width = UIScreen.main.bounds.size.width
        self.mainView.pickup.isSelected = true
        self.mainView.letter.isSelected = false
        self.mainView.infoScrollView.setContentOffset(CGPoint(x: width, y: 0), animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeTextField = textField
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if self.view.frame.origin.y != 64 {
            UIView.animate(withDuration: 0.25) {
                self.view.frame = CGRect(x:self.view.frame.origin.x , y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
        return true
    }
    
    @objc func keybordWasShown(notification: NSNotification) {
        let textFieldBottom = self.activeTextField?.frame.maxY
        let visibleHeight = self.view.bounds.height - textFieldBottom!
        let keyboardRect: CGRect = notification.userInfo!["UIKeyboardBoundsUserInfoKey"] as! CGRect
        let keyboardHeight = keyboardRect.size.height
        
        if keyboardHeight > visibleHeight {
            let heightToAnimate = keyboardHeight - visibleHeight
            UIView.animate(withDuration: 0.25) {
                self.view.frame = CGRect(x:self.view.frame.origin.x , y: -heightToAnimate, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        }
    }
}






