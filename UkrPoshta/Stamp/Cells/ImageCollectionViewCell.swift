//
//  ImageViewCell.swift
//  UkrPoshta
//
//  Created by Zhekon on 17.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgImage: UIImageView!
}
