//
//  GeneralCropVC.swift
//  UkrPoshta
//
//  Created by Zhekon on 17.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//
import Foundation
import UIKit
import Photos

class GeneralCropVC: UIViewController {
    var photos:[PHAsset]!
    var imageViewToDrag: UIImageView!
    var indexPathOfImageViewToDrag: IndexPath!
    var cropArray = [UIImageView]()
    var selectedType:TypeStamp?
    let cellWidth = ((UIScreen.main.bounds.size.width)/3)-1
    private let imageLoader = ImageLoader()
    private var croppedImage: UIImage? = nil
    
    @IBOutlet weak var scrollContainerView: UIView!
    @IBOutlet weak var scrollView: CropScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnCrop: UIButton!

    
    @IBAction func crop(_ sender: Any) {
        croppedImage = captureVisibleRect()
        let imageView = UIImageView()
        imageView.image = resizeImage(image: croppedImage!, targetSize: CGSize(width: 484, height: 512))
        self.cropArray.append(imageView)
        switch self.selectedType! {
        case .nine: performSegue(withIdentifier: "SegueNineStamp", sender: nil);
        if cropArray.count >= 9 {
            cropArray.removeAll()
            }
        case .eight: performSegue(withIdentifier: "SegueEightStamp", sender: nil);
        if cropArray.count >= 8 {
            cropArray.removeAll()
            }
        case .twentyEight: performSegue(withIdentifier: "SegueTwentyEightStamp", sender: nil);
        if cropArray.count >= 28 {
            cropArray.removeAll()
            }
        }
    }
 
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
       
    override func viewDidLoad() {
        super.viewDidLoad()
        checkForPhotosPermission()
        self.title = "Власна марка"
       hideBackNavigationButtonText()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueNineStamp" {
            let detailVC = segue.destination as? DetailCropNineVC
            detailVC?.cropedImagesArray = self.cropArray
            detailVC?.croppedImage = croppedImage
            detailVC?.markCost = 165
            detailVC?.countLetter = 9
        }
        if segue.identifier == "SegueEightStamp" {
            let detailVC = segue.destination as? DetailCropEightVC
            detailVC?.cropedImagesArray = self.cropArray
            detailVC?.croppedImage = croppedImage
            detailVC?.markCost = 165
            detailVC?.countLetter = 8
        }
        if segue.identifier == "SegueTwentyEightStamp" {
            let detailVC = segue.destination as? DetailCropTwentyEightVC
                detailVC?.cropedImagesArray = self.cropArray
              detailVC?.croppedImage = croppedImage
              detailVC?.markCost = 390
            detailVC?.countLetter = 28
        }
    }
    
    private func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {

        let rect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height)
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    private func checkForPhotosPermission(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            loadPhotos()
        }
        else if (status == PHAuthorizationStatus.denied) {
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    DispatchQueue.main.async {
                        self.loadPhotos()
                    }
                }
                else {
                }
            })
        }
        else if (status == PHAuthorizationStatus.restricted) {
        }
    }


    @objc func postalOrderButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func loadPhotos(){
        imageLoader.loadPhotos { (assets) in
            self.configureImageCropper(assets: assets)
        }
    }
    
    private func configureImageCropper(assets:[PHAsset]){
        if assets.count != 0{
            photos = assets
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.reloadData()
            selectDefaultImage()
        }
    }
    
    private func selectDefaultImage(){
        collectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .top)
        selectImageFromAssetAtIndex(index: 0)
    }
    
    private func captureVisibleRect() -> UIImage{
        var croprect = CGRect.zero
        let xOffset = (scrollView.imageToDisplay?.size.width)! / scrollView.contentSize.width;
        let yOffset = (scrollView.imageToDisplay?.size.height)! / scrollView.contentSize.height;
        croprect.origin.x = scrollView.contentOffset.x * xOffset;
        croprect.origin.y = scrollView.contentOffset.y * yOffset;
        let normalizedWidth = (scrollView?.frame.width)! / (scrollView?.contentSize.width)!
        let normalizedHeight = (scrollView?.frame.height)! / (scrollView?.contentSize.height)!
        croprect.size.width = scrollView.imageToDisplay!.size.width * normalizedWidth
        croprect.size.height = scrollView.imageToDisplay!.size.height * normalizedHeight
        let toCropImage = scrollView.imageView.image?.fixImageOrientation()
        let cr: CGImage? = toCropImage?.cgImage?.cropping(to: croprect)
        let cropped = UIImage(cgImage: cr!)
        return cropped
    }
    
    private func isSquareImage() -> Bool{
        let image = scrollView.imageToDisplay
        if image?.size.width == image?.size.height { return true }
        else { return false }
    }
    
    func selectImageFromAssetAtIndex(index:NSInteger){
        ImageLoader.imageFrom(asset: photos[index], size: PHImageManagerMaximumSize) { (image) in
            DispatchQueue.main.async {
                self.displayImageInScrollView(image: image)
            }
        }
    }
    
    func displayImageInScrollView(image:UIImage){
        self.scrollView.imageToDisplay = image
    }
    
    func replicate(_ image:UIImage) -> UIImage? { guard let cgImage = image.cgImage?.copy()
        else { return nil }
        return UIImage(cgImage: cgImage, scale: image.scale, orientation: image.imageOrientation)
    }
}

extension GeneralCropVC:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:FAImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FAImageCell", for: indexPath) as! FAImageCell
        cell.populateDataWith(asset: photos[indexPath.item])
        return cell
    }
}

extension GeneralCropVC:UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell:FAImageCell = collectionView.cellForItem(at: indexPath) as! FAImageCell
        cell.isSelected = true
        selectImageFromAssetAtIndex(index: indexPath.item)
    }
}

extension GeneralCropVC:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellWidth, height: cellWidth)
    }
}

