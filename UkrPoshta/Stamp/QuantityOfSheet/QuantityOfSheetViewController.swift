//
//  QuantityOfSheetViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 18.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit


class QuantityOfSheetViewController: UIViewController {
    var cropedImagesArray:[UIImageView]?
    var sheetName = String()
    var sheetId = String()
    var markCost: Int?
    var countLetter : Int?
    var summCostOfMark: Int?
    
    @IBOutlet weak var sheetImage: UIImageView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var addCommentOutlet: UIButton!
    @IBOutlet weak var sheetCount: UILabel!
    @IBOutlet weak var textFieldOutlet: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var greyLabelOutlet: UILabel!
    @IBOutlet weak var numberOfSheetOutlet: UILabel!
    @IBOutlet weak var lettersCountOtlet: UILabel!
    @IBOutlet weak var letterStampOutlet: UILabel!
    @IBOutlet weak var lettersOutlet: UILabel!
    @IBOutlet weak var priceOutlet: UILabel!
   
    @IBAction func nextButtonAction(_ sender: UIButton) {
        let vc = UserInfoViewController()
        vc.sheetsCount = Int(self.textFieldOutlet.text!)
        vc.comment = self.commentTextView.text!
        vc.cropedImagesArray = self.cropedImagesArray
        vc.sheetId = self.sheetId
        vc.markCost = self.summCostOfMark
         vc.countLetter = self.countLetter
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addComment(_ sender: UIButton) {
        commentTextView.isHidden = false
    }
    
    @IBAction func textFieldAction(_ sender: UITextField) {
        if Int((textFieldOutlet.text?.count.description)!)! <= 0 || Int(textFieldOutlet.text!) == 0 {
            nextButton.isEnabled = false
            textFieldOutlet.shake()
        } else {
            if (textFieldOutlet.text?.isStringAnInt())! {
            sheetCount.text = textFieldOutlet.text
            summCostOfMark = Int(textFieldOutlet.text!)! * markCost!
            nextButton.isEnabled = true
            } else {
                let alertController = UIAlertController(title: "помилка", message: "введiть будь-ласка цифрове значення", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Добре", style: .default) { (action:UIAlertAction) in
                }
                alertController.addAction(action1)
                self.present(alertController, animated: true, completion: nil)
                textFieldOutlet.text = ""
                nextButton.isEnabled = true
            }
        }
    }
    
    let markImagesDictionary = [ "142" : "template_nine_stamp_0",
                                                   "108" : "template_nine_stamp_4",
                                                   "109" : "template_nine_stamp_5" ,
                                                   "111" : "template_nine_stamp_6" ,
                                                   "119": "template_nine_stamp_7",
                                                   "100" : "template_nine_stamp_3" ,
                                                   "99" : "template_nine_stamp_9",
                                                   "143" : "template_stamp_nine_para",
                                                   "126" : "template_nine_stamp_8",
                                                   "136" :  "template_nine_stamp_2",
                                                   "117" : "template_eight_stamp_0",
                                                   "127" : "template_twenty_eight_stamp_0"]
    
    func writeImageId() {
        for (_) in markImagesDictionary.keys {
            if let key = markImagesDictionary.someKey(forValue: sheetName) {
                sheetId = key
                return
            }
        }
    }
    
    func hideBackNavigationButtonText() {
        let imgBack = UIImage(named: "ic_back")
        navigationController?.navigationBar.backIndicatorImage = imgBack
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = imgBack
        navigationItem.leftItemsSupplementBackButton = true
        navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Власна марка"
        hideBackNavigationButtonText()
        nextButton.isEnabled = false
        writeImageId()
        sheetCount.text = ""
        numberOfSheetOutlet.text = countLetter?.description
        priceOutlet.text = (markCost?.description)! + "грн"
        letterStampOutlet.text = "В аркушi" + " " + (countLetter?.description)! + " " + "марок."
        commentTextView.isHidden = true
        hideKeyboard()
        view.backgroundColor = UPColor.whiteSmoke
        greyLabelOutlet.backgroundColor = .white
        greyLabelOutlet.layer.shadowColor = UPColor.darkGray.cgColor
        greyLabelOutlet.layer.shadowOpacity = 0.2
        greyLabelOutlet.layer.shadowOffset = .zero
        greyLabelOutlet.layer.shadowRadius = 2
        greyLabelOutlet.layer.cornerRadius = 3
        commentTextView.backgroundColor = .white
        commentTextView.layer.shadowColor = UPColor.darkGray.cgColor
        commentTextView.layer.shadowOpacity = 0.2
        commentTextView.layer.shadowOffset = .zero
        commentTextView.layer.shadowRadius = 2
        commentTextView.layer.cornerRadius = 3
        commentTextView.textColor = UPColor.darkGray
        commentTextView.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        sheetCount.textColor = UPColor.darkGray
        sheetCount.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        greyLabelOutlet.textColor = UPColor.darkGray
        greyLabelOutlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
      
        numberOfSheetOutlet.textColor = UPColor.darkGray
        numberOfSheetOutlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        lettersCountOtlet.textColor = UPColor.darkGray
        lettersCountOtlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        letterStampOutlet.textColor = UPColor.darkGray
        letterStampOutlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        lettersOutlet.textColor = UPColor.darkGray
        lettersOutlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        priceOutlet.textColor = UPColor.darkGray
        priceOutlet.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        addCommentOutlet.setTitleColor(UPColor.darkGray, for: .normal)
        addCommentOutlet.titleLabel?.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)

    }
    
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard) )
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

extension Dictionary where Value: Equatable {
    func someKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}
extension String
{
    func isStringAnInt() -> Bool {
        if let _ = Int(self) {
            return true
        }
        return false
    }
}
