//
//  IssueViewController.swift
//  UkrPoshta
//
//  Created by Zhekon on 24.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit
import ANLoader

class IssueViewController: UIViewController {

    let mainView = IssueView()
    //MARK: ссылаюсь сюда для передачи
    var countLetter = Int()
    var delivaryCost = Float()
    var sheetsCount = Int()
    var phoneNumber = String()
    var comment = String()
    var fullName = String()
    var addressStr = String()
    var deliveryType = Int()
    var city = String()
    var ddlIndex = String()
    var cropedImagesArray: [UIImageView]?
    var sheetId: String?
    var markCost: Int = 0
    var regionDDL = String()
    var ddlDistrict = String()
    var allCost = Float()
    var topTitle = String()
    var flat = String()
    private var stampOrder: StampOrder!
    var orderId = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        mainView.summitButton.isEnabled = false
        if deliveryType == 1 {
            delivaryCost = 22.0
        }
        if deliveryType == 2 {
            delivaryCost = 0.0
        }
        self.mainView.backgroundColor = .white
        mainView.countOfMarkNumber.text = String(sheetsCount) + " шт"
        mainView.countOfMark.text = "Кiлькiсть маркованих аркушiв"
        let countLetter = String(self.countLetter)
        mainView.stampsTopInfo.text = "В аркушi \(countLetter) марок з додатковим оформленням полiв"
        mainView.stampsTop.text = "\(countLetter) марок"
        self.title = "Iнформацiя про замовлення"
        mainView.letterLabel.text = "Вартiсть аркуша, з ПДВ"
        let markcost = String(markCost.description)
        mainView.letterCost.text = "\(markcost)" + " грн"
        mainView.delivaryText.text = "Вартiсть доставки"
        mainView.delivaryCost.text = "\(delivaryCost)" + " грн"
        mainView.generalCost.text = "Загальна вартiсть"
        allCost = Float((markCost)) + delivaryCost
        mainView.generalCostNumber.text = "\(allCost)" + " грн"
        mainView.bottomFooter.text = "Iнформацiя про доставку:"
        mainView.summitButton.setTitle("Пiдтвердити", for: .normal)
        mainView.bottomData.text = "\(fullName)\n\(phoneNumber)\n\(addressStr)"
        self.mainView.frame = self.view.frame
        self.view.addSubview(mainView)
        mainView.summitButton.addTarget(self, action: #selector(submitPressed), for: .touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ANLoader.showLoading()
        createOrder()
    }

    @objc func submitPressed() {
        createOrder()
        let vc = UPPayController()
        vc.order = self.stampOrder
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func createOrder() {
        let count = self.sheetsCount
        let listID = Int(self.sheetId!)
        let paySum = "\(Int(self.allCost) * 100)"
        let comment = self.comment
        var imgList = [UIImage]()
        for item in self.cropedImagesArray! {
            imgList.append(item.image!)
        }
        let apiPhone = rawStringFrom(displayString: phoneNumber)
        phoneNumber = apiPhone
        let adressFull = (addressStr + " .Квартира:\(flat)")
        UPAPIManager.createOrder(name: fullName, addressFull: adressFull, phone: apiPhone, count: count, listID: listID!, paySum: paySum, comment: comment, deliverType: deliveryType, images: imgList, ddlRegion: regionDDL, ddlDistrict: ddlDistrict, ddlCity: city, ddlIndex: ddlIndex) { (result) in
            ANLoader.hide()
            switch result {
            case .success(let data):
                self.stampOrder = data
                self.orderId = data.id!
                self.mainView.keyButton.setTitle("№\(self.orderId)", for: .normal)
                self.mainView.summitButton.isEnabled = true
            case .partialSuccess(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
}
