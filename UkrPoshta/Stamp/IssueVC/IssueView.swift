//
//  IssueView.swift
//  UkrPoshta
//
//  Created by Zhekon on 24.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class IssueView: UIView {

    let keyButton = UPSubmitButton()
    let backgroundButton = UIButton()
    let image = UIImageView()
    let stampsTop = UILabel()
    let stampsTopInfo = UILabel()
    let countOfMark =  UILabel()
    let countOfMarkNumber =  UILabel()
    let letterLabel = UILabel()
    let letterCost = UILabel()
    let delivaryText = UILabel()
    let delivaryCost = UILabel()
     let generalCost = UILabel()
    let generalCostNumber = UILabel()
   let bottomFooter = UILabel()
   let bottomData = UILabel()
   let summitButton = UPSubmitButton()

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        keyButton.setTitle("", for: .normal)
        addSubview(keyButton)
        backgroundButton.backgroundColor = .white
        backgroundButton.layer.shadowColor = UPColor.darkGray.cgColor
        backgroundButton.layer.shadowOpacity = 0.2
        backgroundButton.layer.shadowOffset = .zero
        backgroundButton.layer.shadowRadius = 2
        backgroundButton.layer.cornerRadius = 3
        addSubview(backgroundButton)
      
        addSubview(image)
        image.image = UIImage(named: "tile_stamp")
        stampsTop.numberOfLines = 2
        stampsTop.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        stampsTop.textColor = UPColor.darkGray
        addSubview(stampsTop)
        stampsTopInfo.numberOfLines = 0
        stampsTopInfo.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
        stampsTopInfo.textColor = UPColor.darkGray
        addSubview(stampsTopInfo)
      
      addSubview(countOfMark)
      countOfMark.numberOfLines = 0
      countOfMark.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
      countOfMark.textColor = UPColor.darkGray
      
      addSubview(countOfMarkNumber)
      countOfMarkNumber.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
      countOfMarkNumber.textColor = UPColor.darkGray
      
      addSubview(letterLabel)
      letterLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
      
      addSubview(letterCost)
      letterCost.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
      
      addSubview(delivaryText)
      delivaryText.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)

      addSubview(delivaryCost)
      delivaryCost.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)

      addSubview(generalCost)
      let font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
      let boldFont = UIFont(descriptor: font.fontDescriptor.withSymbolicTraits(.traitBold)!, size: font.pointSize)
      generalCost.font = boldFont
      
      addSubview(generalCostNumber)
      generalCostNumber.font = boldFont
      
      generalCostNumber.font = UIFont.boldSystemFont(ofSize: 16.0)

      addSubview(bottomFooter)
      bottomFooter.numberOfLines = 0
      bottomFooter.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
      bottomFooter.textColor = UPColor.darkGray
    
      addSubview(bottomData)
      bottomData.numberOfLines = 0
      bottomData.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.footnote)
      bottomData.textColor = UPColor.darkGray
      
      addSubview(summitButton)
      summitButton.backgroundColor = UPColor.headerDarkYellow
      summitButton.titleLabel?.textColor = UPColor.darkGray
    }

      override func layoutSubviews() {
        super.layoutSubviews()
      let width = self.frame.size.width
      let height = self.frame.size.height
      
        keyButton.pin.top(0).left(20).right(20).bottom().height(40)
        backgroundButton.pin.below(of: keyButton).marginTop(0).left(20).right(20).height(115)
  image.pin.below(of:keyButton).marginTop(20).left(to:backgroundButton.edge.left).marginLeft(10).bottom(to:backgroundButton.edge.bottom).marginBottom(20).width(75)
        stampsTop.pin.below(of: keyButton).marginTop(15).left(to: image.edge.right).marginLeft(7.5).height(25).bottom().right()
        stampsTopInfo.pin.below(of: stampsTop).marginTop(0).left(to: image.edge.right).marginLeft(7.5).right(to:backgroundButton.edge.right ).marginRight(10).bottom(to: backgroundButton.edge.bottom).marginBottom(20)
      
      countOfMark.pin.top(to: backgroundButton.edge.bottom).marginTop(15).left(20).height(20).width(width/1.65)
      countOfMarkNumber.pin.top(to: backgroundButton.edge.bottom).marginTop(15).right(20).height(20).width(width/5)
      letterLabel.pin.top(to: countOfMark.edge.bottom).marginTop(10).left(20).height(20).width(width/1.75)
      letterCost.pin.top(to: countOfMarkNumber.edge.bottom).marginTop(10).right(20).height(20).width(width/5)
      delivaryText.pin.top(to: letterLabel.edge.bottom).marginTop(10).left(20).height(20).width(width/1.75)
      delivaryCost.pin.top(to: letterCost.edge.bottom).marginTop(10).right(20).height(20).width(width/5)
      generalCost.pin.top(to: delivaryText.edge.bottom).marginTop(10).left(20).height(20).width(width/1.75)
      generalCostNumber.pin.top(to: delivaryCost.edge.bottom).marginTop(10).right(30).height(20).width(width/4)
      bottomFooter.pin.top(to: generalCost.edge.bottom).marginTop(25).left(20).height(20).width(width/1.5)
      bottomData.pin.top(to: bottomFooter.edge.bottom).marginTop(0).left(20).height(100).right(20)
      summitButton.pin.bottom(64).right(0).left(0).height(64)
    }

}
