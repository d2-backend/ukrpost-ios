//
//  MoneyTransferView.swift
//  UkrPoshta
//
//  Created by Zhekon on 07.05.2018.
//  Copyright © 2018 D2. All rights reserved.
//

import UIKit

class MoneyTransferView: UIView {
    
    let moneyScrollView = UIScrollView()
    let fromCardHomeButton = MoneyGeneralButton()
    let fromCardDepartmentButton = MoneyGeneralButton()
    let fromCardToCardButton = MoneyGeneralButton()
    let extraViewBetweenButtonOne = UIView()
    let extraButtonPostTransfer = MoneyExtraButton()
    let extraButtonInternatioanlTransfer = MoneyExtraButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = UPColor.whiteSmoke
        addSubview(moneyScrollView)
        moneyScrollView.backgroundColor = UPColor.whiteSmoke
        
        moneyScrollView.addSubview(fromCardHomeButton)
        fromCardHomeButton.fromCardHomeLabelFirst.text = NSLocalizedString("MoneyTransferV.fromCardHomeLabelFirst.text", comment: "")
        fromCardHomeButton.fromCardHomeLabelSecond.text = NSLocalizedString("MoneyTransferV.fromCardHomeLabelSecond.text", comment: "")
        fromCardHomeButton.setImage(UIImage(named: "card_to_home"), for: UIControlState.normal)
        fromCardHomeButton.imageView?.contentMode = .scaleAspectFit
        fromCardDepartmentButton.imageView?.clipsToBounds = true

        moneyScrollView.addSubview(fromCardDepartmentButton)
        fromCardDepartmentButton.fromCardHomeLabelFirst.text = NSLocalizedString("MoneyTransferV.fromCardDepartmentButton.text", comment: "")
        fromCardDepartmentButton.fromCardHomeLabelSecond.text = NSLocalizedString("MoneyTransferV.fromCardDepartmentTwoButton.text", comment: "")
        fromCardDepartmentButton.setImage(UIImage(named: "card_to_post_office"), for: UIControlState.normal)
        fromCardDepartmentButton.imageView?.contentMode = .scaleAspectFit
        fromCardDepartmentButton.imageView?.clipsToBounds = true

        moneyScrollView.addSubview(fromCardToCardButton)
        fromCardToCardButton.fromCardHomeLabelFirst.text = NSLocalizedString("fromCardToCardButton.t.text", comment: "")
        fromCardToCardButton.fromCardHomeLabelSecond.text = NSLocalizedString("fromCardToCardButton.fromCardHomeLabel.text", comment: "")
        fromCardToCardButton.setImage(UIImage(named: "card_to_card"), for: UIControlState.normal)
        fromCardToCardButton.imageView?.contentMode = .scaleAspectFit
        fromCardDepartmentButton.imageView?.clipsToBounds = true

        moneyScrollView.addSubview(extraButtonPostTransfer)
        extraButtonPostTransfer.extraLabel.text =  NSLocalizedString("extraButtonPostTransfer.extraLabel.text", comment: "")
        extraButtonPostTransfer.extraImage.image = UIImage(named: "icon_letter")
        
        moneyScrollView.addSubview(extraButtonInternatioanlTransfer)
        extraButtonInternatioanlTransfer.extraLabel.text =  NSLocalizedString("extraButtonInternatioanlTransfer.extraLabel.text", comment: "")
        extraButtonInternatioanlTransfer.extraImage.image = UIImage(named: "icon_world")
        
        moneyScrollView.addSubview(extraViewBetweenButtonOne)
        extraViewBetweenButtonOne.backgroundColor = UPColor.separatorGray
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        moneyScrollView.contentSize = CGSize(width: self.frame.size.width, height: 765)
        moneyScrollView.pin.left().top().bottom().right()
        fromCardHomeButton.pin.left(15).right(15).height(175).top(20)
        fromCardDepartmentButton.pin.top(to: fromCardHomeButton.edge.bottom).marginTop(10).left(15).right(15).height(175)
        fromCardToCardButton.pin.top(to: fromCardDepartmentButton.edge.bottom).marginTop(10).left(15).right(15).height(175)
        extraButtonPostTransfer.pin.top(to: fromCardToCardButton.edge.bottom).marginTop(10).left(15).right(15).height(60)
        extraViewBetweenButtonOne.pin.top(to: extraButtonPostTransfer.edge.bottom).marginTop(0).left(60).right(15).height(1)
        extraButtonInternatioanlTransfer.pin.top(to: extraButtonPostTransfer.edge.bottom).marginTop(0).left(15).right(15).height(60)
    }
}









